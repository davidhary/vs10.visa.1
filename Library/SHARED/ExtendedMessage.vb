Namespace Text

    ''' <summary>
    '''   Defines an extended message with broadcasting and trace levels..
    ''' </summary>
    ''' <license>
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="10/25/2006" by="David Hary" revision="2.0.2489.x">
    ''' created.
    ''' </history>
    Public Class ExtendedMessage

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            Me.New(String.Empty)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="details">Specifies the message details.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal details As String)
            Me.New(String.Empty, details)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="synopsis">Specifies the message synopsis.</param>
        ''' <param name="format">Specifies the message formatting string</param>
        ''' <param name="args">Specifies the arguments.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
            Me.New(synopsis, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
        ''' <param name="synopsis">Specifies the message synopsis.</param>
        ''' <param name="format">Specifies the message formatting string</param>
        ''' <param name="args">Specifies the arguments.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
            Me.New(synopsis, String.Format(Globalization.CultureInfo.CurrentCulture, format, args), trace)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="broadcast">Specifies the <see cref="System.Diagnostics.TraceEventType">broadcast level</see></param>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
        ''' <param name="synopsis">Specifies the message synopsis.</param>
        ''' <param name="format">Specifies the message formatting string</param>
        ''' <param name="args">Specifies the arguments.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal broadcast As TraceEventType, ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
            Me.New(broadcast, trace, synopsis, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="details">Specifies the message details.</param>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal trace As TraceEventType, ByVal details As String)
            Me.New(trace, String.Empty, details)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="details">Specifies the message details.</param>
        ''' <param name="synopsis">Specifies the message</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal synopsis As String, ByVal details As String)
            Me.New(TraceEventType.Warning, synopsis, details)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="details">Specifies the message details.</param>
        ''' <param name="synopsis">Specifies the message</param>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace and broadcast levels</see></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal trace As TraceEventType, ByVal synopsis As String, ByVal details As String)
            Me.New(trace, trace, synopsis, details)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="details">Specifies the message details.</param>
        ''' <param name="broadcast">Specifies the <see cref="System.Diagnostics.TraceEventType">broadcast level</see></param>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace level</see></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal details As String, ByVal broadcast As TraceEventType, _
            ByVal trace As TraceEventType)
            Me.New(String.Empty, details, broadcast, trace)
        End Sub

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <param name="synopsis">Specifies the message</param>
        ''' <param name="details">Specifies the message details.</param>
        ''' <param name="broadcast">Specifies the <see cref="System.Diagnostics.TraceEventType">broadcast level</see></param>
        ''' <param name="trace">Specifies the <see cref="System.Diagnostics.TraceEventType">trace level</see></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal broadcast As TraceEventType, ByVal trace As TraceEventType, ByVal synopsis As String, ByVal details As String)
            MyBase.New()

            _hasSynopsis = Not String.IsNullOrEmpty(synopsis)
            If _hasSynopsis Then
                _synopsis = synopsis
            Else
                _synopsis = String.Empty
            End If

            _isEmpty = String.IsNullOrEmpty(details)
            If _isEmpty Then
                _details = String.Empty
            Else
                _details = details
            End If

            _timestamp = Date.Now
            _broadcastLevel = broadcast
            _traceLevel = trace

        End Sub

        ''' <summary>
        ''' Gets an empty <see cref="text.ExtendedMessage">extended message</see>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property Empty() As Text.ExtendedMessage
            Get
                Return New Text.ExtendedMessage()
            End Get
        End Property

#End Region

#Region " PROPERTIES "

        Private _details As String
        ''' <summary>Gets or sets the extended message message.
        ''' </summary>
        Public Property Details() As String
            Get
                Return _details
            End Get
            Set(ByVal value As String)
                _details = value
            End Set
        End Property

        Private _broadcastLevel As System.Diagnostics.TraceEventType
        ''' <summary>Gets or sets the
        ''' <see cref="System.Diagnostics.TraceEventType">broadcast level</see>.</summary>
        Public Property BroadcastLevel() As System.Diagnostics.TraceEventType
            Get
                Return _broadcastLevel
            End Get
            Set(ByVal value As System.Diagnostics.TraceEventType)
                _broadcastLevel = value
            End Set
        End Property

        Private _defaultFormat As String = "{0:HH:mm:ss.fff}, {1}, {2}, {3}"
        ''' <summary>
        ''' Gets or sets the default format for displaying the message
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Format() As String
            Get
                Return _defaultFormat
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) Then
                    _defaultFormat = value
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the synopsis condition.  The extended message
        ''' has a synopsis if the sysnopsis message length is longer than
        ''' zero.
        ''' </summary>
        Private _hasSynopsis As Boolean

        ''' <summary>Gets or sets the synopsis condition.  The extended message
        ''' has a synopsis if the sysnopsis message length is longer than
        ''' zero.
        ''' </summary>
        Public ReadOnly Property HasSynopsis() As Boolean
            Get
                Return _hasSynopsis
            End Get
        End Property

        ''' <summary>Gets or sets the condition for flagging user or element cancellation.</summary>
        Private _isCanceled As Boolean

        ''' <summary>Gets or sets the cancelation flag.
        ''' </summary>
        Public Property IsCanceled() As Boolean
            Get
                Return _isCanceled
            End Get
            Set(ByVal Value As Boolean)
                _isCanceled = Value
            End Set
        End Property

        ''' <summary>Gets or sets the empty condition.  The extended message
        ''' is empty if the message is empty.</summary>
        Private _isEmpty As Boolean
        ''' <summary>Gets the empty condition.  The extended message
        ''' is empty if the message is empty.</summary>
        Public ReadOnly Property IsEmpty() As Boolean
            Get
                Return _isEmpty
            End Get
        End Property

        Private _lastAction As String = String.Empty
        ''' <summary>Gets or sets the last action description.  This useful for extended messages that
        ''' are using in event arguments.</summary>
        ''' <value><c>LastAction</c> is a string property.</value>
        Public Property LastAction() As String
            Get
                Return _lastAction
            End Get
            Set(ByVal Value As String)
                _lastAction = Value
            End Set
        End Property

        Private _synopsis As String = String.Empty
        ''' <summary>Gets or sets the synopsis of the message.</summary>
        ''' <value><c>Synopsis</c> is a string property.</value>
        Public Property Synopsis() As String
            Get
                Return _synopsis
            End Get
            Set(ByVal Value As String)
                _synopsis = Value
            End Set
        End Property

        Private _timestamp As DateTime
        ''' <summary>Gets the extended message time stamp.
        ''' </summary>
        Public ReadOnly Property Timestamp() As DateTime
            Get
                Return _timestamp
            End Get
        End Property

        Private _traceLevel As System.Diagnostics.TraceEventType
        ''' <summary>Gets or sets the
        ''' <see cref="System.Diagnostics.TraceEventType">trace level</see>.</summary>
        Public Property TraceLevel() As System.Diagnostics.TraceEventType
            Get
                Return _traceLevel
            End Get
            Set(ByVal value As System.Diagnostics.TraceEventType)
                _traceLevel = value
            End Set
        End Property

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Returns a compand message based on the default format.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function ToString() As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _defaultFormat, Me.Timestamp, Me.TraceLevel, Me.Synopsis, Me.Details)
        End Function

#End Region

    End Class

End Namespace
