Imports System.Text.RegularExpressions
Imports System.Security.Cryptography
Imports System.IO

Namespace Text

    ''' <summary>Provides general purpose support services.</summary>
    ''' <remarks>Use this class to obtain general purpose support services.</remarks>
    ''' <license>
    ''' (c) 2003 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/03/03" by="David Hary" revision="1.0.1102.x">
    ''' Created
    ''' </history>
    Public NotInheritable Class Helper

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Prevents instantiation of the class.</summary>
        Private Sub New()
        End Sub

#If False Then
  ''' <summary>Instantiates the class.</summary>
  ''' <returns>A new or existing instance of the class.</returns>
  ''' <remarks>This class does not need a constructor because, at this time it only has shared methods..
  '''   Once this changes, use this method to instantiate this class.  This class uses lazy 
  '''   instantiation, meaning the instance isn't created until the first time it's retrieved.</remarks>
  Shared _instance As Helper
  Public Shared ReadOnly property Instance() As Helper
    Get
      If _instance is nothing Then
        _instance = New Helper
      End If
      Return _instance
    End Get
  End Property

#End If

#End Region

#Region " CRYPTO "

#If False Then
    ''' <summary>
    ''' Uses the password to generate key bytes.
    ''' </summary>
    ''' <param name="password"></param>
    ''' <param name="salt"></param>
    ''' <param name="keySize"></param>
    ''' <param name="blockSize"></param>
    ''' <param name="key"></param>
    ''' <param name="vector"></param>
    ''' <remarks>
    ''' This subroutine makes an Rfc2898DeriveBytes object. This object uses the HMACSHA1 algorithm 
    ''' to generate a series of pseudo-random bytes. There are other classes that you probably won't 
    ''' recognize either that you can use if you have some sort of grudge against the 
    ''' HMACSHA1 algorithm. 
    ''' The program calls the object's GetBytes function to grab some random bytes that it can use 
    ''' for the key and initialization vector.
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="5#")> _
    Private Sub MakeKeyAndInitializationVector(ByVal password As String, ByVal salt() As Byte, _
                            ByVal keySize As Integer, ByVal blockSize As Integer, _
                            ByRef key() As Byte, ByRef vector() As Byte)

      Dim deriveBytes As New Rfc2898DeriveBytes(password, salt, 1234)
      key = deriveBytes.GetBytes(keySize \ 8)
      vector = deriveBytes.GetBytes(blockSize \ 8)

    End Sub
#End If

        ''' <summary>
        ''' Encrypt or decrypt a byte array.
        ''' </summary>
        ''' <param name="password"></param>
        ''' <param name="values"></param>
        ''' <param name="encrypt"></param>
        ''' <returns></returns>
        ''' <remarks>
        ''' Uses a triple DES crypto provider, which uses the DES encryption algorithm three times 
        ''' to encrypt or decrypt data. You can pick from among other crypto providers if you like.
        ''' This function tries to find a key size that is supported by the operating system. 
        ''' Different versions of the operating systems in different countries support different 
        ''' key sizes so the code must find one that works on this system. 
        ''' If sending an encrypted message to someone else, make sure they use the same key size, 
        ''' particularly if in different countries. 
        ''' Next the routine builds a "salt" array. Salt is a cryptographic term that's been around 
        ''' for a while so we're stuck with it. The salt array is an array of "random" bytes that you 
        ''' pick to make it harder for cyber-desperados to use a dictionary attack that guesses every 
        ''' possible password for your message. The salt guarantees that they'll also have to guess the salt.
        ''' You must use the same salt when you encrypt and decrypt or the decryption won't work. For increased 
        ''' security, use different salt values than the ones shown here.
        ''' </remarks>
        Private Shared Function Crypt(ByVal password As String, ByVal values() As Byte, _
                                    ByVal encrypt As Boolean) As Byte()

            ' Make a triple DES service provider.
            Dim desProvider As New TripleDESCryptoServiceProvider

            ' Find a valid key size for this provider.
            Dim keySize As Integer = 0
            For i As Integer = 1024 To 1 Step -1
                If desProvider.ValidKeySize(i) Then
                    keySize = i
                    Exit For
                End If
            Next i
            Debug.Assert(keySize > 0)

            ' Get the block size for this provider.
            Dim blockSize As Integer = desProvider.BlockSize

            ' Generate the key and initialization vector.
            Dim key As Byte() = Nothing
            Dim vector As Byte() = Nothing
            ' Original Dim salt As Byte() = {&H10, &H20, &H12, &H23, &H37, &HA4, &HC5, &HA6, &HF1, &HF0, &HEE, &H21, &H22, &H45}
            Dim salt As Byte() = {&H10, &H20, &H32, &H43, &H57, &HA4, &HB5, &HC6, &HD1, &HE0, &HFE, &H61, &H72, &H85}

            ' MakeKeyAndInitializationVector(password, salt, keySize, blockSize, key, vector)
            Dim deriveBytes As New Rfc2898DeriveBytes(password, salt, 1234)
            key = deriveBytes.GetBytes(keySize \ 8)
            vector = deriveBytes.GetBytes(blockSize \ 8)

            ' Make the encryptor or decryptor.
            Dim cryptoTransform As ICryptoTransform
            If encrypt Then
                cryptoTransform = desProvider.CreateEncryptor(key, vector)
            Else
                cryptoTransform = desProvider.CreateDecryptor(key, vector)
            End If

            ' Create the output stream.
            Dim outStream As New MemoryStream()

            ' Attach a crypto stream to the output stream.
            Dim cryptoStream As New CryptoStream(outStream, cryptoTransform, CryptoStreamMode.Write)

            ' Write the bytes into the CryptoStream.
            cryptoStream.Write(values, 0, values.Length)
            Try
                cryptoStream.FlushFinalBlock()
            Catch ex As CryptographicException
                ' Ignore this one. The password is bad.
            Catch
                ' Re-raise this one.
                Throw
            End Try

            ' Save the result.
            Dim result() As Byte = outStream.ToArray()

            ' Close the stream.
            Try
                cryptoStream.Close()
            Catch ex As CryptographicException
                ' Ignore this one. The password is bad.
            Catch
                ' Re-raise this one.
                Throw
            End Try
            outStream.Close()

            Return result
        End Function

        ''' <summary>
        ''' Reassembles the original string.
        ''' </summary>
        ''' <param name="encrypted"></param>
        ''' <param name="password"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Decrypt(ByVal encrypted() As Byte, ByVal password As String) As String

            Dim decrypted() As Byte = Crypt(password, encrypted, False)
            Dim asciiEncoder As New System.Text.ASCIIEncoding()
            Return asciiEncoder.GetChars(decrypted)

        End Function

        ''' <summary>
        ''' Decrypts a string encrypted into an string of hex values.
        ''' </summary>
        ''' <param name="value">Specifies the hex-valued encrypted string</param>
        ''' <param name="password">Specifies the password used to encrypt the array.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DecryptFromHexString(ByVal value As String, ByVal password As String) As String
            Return Decrypt(ConvertHexStringToBytes(value), password)
        End Function

        ''' <summary>
        ''' Encrypts a string into a byte array.
        ''' </summary>
        ''' <param name="value">Specifies the value to encrypt</param>
        ''' <param name="password">Specifies the encryption password.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Encrypt(ByVal value As String, ByVal password As String) As Byte()
            Dim asciiEncoder As New System.Text.ASCIIEncoding()
            Dim plain_bytes As Byte() = asciiEncoder.GetBytes(value)
            Return Crypt(password, plain_bytes, True)
        End Function

        ''' <summary>
        ''' Encryptes to a string of hex values.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <param name="password"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function EncryptToHexString(ByVal value As String, ByVal password As String) As String
            Return ConvertToHexString(Encrypt(value, password))
        End Function

        ''' <summary>
        ''' Converts the specified byte array of values to string in HEX format.
        ''' </summary>
        ''' <param name="values">Specifies the array of bytes to convert.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertToHexString(ByVal values() As Byte) As String
            Dim result As New System.Text.StringBuilder
            For Each b As Byte In values
                result.Append(b.ToString("X", Globalization.CultureInfo.CurrentCulture).PadLeft(2, "0"c))
            Next b
            Return result.ToString
        End Function

        ''' <summary>
        ''' Converts the specified byte array of values to string in HEX format.
        ''' </summary>
        ''' <param name="values">Specifies the array of bytes to convert.</param>
        ''' <param name="delimiter">Specifies the delimiter between values.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertToHexString(ByVal values() As Byte, ByVal delimiter As String) As String
            Dim result As New System.Text.StringBuilder
            For Each b As Byte In values
                result.Append(delimiter)
                result.Append(b.ToString("X", Globalization.CultureInfo.CurrentCulture).PadLeft(2, "0"c))
            Next b
            If result.Length > 0 Then
                Return result.ToString.Substring(delimiter.Length)
            Else
                Return ""
            End If
        End Function

        ''' <summary>
        ''' Converts a hex string to a byte array.
        ''' </summary>
        ''' <param name="value">Specifies the string</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertHexStringToBytes(ByVal value As String) As Byte()

            Dim upperBound As Integer = value.Length \ 2 - 1
            Dim values(upperBound) As Byte
            For i As Integer = 0 To upperBound
                values(i) = CByte("&H" & value.Substring(2 * i, 2))
            Next i
            Return values

        End Function

        ''' <summary>
        ''' Converts a hex string to a byte array.
        ''' </summary>
        ''' <param name="value">Specifies the string</param>
        ''' <param name="delimiter">Specifies any delimited used to separater the hex values.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertHexStringToBytes(ByVal value As String, ByVal delimiter As String) As Byte()

            value = value.Replace(delimiter, "")
            Return ConvertHexStringToBytes(value)

        End Function

#End Region

#Region " HASH CODE "

#Region " CLR CODE "
#If False Then

  // Framework 1.x string hash code
    inline ULONG HashStringA(LPCSTR szStr)
      {
        ULONG hash = 5381;
        int c;
        while ((c = *szStr) != 0)
          {
            hash = ((hash << 5) + hash) ^ c;
            ++szStr;
          }
        return hash;
      }

  ''' <summary>Returns the hash code for this string.</summary>
  ''' <returns>A 32-bit signed integer hash code.</returns>
  ''' <filterpriority>2</filterpriority>
  ''' <remarks>From VS9 CLR.</remarks>
  <ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)> _
  Public Overrides Function GetHashCode() As Integer
    Dim str As Char*
    Fixed str = DirectCast(Me, Char*)
      Dim chPtr As Char* = str
      Dim num As Integer = &H15051505
      Dim num2 As Integer = num
      Dim numPtr As Integer* = DirectCast(chPtr, Integer*)
      Dim i As Integer = Me.Length
      Do While (i > 0)
        num = ((((num << 5) + num) + (num >> &H1B)) Xor numPtr(0))
        If (i <= 2) Then
          Exit Do
        End If
        num2 = ((((num2 << 5) + num2) + (num2 >> &H1B)) Xor numPtr(1))
        numPtr = (numPtr + 2)
        i = (i - 4)
      Loop
      Return (num + (num2 * &H5D588B65))
    End Fixed
  End Function

  [ReliabilityContract(Consistency.WillNotCorruptState,Cer.MayFail)]
  public override unsafe int GetHashCode()
    {
      fixed (char* text = ((char*) this))
    {
      char* chPtr = text;
      int num = 0x15051505;
      int num2 = num;
      int* numPtr = (int*) chPtr;
      for (int i = this.Length; i > 0; i -= 4)
      {
        num = (((num << 5) + num) + (num >> 0x1b)) ^ numPtr[0];
        if (i <= 2)
        {
          break;
        }
        num2 = (((num2 << 5) + num2) + (num2 >> 0x1b)) ^ numPtr[1];
        numPtr += 2;
      }
      return (num + (num2 * 0x5d588b65));
    }
  }

    ' not sure if this supposed to work like VS9
  Public Function MyHashCode(ByVal value As String) As Integer
    Dim charIndex As Integer = 0
    Dim num1 As Int32
    Dim num2 As Int32 = &H1505
    Dim num3 As Int32 = num2
    Dim currentChar As Char
    Do While charIndex < value.Length
      currentChar = value.Chars(charIndex)
      num1 = System.Convert.ToInt32(currentChar)
      num2 = ((num2 << 5) + num2) Xor num1
      If charIndex = value.Length - 1 Then
        Exit Do
      End If
      num1 = System.Convert.ToInt32(value.Chars(charIndex + 1))
      If num1 = 0 Then
        Exit Do
      End If
      num3 = ((num3 << 5) + num3) Xor num1
      charIndex += 2
    Loop
    ' return (num2 + (num3 * &H5D588B65))
    Return (num2 + (num3 Xor &H5D588B65))
  End Function

#End If
#End Region

        ''' <summary>Returns the hash code for <paramref name="value">this string</paramref>
        ''' Using the Visual Studio 2003 algorithm.</summary>
        ''' <returns>A 32-bit signed integer hash code.</returns>
        ''' <remarks>From VS8 CLR http://www.orthogonal.com.au/computers/stringhash.htm</remarks>
        ''' <param name="value"></param>
        Public Shared Function DeriveLegacyHashCode(ByVal value As String) As Integer
            Dim characters() As Char = value.ToCharArray
            Dim i As Integer = 0
            Dim hash As Integer = 5381
            Dim hash64 As Long
            Do While i < value.Length
                ' hash = ((hash << 5) + hash) Xor Convert.ToInt32(characters(i))
                hash64 = (hash << 5I)
                hash64 += hash
                hash = Algebra.Scalar.Truncate(hash64) Xor Convert.ToInt32(characters(i))
                i += 1
            Loop
            Return hash
        End Function

        ''' <summary>Returns the hash code for <paramref name="value">this string</paramref>.</summary>
        ''' <returns>A 32-bit signed integer hash code.</returns>
        ''' <remarks>From VS9 CLR.
        ''' Code does not correctly produce the VS9 outcome.  
        ''' </remarks>
        ''' <param name="value"></param>
        Public Shared Function CalculateHashCode(ByVal value As String) As Integer
            Dim characters() As Char = value.ToCharArray
            Dim a As Integer = &H15051505
            Dim b As Integer = a
            Dim charIndex As Integer = 0
            Dim hash64 As Long
            Do While charIndex < value.Length
                hash64 = (a << 5I)
                hash64 = Algebra.Scalar.Truncate(hash64 + a)
                a = Algebra.Scalar.Truncate(hash64 + (a >> &H1BI)) _
                    Xor Convert.ToInt32(characters(charIndex))
                charIndex += 1
                If charIndex < value.Length Then
                    hash64 = (b << 5I)
                    hash64 = Algebra.Scalar.Truncate(hash64 + b)
                    b = Algebra.Scalar.Truncate(hash64 + (b >> &H1BI)) _
                      Xor Convert.ToInt32(characters(charIndex))
                End If
                charIndex += 1
            Loop
            hash64 = Algebra.Scalar.Truncate(b * &H5D588B65&)
            Return Algebra.Scalar.Truncate(a + hash64)
        End Function

#End Region

#Region " STRING BUILDER METHODS "

        ''' <summary>
        ''' Adds text to string builder starting with a new line.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>
        Public Shared Sub AddLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
            If Not String.IsNullOrEmpty(value) Then
                If builder Is Nothing Then
                    builder = New System.Text.StringBuilder
                End If
                If builder.Length > 0 Then
                    builder.Append(Environment.NewLine)
                End If
                builder.Append(value)
            End If
        End Sub

        ''' <summary>
        ''' Adds text to string builder starting with a new line.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <remarks></remarks>
        Public Shared Sub AddLine(ByVal builder As System.Text.StringBuilder, ByVal value As System.Text.StringBuilder)
            If value IsNot Nothing Then
                AddLine(builder, value.ToString)
            End If
        End Sub

#End Region

#Region " STRING NUMERIC METHODS "

        ''' <summary>Returns true if the provided value is a floating number.</summary>
        ''' <param name="value">Specifies the floating number candidate.</param>
        Public Shared Function IsFloat(ByVal value As String) As Boolean
            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Float, System.Globalization.CultureInfo.CurrentCulture, a)
        End Function

        ''' <summary>Returns true if the provided value is an whole number.</summary>
        ''' <param name="value">Specifies the integer candidate.</param>
        Public Shared Function IsInteger(ByVal value As String) As Boolean

            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, a)

        End Function

        ''' <summary>Returns true if the provides string is any number.</summary>
        ''' <param name="value">Specifies the number candidate.</param>
        Public Shared Function IsNumber(ByVal value As String) As Boolean

            Dim a As Double
            Return System.Double.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                                   System.Globalization.CultureInfo.CurrentCulture, a)

        End Function

        ''' <summary>Returns true if the provides string is any number.</summary>
        ''' <param name="value">Specifies the number candidate.</param>
        Public Shared Function IsNumeric(ByVal value As String) As Boolean
            Return Helper.IsNumber(value)
        End Function

#End Region

#Region " STRING NUMERIC METHODS: TRY PARSE "

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Double, ByRef numericValue As Double) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf Double.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Integer, ByRef numericValue As Integer) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Short, ByRef numericValue As Short) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf Short.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If


        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Single, ByRef numericValue As Single) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf Single.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If


        End Function

#End Region

#Region " STRING NUMERIC METHODS: PARSE "


        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Double) As Double

            Dim numericValue As Double
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Double.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Double?) As Double?

            Dim numericValue As Double
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Double.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Integer) As Integer

            Dim numericValue As Integer
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Integer?) As Integer?

            Dim numericValue As Integer
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Short) As Short

            Dim numericValue As Short
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Short.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Short?) As Short?

            Dim numericValue As Short
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf Short.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Single) As Single

            Dim numericValue As Single
            If String.IsNullOrEmpty(value) Then

                Return [default]

            ElseIf Single.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Single?) As Single?

            Dim numericValue As Single
            If String.IsNullOrEmpty(value) Then

                Return [default]

            ElseIf Single.TryParse(value, Globalization.NumberStyles.Number _
                                   Or Globalization.NumberStyles.AllowExponent, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

#End Region

#Region " STRING NUMERIC METHODS: PARSE HEX - BYTE "

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="byte")> _
        Public Shared Function ParseByte(ByVal value As String, ByVal style As Globalization.NumberStyles) As Byte

            If String.IsNullOrEmpty(value) Then
                Throw New ArgumentNullException("value")
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.Substring(2)
            End If
            Return Byte.Parse(value, style, Globalization.CultureInfo.CurrentCulture)

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Byte, ByVal style As Globalization.NumberStyles) As Byte

            Dim numericValue As Byte
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
                If Byte.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.Substring(2)
                If Byte.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf Byte.TryParse(value, style, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal style As Globalization.NumberStyles, ByRef numericValue As Byte) As Boolean

            If String.IsNullOrEmpty(value) Then
                Return False
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.Substring(2)
            End If
            Return Byte.TryParse(value, style, Globalization.CultureInfo.CurrentCulture, numericValue)

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Byte, ByVal style As Globalization.NumberStyles, ByRef numericValue As Byte) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf TryParse(value, style, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

#End Region

#Region " STRING NUMERIC METHODS: PARSE HEX - INTEGER "

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="integer")> _
        Public Shared Function ParseInteger(ByVal value As String, ByVal style As Globalization.NumberStyles) As Integer

            If String.IsNullOrEmpty(value) Then
                Throw New ArgumentNullException("value")
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Integer.Parse(value, style, Globalization.CultureInfo.CurrentCulture)

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Integer, ByVal style As Globalization.NumberStyles) As Integer

            Dim numericValue As Integer
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
                If Integer.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
                If Integer.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf Integer.TryParse(value, style, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal style As Globalization.NumberStyles, ByRef numericValue As Integer) As Boolean

            If String.IsNullOrEmpty(value) Then
                Return False
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Integer.TryParse(value, style, Globalization.CultureInfo.CurrentCulture, numericValue)

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Integer, ByVal style As Globalization.NumberStyles, ByRef numericValue As Integer) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf TryParse(value, style, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

#End Region

#Region " STRING NUMERIC METHODS: PARSE HEX - LONG "

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="long")> _
        Public Shared Function ParseLong(ByVal value As String, ByVal style As Globalization.NumberStyles) As Long

            If String.IsNullOrEmpty(value) Then
                Throw New ArgumentNullException("value")
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Long.Parse(value, style, Globalization.CultureInfo.CurrentCulture)

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Long, ByVal style As Globalization.NumberStyles) As Long

            Dim numericValue As Long
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
                If Long.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
                If Long.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf Long.TryParse(value, style, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal style As Globalization.NumberStyles, ByRef numericValue As Long) As Boolean

            If String.IsNullOrEmpty(value) Then
                Return False
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Long.TryParse(value, style, Globalization.CultureInfo.CurrentCulture, numericValue)

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Long, ByVal style As Globalization.NumberStyles, ByRef numericValue As Long) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf TryParse(value, style, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

#End Region

#Region " STRING NUMERIC METHODS: PARSE HEX - SHORT "

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")> _
        Public Shared Function ParseShort(ByVal value As String, ByVal style As Globalization.NumberStyles) As Short

            If String.IsNullOrEmpty(value) Then
                Throw New ArgumentNullException("value")
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Short.Parse(value, style, Globalization.CultureInfo.CurrentCulture)

        End Function

        ''' <summary>
        ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Parse(ByVal value As String, ByVal [default] As Short, ByVal style As Globalization.NumberStyles) As Short

            Dim numericValue As Short
            If String.IsNullOrEmpty(value) Then
                Return [default]
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
                If Short.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
                If Short.TryParse(value, style, _
                                   Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    Return numericValue
                Else
                    Return [default]
                End If
            ElseIf Short.TryParse(value, style, _
                          System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
                Return numericValue
            Else
                Return [default]
            End If

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal style As Globalization.NumberStyles, ByRef numericValue As Short) As Boolean

            If String.IsNullOrEmpty(value) Then
                Return False
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("&") Then
                value = value.TrimStart("&Hh".ToCharArray)
            ElseIf (style And Globalization.NumberStyles.AllowHexSpecifier) <> 0 AndAlso value.StartsWith("0x") Then
                value = value.TrimStart("0x".ToCharArray)
            End If
            Return Short.TryParse(value, style, Globalization.CultureInfo.CurrentCulture, numericValue)

        End Function

        ''' <summary>
        ''' Parse and set the <paramref name="numericValue">numeric value</paramref> and return True.
        ''' or set to <paramref name="default">default</paramref> and return false.
        ''' </summary>
        ''' <param name="value">The string containing the value</param>
        ''' <param name="default">The default value</param>
        ''' <param name="style">Specifies the <see cref="Globalization.NumberStyles">number style.</see>
        ''' If <see cref="Globalization.NumberStyles.AllowHexSpecifier">allows hex</see>, checks and strips for hex prefixes
        ''' such as '&amp;H' or '0x'.
        ''' </param>
        ''' <param name="numericValue">The parsed numeric value.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#")> _
        Public Shared Function TryParse(ByVal value As String, ByVal [default] As Short, ByVal style As Globalization.NumberStyles, ByRef numericValue As Short) As Boolean

            If String.IsNullOrEmpty(value) Then
                numericValue = [default]
                Return False
            ElseIf TryParse(value, style, numericValue) Then
                Return True
            Else
                numericValue = [default]
                Return False
            End If

        End Function

#End Region

#Region " STRING CONTENTS METHODS "

        ''' <summary>
        ''' Replaces the given <paramref name="characters">characters</paramref> with the
        ''' <paramref name="replacing">new characters.</paramref>
        ''' </summary>
        ''' <param name="source">The original string</param>
        ''' <param name="characters">The characters to be replaced</param>
        ''' <param name="replacing">The new characters.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Replace(ByVal source As String, ByVal characters As String, ByVal replacing As Char) As String
            If String.IsNullOrEmpty(source) Then
                Throw New ArgumentNullException("source")
            End If
            If String.IsNullOrEmpty(characters) Then
                Throw New ArgumentNullException("characters")
            End If
            If String.IsNullOrEmpty(replacing) Then
                Throw New ArgumentNullException("replacing")
            End If
            For Each c As Char In characters.ToCharArray
                source = source.Replace(c, replacing)
            Next
            Return source
        End Function

        Public Const IllegalFileCharacters As String = "/\\:*?""<>|"
        ''' <summary>Returns True if the validated string contains only alpha characters.</summary>
        ''' <param name="source">The string to validate.</param>
        Public Shared Function IncludesIllegalFileCharacters(ByVal source As String) As Boolean

            Return Helper.IncludesCharacters(source, Helper.IllegalFileCharacters)

        End Function

        ''' <summary>Returns true if the string includes the specified characters.</summary>
        ''' <param name="source">The string that is being searched.</param>
        ''' <param name="characters">The characters to search for.</param>
        Public Shared Function IncludesCharacters(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException("characters")
            End If

            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex( _
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

        ''' <summary>Returns true if the string includes any of the specified characters.</summary>
        ''' <param name="source">The string that is being searched.</param>
        ''' <param name="characters">The characters to search for.</param>
        Public Shared Function IncludesAny(ByVal source As String, ByVal characters As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If
            If characters Is Nothing Then
                Throw New ArgumentNullException("characters")
            End If

            ' 2954: was ^[{0}]+$ 
            Dim expression As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, "[{0}]", characters)
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex( _
                expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

        ''' <summary>Returns True if the validated string contains only alpha characters.</summary>
        ''' <param name="source">The string to validate.</param>
        Public Shared Function ConsistsOfAlphaCharacters(ByVal source As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If

            Dim r As Regex = New Regex("^[a-z]+$", RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

        ''' <summary>Returns True if the validated string contains only alpha characters.</summary>
        ''' <param name="source">The string to validate.</param>
        Public Shared Function ConsistsOfNumbers(ByVal source As String) As Boolean

            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If

            Dim r As Regex = New Regex("^[0-9]+$", RegexOptions.IgnoreCase)
            Return r.IsMatch(source)

        End Function

#End Region

#Region " STRING COMPARE METHODS "

        ''' <summary>Returns True if the string 'search' is the same as string 'source'.</summary>
        ''' <param name="source">The string to search.</param>
        ''' <param name="search">The string to search for.</param>
        ''' <remarks>The search ignores case.</remarks>
        Public Shared Function AreEqual(ByVal source As String, ByVal search As String) As Boolean

            If search Is Nothing Then
                Throw New ArgumentNullException("search")
            End If
            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If

            Return String.Compare(source, search, True, System.Globalization.CultureInfo.CurrentCulture) = 0

        End Function

        ''' <summary>Returns True if the string 'search' is the same as string 'source'.</summary>
        ''' <param name="source">The string to search.</param>
        ''' <param name="search">The string to search for.</param>
        ''' <remarks>The search ignores case.</remarks>
        Public Shared Function AreEqual(ByVal source As String, ByVal search As String, ByVal cultureInfo As System.Globalization.CultureInfo) As Boolean

            Return String.Compare(source, search, True, cultureInfo) = 0

        End Function

        ''' <summary>Returns True if the string contains only alpha characters.</summary>
        ''' <param name="source">The string to search.</param>
        Public Shared Function Contained(ByVal source As String, ByVal search As String) As Boolean

            If search Is Nothing Then
                Throw New ArgumentNullException("search")
            End If
            If source Is Nothing Then
                Throw New ArgumentNullException("source")
            End If

            Dim ci As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
            Return ci.IndexOf(source, search, Globalization.CompareOptions.IgnoreCase) >= 0

        End Function

#End Region

#Region " STRING CONVERT METHODS "

        ''' <summary>Returns an HEX string.
        ''' </summary>
        Public Shared Function ToHex(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return value.ToString("X", Globalization.CultureInfo.InvariantCulture).PadLeft(nibbleCount, "0"c)
        End Function

        ''' <summary>Returns an HEX string caption with preceeding "0x".
        ''' </summary>
        Public Shared Function ToHexCaption(ByVal value As Integer, ByVal nibbleCount As Integer) As String
            Return "0x" & value.ToString("X", Globalization.CultureInfo.InvariantCulture).PadLeft(nibbleCount, "0"c)
        End Function

#End Region

#Region " RANDOM STRING METHODS "

        ''' <summary>
        ''' Specifies the EN-US alpha numeric character set.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const AlphanumericCharacters As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

        ''' <summary>
        ''' Returns a randmo string based on the base string of alpha numric upper and lower case characters.
        ''' </summary>
        ''' <param name="length"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function RandomString(ByVal length As Integer) As String

            Return Helper.RandomString(length, Helper.AlphanumericCharacters)

        End Function

        ''' <summary>
        ''' Returns a random string of the specified <paramref name="length">length</paramref> using the 
        ''' specified <paramref name="value">string</paramref>.
        ''' </summary>
        ''' <param name="length">The desired length.</param>
        ''' <param name="value">The desired source string.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function RandomString(ByVal length As Integer, ByVal value As String) As String

            If String.IsNullOrEmpty(value) Then
                Throw New System.ArgumentNullException("value")
            End If

            Dim baseStringLength As Integer = value.Length
            Dim characters As Char() = value.ToCharArray()

            Dim randomStringBuilder As New System.Text.StringBuilder
            Dim randomIndex As Random = New Random
            For i As Integer = 1 To length
                randomStringBuilder.Append(characters(randomIndex.Next(0, baseStringLength)))
            Next
            Return randomStringBuilder.ToString

        End Function

#End Region

#Region " SUB-STRING METHODS "

        ''' <summary>
        ''' Compacts the string to permit display within the given width.
        ''' For example, using <see cref="Windows.Forms.TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="formatInstruction">format instruction</paramref>
        ''' the string 'c:\program files\test app\runme.exe' might turn 
        ''' into 'c:\program files\...\runme.exe' depending on the font and width.
        ''' </summary>
        ''' <param name="value">Specified the string to compact.</param>
        ''' <param name="width">Specifes the width</param>
        ''' <param name="font">Specifies the <see cref="Drawing.Font">font</see></param>
        ''' <param name="formatInstruction">Specifies the designed 
        ''' <see cref="Windows.Forms.TextFormatFlags">formatting</see></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CompactString(ByVal value As String, ByVal width As Integer, _
            ByVal font As Drawing.Font, ByVal formatInstruction As Windows.Forms.TextFormatFlags) As String
            If String.IsNullOrEmpty(value) Then
                Return String.Empty
            End If
            If font Is Nothing Then
                Throw New ArgumentNullException("font")
            End If
            If width <= 0 Then
                Throw New ArgumentOutOfRangeException("width", width, "Must be non-zero")
            End If
            Dim result As String = String.Copy(value)
            System.Windows.Forms.TextRenderer.MeasureText(result, font, New Drawing.Size(width, 0), formatInstruction Or Windows.Forms.TextFormatFlags.ModifyString)
            Return result
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given width.
        ''' For example, the string 'c:\program files\test app\runme.exe' might turn 
        ''' into 'c:\program files\...\runme.exe' depending on the font and width.
        ''' </summary>
        ''' <param name="value">Specified the string to compact.</param>
        ''' <param name="width">Specifes the width</param>
        ''' <param name="font">Specifies the <see cref="Drawing.Font">font</see></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CompactString(ByVal value As String, ByVal width As Integer, _
            ByVal font As Drawing.Font) As String
            Return Helper.CompactString(value, width, font, Windows.Forms.TextFormatFlags.PathEllipsis)
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given control.
        ''' For example, using <see cref="Windows.Forms.TextFormatFlags.PathEllipsis">Path Ellipsis</see>
        ''' as the <paramref name="formatInstruction">format instruction</paramref>
        ''' the string 'c:\program files\test app\runme.exe' might turn 
        ''' into 'c:\program files\...\runme.exe' depending on the font and width.
        ''' </summary>
        ''' <param name="value">Specifies the text</param>
        ''' <param name="control">Specifies the control control within which to format the text.</param>
        ''' <param name="formatInstruction">Specifies the designed 
        ''' <see cref="Windows.Forms.TextFormatFlags">formatting</see></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CompactString(ByVal value As String, _
                                             ByVal control As System.Windows.Forms.Control, _
                                             ByVal formatInstruction As Windows.Forms.TextFormatFlags) As String
            If String.IsNullOrEmpty(value) Then
                Return String.Empty
            End If
            If control Is Nothing Then
                Throw New ArgumentNullException("control")
            End If
            Return Helper.CompactString(value, control.Width, control.Font, formatInstruction)
        End Function

        ''' <summary>
        ''' Compacts the string to permit display within the given control.
        ''' For example, the string 'c:\program files\test app\runme.exe' might turn 
        ''' into 'c:\program files\...\runme.exe' depending on the font and width.
        ''' </summary>
        ''' <param name="value">Specified the string to compact.</param>
        ''' <param name="ctrl">Specifes the control receiving the string</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CompactString(ByVal value As String, ByVal ctrl As Windows.Forms.Control) As String
            If String.IsNullOrEmpty(value) Then
                Return String.Empty
            End If
            If ctrl Is Nothing Then
                Throw New ArgumentNullException("ctrl")
            End If
            Return Helper.CompactString(value, ctrl, Windows.Forms.TextFormatFlags.PathEllipsis)
        End Function

        ''' <summary>Returns the next token string from the given string using 
        '''   the delimitor.</summary>
        ''' <param name="tokens">Includes one or more tokens and where a 
        '''   remainder string is returned</param>
        ''' <param name="delimiter">Specifies the delimitor to use for 
        '''   returning the token.</param>
        ''' <returns><c>The</c> first sub string ending but not including the
        '''   the limiter or the original string if the delimiter
        '''   was not located.</returns>
        ''' <remarks>Use this function to extract a substring from a
        '''   string up to but not including the delimitor.</remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#", Justification:="ok")> _
        Public Shared Function RetrieveToken(ByRef tokens As String, ByVal delimiter As String) _
        As String

            If String.IsNullOrEmpty(delimiter) Then
                Throw New ArgumentNullException("delimiter")
            End If

            If String.IsNullOrEmpty(tokens) Then
                Return String.Empty
            End If

            ' find the delimitor in the string
            Dim delimiterPosition As Int32 = tokens.IndexOf(delimiter.Chars(0))

            ' check if we have a delimitor in the string
            If delimiterPosition > 0 Then

                ' if we have a delimitor

                ' return the string all the way up to but not including
                ' the delimitor
                RetrieveToken = tokens.Substring(0, delimiterPosition)

                ' return the string past the delimitor
                tokens = tokens.Substring(delimiterPosition + delimiter.Length)

            Else
                ' if the string does not include the delimitor

                ' return the original string
                RetrieveToken = tokens

                ' empty the returned string
                tokens = String.Empty

            End If

        End Function

        ''' <summary>Returns the next token string of the specified length.</summary>
        ''' <param name="tokens">Includes one or more tokens and where a 
        '''   remainder string is returned</param>
        ''' <param name="tokenLength">Specifies the length of the token.</param>
        ''' <returns>The first sub string starting at the first character of tokenLength.</returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#", Justification:="ok")> _
        Public Shared Function RetrieveToken(ByRef tokens As String, ByVal tokenLength As Int32) As String

            If String.IsNullOrEmpty(tokens) OrElse tokenLength <= 0 Then
                Return String.Empty
            End If

            ' get the sub-string
            RetrieveToken = tokens.Substring(0, tokenLength)

            ' return the string past the substring
            tokens = tokens.Substring(tokenLength)

        End Function

        ''' <summary>Returns a substring of the input string taking not of start index and length 
        '''   exceptions.</summary>
        ''' <param name="value">The input string to sub string.</param>
        ''' <param name="startIndex">The zero bbased starting index.</param>
        ''' <param name="length">The total length.</param>
        Public Shared Function SafeSubstring(ByVal value As String, ByVal startIndex As Int32, ByVal length As Int32) As String

            If String.IsNullOrEmpty(value) OrElse length <= 0 Then
                Return String.Empty
            End If

            Dim inputLength As Int32 = value.Length
            If startIndex > inputLength Then
                Return String.Empty
            Else
                If startIndex + length > inputLength Then
                    length = inputLength - startIndex
                End If
                Return value.Substring(startIndex, length)
            End If

        End Function

        ''' <summary>Returns the substring after the last occurrence of the specified string</summary>
        ''' <param name="source">The string to substring.</param>
        ''' <param name="search">The string to search for.</param>
        Public Shared Function SubstringAfter(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrEmpty(source) Then
                Return String.Empty
            End If

            If String.IsNullOrEmpty(search) Then
                Return String.Empty
            End If

            Dim location As Int32 = source.LastIndexOf(search)
            If location >= 0 Then
                If location + search.Length < source.Length Then
                    Return source.Substring(location + search.Length)
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If

        End Function

        ''' <summary>Returns the substring before the last occurrence of the specified string</summary>
        ''' <param name="source">The string to substring.</param>
        ''' <param name="search">The string to search for.</param>
        Public Shared Function SubstringBefore(ByVal source As String, ByVal search As String) As String

            If String.IsNullOrEmpty(source) Then
                Return String.Empty
            End If

            If String.IsNullOrEmpty(search) Then
                Return String.Empty
            End If

            Dim location As Int32 = source.LastIndexOf(search)
            If location >= 0 Then
                Return source.Substring(0, location)
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>Returns the substring after the last occurrence of the specified string</summary>
        ''' <param name="source">The string to substring.</param>
        ''' <param name="startDelimiter">The start delimiter to search for.</param>
        ''' <param name="endDelimiter">The end delimiter to search for.</param>
        Public Shared Function SubstringBetween(ByVal source As String, ByVal startDelimiter As String, ByVal endDelimiter As String) As String

            If String.IsNullOrEmpty(source) Then
                Return String.Empty
            End If

            If String.IsNullOrEmpty(startDelimiter) Then
                Return String.Empty
            End If

            Dim startLocation As Int32 = source.LastIndexOf(startDelimiter) + startDelimiter.Length
            Dim endLocation As Int32 = source.LastIndexOf(endDelimiter)

            If startLocation >= 0 AndAlso startLocation < endLocation Then
                Return source.Substring(startLocation, endLocation - startLocation)
            Else
                Return String.Empty
            End If

        End Function

#End Region

#Region " COMMON ESCAPE SEQUENCES "

        ''' <summary>
        ''' Replaces common escape strings such as <code>'\n'</code> or <code>'\r'</code>with control 
        ''' characters such as <code>10</code> and <code>13</code>, respectively.
        ''' </summary>
        ''' <param name="compoundText">Text including escape sequences.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ReplaceCommonEscapeSequences(ByVal compoundText As String) As String
            If (compoundText <> Nothing) Then
                Return compoundText.Replace("\n", Convert.ToChar(10)).Replace("\r", Convert.ToChar(13))
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Replaces control characters such as <code>10</code> and <code>13</code> with 
        ''' common escape strings such as <code>'\n'</code> or <code>'\r'</code>, respectively.
        ''' </summary>
        ''' <param name="compoundText">Text including control characters.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function InsertCommonEscapeSequences(ByVal compoundText As String) As String
            If (compoundText <> Nothing) Then
                Return compoundText.Replace(Convert.ToChar(10), "\n").Replace(Convert.ToChar(13), "\r")
            Else
                Return Nothing
            End If
        End Function

#End Region

#Region " FORMATTING "

        ''' <summary>Returns a centered string.
        ''' </summary>
        ''' <returns>The input string centered with appropriate number of spaces
        '''   on each side.
        ''' </returns>
        ''' <param name="value">
        ''' </param>
        ''' <param name="length">
        ''' </param>
        ''' <history>
        ''' </history>
        Public Shared Function Centered(ByVal value As String, ByVal length As Integer) As String

            Dim leftSpacesCount As Double
            Dim rightSpacesCount As Double
            If length <= 0 Then
                Return String.Empty
            ElseIf String.IsNullOrEmpty(value) Then
                Return String.Empty.PadLeft(length, " "c)
            ElseIf value.Length > length Then
                Return value.Substring(0, length)
            ElseIf value.Length = length Then
                Return value
            Else
                leftSpacesCount = (length - value.Length) \ 2S
                rightSpacesCount = length - value.Length - leftSpacesCount
                Return String.Empty.PadLeft(CInt(leftSpacesCount), " "c) & value _
                        & String.Empty.PadLeft(CInt(rightSpacesCount), " "c)
            End If

        End Function

        ''' <summary>Returns a left aligned string.  If the string is too large, the
        ''' only 'length' values are returned with the last character
        ''' set to '~' to signify the lost character.
        ''' </summary>
        Public Shared Function LeftAligned(ByVal caption As String, ByVal captionWidth As Integer) As String

            Const spaceCharacter As Char = " "c
            Const lostCharacter As Char = "~"c

            ' make sure we have one left space.
            If captionWidth <= 0 Then
                Return String.Empty
            ElseIf caption.Length > captionWidth Then
                Return caption.Substring(0, captionWidth - 1) & lostCharacter
            ElseIf caption.Length = captionWidth Then
                Return caption
            Else
                Return caption.PadRight(captionWidth, spaceCharacter)
            End If

        End Function

        ''' <summary>Returns a right aligned string.  If the string is too large, the
        ''' only 'length' values are returned with the first character
        ''' set to '~' to signify the lost character.
        ''' </summary>
        Public Shared Function RightAligned(ByVal caption As String, ByVal captionWidth As Integer) As String

            Const spaceCharacter As Char = " "c
            Const lostCharacter As Char = "~"c

            ' make sure we have one left space.
            If captionWidth <= 0 Then
                Return String.Empty
            ElseIf caption.Length > captionWidth Then
                Return lostCharacter & caption.Substring(0, captionWidth - 1)
            ElseIf caption.Length = captionWidth Then
                Return caption
            Else
                Return caption.PadLeft(captionWidth, spaceCharacter)
            End If

        End Function

#End Region

    End Class

End Namespace
