Imports System.ComponentModel
Namespace WinForms

    '''   <summary>A message logging form.</summary>
    ''' <license>
    ''' (c) 2002 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="09/21/02" by="David Hary" revision="1.0.839.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("Messages List Control"), _
        System.Drawing.ToolboxBitmap(GetType(WinForms.MessagesBox))> _
    Public Class MessagesBox

        ' work:
        ' System.Drawing.ToolboxBitmap("isr.Support.WinForms.MessagesBox.bmp")

        ' all of these guys did not work.  The resource is correct without the WinForms but
        ' the compile still does not find this resource!
        ' C:\My\Projects\VS8\Support\Solution\Library\WinForms\MessagesBox.bmp
        ' System.Drawing.ToolboxBitmap(GetType(isr.Support.WinForms.MessagesBox))
        ' System.Drawing.ToolboxBitmap("isr.Support.WinForms.MessagesBox.bmp")
        ' System.Drawing.ToolboxBitmap("isr.Support.MessagesBox.bmp")
        ' System.Drawing.ToolboxBitmap("C:\My\Projects\VS8\Support\Solution\Library\WinForms\MessagesBox.bmp")
        ' System.Drawing.ToolboxBitmap(GetType(About), "isr.Support.WinForms.MessagesBox.bmp")> _

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>
        ''' Construtor for this class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            ' set defaults
            Me.BackColor = Drawing.SystemColors.Info

            'Add any initialization after the InitializeComponent() call
            MyBase.ContextMenuStrip = createContextMenuStrip()
            worker = New System.ComponentModel.BackgroundWorker()
            worker.WorkerSupportsCancellation = True
            contentWorker = New System.ComponentModel.BackgroundWorker()
            contentWorker.WorkerSupportsCancellation = True
            messageQueue = New System.Collections.Generic.Queue(Of Text.ExtendedMessage)()
            contentQueue = New System.Collections.Generic.Queue(Of String)
        End Sub

#End Region

#Region " CONTENT QUEUE: THREAD SAFE DISPLAY "

        ''' <summary>
        ''' The content background worker is used for setting the messages text box
        ''' in a thread safe way. 
        ''' </summary>
        ''' <remarks></remarks>
        Private WithEvents contentWorker As System.ComponentModel.BackgroundWorker

        ''' <summary>
        ''' This event handler sets the Text property of the TextBox control. It is called on the 
        ''' thread that created the TextBox control, so the call is thread-safe.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub updateContent(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) _
            Handles contentWorker.RunWorkerCompleted

            If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
                invokeDisplayQueuedContent()
            End If

        End Sub

        ''' <summary>
        ''' Executed on the worker thread and makes a thread-safe call on the 
        ''' user interface.
        ''' </summary>
        ''' <remarks>
        ''' If the calling thread is different from the thread that created the 
        ''' TextBox control, this method creates a SetTextCallback and calls 
        ''' itself asynchronously using the Invoke method.
        ''' </remarks>
        Private Sub invokeDisplayQueuedContent()

            ' check if the creating and calling threads are different
            If MyBase.InvokeRequired Then
                ' if so, invoke a new thread.
                Dim d As New displayQueuedMessagesCallback(AddressOf DisplayQueuedContent)
                MyBase.Invoke(d, New Object() {})
            Else
                Me.DisplayQueuedContent()
            End If

        End Sub


        ''' <summary>
        ''' Displays all the messages that were accumulated in the messages queue.
        ''' </summary>
        Private Sub DisplayQueuedContent()

            Do While Me.contentQueue IsNot Nothing AndAlso Me.contentQueue.Count > 0

                Dim message As String = Me.contentQueue.Dequeue
                If Me._isAppend Then
                    MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                          "{0}{1}{2}", MyBase.Text, Environment.NewLine, message)
                    If MyBase.Lines.Length > Me._resetCount Then
                        Array.Reverse(MyBase.Lines)
                        ReDim Preserve MyBase.Lines(Me._presetCount)
                        Array.Reverse(MyBase.Lines)
                    End If
                Else
                    MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                          "{0}{1}{2}", message, Environment.NewLine, MyBase.Text)
                    If MyBase.Lines.Length > Me._resetCount Then
                        ReDim Preserve MyBase.Lines(Me._presetCount)
                    End If
                End If

                MyBase.Invalidate()
                Windows.Forms.Application.DoEvents()

            Loop

        End Sub

#End Region

#Region " MESSAGE QUEUE:  THREAD SAFE DISPLAY "

        ''' <summary>
        ''' The background worker is used for setting the messages text box
        ''' in a thread safe way. 
        ''' </summary>
        ''' <remarks></remarks>
        Private WithEvents worker As System.ComponentModel.BackgroundWorker

        ''' <summary>
        ''' This event handler sets the Text property of the TextBox control. It is called on the 
        ''' thread that created the TextBox control, so the call is thread-safe.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

            If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
                invokeDisplayQueuedMessages()
            End If

        End Sub

        ''' <summary>
        ''' Executed on the worker thread and makes a thread-safe call on the 
        ''' user interface.
        ''' </summary>
        ''' <remarks>
        ''' If the calling thread is different from the thread that created the 
        ''' TextBox control, this method creates a SetTextCallback and calls 
        ''' itself asynchronously using the Invoke method.
        ''' </remarks>
        Private Sub invokeDisplayQueuedMessages()

            ' check if the creating and calling threads are different
            If MyBase.InvokeRequired Then
                ' if so, invoke a new thread.
                Dim d As New displayQueuedMessagesCallback(AddressOf DisplayQueuedMessages)
                MyBase.Invoke(d, New Object() {})
            Else
                Me.DisplayQueuedMessages()
            End If

        End Sub

        ''' <summary>
        ''' Enables asynchronous calls for handling updates of the messages box.
        ''' </summary>
        Private Delegate Sub displayQueuedMessagesCallback()

#End Region

#Region " METHODS "

        Public Sub ShowResourceNames()
            Dim resouceNames As Array = Me.GetType().Assembly.GetManifestResourceNames
            For Each name As String In resouceNames
                Debug.WriteLine(name)
            Next
        End Sub

        ''' <summary>
        ''' Prepends or appends a new content to the content queue.
        ''' </summary>
        ''' <param name="content ">Specifies the <see cref="System.String">message</see>.</param>
        ''' <param name="append">True to append or false to push the message.</param>
        ''' <remarks>
        ''' Starts the form's Background Worker by calling RunWorkerAsync. 
        ''' The Text property of the TextBox control is set when the Background Worker
        ''' raises the RunWorkerCompleted event.
        ''' </remarks>
        Public Sub AddContent(ByVal content As String, ByVal append As Boolean)

            Me._isAppend = append
            ' add an item to the queue
            Me.contentQueue.Enqueue(content)
            Me.NewMessagesCount += 1
            If Not (MyBase.IsDisposed OrElse Me.contentWorker.IsBusy) Then
                ' Start the form's Background Worker to display the queued messages.
                Me.contentWorker.RunWorkerAsync()
            End If
            Windows.Forms.Application.DoEvents()
        End Sub

        ''' <summary>
        ''' Prepends or appends a new message to the messages list.
        ''' </summary>
        ''' <param name="condition">True to add the message or false to ignore.</param>
        ''' <param name="newMessage">Specifies the <see cref="Text.ExtendedMessage">message</see>.</param>
        ''' <param name="append">True to append or false to push the message.</param>
        ''' <remarks>
        ''' Starts the form's Background Worker by calling RunWorkerAsync. 
        ''' The Text property of the TextBox control is set when the Background Worker
        ''' raises the RunWorkerCompleted event.
        ''' </remarks>
        Public Sub AddMessage(ByVal condition As Boolean, ByVal newMessage As Text.ExtendedMessage, _
                              ByVal append As Boolean)

            If condition Then
                Me._lastMessage = newMessage
                Me._isAppend = append
                ' add an item to the queue
                Me.messageQueue.Enqueue(Me._lastMessage)
                Me.NewMessagesCount += 1
                If Not (MyBase.IsDisposed OrElse Me.worker.IsBusy) Then
                    ' Start the form's Background Worker to display the queued messages.
                    Me.worker.RunWorkerAsync()
                End If
            End If
            Windows.Forms.Application.DoEvents()
        End Sub

        ''' <summary>
        ''' Prepends or appends a new message to the messages list.
        ''' </summary>
        ''' <param name="condition">True to add the message or false to ignore.</param>
        ''' <param name="newMessage">Specifies the message.</param>
        ''' <param name="append">True to append or false to push the message.</param>
        ''' <remarks>
        ''' Starts the form's Background Worker by calling RunWorkerAsync. 
        ''' The Text property of the TextBox control is set when the Background Worker
        ''' raises the RunWorkerCompleted event.
        ''' </remarks>
        Public Sub AddMessage(ByVal condition As Boolean, ByVal newMessage As String, ByVal append As Boolean)
            If condition Then
                Me.AddMessage(condition, New Text.ExtendedMessage(newMessage), append)
            End If
        End Sub

        ''' <summary>
        ''' Displays all the messages that were accumulated in the messages queue.
        ''' </summary>
        Private Sub DisplayQueuedMessages()

            Do While Me.messageQueue IsNot Nothing AndAlso Me.messageQueue.Count > 0

                Dim message As Text.ExtendedMessage = Me.messageQueue.Dequeue
                If Me._isAppend Then
                    MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                          "{0}{1}{2}", MyBase.Text, Environment.NewLine, Me.MessageRecord(message))
                    If MyBase.Lines.Length > Me._resetCount Then
                        Array.Reverse(MyBase.Lines)
                        ReDim Preserve MyBase.Lines(Me._presetCount)
                        Array.Reverse(MyBase.Lines)
                    End If
                Else
                    MyBase.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                          "{0}{1}{2}", Me.MessageRecord(message), Environment.NewLine, MyBase.Text)
                    If MyBase.Lines.Length > Me._resetCount Then
                        ReDim Preserve MyBase.Lines(Me._presetCount)
                    End If
                End If

                MyBase.Invalidate()
                Windows.Forms.Application.DoEvents()

            Loop

        End Sub

        ''' <summary>Appends a new message to the message list.</summary>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub AppendMessage(ByVal newMessage As String)
            Me.AddMessage(True, newMessage, True)
        End Sub

        ''' <summary>Appends a message if the condition is met.</summary>
        ''' <param name="condition">True to log.  Use the application trace switch
        '''   to identify the condition such as TraceSwitch.TraceError meaning that
        '''   the condition is true if the TraceSwitch level is at error, info, warning, 
        '''   or verbose.</param>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        Public Sub AppendMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
            Me.AddMessage(condition, newMessage, True)
        End Sub

        ''' <summary>Returns a message record. This consists of a prefix (bullet or time stamp),
        ''' Trace level, synopsis, and details separated with a delimiter.</summary>
        Private Function MessageRecord(ByVal message As Text.ExtendedMessage) As String

            Dim prefix As String = String.Empty
            If _usingBullet Then
                If _usingTimeBullet Then
                    prefix = message.Timestamp.ToString(_timeFormat, System.Globalization.CultureInfo.CurrentCulture) & _delimiter
                Else
                    prefix = Me._bullet
                End If
            End If

            Dim traceLevelCode As String = String.Empty
            If Me._usingTraceLevel Then
                traceLevelCode = message.TraceLevel.ToString.Substring(0, 1) & _delimiter
            End If

            Dim details As String
            If Me._usingSynopsis Then
                details = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{1}{0}{2}", _delimiter, _
                      message.Synopsis, message.Details)
            Else
                details = message.Details
            End If

            Return String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", _
                prefix, traceLevelCode, details)

        End Function

        ''' <summary>Adds an error message to the top of the message box.</summary>
        ''' <param name="value">The <see cref="System.Exception">error</see> to add.</param>
        Public Sub PrependException(ByVal value As Exception)

            If Not String.IsNullOrEmpty(value.Message) Then

                If value.InnerException IsNot Nothing Then
                    Me.PrependException(value.InnerException)
                End If

                Me.AddMessage(True, New Text.ExtendedMessage( _
                              String.Format(Globalization.CultureInfo.CurrentCulture, _
                                            "{0}{1}{2}", value.Message, Environment.NewLine, value.StackTrace)), False)

            End If

        End Sub

        ''' <summary>Adds an exception message on top of the message list.</summary>
        ''' <param name="details">Specifies the message which to add.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub PrependException(ByVal details As String)
            Me.AddMessage(True, New Text.ExtendedMessage(TraceEventType.Error, details), False)
        End Sub

        ''' <summary>Add message on top of the message list.</summary>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub PrependMessage(ByVal newMessage As String)
            Me.AddMessage(True, newMessage, False)
        End Sub

        ''' <summary>Add message on top of the message list.</summary>
        ''' <param name="details">Specifies the message which to add.</param>
        ''' <param name="traceLevel">Specifies the message trace level.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub PrependMessage(ByVal traceLevel As Diagnostics.TraceEventType, ByVal details As String)
            Me.AddMessage(True, New Text.ExtendedMessage(traceLevel, details), False)
        End Sub

        ''' <summary>Add message on top of the message list.</summary>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub PrependMessage(ByVal newMessage As Text.ExtendedMessage)
            Me.AddMessage(True, newMessage, False)
        End Sub

        ''' <summary>Add message on top of the message list if the condition is met.</summary>
        ''' <param name="condition">Specifies True to push the message onto the 
        '''   message list.  Use the application trace switch to identify the condition 
        '''   such as TraceSwitch.TraceError meaning that the condition is true if the 
        '''   TraceSwitch level is at error, info, warning, or verbose.</param>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        Public Sub PrependMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
            Me.AddMessage(condition, newMessage, False)
        End Sub

        ''' <summary>Add message on top of the message list.</summary>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        ''' <remarks>Use this method to add a message.  Adding a message takes less than 1ms.</remarks>
        Public Sub PushMessage(ByVal newMessage As String)
            Me.AddMessage(True, newMessage, False)
        End Sub

        ''' <summary>Add message on top of the message list if the condition is met.</summary>
        ''' <param name="condition">Specifies True to push the message onto the 
        '''   message list.  Use the application trace switch to identify the condition 
        '''   such as TraceSwitch.TraceError meaning that the condition is true if the 
        '''   TraceSwitch level is at error, info, warning, or verbose.</param>
        ''' <param name="newMessage">Specifies the message which to add.</param>
        Public Sub PushMessageIf(ByVal condition As Boolean, ByVal newMessage As String)
            Me.AddMessage(condition, newMessage, False)
        End Sub

#End Region

#Region " BULLET, TRACE LEVEL, DELIMITER "

        Private _delimiter As String = ", "
        ''' <summary>Gets or sets the Delimiter to use for prefixing messages.</summary>
        ''' <value><c>Delimiter</c>is a <see cref="System.String">String</see> property.</value>
        <Category("Appearance"), Description("Delimited for message data"), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue(", ")> _
        Public Property Delimiter() As String
            Get
                Return _delimiter
            End Get
            Set(ByVal Value As String)
                _delimiter = Value
            End Set
        End Property

        Private _bullet As String = "* "
        ''' <summary>Gets or sets the bullet to use for prefixing messages.</summary>
        ''' <value><c>Bullet</c>is a <see cref="System.String">String</see> property.</value>
        <Category("Appearance"), Description("Bullet for prefixing messages when turning off time prefix."), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue("* ")> _
        Public Property Bullet() As String
            Get
                Return _bullet
            End Get
            Set(ByVal Value As String)
                _bullet = Value
            End Set
        End Property

        Private _usingBullet As Boolean = True
        ''' <summary>Gets or sets the condition for each message is prefixed.
        ''' This would be the <see cref="Bullet"/> unless <see cref="UsingTimeBullet"/> is
        ''' True. Default is True.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
        <Category("Appearance"), Description("Use a time or bullet prefix."), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue(True)> _
        Public Property UsingBullet() As Boolean
            Get
                Return _usingBullet
            End Get
            Set(ByVal Value As Boolean)
                _usingBullet = Value
            End Set
        End Property

        Private _usingSynopsis As Boolean
        ''' <summary>Gets or sets the condition for each message includes the synopsys.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
        <Category("Appearance"), Description("Include a synopsis in addition to the message details."), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue(False)> _
        Public Property UsingSynopsis() As Boolean
            Get
                Return _usingSynopsis
            End Get
            Set(ByVal Value As Boolean)
                _usingSynopsis = Value
            End Set
        End Property

        Private _usingTimeBullet As Boolean = True
        ''' <summary>Gets or sets the condition for each message is prefixed with a time format.
        '''   Default is True.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
        <Category("Appearance"), Description("Use a time prefix."), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue(True)> _
        Public Property UsingTimeBullet() As Boolean
            Get
                Return _usingTimeBullet
            End Get
            Set(ByVal Value As Boolean)
                _usingTimeBullet = Value
            End Set
        End Property

        Private _usingTraceLevel As Boolean
        ''' <summary>Gets or sets the condition for each message is prefixed with a trace level code.
        ''' Default is False.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see>.</value>
        <Category("Appearance"), Description("Display the trace level as part of the prefix or bullet."), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue(False)> _
        Public Property UsingTraceLevel() As Boolean
            Get
                Return _usingTraceLevel
            End Get
            Set(ByVal Value As Boolean)
                _usingTraceLevel = Value
            End Set
        End Property

#End Region

#Region " TAB CAPTION "

        ''' <summary>Gets or sets the number of new messages in the cache.</summary>
        Private _newMessagesCount As Integer

        ''' <summary>Gets or sets the number of new messages in the cache.</summary>
        <Browsable(False)> _
        Public Property NewMessagesCount() As Integer
            Get
                Return _newMessagesCount
            End Get
            Set(ByVal Value As Integer)
                If (_newMessagesCount <> Value) OrElse (Value = 0) Then
                    _newMessagesCount = Value
                    Me.OnMessageAdded()
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the tab caption.</summary>
        Private _tabCaption As String = "Messages"

        ''' <summary>Gets or sets the tab caption.</summary>
        <Category("Appearance"), Description("Default title for the parent tab"), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue("Messages")> _
        Public Property TabCaption() As String
            Get
                Return _tabCaption
            End Get
            Set(ByVal Value As String)
                If String.Compare(_tabCaption, Value) <> 0 Then
                    _tabCaption = Value
                    Me.OnMessageAdded()
                Else
                    _tabCaption = Value
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the tab caption.</summary>
        Private _tabCaptionformat As String = "{0} ({1})"

        ''' <summary>Gets or sets the tab caption format.</summary>
        <Category("Appearance"), Description("Formats the tab caption with number of new messages"), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue("{0} ({1})")> _
        Public Property TabCaptionFormat() As String
            Get
                Return _tabCaptionformat
            End Get
            Set(ByVal Value As String)
                _tabCaptionformat = Value
            End Set
        End Property

        ''' <summary>Returns a caption based on the value plus the number of new messges.</summary>
        Public Function BuildTabCaption() As String
            Return Me.BuildTabCaption(_tabCaptionformat)
        End Function

        ''' <summary>Returns a caption based on the value plus the number of new messges.</summary>
        Public Function BuildTabCaption(ByVal captionFormat As String) As String
            If _newMessagesCount = 0 Then
                BuildTabCaption = _tabCaption
            Else
                BuildTabCaption = String.Format(Globalization.CultureInfo.CurrentCulture, _
                                                captionFormat, _tabCaption, _newMessagesCount)
            End If
        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>
        ''' Holds the queue of text.  The queue gets filled with content each time
        ''' the worker is busy.
        ''' </summary>
        ''' <remarks></remarks>
        Private contentQueue As System.Collections.Generic.Queue(Of String)

        ''' <summary>
        ''' Holds the append mode.  If true, messages are appended to the end of the 
        ''' messages box.  Otherwise, messages are pushed onto the top of the message box.
        ''' </summary>
        Private _isAppend As Boolean

        Private _lastMessage As Text.ExtendedMessage
        ''' <summary>Gets or sets the last message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        ''' <remarks>Use this property to get the status message generated by the object.</remarks>
        <Browsable(False)> _
        Public ReadOnly Property LastMessage() As Text.ExtendedMessage
            Get
                Return _lastMessage
            End Get
        End Property

        ''' <summary>
        ''' Holds the queue of messages.  The queue gets filled with messages each time
        ''' the worker is busy.
        ''' </summary>
        ''' <remarks></remarks>
        Private messageQueue As System.Collections.Generic.Queue(Of Text.ExtendedMessage)

        Private _resetCount As Int32 = 100
        ''' <summary>Gets or sets the reset count.</summary>
        ''' <value><c>ResetSize</c>is an Int32 property.</value>
        ''' <remarks>The message list gets reset to the preset count when the
        '''   message count exceeds the reset count.</remarks>
        <Category("Appearance"), Description("Number of lines aty which to reset"), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue("100")> _
        Public Property ResetCount() As Int32
            Get
                Return _resetCount
            End Get
            Set(ByVal value As Int32)
                _resetCount = value
            End Set
        End Property

        Private _presetCount As Int32 = 50
        ''' <summary>Gets or sets the preset count.</summary>
        ''' <value><c>PresetSize</c>is an Int32 property.</value>
        ''' <remarks>The message list gets reset to the preset count when the
        '''   message count exceeds the reset count.</remarks>
        <Category("Appearance"), Description("Number of lines to reset to"), _
            Browsable(True), _
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
            DefaultValue("50")> _
        Public Property PresetCount() As Int32
            Get
                Return _presetCount
            End Get
            Set(ByVal value As Int32)
                _presetCount = value
            End Set
        End Property

        Private _timeFormat As String = "HH:mm:ss.fff"
        ''' <summary>Gets or sets the format for displaying the time prefix, such as 'HH:mm:ss.f'.</summary>
        ''' <value><c>TimeFormat</c>is a <see cref="System.String">String</see> property.</value>
        <Category("Appearance"), Description("Formats the time prefix"), _
          Browsable(True), _
          DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), _
          DefaultValue("HH:mm:ss.fff")> _
        Public Property TimeFormat() As String
            Get
                Return _timeFormat
            End Get
            Set(ByVal Value As String)
                ' try the time format to raise an error if not set correctly.
                Date.Now.ToString(Value, System.Globalization.CultureInfo.CurrentCulture)
                _timeFormat = Value
            End Set
        End Property

#End Region

#Region " CONTEXT MENU STRIP "

        ''' <summary>
        ''' Creates a context menu strip.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function createContextMenuStrip() As Windows.Forms.ContextMenuStrip

            ' Create a new ContextMenuStrip control.
            Dim myContextMenuStrip As Windows.Forms.ContextMenuStrip = New Windows.Forms.ContextMenuStrip()

            ' Attach an event handler for the 
            ' ContextMenuStrip control's Opening event.
            AddHandler myContextMenuStrip.Opening, AddressOf cms_Opening

            Return myContextMenuStrip

        End Function

        ''' <summary>
        ''' Adds menu items.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>
        ''' This event handler is invoked when the <see cref="ContextMenuStrip"/> control's 
        ''' Opening event is raised. 
        ''' </remarks>
        Private Sub cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            Dim myContextMenuStrip As Windows.Forms.ContextMenuStrip = CType(sender, Windows.Forms.ContextMenuStrip)

            ' Clear the ContextMenuStrip control's Items collection.
            myContextMenuStrip.Items.Clear()

            ' Populate the ContextMenuStrip control with its default items.
            ' myContextMenuStrip.Items.Add("-")
            myContextMenuStrip.Items.Add(New Windows.Forms.ToolStripMenuItem("Clear &All", Nothing, _
                AddressOf clearAllMenuItem_Click, "Clear"))

            ' Set Cancel to false. 
            ' It is optimized to true based on empty entry.
            e.Cancel = False

        End Sub

        ''' <summary>Applies the high point Output.</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub clearAllMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            MyBase.Clear()
            Me.NewMessagesCount = 0

        End Sub

#End Region

#Region " EVENTS "

        ''' <summary>
        ''' Occurs when a new message is added.
        ''' </summary>
        ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.</param>
        Public Event MessageAdded As EventHandler(Of EventArgs)

        ''' <summary>
        ''' Occurs when a new message is added.  Adds tthe message to the message
        ''' count and returns a new caption.
        ''' </summary>
        Public Sub OnMessageAdded()

            RaiseEvent MessageAdded(Me, EventArgs.Empty)

        End Sub

#End Region

    End Class

End Namespace

