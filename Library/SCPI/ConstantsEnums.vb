Namespace Scpi

#Region " Event Flags "

    ''' <summary>Gets or sets the status byte flags of the service request register.</summary>
    ''' <remarks>Extends the VISA 
    '''   <see cref="NationalInstruments.VisaNS.StatusByteFlags.EventStatusRegister">event status flags</see>.
    '''   Because the VISA status returns a signed byte, we need to use Int16.</remarks>
    <System.Flags()> Public Enum ServiceRequests
        None = 0
        MeasurementEvent = 1
        NotUsed = 2
        ErrorAvailable = 4
        QuestionableEvent = 8
        MessageAvailable = 16 ' MAV
        StandardEvent = 32 ' ESB
        RequestingService = 64 ' RQS
        OperationEvent = 128
        All = 255
    End Enum

    ''' <summary>Gets or sets the status byte flags of the standard event register.</summary>
    <System.Flags()> Public Enum StandardEvents
        None = 0
        OperationComplete = 1
        NotUsed = 2
        QueryError = 4
        DeviceDependentError = 8
        ExecutionError = 16
        CommandError = 32
        UserRequest = 64
        PowerOn = 128
        All = 255
    End Enum

#End Region

#Region " SCPI Modes "

    ''' <summary>Specifies the output off mode.</summary>
    Public Enum OutputOffMode
        None
        Guard
        HighImpedance
        Normal
        Zero
    End Enum

    ''' <summary>Specifies the route terminal mode.</summary>
    Public Enum RouteTerminalMode
        None
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="FRONT")> _
        FRONT
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="REAR")> _
        REAR
    End Enum

    ''' <summary>Specifies the sense function modes.</summary>
    <System.Flags()> Public Enum SenseFunctionModes
        None = 0
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="VOLT_DC")> _
        VOLT_DC = 1
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="CURR_DC")> _
        CURR_DC = 2
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="VOLT_AC")> _
        VOLT_AC = 4
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="CURR_AC")> _
        CURR_AC = 8
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="RES")> _
        RES = 16
    End Enum

    ''' <summary>Specifies the sense function modes.</summary>
    Public Enum SenseMode
        None
        TwoWire
        FourWire
    End Enum

    ''' <summary>Specifies the source function modes.</summary>
    Public Enum SourceFunctionMode
        None
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="VOLT")> _
        VOLT
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="CURR")> _
        CURR
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="Mem")> _
        Mem
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="VOLT_DC")> _
        VOLT_DC
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="CURR_DC")> _
        CURR_DC
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="VOLT_AC")> _
        VOLT_AC
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")> _
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId:="CURR_AC")> _
        CURR_AC
    End Enum

    ''' <summary>Specifies the source modes.</summary>
    Public Enum SourceMode
        None
        Fixed
        Sweep
        List
    End Enum

#End Region

End Namespace
