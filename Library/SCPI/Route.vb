Imports NationalInstruments
Namespace Scpi

    ''' <summary>Defines a SCPI Route Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class RouteSubsystem
        Inherits Subsystem

#Region " Shared Methods and Properties "

        ''' <summary>Closes the specified channels in the list.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <param name="channelList"></param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub CloseChannels(ByVal session As VisaNS.MessageBasedSession, ByVal channelList As String)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CLOS {0}", channelList))
        End Sub

        ''' <summary>Recalls channel pattern from a memory location.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <param name="memoryLocation">Specifies a memory location between 1 and 100.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub MemoryRecall(ByVal session As VisaNS.MessageBasedSession, ByVal memoryLocation As Int32)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MEM:REC M{0}", memoryLocation))
        End Sub

        ''' <summary>Saves existing channel pattern into a memory location.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <param name="memoryLocation">Specifies a memory location between 1 and 100.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub MemorySave(ByVal session As VisaNS.MessageBasedSession, ByVal memoryLocation As Int32)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:MEM:SAVE M{0}", memoryLocation))
        End Sub

        ''' <summary>Opens all channels</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub OpenAll(ByVal session As VisaNS.MessageBasedSession)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":ROUT:OPEN ALL")
        End Sub

        ''' <summary>Opens the specified channels in the list.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <param name="channelList"></param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub OpenChannels(ByVal session As VisaNS.MessageBasedSession, ByVal channelList As String)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:OPEN {0}", channelList))
        End Sub

        ''' <summary>Gets or sets the scan list.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property ScanList(ByVal session As GpibSession) As String
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryTrimEnd(":ROUT:SCAN?")
            End Get
            Set(ByVal value As String)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:SCAN {0}", value))
            End Set
        End Property

        ''' <summary>Gets or sets the slot type.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <param name="slotNumber">Specifies the slot number.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property SlotCardType(ByVal session As GpibSession, ByVal slotNumber As Int32) As String
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:CTYPE?", slotNumber))
            End Get
            Set(ByVal value As String)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:CTYPE {1}", slotNumber, value))
            End Set
        End Property

        ''' <summary>Gets or sets the slot settling time.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <param name="slotNumber">Specifies the slot number.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property SlotSettlingTime(ByVal session As GpibSession, ByVal slotNumber As Int32) As Double
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryDouble(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:STIME?", slotNumber))
            End Get
            Set(ByVal value As Double)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:CONF:SLOT{0}:STIME {1}", slotNumber, value))
            End Set
        End Property

        ''' <summary>Gets or sets the terminal <see cref="SCPI.RouteTerminalMode">mode</see>.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property TerminalMode(ByVal session As GpibSession) As Scpi.RouteTerminalMode
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Dim mode As String = session.QueryTrimEnd(":ROUT:TERM?")
                Return CType(mode, Scpi.RouteTerminalMode)
            End Get
            Set(ByVal value As Scpi.RouteTerminalMode)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write( _
                              String.Format(Globalization.CultureInfo.CurrentCulture, ":ROUT:TERM {0}", value))
            End Set
        End Property
#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        Private _scanList As String
        ''' <summary>Gets or sets the scan list.</summary>
        Public Property ScanList() As String
            Get
                If MyBase.ForceRead OrElse _scanList Is Nothing Then
                    _scanList = RouteSubsystem.ScanList(MyBase.Instrument.GpibSession)
                End If
                Return _scanList
            End Get
            Set(ByVal value As String)
                If MyBase.ForceWrite OrElse (String.Compare(value, Me.ScanList) <> 0) Then
                    RouteSubsystem.ScanList(MyBase.Instrument.GpibSession) = value
                End If
                _scanList = value
            End Set
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()

        End Sub

#End Region

    End Class

End Namespace
