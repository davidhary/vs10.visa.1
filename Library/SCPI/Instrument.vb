Imports NationalInstruments
Namespace Scpi

    ''' <summary>Provides a Core. for SCPI (488.2) instruments.</summary>
    ''' <remarks>Later on we can tease out the non-SCPI parts of this instrument 
    '''   to define a Instrument that inherits a Instrument instrument.</remarks>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/07/05" by="David Hary" revision="1.0.2137.x">
    ''' Created
    ''' </history>
    Public Class Instrument

        ' based on the Connect inheritable class
        Inherits IO.ConnectLogBaseClass

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Protected Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        ' dispose of 'status' properties
                        _Id = String.Empty

                        ' terminate the SCPI sub systems
                        If _trigger IsNot Nothing Then
                            _trigger.Dispose()
                            _trigger = Nothing
                        End If
                        If _system IsNot Nothing Then
                            _system.Dispose()
                            _system = Nothing
                        End If
                        If _sense IsNot Nothing Then
                            _sense.Dispose()
                            _sense = Nothing
                        End If
                        If _output IsNot Nothing Then
                            _output.Dispose()
                            _output = Nothing
                        End If
                        If _status IsNot Nothing Then
                            _status.Dispose()
                            _status = Nothing
                        End If
                        If _route IsNot Nothing Then
                            _route.Dispose()
                            _route = Nothing
                        End If
                        If _source IsNot Nothing Then
                            _source.Dispose()
                            _source = Nothing
                        End If

                        If _gpibSession IsNot Nothing Then
                            If MyBase.IsConnected Then
                                ' remove the service request handler ignoring exceptions.
                                RemoveHandler _gpibSession.ServiceRequest, AddressOf OnVisaServiceRequest
                            End If
                            _gpibSession.Dispose()
                            _gpibSession = Nothing
                        End If

                        If _gpibInterface IsNot Nothing Then
                            _gpibInterface.Dispose()
                            _gpibInterface = Nothing
                        End If

                        ' remove the reference to the gui
                        _gui = Nothing


                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " GENERIC "

        ''' <summary>
        ''' Makes the standard immediate if function type safe.
        ''' </summary>
        ''' <typeparam name="T">Specifies the returned value.</typeparam>
        ''' <param name="condition">Specifies the condition.</param>
        ''' <param name="truePart">Specifies the value to return if the condition is true.</param>
        ''' <param name="falsePart">Specifies the value to return if the condition is false.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function IIf(Of T)(ByVal condition As Boolean, ByVal truePart As T, ByVal falsePart As T) As T
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

#End Region

#Region " INSTRUMENT METHODS "


        ''' <summary>Clears the resource.</summary>
        Public Overrides Sub Clear()
            Me.ResetAndClear()
        End Sub

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Dim lastAction As String = String.Empty

            Try

                If MyBase.UsingDevices Then

                    ' open a gpib session to this instrument
                    ' Me._gpibSession = isr.Visa.Helper.OpenGpibSession(resourceName)
                    Me._gpibSession = New isr.Visa.GpibSession(resourceName)

                    If _gpibSession Is Nothing Then

                        Me.Disconnect()

                    Else

                        ' clear status and disable service request on all operations.
                        Me.ClearStatus()
                        Me.ServiceRequestEventEnable = ServiceRequests.None

                        ' register the handler before enabling the event
                        AddHandler _gpibSession.ServiceRequest, AddressOf OnVisaServiceRequest
                        _gpibSession.EnableEvent(VisaNS.MessageBasedSessionEventType.ServiceRequest, VisaNS.EventMechanism.Handler)

                        ' instantiate the SCPI sub systems
                        _system = New SystemSubsystem(Me)
                        _sense = New SenseSubsystem(Me)
                        _output = New OutputSubsystem(Me)
                        _route = New RouteSubsystem(Me)
                        _status = New StatusSubsystem(Me)
                        _source = New SourceSubsystem(Me)
                        _trigger = New TriggerSubsystem(Me)

                        ' issue a selective device clear and total clear of the instrument.
                        Me.ResetAndClear()

                        MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} opened.", Me.InstanceName)

                        lastAction = "connecting"
                        MyBase.Connect(resourceName)

                    End If

                Else

                    lastAction = "connecting"
                    MyBase.Connect(resourceName)

                End If

                If Me.IsConnected Then
                    If Me.Visible Then
                        _gui.Connect()
                    End If
                Else
                    If Me.Visible Then
                        _gui.Disconnect()
                    End If
                End If

                Return MyBase.IsConnected

            Catch ex As isr.Visa.BaseException

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                ' throw an exception
                MyBase.StatusMessage = String.Format(System.Windows.Forms.Application.CurrentCulture, _
                    "{0} failed {1}", Me.InstanceName, lastAction)
                Throw New isr.Visa.OperationOpenException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="gpibAddress">Specifies the Gpib address of the instrument.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overridable Overloads Function Connect(ByVal gpibAddress As Int32) As Boolean

            Return Me.Connect(Helper.BuildGpibResourceName(gpibAddress))

        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        Public Overrides Function Disconnect() As Boolean

            Dim lastAction As String = String.Empty

            Try

                lastAction = "disconnecting"
                MyBase.Disconnect()

                If Not Me.IsConnected Then
                    If Me.Visible Then
                        _gui.Disconnect()
                    End If
                End If

                Return Not MyBase.IsConnected

            Catch ex As isr.Visa.BaseException
                ' throw an exception
                MyBase.StatusMessage = String.Format(System.Windows.Forms.Application.CurrentCulture, _
                    "{0} failed {1}", Me.InstanceName, lastAction)
                Throw New isr.Visa.OperationCloseException(MyBase.StatusMessage, ex)
            End Try

        End Function

        ''' <summary>This overrides the ToString method returning the instance name if not empty.</summary>
        ''' <remarks>Use this method to return the instance name.</remarks>
        Public Overrides Function ToString() As String
            If MyBase.InstanceName.Length > 0 Then
                Return MyBase.InstanceName
            Else
                Return MyBase.ToString
            End If
        End Function

#End Region

#Region " INSTRUMENT PROPERTIES "

        Private _gui As InstrumentPanel
        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property BaseGui() As InstrumentPanel
            Get
                Return _gui
            End Get
            Set(ByVal Value As InstrumentPanel)
                _gui = Value
            End Set
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public ReadOnly Property HadError() As Boolean
            Get
                Return _lastServiceEventArgs.HasError OrElse _gpibSession.HasError
#If DEBUG Then
                ' In debug mode, make sure to wait for the end of service
                ' request processing to get the error immediately after
                ' the instrument reports it.  In this mode, we expect to have
                ' syntax error that the instrument is unlikely to be happy about.
                ' once in release mode, the incidences are lower and we can wait for
                ' the error rather than slowing operations due to this delay
                Return Me.HadError(10)
#Else
        Windows.Forms.Application.DoEvents()
        Return _lastServiceEventArgs.HasError
#End If
            End Get
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        ''' <param name="waitMilliseconds">Time to wait before returning the error.</param>
        Private ReadOnly Property HadError(ByVal waitMilliseconds As Int32) As Boolean
            Get
                ' add a couple of milliseconds wait
                Dim endTime As Date = Date.Now.Add(New TimeSpan(0, 0, 0, 0, waitMilliseconds))
                Do
                    Windows.Forms.Application.DoEvents()
                Loop While _lastServiceEventArgs.ServicingRequest And (Date.Now <= endTime)
                Return _lastServiceEventArgs.HasError
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If String.IsNullOrEmpty(id) Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            _manufacturerName = idItems(0)

            ' model: MODEL 2420
            _model = idItems(1)

            ' Serial Number: 0669977
            _serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            _firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(_firmwareRevision)

        End Sub

        ''' <summary>Queries the Gpib instrument and returns a Boolean value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryBoolean(ByVal question As String) As Boolean
            If _gpibSession Is Nothing Then
                Return True
            Else
                Return _gpibSession.QueryBoolean(question)
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns a double value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryDouble(ByVal question As String) As Double
            If _gpibSession Is Nothing Then
                Return 0
            Else
                Return _gpibSession.QueryDouble(question)
            End If
        End Function

        ''' <summary>Queries the Gpib instrument and returns an Int32 value.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryInt32(ByVal question As String) As Int32
            If _gpibSession Is Nothing Then
                Return 0
            Else
                Return _gpibSession.QueryInt32(question)
            End If
        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overridable Overloads Function ResetAndClear() As Boolean

            Try

                ' clear the device
                Me.SelectiveDeviceClear()

                ' reset the device and set default properties
                Me.ResetToKnownState()

                ' Clear the device Status and set more defaults
                Me.ClearStatus()

                If _gpibSession IsNot Nothing Then

                    ' preset the event registers to their power on conditions
                    ' including the event transition registers.
                    Scpi.StatusSubsystem.Preset(_gpibSession)

                End If

                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} reset and cleared.", Me.InstanceName)

                Return True

            Catch ex As NationalInstruments.VisaNS.VisaException

                MyBase.StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                Return False

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session.  Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <param name="timeout"></param>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If _gpibSession Is Nothing Then
                Return True
            End If

            Dim newTimeout As Int32 = _gpibSession.Timeout
            _gpibSession.Timeout = Math.Max(_gpibSession.Timeout, timeout)
            Dim outcome As Boolean = ResetAndClear()
            _gpibSession.Timeout = newTimeout
            Return outcome

        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & Scpi.Instrument.IIf(Of String)(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & Scpi.Instrument.IIf(Of String)(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As Scpi.ServiceEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="isr.Visa.Scpi.ServiceEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As Scpi.ServiceEventArgs
            Get
                Return _lastServiceEventArgs
            End Get
            Set(ByVal value As Scpi.ServiceEventArgs)
                _lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return _manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return _model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return _serialNumber
            End Get
        End Property

#End Region

#Region " GPIB VISA INTERFACE METHODS "

        Private _gpibInterface As VisaNS.GpibInterface
        ''' <summary>Gets or sets reference to the Gpib interface for this instrument.</summary>
        Public ReadOnly Property GpibInterface() As VisaNS.GpibInterface
            Get
                If _gpibInterface Is Nothing AndAlso MyBase.UsingDevices Then
                    _gpibInterface = Helper.OpenGpibInterface(_gpibSession.HardwareInterfaceName & "::INTFC")
                End If
                Return _gpibInterface
            End Get
        End Property

        ''' <summary>Issues an interface clear.</summary>
        Public Sub InterfaceClear()

            If GpibInterface IsNot Nothing Then
                GpibInterface.SendInterfaceClear()
            End If

        End Sub

#End Region

#Region " COMMON COMMANDS "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        Public Overridable Sub ClearStatus()

            If Me._gpibSession IsNot Nothing Then

                ' Clear the device status.  This will set all the
                ' standard subsystem properties.
                Me._gpibSession.ClearStatus()

            End If

            ' Clear the event arguments
            Me._lastServiceEventArgs = New Scpi.ServiceEventArgs

            ' clear the error and information enunciators
            If Me.Visible Then
                Me.BaseGui.DisplayMessage("Status Cleared")
            End If

        End Sub

        Private _Id As String
        ''' <summary>Queries the Gpib instrument and returns the string save the termination character.</summary>
        Public ReadOnly Property Id() As String
            Get
                If String.IsNullOrEmpty(_Id) Then
                    If Me._gpibSession Is Nothing Then
                        _Id = "GPib Corp., Model 1, 123, A"
                    Else
                        _Id = _gpibSession.ReadInstrumentId
                    End If
                    Me.ParseInstrumentId(_Id)
                End If
                Return _Id
            End Get
        End Property

        ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
        Public Function IsOpc() As Boolean
            If Me._gpibSession Is Nothing Then
                Return True
            End If
            _lastServiceEventArgs.OperationCompleted = _gpibSession.IsOpc
            Return _lastServiceEventArgs.OperationCompleted
        End Function

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overridable Sub ResetToKnownState()

            If Me._gpibSession Is Nothing Then
                Return
            End If

            _gpibSession.ResetToKnownState()

            ' Set default values if any
            Me._output.ResetToKnownState()
            Me._route.ResetToKnownState()
            Me._status.ResetToKnownState()
            Me._system.ResetToKnownState()
            Me._trigger.ResetToKnownState()

        End Sub

        ''' <summary>Reads the service request event status from the instrument.</summary>
        Public ReadOnly Property ServiceRequestEventStatus() As ServiceRequests
            Get
                If Me._gpibSession Is Nothing Then
                    Return ServiceRequests.None
                End If
                Return _gpibSession.ServiceRequestEventStatus()
            End Get
        End Property

        ''' <summary>Programs or reads back the service request event request.</summary>
        ''' <returns>The <see cref="isr.Visa.Scpi.ServiceRequests">mask</see>
        '''    to use for enabling the events.</returns>
        ''' <remarks>Make sure to issue <see cref="ClearStatus">status clear</see> before
        '''   setting the service request.</remarks>
        Public Property ServiceRequestEventEnable() As ServiceRequests
            Get
                If _gpibSession Is Nothing Then
                    Return ServiceRequests.None
                Else
                    Return _gpibSession.ServiceRequestEventEnable()
                End If
            End Get
            Set(ByVal value As ServiceRequests)
                If _gpibSession IsNot Nothing Then
                    _gpibSession.ServiceRequestEventEnable() = value
                End If
            End Set
        End Property

        ''' <summary>Reads the standard event status from the instrument.</summary>
        Public ReadOnly Property StandardEventStatus() As StandardEvents
            Get
                If Me._gpibSession Is Nothing Then
                    Return StandardEvents.None
                End If
                Return _gpibSession.StandardEventStatus()
            End Get
        End Property

        ''' <summary>Programs or reads back the standard event request.</summary>
        ''' <returns>The <see cref="isr.Visa.Scpi.StandardEvents">mask</see>
        '''    to use for enabling the events.</returns>
        Public Property StandardEventEnable() As StandardEvents
            Get
                If _gpibSession Is Nothing Then
                    Return StandardEvents.None
                Else
                    Return _gpibSession.StandardEventEnable()
                End If
            End Get
            Set(ByVal value As StandardEvents)
                If _gpibSession IsNot Nothing Then
                    _gpibSession.StandardEventEnable() = value
                End If
            End Set
        End Property

#End Region

#Region " GPIB VISA Session Methods and Properties "

        ''' <summary>Selectively clears the instrument.</summary>
        Protected Overridable Sub SelectiveDeviceClear()

            If Me._gpibSession IsNot Nothing Then
                ' clear the device
                _gpibSession.SelectiveDeviceClear()
            End If

        End Sub

        ''' <summary>Gets or sets the Gpib Address of the instrument on the Gpib bus.</summary>
        Public ReadOnly Property GpibAddress() As Int32
            Get
                If Me._gpibSession Is Nothing Then
                    Return 0
                Else
                    Return _gpibSession.PrimaryAddress
                End If
            End Get
        End Property

        Private _gpibSession As isr.Visa.GpibSession
        ''' <summary>Gets or sets reference to the Gpib session.</summary>
        Public ReadOnly Property GpibSession() As isr.Visa.GpibSession
            Get
                Return _gpibSession
            End Get
        End Property

        ''' <summary>Queries the Gpib instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimEnd(ByVal question As String) As String
            If Me._gpibSession Is Nothing Then
                Return String.Empty
            Else
                Return _gpibSession.QueryTrimEnd(question)
            End If
        End Function

        ''' <summary>Reads a string from Gpib instrument and returns the string save the termination character.</summary>
        Public Function ReadStringTrimEnd() As String
            If Me._gpibSession Is Nothing Then
                Return String.Empty
            Else
                Return _gpibSession.ReadStringTrimEnd()
            End If
        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If Me._gpibSession IsNot Nothing Then
                _gpibSession.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If Me._gpibSession IsNot Nothing Then
                _gpibSession.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

#Region " SCPI SUBSYSTEMS "

        Private _sense As SenseSubsystem
        ''' <summary>Gets or sets reference to the Sense SCPI subsystem.</summary>
        Public ReadOnly Property Sense() As SenseSubsystem
            Get
                Return _sense
            End Get
        End Property

        Private _output As OutputSubsystem
        ''' <summary>Gets or sets reference to the Output SCPI subsystem.</summary>
        Public ReadOnly Property Output() As OutputSubsystem
            Get
                Return _output
            End Get
        End Property

        Private _route As RouteSubsystem
        ''' <summary>Gets or sets reference to the Route SCPI subsystem.</summary>
        Public ReadOnly Property Route() As RouteSubsystem
            Get
                Return _route
            End Get
        End Property

        Private _source As SourceSubsystem
        ''' <summary>Gets or sets reference to the Source SCPI subsystem.</summary>
        Public ReadOnly Property Source() As SourceSubsystem
            Get
                Return _source
            End Get
        End Property

        Private _status As StatusSubsystem
        ''' <summary>Gets or sets reference to the Status SCPI subsystem.</summary>
        Public ReadOnly Property Status() As StatusSubsystem
            Get
                Return _status
            End Get
        End Property

        Private _system As SystemSubsystem
        ''' <summary>Gets or sets reference to the System SCPI subsystem.</summary>
        Public ReadOnly Property ScpiSystem() As SystemSubsystem
            Get
                Return _system
            End Get
        End Property

        Private _trigger As TriggerSubsystem
        ''' <summary>Gets or sets reference to the Trigger SCPI subsystem.</summary>
        Public ReadOnly Property Trigger() As TriggerSubsystem
            Get
                Return _trigger
            End Get
        End Property

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Raised if an error had occurred.</summary>
        Public Event ErrorAvailable As EventHandler(Of System.EventArgs)

        ''' <summary>Raises the error available event.</summary>
        ''' <param name="e">Passes reference to the <see cref="System.EventArgs">event arguments</see>.</param>
        Protected Overridable Sub OnErrorAvailable(ByVal e As System.EventArgs)
            RaiseEvent ErrorAvailable(Me, e)
        End Sub

        ''' <summary>Raised upon receiving a service request.  This event does not expose the
        '''   internal service request arguments directly.</summary>
        Public Event ServiceRequest As EventHandler(Of Scpi.ServiceEventArgs) ' System.EventArgs)

        ''' <summary>Raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="Scpi.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnServiceRequest(ByVal e As Scpi.ServiceEventArgs) ' System.EventArgs)

            If e.HasError Then

                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Instrument reported an error:{0}{1}", Environment.NewLine, e.LastError)

                If Me.Visible Then

                    _gui.DisplayMessage("Instrument error available.")
                    _gui.PushMessage(MyBase.StatusMessage)
                    _gui.PushMessage("Instrument reported an error")

                End If

            Else

                Dim elapsedMilliseconds As Double = e.OperationElapsedTime.TotalMilliseconds()
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "Operation completed in {0}ms", elapsedMilliseconds)
                If Me.Visible Then
                    _gui.PushMessage(MyBase.StatusMessage)
                End If

            End If

            RaiseEvent ServiceRequest(Me, e)
            e.ServicingRequest = False

        End Sub

        ''' <summary>Handles service requests from the instrument.</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
        Private Sub OnVisaServiceRequest(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)

            If Not _lastServiceEventArgs.ServicingRequest Then
                ' do not init the last service event arguments because this is done with Clear Status.
                _lastServiceEventArgs.ServicingRequest = True
                Dim session As GpibSession = CType(sender, GpibSession)
                _lastServiceEventArgs.ProcessServiceRequest(session)
                _lastServiceEventArgs.ReadRegisters(session)
                If _lastServiceEventArgs.HasError Then
                    ' disable recurring service requests
                    'Me.ServiceRequestEventEnable = ServiceRequests.None
                Else
                    ' check if we have consecutive requests.
                    If _lastServiceEventArgs.RequestCount > 10 Then
                        ' disable recurring service requests
                        Me.ServiceRequestEventEnable = ServiceRequests.None
                        MyBase.StatusMessage = "Instrument had over 10 consecutive service requests"
                        If Me.Visible Then
                            _gui.DisplayMessage("Instrument error available.")
                            _gui.PushMessage(MyBase.StatusMessage)
                            _gui.PushMessage("Instrument experienced service request overflow")
                        End If
                    End If
                End If
                Try
                    OnServiceRequest(_lastServiceEventArgs)
                Catch ex As Exception
                    MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, _
                        "Instrument '{0}' reported an error:{1}{2}", Me.InstanceName, Environment.NewLine, ex.Message)
                    Me.OnErrorAvailable(New System.EventArgs)
                End Try
            End If

        End Sub

#End Region

    End Class

End Namespace