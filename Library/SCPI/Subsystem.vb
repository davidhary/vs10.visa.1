Namespace Scpi

    ''' <summary>Defines a SCPI Base Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public MustInherit Class Subsystem
        Implements IDisposable

#Region " Shared Methods and Properties "

        ''' <summary>Gets or sets the SCPI value for infinity</summary>
        Public Const Infinity As Double = 9.9E+37

        ''' <summary>Gets or sets the SCPI value for negative infinity</summary>
        Public Const NegativeInfinity As Double = -9.9E+37

        ''' <summary>Gets or sets the SCPI value for 'non-a-number</summary>
        Public Const NaN As Double = 9.91E+37

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Protected Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New()

            _instrument = instrument

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isdisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isdisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isdisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    _instrument = Nothing

                End If

                ' Free shared unmanaged resources

            End If

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>Returns true if the absolute difference exceeds the Epsilon.</summary>
        Public Function AreDifferent(ByVal value1 As Double, ByVal value2 As Double) As Boolean
            Return Math.Abs(value1 - value2) > _epsilon
        End Function

        Private _epsilon As Double = Single.Epsilon * 100
        ''' <summary>Gets or sets the Epsilon for comparing values.</summary>
        Public Property Epsilon() As Double
            Get
                Return _epsilon
            End Get
            Set(ByVal value As Double)
                _epsilon = value
            End Set
        End Property

        Private _accessCache As Boolean
        ''' <summary>Gets or Sets the condition as True allow access to cache only without changing the
        '''   instrument value.  This is useful for allowing instruments to set their 
        '''   know state levels.</summary>
        Public Property AccessCache() As Boolean
            Get
                Return _accessCache
            End Get
            Set(ByVal value As Boolean)
                _accessCache = value
            End Set
        End Property

        Private _forceRead As Boolean
        ''' <summary>Gets or Sets the condition as True force reading the instrument when when getting an instrument
        '''   property.</summary>
        Public Property ForceRead() As Boolean
            Get
                Return Me._forceRead
            End Get
            Set(ByVal value As Boolean)
                Me._forceRead = value
            End Set
        End Property

        Private _forceWrite As Boolean
        ''' <summary>Gets or Sets the condition as True force an update of the instrument when setting an instrument
        '''   property.</summary>
        Public Property ForceWrite() As Boolean
            Get
                Return _forceWrite
            End Get
            Set(ByVal value As Boolean)
                _forceWrite = value
            End Set
        End Property

        Private _instrument As isr.Visa.Scpi.Instrument
        ''' <summary>Gets or sets reference to the SCPI parent instrument of the subsystem.</summary>
        Protected ReadOnly Property Instrument() As isr.Visa.Scpi.Instrument
            Get
                Return _instrument
            End Get
        End Property

        Private _lineFrequency As Double
        ''' <summary>Gets or sets the line frequency for this instrument.</summary>
        Public ReadOnly Property LineFrequency() As Double
            Get
                If _lineFrequency < 1 Then
                    _lineFrequency = Me.Instrument.QueryDouble("SYST:LFR?")
                End If
                Return _lineFrequency
            End Get
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public MustOverride Sub ResetToKnownState()

#End Region

    End Class

End Namespace
