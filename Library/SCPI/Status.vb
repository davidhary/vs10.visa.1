Imports NationalInstruments
Namespace Scpi

    ''' <summary>Defines a SCPI Status Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class StatusSubsystem
        Inherits Subsystem

#Region " SHARED "

        ''' <summary>Reads the error queue from the instrument.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>This reflects the real time condition of the instrument.</remarks>
        Public Shared ReadOnly Property ErrorQueue(ByVal session As GpibSession) As String
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Dim srqEventArgs As New ServiceEventArgs
                Dim messageBuilder As New System.Text.StringBuilder
                Do
                    srqEventArgs.ProcessServiceRequest(session)
                    If srqEventArgs.HasError Then
                        If messageBuilder.Length > 0 Then
                            messageBuilder.Append(Environment.NewLine)
                        End If
                        messageBuilder.Append(session.Query(":STAT:QUE?"))
                    End If
                Loop Until Not srqEventArgs.HasError
                Return messageBuilder.ToString
            End Get
        End Property

        ''' <summary>Reads the measurement event register condition from the instrument.  This 
        '''   needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.MeasurementEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>This reflects the real time condition of the instrument.</remarks>
        Public Shared ReadOnly Property MeasurementEventCondition(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:MEAS:COND?")
            End Get
        End Property

        ''' <summary>Reads the measurement event status from the instrument.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.MeasurementEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared ReadOnly Property MeasurementEventStatus(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:MEAS?")
            End Get
        End Property

        ''' <summary>Programs or reads back the measurement event request.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.MeasurementEvents">status</see>.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <returns>The mask to use for enabling the events.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property MeasurementEventEnable(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:MEAS:ENAB?")
            End Get
            Set(ByVal value As Int32)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:MEAS:ENAB {0:D}", value))
            End Set
        End Property

        ''' <summary>Programs or reads back the operation negative transition event request. 
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K7000.OperationTransitionEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <returns>The Operation Transition Events mask to use for enabling the events.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
        Public Shared Property OperationNegativeTransitionEventEnable(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:OPER:NTR?")
            End Get
            Set(ByVal value As Int32)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:OPER:NTR {0:D}", value))
            End Set
        End Property

        ''' <summary>Reads the operation event register Condition from the instrument. 
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K7000.OperationEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>This reflects the real time condition of the instrument.</remarks>
        Public Shared ReadOnly Property OperationEventCondition(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:OPER:COND?")
            End Get
        End Property

        ''' <summary>Programs or reads back the operation event request.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K7000.OperationEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <returns>The mask to use for enabling the events.</returns>
        Public Shared Property OperationEventEnable(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:OPER:ENAB?")
            End Get
            Set(ByVal value As Int32)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:OPER:ENAB {0:D}", value))
            End Set
        End Property

        ''' <summary>Reads the operation event status from the instrument.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K7000.OperationEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared ReadOnly Property OperationEventStatus(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:OPER?")
            End Get
        End Property

        ''' <summary>Programs or reads back the operation positive transition event request.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K7000.OperationTransitionEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <returns>The mask to use for enabling the events.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>At this time this reads back the event status rather than the enable status.</remarks>
        Public Shared Property OperationPositiveTransitionEventEnable(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:OPER:PTR?")
            End Get
            Set(ByVal value As Int32)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:OPER:PTR {0:D}", value))
            End Set
        End Property

        ''' <summary>Returns the instrument registers to there preset power on state.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>When this command is sent, the SCPI event registers are affected as follows:<p>
        '''   1. All bits of the positive transition filter registers are set to one (1).</p><p>
        '''   2. All bits of the negative transition filter registers are cleared to zero (0).</p><p>
        '''   3. All bits of the following registers are cleared to zero (0):</p><p>
        '''     a. Operation Event Enable Register.</p><p>
        '''     b. Questionable Event Enable Register.</p><p>
        '''   4. All bits of the following registers are set to one (1):</p><p>
        '''     a. Trigger Event Enable Register.</p><p>
        '''     b. Arm Event Enable Register.</p><p>
        '''     c. Sequence Event Enable Register.</p><p>
        '''   Note: Registers not included in the above list are not affected by this command.</p></remarks>
        Public Shared Sub Preset(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":STAT:PRES")

        End Sub

        ''' <summary>Reads the Questionable register condition from the instrument.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.QuestionableEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>This reflects the real time condition of the instrument.</remarks>
        Public Shared ReadOnly Property QuestionableEventCondition(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:QUES:COND?")
            End Get
        End Property

        ''' <summary>Reads the Questionable event status from the instrument.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.QuestionableEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared ReadOnly Property QuestionableEventStatus(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:QUES?")
            End Get
        End Property

        ''' <summary>Programs or reads back the Questionable event request.
        '''   This needs to be cast to the specific instrument type, such as 
        '''   <see cref="isr.Visa.SCPI.K2400.QuestionableEvents">status</see></summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <returns>The questionable events mask to use for enabling the events.</returns>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property QuestionableEventEnable(ByVal session As GpibSession) As Int32
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryInt32(":STAT:QUES:ENAB?")
            End Get
            Set(ByVal value As Int32)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.Write(String.Format(Globalization.CultureInfo.CurrentCulture, ":STAT:QUES:ENAB {0:D}", value))
            End Set
        End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
        End Sub

#End Region

    End Class

End Namespace
