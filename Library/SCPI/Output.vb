Imports NationalInstruments

Namespace Scpi

    ''' <summary>Defines a SCPI Output Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class OutputSubsystem
        Inherits Subsystem

#Region " SHARED "

        ''' <summary>Sets or gets the source output state.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">Gpib session</see>.</param>
        Public Shared Property IsOn(ByVal session As GpibSession) As Boolean
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryBoolean(":OUTP:STAT?")
            End Get
            Set(ByVal value As Boolean)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.WriteOnOff(":OUTP:STAT ", value)
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the output off mode.
        ''' </summary>
        ''' <param name="session"></param>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Property OffMode(ByVal session As GpibSession) As OutputOffMode
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Dim currentMode As String = session.Query(":OUTP:SMOD?")
                If String.IsNullOrEmpty(currentMode) Then
                    Return OutputOffMode.Normal
                Else
                    currentMode = currentMode.Substring(0, Math.Min(4, currentMode.Length))
                End If
                If String.Compare("HIMP", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                    Return OutputOffMode.HighImpedance
                ElseIf String.Compare("NORM", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                    Return OutputOffMode.Normal
                ElseIf String.Compare("GUAR", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                    Return OutputOffMode.Guard
                ElseIf String.Compare("ZERO", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                    Return OutputOffMode.Zero
                Else
                    Return OutputOffMode.Normal
                End If
            End Get
            Set(ByVal value As OutputOffMode)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Dim requestedOffMode As String
                Select Case value
                    Case OutputOffMode.Guard
                        requestedOffMode = "GUAR"
                    Case OutputOffMode.HighImpedance
                        requestedOffMode = "HIMP"
                    Case OutputOffMode.Normal
                        requestedOffMode = "NORM"
                    Case OutputOffMode.Zero
                        requestedOffMode = "ZERO"
                    Case Else
                        requestedOffMode = "NORM"
                End Select
                session.Write(":OUTP:SMOD " & requestedOffMode)
            End Set
        End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        Private isOnMessage As String
        Private _isOn As Boolean
        ''' <summary>Sets or gets the source output state.</summary>
        Public Property IsOn() As Boolean
            Get
                If MyBase.ForceRead OrElse String.IsNullOrEmpty(isOnMessage) Then
                    _isOn = OutputSubsystem.IsOn(Instrument.GpibSession)
                    isOnMessage = _isOn.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _isOn
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.IsOn) Then
                    OutputSubsystem.IsOn(Instrument.GpibSession) = value
                End If
                _isOn = value
            End Set
        End Property

        'Private offModeMessage As String
        Private _offMode As OutputOffMode
        ''' <summary>
        ''' Gets or sets the output off mode.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property OffMode() As OutputOffMode
            Get
                If MyBase.ForceRead OrElse String.IsNullOrEmpty(isOnMessage) Then
                    _offMode = OutputSubsystem.OffMode(Instrument.GpibSession)
                    'offModeMessage = _offMode.ToString()
                End If
                Return _offMode
            End Get
            Set(ByVal value As OutputOffMode)
                If MyBase.ForceWrite OrElse (value <> Me._offMode) Then
                    OutputSubsystem.OffMode(Instrument.GpibSession) = value
                End If
                _offMode = value
            End Set
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
            Me._isOn = False
            Me._offMode = OutputOffMode.Normal
        End Sub

#End Region

    End Class

End Namespace
