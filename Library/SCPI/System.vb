Imports NationalInstruments
Namespace Scpi

    ''' <summary>Defines a SCPI System Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class SystemSubsystem
        Inherits Subsystem

#Region " SHARED "

        ''' <summary>Gets or sets the condition for the system auto-zero is enabled</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property AutoZero(ByVal session As GpibSession) As Boolean
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Return session.QueryBoolean(":SYST:AZER?")
            End Get
            Set(ByVal value As Boolean)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                session.WriteOnOff(":SYST:AZER", value)
            End Set
        End Property

        ''' <summary>Clears the messages from the error queue.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub Clear(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":SYST:CLE")

        End Sub

        ''' <summary>Initializes battery backed RAM. This initializes trace, source list, 
        '''   user-defined math, source-memory locations, standard save setups, and all 
        '''   call math expressions.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub InitializeMemory(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":SYST:MEM:INIT")

        End Sub

        ''' <summary>Returns the instrument to states optimized for front panel operations.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub Preset(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":SYST:PRES")

        End Sub

        ''' <summary>Returns the last error from the instrument.</summary>
        ''' <param name="session">A reference to an open Gpib
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadLastError(ByVal session As GpibSession) As String

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return session.QueryTrimEnd(":SYST:ERR?")
        End Function

        ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Function ReadScpiRevision(ByVal session As GpibSession) As Double
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return session.QueryDouble(":SYST:VERS?")
        End Function

        ''' <summary>Gets or sets the measurement <see cref="isr.Visa.SCPI.SenseMode">sense mode</see></summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Property SenseMode(ByVal session As GpibSession) As isr.Visa.Scpi.SenseMode
            Get
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                If session.QueryBoolean(":SYST:RSEN?") Then
                    Return isr.Visa.Scpi.SenseMode.FourWire
                Else
                    Return isr.Visa.Scpi.SenseMode.TwoWire
                End If
            End Get
            Set(ByVal value As isr.Visa.Scpi.SenseMode)
                If session Is Nothing Then
                    Throw New ArgumentNullException("session")
                End If
                Select Case value
                    Case isr.Visa.Scpi.SenseMode.FourWire
                        session.Write(":SYST:RSEN ON")
                    Case isr.Visa.Scpi.SenseMode.TwoWire
                        session.Write(":SYST:RSEN OFF")
                End Select
            End Set
        End Property

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        Private _autoZero As Boolean
        ''' <summary>Gets or sets the condition for auto zero is enabled.</summary>
        Public Property AutoZero() As Boolean
            Get
                If MyBase.ForceRead Then
                    _autoZero = SystemSubsystem.AutoZero(MyBase.Instrument.GpibSession)
                End If
                Return _autoZero
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoZero) Then
                    SystemSubsystem.AutoZero(MyBase.Instrument.GpibSession) = value
                End If
                _autoZero = value
            End Set
        End Property

        Private _senseMode As isr.Visa.Scpi.SenseMode = isr.Visa.Scpi.SenseMode.None
        ''' <summary>Gets or sets the measurement <see cref="isr.Visa.SCPI.SenseMode">sense mode</see></summary>
        Public Property SenseMode() As isr.Visa.Scpi.SenseMode
            Get
                If MyBase.ForceRead OrElse (_senseMode = SenseMode.None) Then
                    _senseMode = SystemSubsystem.SenseMode(MyBase.Instrument.GpibSession)
                End If
                Return _senseMode
            End Get
            Set(ByVal value As isr.Visa.Scpi.SenseMode)
                If MyBase.ForceWrite OrElse (value <> Me.SenseMode) Then
                    SystemSubsystem.SenseMode(MyBase.Instrument.GpibSession) = value
                End If
                _senseMode = value
            End Set
        End Property

        Private scpiRevisionMessage As String
        Private _scpiRevision As Double
        ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
        Public ReadOnly Property ScpiRevision() As Double
            Get
                If scpiRevisionMessage Is Nothing OrElse _scpiRevision < 1 Then
                    _scpiRevision = Scpi.SystemSubsystem.ReadScpiRevision(MyBase.Instrument.GpibSession)
                    scpiRevisionMessage = _scpiRevision.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _scpiRevision
            End Get
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
            _senseMode = isr.Visa.Scpi.SenseMode.None
            Me._autoZero = True
            Me._scpiRevision = 0
        End Sub

#End Region

    End Class

End Namespace
