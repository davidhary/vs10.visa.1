Namespace Scpi

    ''' <summary>Provides event arguments for instrument events.</summary>
    ''' <remarks>The class inherits from <see cref="System.EventArgs">system event arguments</see>.
    ''' It servers to pass event arguments trough the inheritance tree of SCPI instruments 
    ''' when processing VISA service requests.
    ''' </remarks>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    ''' <history date="08/17/05" by="David Hary" revision="1.0.2420.x">
    ''' Add error queue.  All getting error information from the parent instrument.
    ''' </history>
    Public Class ServiceEventArgs
        Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

            ' Date.Now set the member properties.
            _operationTime = Date.Now

        End Sub

        ''' <summary>Constructs this class with a status message.</summary>
        ''' <param name="eventMessage">Specifies the event message.</param>
        ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
        Public Sub New(ByVal eventMessage As String)

            ' instantiate the base class
            Me.New()

            ' Date.Now set the member properties.
            _eventMessage = eventMessage

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Reads the standard registers.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see> requesting the service.</param>
        Public Sub ReadRegisters(ByVal session As GpibSession)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.MeasurementEvent) <> 0 Then
                _measurementEventStatus = StatusSubsystem.MeasurementEventStatus(session)
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.MessageAvailable) <> 0 Then
                _receivedMessage = session.ReadStringTrimEnd()
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.OperationEvent) <> 0 Then
                _operationEventStatus = StatusSubsystem.OperationEventStatus(session)
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.QuestionableEvent) <> 0 Then
                _questionableEventStatus = StatusSubsystem.QuestionableEventStatus(session)
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.RequestingService) <> 0 Then
            End If
            If (_serviceRequestStatus _
              And isr.Visa.Scpi.ServiceRequests.StandardEvent) <> 0 Then
                _standardEventStatus = session.StandardEventStatus()
            End If
        End Sub

        ''' <summary>Reads and processes the service request regsiter byte.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="isr.Visa.GpibSession">session</see> requesting the service.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Sub ProcessServiceRequest(ByVal session As GpibSession)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            _serviceRequestStatus = MessageBasedSession.ReadStatusByte(session)
            _eventTime = Date.Now
            _requestCount += 1
            If Me.HasError Then
                If _isDelegateErrorHandling Then
                    Me.OnErrorInfoRequested(Me)
                Else
                    _lastError = SystemSubsystem.ReadLastError(session)
                    _lastErrorQueue = StatusSubsystem.ErrorQueue(session)
                End If
            Else
                ' if not an error, clear the error queue
                _lastError = String.Empty
                Me.HasError = False
            End If
        End Sub

        ''' <summary>Returns a detailed report for the given standard status register (ESR) byte.
        ''' </summary>
        ''' <param name="registerValue">Specifies the value that was read from the status register.</param>
        Public Shared Function BuildStandardStatusRegisterReport(ByVal registerValue As isr.Visa.Scpi.StandardEvents) As String

            Dim delimiter As String
            delimiter = "; "
            Dim messageBuilder As New System.Text.StringBuilder

            ' operation complete
            If (registerValue And StandardEvents.OperationComplete) = StandardEvents.OperationComplete Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Operation Complete")
            End If

            ' Command error
            If (registerValue And StandardEvents.CommandError) = StandardEvents.CommandError Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Command error")
            End If

            ' device-dependnet error
            If (registerValue And StandardEvents.DeviceDependentError) = StandardEvents.DeviceDependentError Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Device-dependnet error")
            End If

            ' Execution error
            If (registerValue And StandardEvents.ExecutionError) = StandardEvents.ExecutionError Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Execution error")
            End If

            ' power on
            If (registerValue And StandardEvents.PowerOn) = StandardEvents.PowerOn Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Power toggled")
            End If

            ' query error
            If (registerValue And StandardEvents.QueryError) = StandardEvents.QueryError Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("Query error")
            End If

            ' User request
            If (registerValue And StandardEvents.UserRequest) = StandardEvents.UserRequest Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(delimiter)
                End If
                messageBuilder.Append("User request")
            End If

            If messageBuilder.Length > 0 Then
                messageBuilder.Insert(0, "The device standard status register reported: ")
                messageBuilder.Append(".")
            End If

            BuildStandardStatusRegisterReport = messageBuilder.ToString

        End Function

#End Region

#Region " PROPERTIES "

        Private _errorAvailableBIts As isr.Visa.Scpi.ServiceRequests = ServiceRequests.ErrorAvailable
        ''' <summary>
        ''' Holds the status register bits that flag an error.  Default to 
        ''' <see cref="ServiceRequests.ErrorAvailable">Error Available</see>.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ErrorAvailableBits() As isr.Visa.Scpi.ServiceRequests
            Get
                Return _errorAvailableBIts
            End Get
            Set(ByVal value As isr.Visa.Scpi.ServiceRequests)
                _errorAvailableBIts = value
            End Set
        End Property

        Private _eventMessage As String = ""
        ''' <summary>Returns the event message.</summary>
        Public ReadOnly Property EventMessage() As String
            Get
                Return _eventMessage
            End Get
        End Property

        Private _eventTime As DateTime = Date.Now
        ''' <summary>Gets or sets the event time.</summary>
        Public ReadOnly Property EventTime() As DateTime
            Get
                Return _eventTime
            End Get
        End Property

        ''' <summary>Gets or sets the condition for the service request status error flag is on.</summary>
        Public Property HasError() As Boolean
            Get
                Return (_serviceRequestStatus And _errorAvailableBIts) <> 0
            End Get
            Set(ByVal value As Boolean)
                If value Then
                    _serviceRequestStatus = _serviceRequestStatus Or _errorAvailableBIts
                Else
                    _serviceRequestStatus = _serviceRequestStatus And (Not _errorAvailableBIts)
                End If
            End Set
        End Property

        Private _isDelegateErrorHandling As Boolean
        ''' <summary>
        ''' Holds true if error handling is to be delegated to the parent instrument.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property IsDelegateErrorHandling() As Boolean
            Get
                Return _isDelegateErrorHandling
            End Get
            Set(ByVal value As Boolean)
                _isDelegateErrorHandling = value
            End Set
        End Property

        Private _lastError As String
        ''' <summary>Returns the last error read from the instrument.</summary>
        Public ReadOnly Property LastError() As String
            Get
                Return _lastError
            End Get
        End Property

        Private _lastErrorQueue As String
        ''' <summary>Returns the last error queue read from the instrument.</summary>
        Public ReadOnly Property LastErrorQueue() As String
            Get
                Return _lastErrorQueue
            End Get
        End Property

        ''' <summary>Returns the error queue and ESR report.
        ''' </summary>
        Public ReadOnly Property LastErrorReport() As String
            Get

                Dim messageBuilder As New System.Text.StringBuilder

                If _lastErrorQueue.Length > 0 Then
                    If messageBuilder.Length > 0 Then
                        messageBuilder.Append(". ")
                    End If
                    messageBuilder.Append("The device reported the following error(s) from the error queue: ")
                    messageBuilder.Append(Environment.NewLine)
                    messageBuilder.Append(_lastErrorQueue)
                    messageBuilder.Append(".")
                End If

                Dim report As String = Me.LastStandardStatusRegisterReport()
                If report.Length > 0 Then
                    If messageBuilder.Length > 0 Then
                        messageBuilder.Append(Environment.NewLine)
                    End If
                    messageBuilder.Append("The device satandard status register reported the following error(s): ")
                    messageBuilder.Append(Environment.NewLine)
                    messageBuilder.Append(report)
                    messageBuilder.Append(".")
                End If

                Return messageBuilder.ToString

            End Get
        End Property

        Public ReadOnly Property LastStandardStatusRegisterReport() As String
            Get
                Return ServiceEventArgs.BuildStandardStatusRegisterReport(_standardEventStatus)
            End Get
        End Property

        Private _servicingRequest As Boolean
        Public Property ServicingRequest() As Boolean
            Get
                Return _servicingRequest
            End Get
            Set(ByVal value As Boolean)
                _servicingRequest = value
            End Set
        End Property

        Private _measurementEventStatus As Int32
        ''' <summary>Returns the measurement Event register status.  To decipher, case
        '''   to the <see cref="isr.Visa.SCPI.K2400.MeasurementEvents">event flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property MeasurementEventStatus() As Int32
            Get
                Return _measurementEventStatus
            End Get
        End Property

        Private _operationCompleted As Boolean
        Public Property OperationCompleted() As Boolean
            Get
                Return _operationCompleted
            End Get
            Set(ByVal value As Boolean)
                _operationCompleted = value
            End Set
        End Property

        Private _operationCondition As Int32
        ''' <summary>Gets or sets the operation condition status. To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.OperationEvents">Operation Events flags</see>
        '''   of the specific instrument type.</summary>
        Public Property OperationCondition() As Int32
            Get
                Return _operationCondition
            End Get
            Set(ByVal value As Int32)
                _operationCondition = value
            End Set
        End Property

        ''' <summary>Returns the operation elapsed time.</summary>
        Public ReadOnly Property OperationElapsedTime() As TimeSpan
            Get
                Return _eventTime.Subtract(_operationTime)
            End Get
        End Property

        Private _operationTime As DateTime = Date.Now
        ''' <summary>Returns the operation time.  This is typically set to when the 
        '''   operation started so the operation can be timed to its event time.</summary>
        Public ReadOnly Property OperationTime() As DateTime
            Get
                Return _operationTime
            End Get
        End Property

        Private _operationEventStatus As Int32
        ''' <summary>Returns the operation event register status.  To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.OperationEvents">Operation Events flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property OperationEventStatus() As Int32
            Get
                Return _operationEventStatus
            End Get
        End Property

        Private _questionableEventStatus As Int32
        ''' <summary>Returns the Questionable Event register status. To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.QuestionableEvents">Questionable Events flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property QuestionableEventStatus() As Int32
            Get
                Return _questionableEventStatus
            End Get
        End Property

        Private _receivedMessage As String
        ''' <summary>Returns the message that was received from the instrument.</summary>
        Public ReadOnly Property ReceivedMessage() As String
            Get
                Return _receivedMessage
            End Get
        End Property

        Private _requestCount As Int32
        ''' <summary>Returns the number of service request that were recorded since the
        '''   last Clear Status command.</summary>
        Public ReadOnly Property RequestCount() As Int32
            Get
                Return _requestCount
            End Get
        End Property

        Private _serviceRequestStatus As isr.Visa.Scpi.ServiceRequests
        ''' <summary>Returns the service request 
        '''   <see cref="isr.Visa.SCPI.ServiceRequests">status byte</see></summary>
        Public ReadOnly Property ServiceRequestStatus() As isr.Visa.Scpi.ServiceRequests
            Get
                Return _serviceRequestStatus
            End Get
        End Property

        Private _standardEventStatus As isr.Visa.Scpi.StandardEvents
        ''' <summary>Returns the standard event register
        '''   <see cref="isr.Visa.SCPI.StandardEvents">status</see></summary>
        Public ReadOnly Property StandardEventStatus() As isr.Visa.Scpi.StandardEvents
            Get
                Return _standardEventStatus
            End Get
        End Property

#End Region

#Region " EVENT and HANDLERS "

        ''' <summary>Raised upon receiving a request to have the instrument retrieve error information.
        ''' This is done in case the instrument does not comply with standard SCPI methods for retrieving
        ''' the error queue of standard event register errors.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Public Event ErrorInfoRequested As EventHandler(Of Scpi.ServiceEventArgs) ' System.EventArgs)

        ''' <summary>Raises the request for error information.</summary>
        ''' <param name="e">Passes reference to the <see cref="Scpi.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overridable Sub OnErrorInfoRequested(ByVal e As Scpi.ServiceEventArgs) ' System.EventArgs)

            RaiseEvent ErrorInfoRequested(Me, e)

        End Sub

#End Region

    End Class

End Namespace
