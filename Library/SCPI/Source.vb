Namespace Scpi

    ''' <summary>Defines a SCPI Source Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class SourceSubsystem
        Inherits Subsystem

#Region " SHARED "

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

        Private autoClearMessage As String
        Private _autoClear As Boolean
        ''' <summary>Sets the source to auto clear mode.</summary>
        Public Property AutoClear() As Boolean
            Get
                If MyBase.ForceRead OrElse autoClearMessage Is Nothing Then
                    _autoClear = MyBase.Instrument.QueryBoolean(":SOUR:CLE:AUTO?")
                    autoClearMessage = _autoClear.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoClear
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoClear) Then
                    MyBase.Instrument.WriteOnOff(":SOUR:CLE:AUTO ", value)
                End If
                _autoClear = value
            End Set
        End Property

        Private currentLevelMessage As String
        Private _currentLevel As Double
        ''' <summary>Gets or sets the source current level.</summary>
        Public Property CurrentLevel() As Double
            Get
                If MyBase.ForceRead OrElse currentLevelMessage Is Nothing Then
                    _currentLevel = MyBase.Instrument.QueryDouble(":SOUR:CURR?")
                    currentLevelMessage = _currentLevel.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentLevel
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.CurrentLevel) Then
                    MyBase.Instrument.Write(":SOUR:CURR " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _currentLevel = value
            End Set
        End Property

        Private currentRangeMessage As String
        Private _currentRange As Double
        ''' <summary>Gets or sets the source current Range.  Set to Zero for setting an auto 
        '''   range.</summary>
        Public Property CurrentRange() As Double
            Get
                If MyBase.ForceRead OrElse currentRangeMessage Is Nothing Then
                    _currentRange = MyBase.Instrument.QueryDouble(":SOUR:CURR:RANG?")
                    currentRangeMessage = _currentRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentRange
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.CurrentRange) Then
                    MyBase.Instrument.Write(":SOUR:CURR:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _currentRange = value
            End Set
        End Property

        Private delayMessage As String
        Private _delay As Double
        ''' <summary>Gets or sets the source delay.</summary>
        Public Property Delay() As Double
            Get
                If MyBase.ForceRead OrElse delayMessage Is Nothing Then
                    _delay = MyBase.Instrument.QueryDouble(":SOUR:DEL?")
                    delayMessage = _delay.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _delay
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.Delay) Then
                    MyBase.Instrument.Write(":SOUR:DEL " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    Me.AutoDelay = False
                End If
                _delay = value
            End Set
        End Property

        Private _functionMode As isr.Visa.Scpi.SourceFunctionMode = isr.Visa.Scpi.SourceFunctionMode.None
        ''' <summary>Gets or sets the <see cref="isr.Visa.SCPI.SourceFunctionMode">source function mode</see>.</summary>
        ''' <remarks>This method must be set to a non-default value.</remarks>
        Public Property FunctionMode() As isr.Visa.Scpi.SourceFunctionMode
            Get
                If MyBase.ForceRead OrElse Me._functionMode = isr.Visa.Scpi.SourceFunctionMode.None Then
                    Dim mode As String = MyBase.Instrument.QueryTrimEnd(":SOUR:FUNC?").Replace(":"c, "_"c)
                    If mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.CURR_AC.ToString) Then
                        _functionMode = SourceFunctionMode.CURR_AC
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.CURR_DC.ToString) Then
                        _functionMode = SourceFunctionMode.CURR_DC
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.CURR.ToString) Then
                        _functionMode = SourceFunctionMode.CURR
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.Mem.ToString) Then
                        _functionMode = SourceFunctionMode.Mem
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.VOLT_AC.ToString) Then
                        _functionMode = SourceFunctionMode.VOLT_AC
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.VOLT_DC.ToString) Then
                        _functionMode = SourceFunctionMode.VOLT_DC
                    ElseIf mode.StartsWith(isr.Visa.Scpi.SourceFunctionMode.VOLT.ToString) Then
                        _functionMode = SourceFunctionMode.VOLT
                    Else
                        _functionMode = SourceFunctionMode.None
                        Dim message As String = "Unhandled function mode"
                        Debug.Assert(False, message)
                    End If
                    '_functionMode = CType(MyBase.QueryTrimEnd(":SOUR:FUNC?").Replace(":"c, "_"c), isr.Visa.SCPI.SourceFunctionMode)
                End If
                Return _functionMode
            End Get
            Set(ByVal value As isr.Visa.Scpi.SourceFunctionMode)
                If MyBase.ForceWrite OrElse (value <> Me.FunctionMode) Then
                    MyBase.Instrument.Write(":SOUR:FUNC " & value.ToString().Replace("_"c, ":"c))
                End If
                _functionMode = value
            End Set
        End Property

        Private autoCurrentRangeMessage As String
        Private _autoCurrentRange As Boolean
        ''' <summary>Gets or sets the condition for auto current range.</summary>
        Public Property AutoCurrentRange() As Boolean
            Get
                If MyBase.ForceRead OrElse autoCurrentRangeMessage Is Nothing Then
                    _autoCurrentRange = MyBase.Instrument.QueryBoolean(":SOUR:CURR:RANG:AUTO?")
                    autoCurrentRangeMessage = _autoCurrentRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoCurrentRange
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoCurrentRange) Then
                    MyBase.Instrument.WriteOnOff(":SOUR:CURR:RANG:AUTO ", value)
                End If
                _autoCurrentRange = value
            End Set
        End Property

        Private autoDelayMessage As String
        Private _autoDelay As Boolean
        ''' <summary>Gets or sets the condition for auto delay.</summary>
        Public Property AutoDelay() As Boolean
            Get
                If MyBase.ForceRead OrElse autoDelayMessage Is Nothing Then
                    _autoDelay = MyBase.Instrument.QueryBoolean(":SOUR:DEL:AUTO?")
                    autoDelayMessage = _autoDelay.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoDelay
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoCurrentRange) Then
                    MyBase.Instrument.WriteOnOff(":SOUR:DEL:AUTO ", value)
                End If
                _autoDelay = value
            End Set
        End Property

        Private autoVoltageRangeMessage As String
        Private _autoVoltageRange As Boolean
        ''' <summary>Gets or sets the condition for auto voltage range.</summary>
        Public Property AutoVoltageRange() As Boolean
            Get
                If MyBase.ForceRead OrElse autoVoltageRangeMessage Is Nothing Then
                    _autoVoltageRange = MyBase.Instrument.QueryBoolean(":SOUR:VOLT:RANG:AUTO?")
                    autoVoltageRangeMessage = _autoVoltageRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoVoltageRange
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoVoltageRange) Then
                    MyBase.Instrument.WriteOnOff(":SOUR:VOLT:RANG:AUTO ", value)
                End If
                _autoVoltageRange = value
            End Set
        End Property

        Private sweepPointsMessage As String
        Private _sweepPoints As Integer
        ''' <summary>Gets or sets the source sweep points.</summary>
        Public Property SweepPoints() As Integer
            Get
                If MyBase.ForceRead OrElse sweepPointsMessage Is Nothing Then
                    _sweepPoints = MyBase.Instrument.QueryInt32(":SOUR:SWE:POIN?")
                    sweepPointsMessage = _sweepPoints.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _sweepPoints
            End Get
            Set(ByVal value As Integer)
                If MyBase.ForceWrite OrElse (value <> Me.SweepPoints) Then
                    MyBase.Instrument.Write(":SOUR:SWE:POIN " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _sweepPoints = value
            End Set
        End Property

        Private voltageLevelMessage As String
        Private _voltageLevel As Double
        ''' <summary>Gets or sets the source voltage level.</summary>
        Public Property VoltageLevel() As Double
            Get
                If MyBase.ForceRead OrElse voltageLevelMessage Is Nothing Then
                    _voltageLevel = MyBase.Instrument.QueryDouble(":SOUR:VOLT?")
                    voltageLevelMessage = _voltageLevel.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageLevel
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageLevel) Then
                    MyBase.Instrument.Write(":SOUR:VOLT " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageLevel = value
            End Set
        End Property

        Private voltageLimitMessage As String
        Private _voltageLimit As Double
        ''' <summary>Gets or sets the maximum source voltage.  Set value to maximum (9.9E+37) to
        '''   set voltage limit to NONE.</summary>
        Public Property VoltageLimit() As Double
            Get
                If MyBase.ForceRead OrElse voltageLimitMessage Is Nothing Then
                    _voltageLimit = MyBase.Instrument.QueryDouble(":SOUR:PROT?")
                    voltageLimitMessage = _voltageLimit.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageLimit
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageLimit) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SOUR:PROT NONE")
                    Else
                        MyBase.Instrument.Write(":SOUR:PROT " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _voltageLimit = value
            End Set
        End Property

        Private voltageModeMessage As String
        Private _voltageMode As SourceMode
        ''' <summary>
        ''' Gets or sets the output off mode.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property VoltageMode() As SourceMode
            Get
                If MyBase.ForceRead OrElse String.IsNullOrEmpty(voltageModeMessage) Then
                    Dim currentMode As String = MyBase.Instrument.QueryTrimEnd(":SOUR:VOLT:MODE?")
                    If String.IsNullOrEmpty(currentMode) Then
                        Return SourceMode.Fixed
                    Else
                        currentMode = currentMode.Substring(0, Math.Min(3, currentMode.Length))
                    End If
                    If String.Compare("FIX", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        Return SourceMode.Fixed
                    ElseIf String.Compare("LIS", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        Return SourceMode.List
                    ElseIf String.Compare("SWE", currentMode, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        Return SourceMode.Sweep
                    Else
                        Return SourceMode.Fixed
                    End If
                End If
                Return _voltageMode
            End Get
            Set(ByVal value As SourceMode)
                Dim requestedMode As String
                Select Case value
                    Case SourceMode.Fixed
                        requestedMode = "FIX"
                    Case SourceMode.List
                        requestedMode = "LIST"
                    Case SourceMode.Sweep
                        requestedMode = "SWE"
                    Case Else
                        requestedMode = "FIX"
                End Select
                MyBase.Instrument.Write(":SOUR:VOLT:MODE " & requestedMode)
            End Set
        End Property

        Private voltageRangeMessage As String
        Private _voltageRange As Double
        ''' <summary>Gets or sets the source voltage Range.</summary>
        Public Property VoltageRange() As Double
            Get
                If MyBase.ForceRead OrElse voltageRangeMessage Is Nothing Then
                    _voltageRange = MyBase.Instrument.QueryDouble(":SOUR:VOLT:RANG?")
                    voltageRangeMessage = _voltageRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageRange
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageRange) Then
                    MyBase.Instrument.Write(":SOUR:VOLT:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageRange = value
            End Set
        End Property

        Private voltageStartMessage As String
        Private _voltageStart As Double
        ''' <summary>Gets or sets the source voltage sweep start level.</summary>
        Public Property VoltageStart() As Double
            Get
                If MyBase.ForceRead OrElse voltageStartMessage Is Nothing Then
                    _voltageStart = MyBase.Instrument.QueryDouble(":SOUR:VOLT:STAR?")
                    voltageStartMessage = _voltageStart.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageStart
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageStart) Then
                    MyBase.Instrument.Write(":SOUR:VOLT:STAR " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageStart = value
            End Set
        End Property

        Private voltageStopMessage As String
        Private _voltageStop As Double
        ''' <summary>Gets or sets the source voltage sweep start level.</summary>
        Public Property VoltageStop() As Double
            Get
                If MyBase.ForceRead OrElse voltageStopMessage Is Nothing Then
                    _voltageStop = MyBase.Instrument.QueryDouble(":SOUR:VOLT:STOP?")
                    voltageStopMessage = _voltageStop.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageStop
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageStop) Then
                    MyBase.Instrument.Write(":SOUR:VOLT:STOP " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageStop = value
            End Set
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
            ' upgrade: Add properties to allow setting the cached values voltageRangeCache etc. 
            _autoClear = False
            _autoCurrentRange = True
            _autoVoltageRange = True
            _autoDelay = True
            _currentLevel = 0
            _currentRange = 0.000105
            _delay = 0
            _functionMode = SourceFunctionMode.VOLT
            Me._sweepPoints = 2500
            _voltageLevel = 0
            _voltageLimit = Scpi.Subsystem.Infinity
            Me._voltageMode = SourceMode.Fixed
            _voltageRange = 21
            Me._voltageStart = 0
            Me._voltageStop = 0
        End Sub

#End Region

    End Class

End Namespace

