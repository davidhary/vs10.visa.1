Imports NationalInstruments
Namespace Scpi

    ''' <summary>Defines a SCPI Status Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class TriggerSubsystem
        Inherits Subsystem

#Region " SHARED "

        ''' <summary>Initiates operation.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub Initiate(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":INIT")

        End Sub

        ''' <summary>Initiates operation.</summary>
        ''' <param name="session">A reference to an open
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Shared Sub Abort(ByVal session As VisaNS.MessageBasedSession)

            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            session.Write(":ABOR")

        End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Initiates operation.</summary>
        ''' <returns>True if success or false if error.</returns>
        Public Function Abort() As Boolean

            TriggerSubsystem.Abort(MyBase.Instrument.GpibSession)
            Return Not MyBase.Instrument.HadError

        End Function

        ''' <summary>Initiates operation.</summary>
        ''' <returns>True if success or false if error.</returns>
        Public Function Initiate() As Boolean

            TriggerSubsystem.Initiate(MyBase.Instrument.GpibSession)
            Return Not MyBase.Instrument.HadError

        End Function

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
            Me._count = 1
            Me._delay = 0
        End Sub

#End Region

#Region " PROPERTIES "

        Private delayMessage As String
        Private _delay As Double
        ''' <summary>Gets or sets the trigger delay.</summary>
        Public Property Delay() As Double
            Get
                If MyBase.ForceRead OrElse delayMessage Is Nothing Then
                    _delay = MyBase.Instrument.QueryDouble(":TRIG:DEL?")
                    delayMessage = _delay.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _delay
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.Delay) Then
                    MyBase.Instrument.Write(":TRIG:DEL " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _delay = value
            End Set
        End Property

        Private countMessage As String
        Private _count As Integer
        ''' <summary>Gets or sets the trigger count.</summary>
        Public Property Count() As Integer
            Get
                If MyBase.ForceRead OrElse countMessage Is Nothing Then
                    _count = MyBase.Instrument.QueryInt32(":TRIG:COUN?")
                    countMessage = _count.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _count
            End Get
            Set(ByVal value As Integer)
                If MyBase.ForceWrite OrElse (value <> Me.Count) Then
                    MyBase.Instrument.Write(":TRIG:COUN " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _count = value
            End Set
        End Property

#End Region

    End Class

End Namespace
