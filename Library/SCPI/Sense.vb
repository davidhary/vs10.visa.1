Namespace Scpi

    ''' <summary>Defines a SCPI Sense Subsystem.</summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class SenseSubsystem
        Inherits Subsystem

#Region " SHARED "

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instrument ">Reference to an open <see cref="isr.Visa.SCPI.Instrument">SCPI instrument</see>.</param>
        Public Sub New(ByVal instrument As isr.Visa.Scpi.Instrument)

            ' instantiate the base class
            MyBase.New(instrument)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then


                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS"

        ''' <summary>Returns the latest post-processed reading stored in the sample buffer.</summary>
        Public Function FetchData() As String

            _lastFetch = MyBase.Instrument.QueryTrimEnd(":FETC?")
            Return _lastFetch

        End Function

        ''' <summary>Take a measurement using the Read query.</summary>
        Public Function Read() As String
            _lastFetch = MyBase.Instrument.QueryTrimEnd(":READ?")
            Return _lastFetch
        End Function

#End Region

#Region " PROPERTIES "

        Private currentIntegrationPeriodMessage As String
        Private _currentIntegrationPeriod As Double
        ''' <summary>Gets or sets the sense current integration period in seconds.</summary>
        Public Property CurrentIntegrationPeriod() As Double
            Get
                If MyBase.ForceRead OrElse currentIntegrationPeriodMessage Is Nothing Then
                    _currentIntegrationPeriod = MyBase.Instrument.QueryDouble(":SENS:CURR:NPLC?") / MyBase.LineFrequency
                    currentIntegrationPeriodMessage = _currentIntegrationPeriod.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentIntegrationPeriod
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.CurrentIntegrationPeriod) Then
                    MyBase.Instrument.Write(":SENS:CURR:NPLC " & (value * MyBase.LineFrequency).ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _currentIntegrationPeriod = value
            End Set
        End Property

        Private currentLimitMessage As String
        Private _currentLimit As Double
        ''' <summary>Gets or sets the source current Limit for a voltage source. Set to
        ''' <see cref="SCPI.Subsystem.Infinity">infinity</see> to set to maximum or to 
        ''' <see cref="SCPI.Subsystem.Infinity">negative infinity</see> for minimum.</summary>
        Public Property CurrentLimit() As Double
            Get
                If MyBase.ForceRead OrElse currentLimitMessage Is Nothing Then
                    _currentLimit = MyBase.Instrument.QueryDouble(":SENS:CURR:PROT?")
                    currentLimitMessage = _currentLimit.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentLimit
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.CurrentLimit) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SENS:CURR:PROT MAX")
                    ElseIf value <= (Scpi.Subsystem.NegativeInfinity + 1) Then
                        MyBase.Instrument.Write(":SENS:CURR:PROT MAX")
                    Else
                        MyBase.Instrument.Write(":SENS:CURR:PROT " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _currentLimit = value
            End Set
        End Property

        Private currentRangeMessage As String
        Private _currentRange As Double
        ''' <summary>Gets or sets the sense current Range.  Set to
        ''' <see cref="SCPI.Subsystem.Infinity">infinity</see> to set to maximum or to 
        ''' <see cref="SCPI.Subsystem.Infinity">negative infinity</see> for minimum.</summary>
        Public Property CurrentRange() As Double
            Get
                If MyBase.ForceRead OrElse currentRangeMessage Is Nothing Then
                    _currentRange = MyBase.Instrument.QueryDouble(":SENS:CURR:RANG?")
                    currentRangeMessage = _currentRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentRange
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.CurrentRange) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SENS:CURR:RANG MAX")
                    ElseIf value <= (Scpi.Subsystem.NegativeInfinity + 1) Then
                        MyBase.Instrument.Write(":SENS:CURR:RANG MIN")
                    Else
                        MyBase.Instrument.Write(":SENS:CURR:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _currentRange = value
            End Set
        End Property

        Private _functionMode As isr.Visa.Scpi.SenseFunctionModes = isr.Visa.Scpi.SenseFunctionModes.None
        ''' <summary>Gets or sets the <see cref="isr.Visa.SCPI.SenseFunctionModes">Sense function mode</see>.</summary>
        ''' <remarks>This method must be set to a non-default value.</remarks>
        Public Property FunctionMode() As isr.Visa.Scpi.SenseFunctionModes
            Get
                If MyBase.ForceRead OrElse (_functionMode = isr.Visa.Scpi.SenseFunctionModes.None) Then
                    Dim mode As String = MyBase.Instrument.QueryTrimEnd(":SENS:FUNC?").Replace(":"c, "_"c)
                    If mode.IndexOf(isr.Visa.Scpi.SenseFunctionModes.CURR_AC.ToString) >= 0 Then
                        _functionMode = _functionMode Or isr.Visa.Scpi.SenseFunctionModes.CURR_AC
                    End If
                    If mode.IndexOf(isr.Visa.Scpi.SenseFunctionModes.CURR_DC.ToString) >= 0 Then
                        _functionMode = _functionMode Or isr.Visa.Scpi.SenseFunctionModes.CURR_DC
                    End If
                    If mode.IndexOf(isr.Visa.Scpi.SenseFunctionModes.VOLT_AC.ToString) >= 0 Then
                        _functionMode = _functionMode Or isr.Visa.Scpi.SenseFunctionModes.VOLT_AC
                    End If
                    If mode.IndexOf(isr.Visa.Scpi.SenseFunctionModes.VOLT_DC.ToString) >= 0 Then
                        _functionMode = _functionMode Or isr.Visa.Scpi.SenseFunctionModes.VOLT_DC
                    End If
                    If mode.IndexOf(isr.Visa.Scpi.SenseFunctionModes.RES.ToString) >= 0 Then
                        _functionMode = _functionMode Or isr.Visa.Scpi.SenseFunctionModes.RES
                    End If
                End If
                Return _functionMode
            End Get
            Set(ByVal value As isr.Visa.Scpi.SenseFunctionModes)
                If MyBase.ForceWrite OrElse (value <> Me.FunctionMode) Then
                    Dim builder As New System.Text.StringBuilder
                    builder.Append(":SENS:FUNC ")
                    If (value And SenseFunctionModes.CURR_AC) = SenseFunctionModes.CURR_AC Then
                        builder.Append("'")
                        builder.Append(SenseFunctionModes.CURR_AC.ToString.Replace("_"c, ":"c))
                        builder.Append("',")
                    End If
                    If (value And SenseFunctionModes.CURR_DC) = SenseFunctionModes.CURR_DC Then
                        builder.Append("'")
                        builder.Append(SenseFunctionModes.CURR_DC.ToString.Replace("_"c, ":"c))
                        builder.Append("',")
                    End If
                    If (value And SenseFunctionModes.RES) = SenseFunctionModes.RES Then
                        builder.Append("'")
                        builder.Append(SenseFunctionModes.RES.ToString.Replace("_"c, ":"c))
                        builder.Append("',")
                    End If
                    If (value And SenseFunctionModes.VOLT_AC) = SenseFunctionModes.VOLT_AC Then
                        builder.Append("'")
                        builder.Append(SenseFunctionModes.VOLT_AC.ToString.Replace("_"c, ":"c))
                        builder.Append("',")
                    End If
                    If (value And SenseFunctionModes.VOLT_DC) = SenseFunctionModes.VOLT_DC Then
                        builder.Append("'")
                        builder.Append(SenseFunctionModes.VOLT_DC.ToString.Replace("_"c, ":"c))
                        builder.Append("',")
                    End If
                    MyBase.Instrument.Write(builder.ToString.TrimEnd(","c))
                End If
                _functionMode = value
            End Set
        End Property

        Private autoCurrentRangeMessage As String
        Private _autoCurrentRange As Boolean
        ''' <summary>Gets or sets the condition for auto current range.</summary>
        Public Property AutoCurrentRange() As Boolean
            Get
                If MyBase.ForceRead OrElse autoCurrentRangeMessage Is Nothing Then
                    _autoCurrentRange = MyBase.Instrument.QueryBoolean(":SENS:CURR:RANG:AUTO?")
                    autoCurrentRangeMessage = _autoCurrentRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoCurrentRange
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoCurrentRange) Then
                    MyBase.Instrument.WriteOnOff(":SENS:CURR:RANG:AUTO ", value)
                End If
                _autoCurrentRange = value
            End Set
        End Property

        Private autoResistanceRangeMesssage As String
        Private _autoResistanceRange As Boolean
        ''' <summary>Gets or sets the condition for auto Resistance range.</summary>
        Public Property AutoResistanceRange() As Boolean
            Get
                If MyBase.ForceRead OrElse autoResistanceRangeMesssage Is Nothing Then
                    _autoResistanceRange = MyBase.Instrument.QueryBoolean(":SENS:RES:RANG:AUTO?")
                    autoResistanceRangeMesssage = _autoResistanceRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoResistanceRange
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoResistanceRange) Then
                    MyBase.Instrument.WriteOnOff(":SENS:RES:RANG:AUTO ", value)
                End If
                _autoResistanceRange = value
            End Set
        End Property

        Private autoVoltageRangeMessage As String
        Private _autoVoltageRange As Boolean
        ''' <summary>Gets or sets the condition for auto voltage range.</summary>
        Public Property AutoVoltageRange() As Boolean
            Get
                If MyBase.ForceRead OrElse autoVoltageRangeMessage Is Nothing Then
                    _autoVoltageRange = MyBase.Instrument.QueryBoolean(":SENS:VOLT:RANG:AUTO?")
                    autoVoltageRangeMessage = _autoVoltageRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _autoVoltageRange
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.AutoVoltageRange) Then
                    MyBase.Instrument.WriteOnOff(":SENS:VOLT:RANG:AUTO ", value)
                End If
                _autoVoltageRange = value
            End Set
        End Property

        Private functionConcurrentMessage As String
        Private _functionConcurrent As Boolean
        ''' <summary>Gets or sets the condition for sensing all functions concurrently.</summary>
        Public Property IsFunctionConcurrent() As Boolean
            Get
                If MyBase.ForceRead OrElse functionConcurrentMessage Is Nothing Then
                    _functionConcurrent = MyBase.Instrument.QueryBoolean(":SENS:FUNC:CONC?")
                    functionConcurrentMessage = _functionConcurrent.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _functionConcurrent
            End Get
            Set(ByVal value As Boolean)
                If MyBase.ForceWrite OrElse (value <> Me.IsFunctionConcurrent) Then
                    MyBase.Instrument.WriteOnOff(":SENS:FUNC:CONC ", value)
                End If
                _functionConcurrent = value
            End Set
        End Property

        Private _lastFetch As String
        ''' <summary>Gets or sets the last fetch data string.</summary>
        Public ReadOnly Property LastFetch() As String
            Get
                Return _lastFetch
            End Get
        End Property

        ''' <summary>Returns the most recent reading.</summary>
        Public ReadOnly Property LatestData() As String
            Get
                _lastFetch = MyBase.Instrument.QueryTrimEnd(":DATA:LAT?")
                Return _lastFetch
            End Get
        End Property

        Private resistanceIntegrationPeriodMessage As String
        Private _resistanceIntegrationPeriod As Double
        ''' <summary>Gets or sets the sense resistance integration period in seconds.</summary>
        Public Property ResistanceIntegrationPeriod() As Double
            Get
                If MyBase.ForceRead OrElse resistanceIntegrationPeriodMessage Is Nothing Then
                    _resistanceIntegrationPeriod = MyBase.Instrument.QueryDouble(":SENS:RES:NPLC?") / MyBase.LineFrequency
                    resistanceIntegrationPeriodMessage = _resistanceIntegrationPeriod.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _resistanceIntegrationPeriod
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.ResistanceIntegrationPeriod) Then
                    MyBase.Instrument.Write(":SENS:RES:NPLC " & (value * MyBase.LineFrequency).ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageIntegrationPeriod = value
            End Set
        End Property

        Private resistanceRangeMessage As String
        Private _resistanceRange As Double
        ''' <summary>Gets or sets the sense Resistance Range.  Set to
        ''' <see cref="SCPI.Subsystem.Infinity">infinity</see> to set to maximum or to 
        ''' <see cref="SCPI.Subsystem.Infinity">negative infinity</see> for minimum.</summary>
        Public Property ResistanceRange() As Double
            Get
                If MyBase.ForceRead OrElse resistanceRangeMessage Is Nothing Then
                    _resistanceRange = MyBase.Instrument.QueryDouble(":SENS:RES:RANG?")
                    resistanceRangeMessage = _resistanceRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _resistanceRange
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.ResistanceRange) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SENS:RES:RANG MAX")
                    ElseIf value <= (Scpi.Subsystem.NegativeInfinity + 1) Then
                        MyBase.Instrument.Write(":SENS:RES:RANG MIN")
                    Else
                        MyBase.Instrument.Write(":SENS:RES:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _resistanceRange = value
            End Set
        End Property

        Private voltageIntegrationPeriodMessage As String
        Private _voltageIntegrationPeriod As Double
        ''' <summary>Gets or sets the sense voltage integration period in seconds.</summary>
        Public Property VoltageIntegrationPeriod() As Double
            Get
                If MyBase.ForceRead OrElse voltageIntegrationPeriodMessage Is Nothing Then
                    _voltageIntegrationPeriod = MyBase.Instrument.QueryDouble(":SENS:VOLT:NPLC?") / MyBase.LineFrequency
                    voltageIntegrationPeriodMessage = _voltageIntegrationPeriod.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageIntegrationPeriod
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageIntegrationPeriod) Then
                    MyBase.Instrument.Write(":SENS:VOLT:NPLC " & (value * MyBase.LineFrequency).ToString(Globalization.CultureInfo.CurrentCulture))
                End If
                _voltageIntegrationPeriod = value
            End Set
        End Property

        Private voltageLimitMessage As String
        Private _voltageLimit As Double
        ''' <summary>Gets or sets the voltage Limit for current source.  Set to
        ''' <see cref="SCPI.Subsystem.Infinity">infinity</see> to set to maximum or to 
        ''' <see cref="SCPI.Subsystem.Infinity">negative infinity</see> for minimum.</summary>
        Public Property VoltageLimit() As Double
            Get
                If MyBase.ForceRead OrElse voltageLimitMessage Is Nothing Then
                    _voltageLimit = MyBase.Instrument.QueryDouble(":SENS:VOLT:PROT?")
                    voltageLimitMessage = _voltageLimit.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageLimit
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageLimit) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SENS:VOLT:PROT MAX")
                    ElseIf value <= (Scpi.Subsystem.NegativeInfinity + 1) Then
                        MyBase.Instrument.Write(":SENS:VOLT:PROT MAX")
                    Else
                        MyBase.Instrument.Write(":SENS:VOLT:PROT " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _voltageLimit = value
            End Set
        End Property

        Private voltageRangeMessage As String
        Private _voltageRange As Double
        ''' <summary>Gets or sets the sense voltage Range.   Set to
        ''' <see cref="SCPI.Subsystem.Infinity">infinity</see> to set to maximum or to 
        ''' <see cref="SCPI.Subsystem.Infinity">negative infinity</see> for minimum.</summary>
        Public Property VoltageRange() As Double
            Get
                If MyBase.ForceRead OrElse voltageRangeMessage Is Nothing Then
                    _voltageRange = MyBase.Instrument.QueryDouble(":SENS:VOLT:RANG?")
                    voltageRangeMessage = _voltageRange.ToString(Globalization.CultureInfo.CurrentCulture)
                End If
                Return _voltageRange
            End Get
            Set(ByVal value As Double)
                If MyBase.ForceWrite OrElse MyBase.AreDifferent(value, Me.VoltageRange) Then
                    If value >= (Scpi.Subsystem.Infinity - 1) Then
                        MyBase.Instrument.Write(":SENS:VOLT:RANG MAX")
                    ElseIf value <= (Scpi.Subsystem.NegativeInfinity + 1) Then
                        MyBase.Instrument.Write(":SENS:VOLT:RANG MIN")
                    Else
                        MyBase.Instrument.Write(":SENS:VOLT:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                    End If
                End If
                _voltageRange = value
            End Set
        End Property

        ''' <summary>Returns the subsystem to its default known state by setting properties
        '''   to default values.</summary>
        Public Overrides Sub ResetToKnownState()
            ' upgrade: Add properties to allow setting the cached values voltageRangeCache etc. 
            _autoCurrentRange = True
            _autoResistanceRange = True
            _autoVoltageRange = True
            _currentIntegrationPeriod = 1 / MyBase.Instrument.ScpiSystem.LineFrequency
            _currentLimit = 0.000105
            _currentRange = 0.000105
            _functionConcurrent = True
            _functionMode = SenseFunctionModes.CURR_DC
            _resistanceIntegrationPeriod = 1 / MyBase.Instrument.ScpiSystem.LineFrequency
            _resistanceRange = 210000.0
            _voltageIntegrationPeriod = 1 / MyBase.Instrument.ScpiSystem.LineFrequency
            _voltageLimit = 21
            _voltageRange = 21
        End Sub

#End Region

    End Class

End Namespace
