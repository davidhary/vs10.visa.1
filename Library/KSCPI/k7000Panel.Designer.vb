Namespace Scpi

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class K7000Panel
        Inherits isr.Visa.InstrumentPanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.mainTabControl = New System.Windows.Forms.TabControl
            Me.toggleTabPage = New System.Windows.Forms.TabPage
            Me.channelListBuilderGroupBox = New System.Windows.Forms.GroupBox
            Me.relayNumberTextBoxLabel = New System.Windows.Forms.Label
            Me.relayNumberTextBox = New System.Windows.Forms.TextBox
            Me.slotNumberTextBoxLabel = New System.Windows.Forms.Label
            Me.slotNumberTextBox = New System.Windows.Forms.TextBox
            Me.memoryLocationChannelItemTextBoxLabel = New System.Windows.Forms.Label
            Me.memoryLocationChannelItemTextBox = New System.Windows.Forms.TextBox
            Me.addMemoryLocationButton = New System.Windows.Forms.Button
            Me.clearChannelListButton = New System.Windows.Forms.Button
            Me.addChannelToList = New System.Windows.Forms.Button
            Me.memoryLocationTextBox = New System.Windows.Forms.TextBox
            Me.saveToMemoryButton = New System.Windows.Forms.Button
            Me.channelOpenButton = New System.Windows.Forms.Button
            Me.channelCloseButton = New System.Windows.Forms.Button
            Me.channelListComboBox = New System.Windows.Forms.ComboBox
            Me.channelListComboBoxLabel = New System.Windows.Forms.Label
            Me.openAllChannelsButton = New System.Windows.Forms.Button
            Me.scanTabPage = New System.Windows.Forms.TabPage
            Me.updateScanListButton = New System.Windows.Forms.Button
            Me.scanTextBox = New System.Windows.Forms.TextBox
            Me.stepButton = New System.Windows.Forms.Button
            Me.scanListComboBox = New System.Windows.Forms.ComboBox
            Me.scanListComboBoxLabel = New System.Windows.Forms.Label
            Me.slotTabPage = New System.Windows.Forms.TabPage
            Me.updateSlotConfigurationButton = New System.Windows.Forms.Button
            Me.readSlotConfigurationButton = New System.Windows.Forms.Button
            Me.cardTypeTextBox = New System.Windows.Forms.TextBox
            Me.slotNumberComboBoxLabel = New System.Windows.Forms.Label
            Me.slotNumberComboBox = New System.Windows.Forms.ComboBox
            Me.cardTypeTextBoxLabel = New System.Windows.Forms.Label
            Me.settlingTimeTextBoxLabel = New System.Windows.Forms.Label
            Me.settlingtimeTextBoxTextBox = New System.Windows.Forms.TextBox
            Me.serviceRequestTabPage = New System.Windows.Forms.TabPage
            Me.enableEndOfSettlingRequestCheckBox = New System.Windows.Forms.CheckBox
            Me.serviceRequestRegisterGroupBox = New System.Windows.Forms.GroupBox
            Me.serviceRequestMaskRemoveButton = New System.Windows.Forms.Button
            Me.serviceRequestMaskAddButton = New System.Windows.Forms.Button
            Me.serviceRequestFlagsComboBox = New System.Windows.Forms.ComboBox
            Me.enableServiceRequestCheckBox = New System.Windows.Forms.CheckBox
            Me.serviceRequestByteTextBoxLabel = New System.Windows.Forms.Label
            Me.serviceRequestMaskTextBox = New System.Windows.Forms.TextBox
            Me.messagesTabPage = New System.Windows.Forms.TabPage
            Me.messagesMessageList = New isr.Visa.WinForms.MessagesBox
            Me.mainTabControl.SuspendLayout()
            Me.toggleTabPage.SuspendLayout()
            Me.channelListBuilderGroupBox.SuspendLayout()
            Me.scanTabPage.SuspendLayout()
            Me.slotTabPage.SuspendLayout()
            Me.serviceRequestTabPage.SuspendLayout()
            Me.serviceRequestRegisterGroupBox.SuspendLayout()
            Me.messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'mainTabControl
            '
            Me.mainTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom
            Me.mainTabControl.Controls.Add(Me.toggleTabPage)
            Me.mainTabControl.Controls.Add(Me.scanTabPage)
            Me.mainTabControl.Controls.Add(Me.slotTabPage)
            Me.mainTabControl.Controls.Add(Me.serviceRequestTabPage)
            Me.mainTabControl.Controls.Add(Me.messagesTabPage)
            Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.mainTabControl.Enabled = False
            Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
            Me.mainTabControl.Multiline = True
            Me.mainTabControl.Name = "mainTabControl"
            Me.mainTabControl.SelectedIndex = 0
            Me.mainTabControl.Size = New System.Drawing.Size(312, 238)
            Me.mainTabControl.TabIndex = 15
            '
            'toggleTabPage
            '
            Me.toggleTabPage.Controls.Add(Me.channelListBuilderGroupBox)
            Me.toggleTabPage.Controls.Add(Me.memoryLocationTextBox)
            Me.toggleTabPage.Controls.Add(Me.saveToMemoryButton)
            Me.toggleTabPage.Controls.Add(Me.channelOpenButton)
            Me.toggleTabPage.Controls.Add(Me.channelCloseButton)
            Me.toggleTabPage.Controls.Add(Me.channelListComboBox)
            Me.toggleTabPage.Controls.Add(Me.channelListComboBoxLabel)
            Me.toggleTabPage.Controls.Add(Me.openAllChannelsButton)
            Me.toggleTabPage.Location = New System.Drawing.Point(4, 4)
            Me.toggleTabPage.Name = "toggleTabPage"
            Me.toggleTabPage.Size = New System.Drawing.Size(304, 212)
            Me.toggleTabPage.TabIndex = 1
            Me.toggleTabPage.Text = "Toggle"
            Me.toggleTabPage.UseVisualStyleBackColor = True
            '
            'channelListBuilderGroupBox
            '
            Me.channelListBuilderGroupBox.Controls.Add(Me.relayNumberTextBoxLabel)
            Me.channelListBuilderGroupBox.Controls.Add(Me.relayNumberTextBox)
            Me.channelListBuilderGroupBox.Controls.Add(Me.slotNumberTextBoxLabel)
            Me.channelListBuilderGroupBox.Controls.Add(Me.slotNumberTextBox)
            Me.channelListBuilderGroupBox.Controls.Add(Me.memoryLocationChannelItemTextBoxLabel)
            Me.channelListBuilderGroupBox.Controls.Add(Me.memoryLocationChannelItemTextBox)
            Me.channelListBuilderGroupBox.Controls.Add(Me.addMemoryLocationButton)
            Me.channelListBuilderGroupBox.Controls.Add(Me.clearChannelListButton)
            Me.channelListBuilderGroupBox.Controls.Add(Me.addChannelToList)
            Me.channelListBuilderGroupBox.Location = New System.Drawing.Point(32, 6)
            Me.channelListBuilderGroupBox.Name = "channelListBuilderGroupBox"
            Me.channelListBuilderGroupBox.Size = New System.Drawing.Size(240, 97)
            Me.channelListBuilderGroupBox.TabIndex = 0
            Me.channelListBuilderGroupBox.TabStop = False
            Me.channelListBuilderGroupBox.Text = "Channel List Builder"
            '
            'relayNumberTextBoxLabel
            '
            Me.relayNumberTextBoxLabel.Location = New System.Drawing.Point(16, 73)
            Me.relayNumberTextBoxLabel.Name = "relayNumberTextBoxLabel"
            Me.relayNumberTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.relayNumberTextBoxLabel.TabIndex = 5
            Me.relayNumberTextBoxLabel.Text = "Relay: "
            Me.relayNumberTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'relayNumberTextBox
            '
            Me.relayNumberTextBox.Location = New System.Drawing.Point(68, 71)
            Me.relayNumberTextBox.Name = "relayNumberTextBox"
            Me.relayNumberTextBox.Size = New System.Drawing.Size(24, 20)
            Me.relayNumberTextBox.TabIndex = 6
            Me.relayNumberTextBox.Text = "1"
            '
            'slotNumberTextBoxLabel
            '
            Me.slotNumberTextBoxLabel.Location = New System.Drawing.Point(16, 49)
            Me.slotNumberTextBoxLabel.Name = "slotNumberTextBoxLabel"
            Me.slotNumberTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.slotNumberTextBoxLabel.TabIndex = 3
            Me.slotNumberTextBoxLabel.Text = "Slot: "
            Me.slotNumberTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'slotNumberTextBox
            '
            Me.slotNumberTextBox.Location = New System.Drawing.Point(68, 47)
            Me.slotNumberTextBox.Name = "slotNumberTextBox"
            Me.slotNumberTextBox.Size = New System.Drawing.Size(24, 20)
            Me.slotNumberTextBox.TabIndex = 4
            Me.slotNumberTextBox.Text = "1"
            '
            'memoryLocationChannelItemTextBoxLabel
            '
            Me.memoryLocationChannelItemTextBoxLabel.Location = New System.Drawing.Point(8, 22)
            Me.memoryLocationChannelItemTextBoxLabel.Name = "memoryLocationChannelItemTextBoxLabel"
            Me.memoryLocationChannelItemTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.memoryLocationChannelItemTextBoxLabel.TabIndex = 0
            Me.memoryLocationChannelItemTextBoxLabel.Text = "Location: "
            '
            'memoryLocationChannelItemTextBox
            '
            Me.memoryLocationChannelItemTextBox.Location = New System.Drawing.Point(60, 20)
            Me.memoryLocationChannelItemTextBox.Name = "memoryLocationChannelItemTextBox"
            Me.memoryLocationChannelItemTextBox.Size = New System.Drawing.Size(32, 20)
            Me.memoryLocationChannelItemTextBox.TabIndex = 1
            Me.memoryLocationChannelItemTextBox.Text = "1"
            '
            'addMemoryLocationButton
            '
            Me.addMemoryLocationButton.Location = New System.Drawing.Point(101, 18)
            Me.addMemoryLocationButton.Name = "addMemoryLocationButton"
            Me.addMemoryLocationButton.Size = New System.Drawing.Size(128, 24)
            Me.addMemoryLocationButton.TabIndex = 2
            Me.addMemoryLocationButton.Text = "Add Memory Location"
            '
            'clearChannelListButton
            '
            Me.clearChannelListButton.Location = New System.Drawing.Point(181, 51)
            Me.clearChannelListButton.Name = "clearChannelListButton"
            Me.clearChannelListButton.Size = New System.Drawing.Size(48, 40)
            Me.clearChannelListButton.TabIndex = 8
            Me.clearChannelListButton.Text = "Clear List"
            '
            'addChannelToList
            '
            Me.addChannelToList.Location = New System.Drawing.Point(101, 51)
            Me.addChannelToList.Name = "addChannelToList"
            Me.addChannelToList.Size = New System.Drawing.Size(64, 40)
            Me.addChannelToList.TabIndex = 7
            Me.addChannelToList.Text = "Add Relay to List"
            '
            'memoryLocationTextBox
            '
            Me.memoryLocationTextBox.Location = New System.Drawing.Point(164, 167)
            Me.memoryLocationTextBox.MaxLength = 3
            Me.memoryLocationTextBox.Name = "memoryLocationTextBox"
            Me.memoryLocationTextBox.Size = New System.Drawing.Size(35, 20)
            Me.memoryLocationTextBox.TabIndex = 6
            Me.memoryLocationTextBox.Text = "1"
            '
            'saveToMemoryButton
            '
            Me.saveToMemoryButton.Location = New System.Drawing.Point(8, 166)
            Me.saveToMemoryButton.Name = "saveToMemoryButton"
            Me.saveToMemoryButton.Size = New System.Drawing.Size(156, 23)
            Me.saveToMemoryButton.TabIndex = 5
            Me.saveToMemoryButton.Text = "&Save To Memory Location # "
            Me.saveToMemoryButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'channelOpenButton
            '
            Me.channelOpenButton.Location = New System.Drawing.Point(253, 110)
            Me.channelOpenButton.Name = "channelOpenButton"
            Me.channelOpenButton.Size = New System.Drawing.Size(48, 23)
            Me.channelOpenButton.TabIndex = 4
            Me.channelOpenButton.Text = "&Open"
            '
            'channelCloseButton
            '
            Me.channelCloseButton.Location = New System.Drawing.Point(200, 110)
            Me.channelCloseButton.Name = "channelCloseButton"
            Me.channelCloseButton.Size = New System.Drawing.Size(49, 23)
            Me.channelCloseButton.TabIndex = 3
            Me.channelCloseButton.Text = "&Close"
            '
            'channelListComboBox
            '
            Me.channelListComboBox.Location = New System.Drawing.Point(8, 134)
            Me.channelListComboBox.Name = "channelListComboBox"
            Me.channelListComboBox.Size = New System.Drawing.Size(293, 21)
            Me.channelListComboBox.TabIndex = 2
            Me.channelListComboBox.Text = "(@ 4!1,5!1)"
            '
            'channelListComboBoxLabel
            '
            Me.channelListComboBoxLabel.Location = New System.Drawing.Point(8, 118)
            Me.channelListComboBoxLabel.Name = "channelListComboBoxLabel"
            Me.channelListComboBoxLabel.Size = New System.Drawing.Size(72, 16)
            Me.channelListComboBoxLabel.TabIndex = 1
            Me.channelListComboBoxLabel.Text = "Channel List:"
            '
            'openAllChannelsButton
            '
            Me.openAllChannelsButton.Location = New System.Drawing.Point(237, 166)
            Me.openAllChannelsButton.Name = "openAllChannelsButton"
            Me.openAllChannelsButton.Size = New System.Drawing.Size(64, 23)
            Me.openAllChannelsButton.TabIndex = 7
            Me.openAllChannelsButton.Text = "Open &All"
            '
            'scanTabPage
            '
            Me.scanTabPage.Controls.Add(Me.updateScanListButton)
            Me.scanTabPage.Controls.Add(Me.scanTextBox)
            Me.scanTabPage.Controls.Add(Me.stepButton)
            Me.scanTabPage.Controls.Add(Me.scanListComboBox)
            Me.scanTabPage.Controls.Add(Me.scanListComboBoxLabel)
            Me.scanTabPage.Location = New System.Drawing.Point(4, 4)
            Me.scanTabPage.Name = "scanTabPage"
            Me.scanTabPage.Size = New System.Drawing.Size(304, 212)
            Me.scanTabPage.TabIndex = 0
            Me.scanTabPage.Text = "Scan"
            Me.scanTabPage.UseVisualStyleBackColor = True
            '
            'updateScanListButton
            '
            Me.updateScanListButton.Location = New System.Drawing.Point(212, 82)
            Me.updateScanListButton.Name = "updateScanListButton"
            Me.updateScanListButton.Size = New System.Drawing.Size(75, 23)
            Me.updateScanListButton.TabIndex = 4
            Me.updateScanListButton.Text = "&Apply"
            '
            'scanTextBox
            '
            Me.scanTextBox.Location = New System.Drawing.Point(18, 114)
            Me.scanTextBox.Multiline = True
            Me.scanTextBox.Name = "scanTextBox"
            Me.scanTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.scanTextBox.Size = New System.Drawing.Size(269, 42)
            Me.scanTextBox.TabIndex = 3
            Me.scanTextBox.Text = "<scan list>"
            '
            'stepButton
            '
            Me.stepButton.Location = New System.Drawing.Point(18, 82)
            Me.stepButton.Name = "stepButton"
            Me.stepButton.Size = New System.Drawing.Size(75, 23)
            Me.stepButton.TabIndex = 2
            Me.stepButton.Text = "&Step"
            '
            'scanListComboBox
            '
            Me.scanListComboBox.Location = New System.Drawing.Point(18, 58)
            Me.scanListComboBox.Name = "scanListComboBox"
            Me.scanListComboBox.Size = New System.Drawing.Size(269, 21)
            Me.scanListComboBox.TabIndex = 1
            Me.scanListComboBox.Text = "(@ M1,M2)"
            '
            'scanListComboBoxLabel
            '
            Me.scanListComboBoxLabel.Location = New System.Drawing.Point(18, 42)
            Me.scanListComboBoxLabel.Name = "scanListComboBoxLabel"
            Me.scanListComboBoxLabel.Size = New System.Drawing.Size(56, 16)
            Me.scanListComboBoxLabel.TabIndex = 0
            Me.scanListComboBoxLabel.Text = "Scan List:"
            '
            'slotTabPage
            '
            Me.slotTabPage.Controls.Add(Me.updateSlotConfigurationButton)
            Me.slotTabPage.Controls.Add(Me.readSlotConfigurationButton)
            Me.slotTabPage.Controls.Add(Me.cardTypeTextBox)
            Me.slotTabPage.Controls.Add(Me.slotNumberComboBoxLabel)
            Me.slotTabPage.Controls.Add(Me.slotNumberComboBox)
            Me.slotTabPage.Controls.Add(Me.cardTypeTextBoxLabel)
            Me.slotTabPage.Controls.Add(Me.settlingTimeTextBoxLabel)
            Me.slotTabPage.Controls.Add(Me.settlingtimeTextBoxTextBox)
            Me.slotTabPage.Location = New System.Drawing.Point(4, 4)
            Me.slotTabPage.Name = "slotTabPage"
            Me.slotTabPage.Size = New System.Drawing.Size(304, 212)
            Me.slotTabPage.TabIndex = 2
            Me.slotTabPage.Text = "Slot"
            Me.slotTabPage.UseVisualStyleBackColor = True
            '
            'updateSlotConfigurationButton
            '
            Me.updateSlotConfigurationButton.Location = New System.Drawing.Point(207, 141)
            Me.updateSlotConfigurationButton.Name = "updateSlotConfigurationButton"
            Me.updateSlotConfigurationButton.Size = New System.Drawing.Size(75, 23)
            Me.updateSlotConfigurationButton.TabIndex = 7
            Me.updateSlotConfigurationButton.Text = "&Apply"
            '
            'readSlotConfigurationButton
            '
            Me.readSlotConfigurationButton.Location = New System.Drawing.Point(119, 141)
            Me.readSlotConfigurationButton.Name = "readSlotConfigurationButton"
            Me.readSlotConfigurationButton.Size = New System.Drawing.Size(80, 23)
            Me.readSlotConfigurationButton.TabIndex = 6
            Me.readSlotConfigurationButton.Text = "&Read"
            '
            'cardTypeTextBox
            '
            Me.cardTypeTextBox.Location = New System.Drawing.Point(135, 83)
            Me.cardTypeTextBox.Name = "cardTypeTextBox"
            Me.cardTypeTextBox.Size = New System.Drawing.Size(100, 20)
            Me.cardTypeTextBox.TabIndex = 3
            Me.cardTypeTextBox.Text = "<card type>"
            '
            'slotNumberComboBoxLabel
            '
            Me.slotNumberComboBoxLabel.Location = New System.Drawing.Point(23, 61)
            Me.slotNumberComboBoxLabel.Name = "slotNumberComboBoxLabel"
            Me.slotNumberComboBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me.slotNumberComboBoxLabel.TabIndex = 0
            Me.slotNumberComboBoxLabel.Text = "Number: "
            Me.slotNumberComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'slotNumberComboBox
            '
            Me.slotNumberComboBox.DisplayMember = "Select slot number"
            Me.slotNumberComboBox.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"})
            Me.slotNumberComboBox.Location = New System.Drawing.Point(135, 59)
            Me.slotNumberComboBox.Name = "slotNumberComboBox"
            Me.slotNumberComboBox.Size = New System.Drawing.Size(40, 21)
            Me.slotNumberComboBox.TabIndex = 1
            Me.slotNumberComboBox.Text = "1"
            Me.slotNumberComboBox.ValueMember = "Select slot number"
            '
            'cardTypeTextBoxLabel
            '
            Me.cardTypeTextBoxLabel.Location = New System.Drawing.Point(23, 87)
            Me.cardTypeTextBoxLabel.Name = "cardTypeTextBoxLabel"
            Me.cardTypeTextBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me.cardTypeTextBoxLabel.TabIndex = 2
            Me.cardTypeTextBoxLabel.Text = "Card Type: "
            Me.cardTypeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'settlingTimeTextBoxLabel
            '
            Me.settlingTimeTextBoxLabel.Location = New System.Drawing.Point(23, 110)
            Me.settlingTimeTextBoxLabel.Name = "settlingTimeTextBoxLabel"
            Me.settlingTimeTextBoxLabel.Size = New System.Drawing.Size(104, 16)
            Me.settlingTimeTextBoxLabel.TabIndex = 4
            Me.settlingTimeTextBoxLabel.Text = "Settling Time [S]: "
            Me.settlingTimeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'settlingtimeTextBoxTextBox
            '
            Me.settlingtimeTextBoxTextBox.Location = New System.Drawing.Point(135, 107)
            Me.settlingtimeTextBoxTextBox.Name = "settlingtimeTextBoxTextBox"
            Me.settlingtimeTextBoxTextBox.Size = New System.Drawing.Size(100, 20)
            Me.settlingtimeTextBoxTextBox.TabIndex = 5
            Me.settlingtimeTextBoxTextBox.Text = "<Settling Time>"
            '
            'serviceRequestTabPage
            '
            Me.serviceRequestTabPage.Controls.Add(Me.enableEndOfSettlingRequestCheckBox)
            Me.serviceRequestTabPage.Controls.Add(Me.serviceRequestRegisterGroupBox)
            Me.serviceRequestTabPage.Location = New System.Drawing.Point(4, 4)
            Me.serviceRequestTabPage.Name = "serviceRequestTabPage"
            Me.serviceRequestTabPage.Size = New System.Drawing.Size(304, 212)
            Me.serviceRequestTabPage.TabIndex = 4
            Me.serviceRequestTabPage.Text = "SRQ"
            Me.serviceRequestTabPage.UseVisualStyleBackColor = True
            '
            'enableEndOfSettlingRequestCheckBox
            '
            Me.enableEndOfSettlingRequestCheckBox.Location = New System.Drawing.Point(11, 102)
            Me.enableEndOfSettlingRequestCheckBox.Name = "enableEndOfSettlingRequestCheckBox"
            Me.enableEndOfSettlingRequestCheckBox.Size = New System.Drawing.Size(232, 32)
            Me.enableEndOfSettlingRequestCheckBox.TabIndex = 4
            Me.enableEndOfSettlingRequestCheckBox.Text = "Enable End of Settling Request"
            '
            'serviceRequestRegisterGroupBox
            '
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskRemoveButton)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskAddButton)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestFlagsComboBox)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.enableServiceRequestCheckBox)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestByteTextBoxLabel)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskTextBox)
            Me.serviceRequestRegisterGroupBox.Location = New System.Drawing.Point(8, 8)
            Me.serviceRequestRegisterGroupBox.Name = "serviceRequestRegisterGroupBox"
            Me.serviceRequestRegisterGroupBox.Size = New System.Drawing.Size(288, 88)
            Me.serviceRequestRegisterGroupBox.TabIndex = 3
            Me.serviceRequestRegisterGroupBox.TabStop = False
            Me.serviceRequestRegisterGroupBox.Text = "Service Request Register"
            '
            'serviceRequestMaskRemoveButton
            '
            Me.serviceRequestMaskRemoveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.serviceRequestMaskRemoveButton.Location = New System.Drawing.Point(222, 56)
            Me.serviceRequestMaskRemoveButton.Name = "serviceRequestMaskRemoveButton"
            Me.serviceRequestMaskRemoveButton.Size = New System.Drawing.Size(24, 23)
            Me.serviceRequestMaskRemoveButton.TabIndex = 5
            Me.serviceRequestMaskRemoveButton.Text = "-  "
            '
            'serviceRequestMaskAddButton
            '
            Me.serviceRequestMaskAddButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.serviceRequestMaskAddButton.Location = New System.Drawing.Point(197, 56)
            Me.serviceRequestMaskAddButton.Name = "serviceRequestMaskAddButton"
            Me.serviceRequestMaskAddButton.Size = New System.Drawing.Size(24, 23)
            Me.serviceRequestMaskAddButton.TabIndex = 4
            Me.serviceRequestMaskAddButton.Text = "+  "
            '
            'serviceRequestFlagsComboBox
            '
            Me.serviceRequestFlagsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.serviceRequestFlagsComboBox.Location = New System.Drawing.Point(9, 56)
            Me.serviceRequestFlagsComboBox.Name = "serviceRequestFlagsComboBox"
            Me.serviceRequestFlagsComboBox.Size = New System.Drawing.Size(187, 21)
            Me.serviceRequestFlagsComboBox.TabIndex = 3
            '
            'enableServiceRequestCheckBox
            '
            Me.enableServiceRequestCheckBox.Location = New System.Drawing.Point(8, 16)
            Me.enableServiceRequestCheckBox.Name = "enableServiceRequestCheckBox"
            Me.enableServiceRequestCheckBox.Size = New System.Drawing.Size(152, 24)
            Me.enableServiceRequestCheckBox.TabIndex = 0
            Me.enableServiceRequestCheckBox.Text = "Enable Service Request"
            '
            'serviceRequestByteTextBoxLabel
            '
            Me.serviceRequestByteTextBoxLabel.Location = New System.Drawing.Point(8, 40)
            Me.serviceRequestByteTextBoxLabel.Name = "serviceRequestByteTextBoxLabel"
            Me.serviceRequestByteTextBoxLabel.Size = New System.Drawing.Size(120, 16)
            Me.serviceRequestByteTextBoxLabel.TabIndex = 1
            Me.serviceRequestByteTextBoxLabel.Text = "Service Request Mask: "
            '
            'serviceRequestMaskTextBox
            '
            Me.serviceRequestMaskTextBox.Location = New System.Drawing.Point(247, 56)
            Me.serviceRequestMaskTextBox.MaxLength = 3
            Me.serviceRequestMaskTextBox.Name = "serviceRequestMaskTextBox"
            Me.serviceRequestMaskTextBox.Size = New System.Drawing.Size(32, 20)
            Me.serviceRequestMaskTextBox.TabIndex = 2
            Me.serviceRequestMaskTextBox.Text = "239"
            '
            'messagesTabPage
            '
            Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
            Me.messagesTabPage.Location = New System.Drawing.Point(4, 4)
            Me.messagesTabPage.Name = "messagesTabPage"
            Me.messagesTabPage.Size = New System.Drawing.Size(304, 212)
            Me.messagesTabPage.TabIndex = 3
            Me.messagesTabPage.Text = "Messages"
            Me.messagesTabPage.UseVisualStyleBackColor = True
            '
            'messagesMessageList
            '
            Me.messagesMessageList.BackColor = System.Drawing.SystemColors.Info
            Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
            Me.messagesMessageList.Multiline = True
            Me.messagesMessageList.Name = "messagesMessageList"
            Me.messagesMessageList.NewMessagesCount = 0
            Me.messagesMessageList.PresetCount = 50
            Me.messagesMessageList.ReadOnly = True
            Me.messagesMessageList.ResetCount = 100
            Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.messagesMessageList.Size = New System.Drawing.Size(304, 212)
            Me.messagesMessageList.TabIndex = 0
            Me.messagesMessageList.TimeFormat = "HH:mm:ss. "
            '
            'K7000Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.mainTabControl)
            Me.Name = "K7000Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.mainTabControl, 0)
            Me.mainTabControl.ResumeLayout(False)
            Me.toggleTabPage.ResumeLayout(False)
            Me.toggleTabPage.PerformLayout()
            Me.channelListBuilderGroupBox.ResumeLayout(False)
            Me.channelListBuilderGroupBox.PerformLayout()
            Me.scanTabPage.ResumeLayout(False)
            Me.scanTabPage.PerformLayout()
            Me.slotTabPage.ResumeLayout(False)
            Me.slotTabPage.PerformLayout()
            Me.serviceRequestTabPage.ResumeLayout(False)
            Me.serviceRequestRegisterGroupBox.ResumeLayout(False)
            Me.serviceRequestRegisterGroupBox.PerformLayout()
            Me.messagesTabPage.ResumeLayout(False)
            Me.messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
        Friend WithEvents toggleTabPage As System.Windows.Forms.TabPage
        Friend WithEvents channelListBuilderGroupBox As System.Windows.Forms.GroupBox
        Friend WithEvents relayNumberTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents relayNumberTextBox As System.Windows.Forms.TextBox
        Friend WithEvents slotNumberTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents slotNumberTextBox As System.Windows.Forms.TextBox
        Friend WithEvents memoryLocationChannelItemTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents memoryLocationChannelItemTextBox As System.Windows.Forms.TextBox
        Friend WithEvents addMemoryLocationButton As System.Windows.Forms.Button
        Friend WithEvents clearChannelListButton As System.Windows.Forms.Button
        Friend WithEvents addChannelToList As System.Windows.Forms.Button
        Friend WithEvents memoryLocationTextBox As System.Windows.Forms.TextBox
        Friend WithEvents saveToMemoryButton As System.Windows.Forms.Button
        Friend WithEvents channelOpenButton As System.Windows.Forms.Button
        Friend WithEvents channelCloseButton As System.Windows.Forms.Button
        Friend WithEvents channelListComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents channelListComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents openAllChannelsButton As System.Windows.Forms.Button
        Friend WithEvents scanTabPage As System.Windows.Forms.TabPage
        Friend WithEvents updateScanListButton As System.Windows.Forms.Button
        Friend WithEvents scanTextBox As System.Windows.Forms.TextBox
        Friend WithEvents stepButton As System.Windows.Forms.Button
        Friend WithEvents scanListComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents scanListComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents slotTabPage As System.Windows.Forms.TabPage
        Friend WithEvents updateSlotConfigurationButton As System.Windows.Forms.Button
        Friend WithEvents readSlotConfigurationButton As System.Windows.Forms.Button
        Friend WithEvents cardTypeTextBox As System.Windows.Forms.TextBox
        Friend WithEvents slotNumberComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents slotNumberComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents cardTypeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents settlingTimeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents settlingtimeTextBoxTextBox As System.Windows.Forms.TextBox
        Friend WithEvents serviceRequestTabPage As System.Windows.Forms.TabPage
        Friend WithEvents enableEndOfSettlingRequestCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents serviceRequestRegisterGroupBox As System.Windows.Forms.GroupBox
        Friend WithEvents serviceRequestMaskRemoveButton As System.Windows.Forms.Button
        Friend WithEvents serviceRequestMaskAddButton As System.Windows.Forms.Button
        Friend WithEvents serviceRequestFlagsComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents enableServiceRequestCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents serviceRequestByteTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents serviceRequestMaskTextBox As System.Windows.Forms.TextBox
        Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents messagesMessageList As isr.Visa.WinForms.MessagesBox

    End Class

End Namespace
