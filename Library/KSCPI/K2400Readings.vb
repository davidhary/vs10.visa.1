Namespace Scpi

    ''' <summary>
    ''' Holds a single set of 2400 source meter reading elements.
    ''' </summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    ''' <history date="05/12/06" by="David Hary" revision="1.0.2323.x">
    ''' Confine only to the 2400
    ''' </history>
    Public Class K2400Readings
        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <remarks>Use this constructor to instantiate this class
        '''   and set its properties.</remarks>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

            _reading = String.Empty
            _metaOutcome = New ReadingOutcome
            _voltageReading = New Visa.ReadingR()
            _voltageReading.DisplayCaption.Units = "V"
            _voltageReading.SaveCaption.Units = _voltageReading.DisplayCaption.Units
            _currentReading = New Visa.ReadingR()
            _currentReading.DisplayCaption.Units = "A"
            _currentReading.SaveCaption.Units = _currentReading.DisplayCaption.Units
            _resistanceReading = New Visa.ReadingR()
            _resistanceReading.DisplayCaption.Units = Convert.ToChar(&H3A9)   ' ChrW(&H2126) ' Omega
            _resistanceReading.SaveCaption.Units = _resistanceReading.DisplayCaption.Units
            _timeStampReading = New Visa.ReadingR()
            _timeStampReading.DisplayCaption.Units = "S"
            _timeStampReading.SaveCaption.Units = _timeStampReading.DisplayCaption.Units
            _statusReading = New Scpi.K2400Status()

        End Sub

        ''' <summary>
        ''' Create a copy of the model.
        ''' </summary>
        ''' <param name="model"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal model As K2400Readings)

            ' instantiate the base class
            MyBase.New()

            _reading = model._reading
            _complianceLimit = model._complianceLimit
            _metaOutcome = New ReadingOutcome(model._metaOutcome)
            _voltageReading = New isr.Visa.ReadingR(model._voltageReading)
            _currentReading = New isr.Visa.ReadingR(model._currentReading)
            _resistanceReading = New isr.Visa.ReadingR(model._resistanceReading)
            _timeStampReading = New isr.Visa.ReadingR(model._timeStampReading)
            _statusReading = New Scpi.K2400Status(model._statusReading)

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    _reading = String.Empty
                    _reading = Nothing

                    If _voltageReading IsNot Nothing Then
                        _voltageReading.Dispose()
                    End If

                    If _currentReading IsNot Nothing Then
                        _currentReading.Dispose()
                    End If

                    If _timeStampReading IsNot Nothing Then
                        _timeStampReading.Dispose()
                    End If

                    If _resistanceReading IsNot Nothing Then
                        _resistanceReading.Dispose()
                    End If

                    If _statusReading IsNot Nothing Then
                        _statusReading.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources

            End If

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " SHARED METHODS "

        ''' <summary>
        ''' Parse reading data into a readings array.
        ''' </summary>
        ''' <param name="baseReading">Specifies the base reading which includes the limits for all reading
        ''' elements.</param>
        ''' <param name="measuredData"></param>
        ''' <param name="complianceLimit"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")> _
        Public Shared Function ParseMulti(ByVal baseReading As K2400Readings, ByVal measuredData As String, ByVal complianceLimit As Double) As K2400Readings()

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length = 0 Then
                Dim r As K2400Readings() = {}
                Return r
            End If

            Dim readings As String() = measuredData.Split(","c)
            If readings.Length < 5 Then
                Dim r As K2400Readings() = {}
                Return r
            End If

            Dim readingsArray(readings.Length \ 5 - 1) As K2400Readings
            Dim j As Integer = 0
            For i As Integer = 0 To readings.Length - 1 Step 5
                Dim reading As New K2400Readings(baseReading)
                reading.Parse(readings, complianceLimit, i)
                readingsArray(j) = reading
                j += 1
            Next
            Return readingsArray

        End Function

        ''' <summary>
        ''' Parse reading data into a readings List.
        ''' </summary>
        ''' <param name="measuredData"></param>
        ''' <param name="complianceLimit"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ParseReadingsCollection(ByVal measuredData As String, ByVal complianceLimit As Double) As Collections.ObjectModel.Collection(Of K2400Readings)

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length = 0 Then
                Return New Collections.ObjectModel.Collection(Of K2400Readings)
            End If

            Dim readings As String() = measuredData.Split(","c)
            Dim readingsList As New Collections.ObjectModel.Collection(Of K2400Readings)
            For i As Integer = 0 To readings.Length - 1 Step 5
                Dim reading As New K2400Readings
                reading.Parse(readings, complianceLimit, i)
                readingsList.Add(reading)
            Next
            Return readingsList

        End Function

#If False Then
    ''' <summary>
    ''' Parse reading data into a readings List.
    ''' FX COP does not like this exposed so we created the <see cref="ParseReadingsCollection"/>
    ''' </summary>
    ''' <param name="measuredData"></param>
    ''' <param name="complianceLimit"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function ParseReadingsList(ByVal measuredData As String, ByVal complianceLimit As Double) As Collections.Generic.List(Of K2400Readings)

      If measuredData Is Nothing Then
        Throw New ArgumentNullException("measuredData")
      ElseIf measuredData.Length = 0 Then
        Return New Collections.Generic.List(Of K2400Readings)
      End If

      Dim readings As String() = measuredData.Split(","c)
      Dim readingsList As New Collections.Generic.List(Of K2400Readings)
      For i As Integer = 0 To readings.Length - 1 Step 5
        Dim reading As New K2400Readings
        reading.Parse(readings, complianceLimit, i)
        readingsList.Add(reading)
      Next
      Return readingsList

    End Function
#End If

#End Region

#Region " METHODS "

        ''' <summary>Parses the measured data.</summary>
        ''' <param name="measuredData"></param>
        ''' <param name="complianceLimit">Specifies the voltage or current limit for
        '''   compliance.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Sub Parse(ByVal measuredData As String, ByVal complianceLimit As Double)

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length = 0 Then
                ' indicate that we do not have a valid value
                Me.Reset()
                Me._metaOutcome.IsValid = False
                Return
            End If

            Me.Parse(measuredData.Split(","c), complianceLimit, 0)

        End Sub

        ''' <summary>Parses the measured data.</summary>
        ''' <param name="measuredData"></param>
        ''' <param name="complianceLimit">Specifies the voltage or current limit for
        '''   compliance.</param>
        ''' <param name="firstElementIndex">Zero-based index of first element.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Sub Parse(ByVal measuredData As String(), ByVal complianceLimit As Double, ByVal firstElementIndex As Integer)

            If measuredData Is Nothing Then
                Throw New ArgumentNullException("measuredData")
            ElseIf measuredData.Length < firstElementIndex + 5 Then
                ' indicate that we do not have a valid value
                Me.Reset()
                Me._metaOutcome.IsValid = False
                Return
            End If

            ' indicate that we have a valid value
            Me._metaOutcome.IsValid = True

            ' set compliance limit
            If complianceLimit = 0 Then
                _complianceLimit = Subsystem.Infinity
            Else
                _complianceLimit = (1 - _complianceLimitMargin) * complianceLimit
            End If

            _voltageReading.Reading = measuredData(firstElementIndex)
            _currentReading.Reading = measuredData(firstElementIndex + 1)
            _resistanceReading.Reading = measuredData(firstElementIndex + 2)
            _timeStampReading.Reading = measuredData(firstElementIndex + 3)
            _statusReading.Reading = measuredData(firstElementIndex + 4)

            ' set the outcomes of the perspective value
            Me.UpdateOutcomes()

        End Sub

        ''' <summary>Parses the measured data.</summary>
        ''' <param name="unprocessedReading">Specifies a reading elements set where thresholds were not set.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Sub Parse(ByVal unprocessedReading As K2400Readings)

            If unprocessedReading Is Nothing Then
                Throw New ArgumentNullException("unprocessedReading")
            End If

            ' indicate that we do not have a valid value
            Me.Reset()

            ' indicate that we have a valid value
            Me._metaOutcome.IsValid = unprocessedReading._metaOutcome.IsValid
            _complianceLimit = unprocessedReading._complianceLimit

            _voltageReading.Reading = unprocessedReading._voltageReading.Reading
            _currentReading.Reading = unprocessedReading._currentReading.Reading
            _resistanceReading.Reading = unprocessedReading._resistanceReading.Reading
            _timeStampReading.Reading = unprocessedReading._timeStampReading.Reading
            _statusReading.Reading = unprocessedReading._statusReading.Reading

            ' set the outcomes of the perspective value
            Me.UpdateOutcomes()

        End Sub

        ''' <summary>Updates the outcomes based on the readings and compliance.</summary>
        Public Sub UpdateOutcomes()

            ' set the outcomes of the perspective value
            If Me.IsHitCompliance Then
                _metaOutcome.HitCompliance = True
                _voltageReading.MetaOutcome.HitCompliance = True
                _currentReading.MetaOutcome.HitCompliance = True
                _resistanceReading.MetaOutcome.HitCompliance = True
                _timeStampReading.MetaOutcome.HitCompliance = True
                _statusReading.MetaOutcome.HitCompliance = True
            End If

            If Me.IsHitLevelCompliance Then
                _metaOutcome.HitLevelCompliance = True
                _voltageReading.MetaOutcome.HitLevelCompliance = True
                _currentReading.MetaOutcome.HitLevelCompliance = True
                _resistanceReading.MetaOutcome.HitLevelCompliance = True
                _timeStampReading.MetaOutcome.HitLevelCompliance = True
                _statusReading.MetaOutcome.HitLevelCompliance = True
            End If

            If _statusReading.IsHitRangeCompliance Then
                _metaOutcome.HitRangeCompliance = True
                _voltageReading.MetaOutcome.HitRangeCompliance = True
                _currentReading.MetaOutcome.HitRangeCompliance = True
                _resistanceReading.MetaOutcome.HitRangeCompliance = True
                _timeStampReading.MetaOutcome.HitRangeCompliance = True
                _statusReading.MetaOutcome.HitRangeCompliance = True
            End If

        End Sub

        ''' <summary>Resets the measured outcomes.</summary>
        Public Overloads Sub Reset()
            Me.MetaOutcome.Reset()
            _complianceLimit = Scpi.Subsystem.Infinity
            _voltageReading.Reset()
            _currentReading.Reset()
            _resistanceReading.Reset()
            _timeStampReading.Reset()
            _statusReading.Reset()
        End Sub

#End Region

#Region " PROPERTIES "

        Private _currentReading As Visa.ReadingR
        ''' <summary>Gets or sets the measured current <see cref="Visa.ReadingR">reading</see>.</summary>
        Public Property CurrentReading() As Visa.ReadingR
            Get
                Return _currentReading
            End Get
            Set(ByVal value As Visa.ReadingR)
                _currentReading = value
            End Set
        End Property

        Private _complianceLimit As Double
        ''' <summary>Gets or sets the compliance limit for testing if the reading exceeded the
        '''   compliance level.</summary>
        ''' <value>A <see cref="System.Double">Double</see> value</value>
        Public Property ComplianceLimit() As Double
            Get
                Return _complianceLimit
            End Get
            Set(ByVal value As Double)
                _complianceLimit = value
            End Set
        End Property

        Private _complianceLimitMargin As Double = 0.001
        ''' <summary>Gets or sets the margin of how close will allow the measured value to 
        '''   the compliance limit.  For instance, if the margin is 0.001, the measured
        '''   value must not exceed 99.9% of the compliance limit. The default is 0.001.</summary>
        ''' <value>A <see cref="System.Double">Double</see> value</value>
        Public Property ComplianceLimitMargin() As Double
            Get
                Return _complianceLimitMargin
            End Get
            Set(ByVal value As Double)
                _complianceLimitMargin = value
            End Set
        End Property

        ''' <summary>Gets or sets the condition for the voltage or current exceeded the 
        '''   <see cref="ComplianceLimit">compliance limit</see> as flagged by
        '''   <see cref="IsHitLevelCompliance">compliance level flag</see> or if
        '''   the <see cref="StatusReading">status reading</see> compliance bit turned
        '''   on as flagged by <see cref="isr.Visa.Scpi.K2400Status.IsHitCompliance">real compliance</see>.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsHitCompliance() As Boolean
            Get
                Return Me.IsHitLevelCompliance OrElse _statusReading.IsHitCompliance
            End Get
        End Property

        ''' <summary>Gets or sets the condition for the voltage or current exceeded the 
        '''   <see cref="ComplianceLimit">compliance limit</see>. Typically this value should
        '''   be the same as <see cref="IsHitCompliance">real compliance</see>,  However,
        '''   we found that, at times, the instrument real compliance status bit does
        '''   not flag level compliance properly.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsHitLevelCompliance() As Boolean
            Get
                Dim value As Double
                If _statusReading.IsVoltageSource Then
                    value = Math.Abs(_currentReading.Value)
                ElseIf _statusReading.IsCurrentSource Then
                    value = Math.Abs(_voltageReading.Value)
                Else
                    Return True
                End If
                Return Not (value >= _complianceLimit) Xor (_complianceLimit > 0)
            End Get
        End Property

        Private _metaOutcome As ReadingOutcome
        ''' <summary>
        ''' Holds the reading meta outcome.
        ''' </summary>
        Public Property MetaOutcome() As ReadingOutcome
            Get
                Return _metaOutcome
            End Get
            Set(ByVal value As ReadingOutcome)
                _metaOutcome = value
            End Set
        End Property

        Private _reading As String
        ''' <summary>Gets or sets the measured data string.  This string is parsed to derive
        '''   the numeric values of the <see cref="VoltageReading">voltage</see>, 
        '''   <see cref="CurrentReading">Current</see>, <see cref="ResistanceReading">Resistance</see>
        '''   <see cref="TimestampReading">timestamp</see>, and <see cref="StatusReading">Status</see>
        '''   readings.</summary>
        Public ReadOnly Property Reading() As String
            Get
                Return _reading
            End Get
        End Property

        Private _resistanceReading As Visa.ReadingR
        ''' <summary>Gets or sets the measured resistance <see cref="Visa.ReadingR">reading</see>.</summary>
        Public Property ResistanceReading() As Visa.ReadingR
            Get
                Return _resistanceReading

            End Get
            Set(ByVal value As Visa.ReadingR)
                _resistanceReading = value
            End Set
        End Property

        Private _statusReading As Scpi.K2400Status
        ''' <summary>Gets or sets the status <see cref="SCPI.k2400Status">reading</see>.</summary>
        Public Property StatusReading() As Scpi.K2400Status
            Get
                Return _statusReading
            End Get
            Set(ByVal value As Scpi.K2400Status)
                _statusReading = value
            End Set
        End Property

        Private _timeStampReading As Visa.ReadingR
        ''' <summary>Gets or sets the timestamp <see cref="Visa.ReadingR">reading</see>.</summary>
        Public Property TimestampReading() As Visa.ReadingR
            Get
                Return _timeStampReading

            End Get
            Set(ByVal value As Visa.ReadingR)
                _timeStampReading = value
            End Set
        End Property

        Private _voltageReading As Visa.ReadingR
        ''' <summary>Gets or sets the measured voltage <see cref="Visa.ReadingR">reading</see>.</summary>
        Public Property VoltageReading() As Visa.ReadingR
            Get
                Return _voltageReading
            End Get
            Set(ByVal value As Visa.ReadingR)
                _voltageReading = value
            End Set
        End Property

#End Region

    End Class

End Namespace
