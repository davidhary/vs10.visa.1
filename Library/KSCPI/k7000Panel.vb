Namespace Scpi

    ''' <summary>Provides a user interface for the Keithley 70XX instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("Keithley 7000 Family Switch - Windows Forms Custom Control")> _
    Public Class K7000Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            onInstantiate()

        End Sub

        ''' <summary>Initializes additional components.</summary>
        ''' <remarks>Called from the class constructor to add other controls or 
        '''   change anything set by the InitializeComponent method.</remarks>
        Private Function onInstantiate() As Boolean

            Try

                ' instantiate the base instrument and set reference to the control.
                MyBase.ResourceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib

                ' instantiate the reference to the instrument
                Me._instrument = New k7000(Me.Name)
                MyBase.BaseInstrument = Me._instrument

                ' set the refernece to the GUI
                Me._instrument.Gui = Me

                ' set combo
                serviceRequestFlagsComboBox.DataSource = Nothing
                serviceRequestFlagsComboBox.Items.Clear()
                serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.ServiceRequests))

                Return True

            Catch ex As isr.Visa.BaseException

                ' throw an exception
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed initializing", Me.Name)
                Throw New isr.Visa.BaseException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub onDisposeManagedResources()

            If _channelListBuilder IsNot Nothing Then
                _channelListBuilder.Dispose()
                _channelListBuilder = Nothing
            End If

            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Updates the user interface upon successful connection.
        ''' </summary>
        Public Overrides Function Connect() As Boolean

            ' enable user interface
            Me.mainTabControl.Enabled = True

            ' update the current scan list
            Me.refreshDisplay()

            ' set combo
            serviceRequestFlagsComboBox.DataSource = Nothing
            serviceRequestFlagsComboBox.Items.Clear()
            serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.ServiceRequests))

            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "SCPI Version: {0:0.0###}", Me._instrument.ScpiSystem.ScpiRevision))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Manufacturer name: {0}", Me._instrument.GpibSession.ResourceManufacturerName))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Manufacturer ID: 0x{0:X}", Me._instrument.GpibSession.ResourceManufacturerID))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Connectred to {0}", Me._instrument.Id()))

            Return MyBase.Connect()

        End Function

        ''' <summary>
        ''' Updates the user interface upon disconnection.
        ''' </summary>
        Public Overrides Function Disconnect() As Boolean

            Me.mainTabControl.Enabled = False
            Return MyBase.Disconnect()

        End Function

        ''' <summary>Display a status message.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub DisplayMessage(ByVal message As String)

            MyBase.DisplayMessage(message)
            Me.PushMessage(message)

        End Sub

        ''' <summary>Adds a message to the message list.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub PushMessage(ByVal message As String)
            messagesMessageList.PushMessage(message)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.Scpi.K7000
        ''' <summary>Gets a reference to the Keithley 7000 instrument.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        <System.ComponentModel.Browsable(False)> Public ReadOnly Property Instrument() As isr.Visa.Scpi.K7000
            Get
                Return _instrument
            End Get
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Gets or sets the channel list.</summary>
        Private _channelListBuilder As Scpi.ChannelListBuilder

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

                ' check if we are asking for a new scan list
                If scanListComboBox IsNot Nothing _
                    AndAlso scanListComboBox.Text.Length > 0 _
                    AndAlso scanListComboBox.FindString(scanListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the scan list
                    scanListComboBox.Items.Add(scanListComboBox.Text)
                End If

                ' update the current scan list
                scanTextBox.Text = Me.Instrument.Route.ScanList()

            End If

        End Sub

        ''' <summary>Adds new items to the combo box.</summary>
        Friend Sub updateChannelListComboBox()

            If MyBase.Visible Then
                ' check if we are asking for a new channel list
                If channelListComboBox IsNot Nothing _
                    AndAlso channelListComboBox.Text.Length > 0 _
                    AndAlso channelListComboBox.FindString(channelListComboBox.Text) < 0 Then
                    ' if we have a new string, add it to the channel list
                    channelListComboBox.Items.Add(channelListComboBox.Text)
                End If
            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

#Region " SLOT "

        Private Sub readSlotConfigurationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readSlotConfigurationButton.Click
            Dim slotNumber As Int32 = Int32.Parse(Me.slotNumberComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            cardTypeTextBox.Text = RouteSubsystem.SlotCardType(Me.Instrument.GpibSession, slotNumber)
            Me.settlingtimeTextBoxTextBox.Text = _
                RouteSubsystem.SlotSettlingTime(Me.Instrument.GpibSession, slotNumber).ToString(Globalization.CultureInfo.CurrentCulture)
        End Sub

        Private Sub updateSlotConfigurationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updateSlotConfigurationButton.Click
            Dim slotNumber As Int32 = Int32.Parse(Me.slotNumberComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            RouteSubsystem.SlotCardType(Me.Instrument.GpibSession, slotNumber) = cardTypeTextBox.Text
            RouteSubsystem.SlotSettlingTime(Me.Instrument.GpibSession, slotNumber) = Double.Parse(Me.settlingtimeTextBoxTextBox.Text, Globalization.CultureInfo.CurrentCulture)
        End Sub

#End Region

#Region " SCAN "

        Private Sub stepButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stepButton.Click

            ' clear registers
            Me.Instrument.ClearStatus()

            ' close the scan list
            Me.Instrument.Trigger.Initiate()

        End Sub

        Private Sub updateScanListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updateScanListButton.Click

            Me.Instrument.Route.ScanList = Me.scanListComboBox.Text
            refreshDisplay()

        End Sub

#End Region

#Region " TOGGLE "

        Private Sub addChannelToList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addChannelToList.Click
            If Me._channelListBuilder Is Nothing Then
                _channelListBuilder = New Scpi.ChannelListBuilder
            End If
            _channelListBuilder.AddChannel(Int32.Parse(Me.slotNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture), _
                Int32.Parse(Me.relayNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture))
            Me.channelListComboBox.Text = _channelListBuilder.ChannelList
        End Sub

        Private Sub addMemoryLocationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addMemoryLocationButton.Click
            If Me._channelListBuilder Is Nothing Then
                _channelListBuilder = New Scpi.ChannelListBuilder
            End If
            _channelListBuilder.AddChannel(Int32.Parse(Me.memoryLocationChannelItemTextBox.Text, Globalization.CultureInfo.CurrentCulture))
            Me.channelListComboBox.Text = _channelListBuilder.ChannelList
        End Sub

        Private Sub channelCloseButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles channelCloseButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearStatus()
            RouteSubsystem.CloseChannels(Me.Instrument.GpibSession, channelListComboBox.Text)
        End Sub

        Private Sub channelOpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles channelOpenButton.Click
            updateChannelListComboBox()
            Me.Instrument.ClearStatus()
            RouteSubsystem.OpenChannels(Me.Instrument.GpibSession, channelListComboBox.Text)
        End Sub

        Private Sub clearChannelListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearChannelListButton.Click
            _channelListBuilder = New Scpi.ChannelListBuilder
            Me.channelListComboBox.Text = _channelListBuilder.ChannelList
        End Sub

        Private Sub memoryLocationTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles memoryLocationTextBox.Validating

            If Int32.Parse(Me.memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture) < 1 _
                OrElse Int32.Parse(Me.memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture) > 100 Then
                e.Cancel = True
            End If

        End Sub

        Private Sub memoryLocationChannelItemTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles memoryLocationChannelItemTextBox.Validating
            If isr.Visa.Text.Helper.IsNumber(memoryLocationChannelItemTextBox.Text) Then
                Dim dataValue As Int32 = Int32.Parse(memoryLocationChannelItemTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                e.Cancel = (dataValue < 1 Or dataValue > 100)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub openAllChannelsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openAllChannelsButton.Click

            Me.Instrument.ClearStatus()
            RouteSubsystem.OpenAll(Me.Instrument.GpibSession)

        End Sub

        Private Sub relayNumberTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles relayNumberTextBox.Validating
            If isr.Visa.Text.Helper.IsNumber(relayNumberTextBox.Text) Then
                Dim dataValue As Int32 = Int32.Parse(relayNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                e.Cancel = (dataValue < 1 Or dataValue > 10)
            Else
                e.Cancel = True
            End If
        End Sub

        Private Sub saveToMemoryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles saveToMemoryButton.Click

            Me.Instrument.ClearStatus()
            RouteSubsystem.MemorySave(Me.Instrument.GpibSession, _
                Int32.Parse(Me.memoryLocationTextBox.Text, Globalization.CultureInfo.CurrentCulture))

        End Sub

        Private Sub slotNumberTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles slotNumberTextBox.Validating
            If isr.Visa.Text.Helper.IsNumber(slotNumberTextBox.Text) Then
                Dim dataValue As Int32 = Int32.Parse(slotNumberTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                e.Cancel = (dataValue < 1 Or dataValue > 10)
            Else
                e.Cancel = True
            End If
        End Sub

#End Region

#Region " SRQ "

        Private Sub enableServiceRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableServiceRequestCheckBox.CheckedChanged
            If MyBase.Visible AndAlso enableServiceRequestCheckBox.Enabled Then
                Me.Instrument.ClearStatus()
                Me.Instrument.ToggleServiceRequest(enableServiceRequestCheckBox.Checked, CType(serviceRequestMaskTextBox.Text, ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskAddButton.Click

            Dim selectedFlag As ServiceRequests = CType(serviceRequestFlagsComboBox.SelectedItem, ServiceRequests)
            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte Or selectedFlag)
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskRemoveButton.Click

            Dim selectedFlag As ServiceRequests = CType(serviceRequestFlagsComboBox.SelectedItem, ServiceRequests)
            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte And (Not selectedFlag))
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub enableEndOfSettlingRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableEndOfSettlingRequestCheckBox.CheckedChanged
            If MyBase.Visible AndAlso enableEndOfSettlingRequestCheckBox.Enabled Then
                Me.Instrument.ToggleEndOfScanService(enableEndOfSettlingRequestCheckBox.Checked, CType(serviceRequestMaskTextBox.Text, ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles serviceRequestMaskTextBox.Validating
            Dim data As String = serviceRequestMaskTextBox.Text.Trim
            If (data.Length = 0) OrElse (Not isr.Visa.Text.Helper.IsNumber(data)) Then
                serviceRequestMaskTextBox.Text = "0"
                e.Cancel = True
                Return
            Else
                Dim value As Int32 = Convert.ToInt32(data, Globalization.CultureInfo.CurrentCulture)
                If value < 0 Or value > 255 Then
                    serviceRequestMaskTextBox.Text = "0"
                    e.Cancel = True
                    Return
                End If
            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace
