Namespace Scpi

    ''' <summary>Implements a Visa interface to a Keithley 24XX instrument.</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="11/07/05" by="David Hary" revision="1.0.2137.x">
    ''' Create from the K24xx User COntrol.
    ''' </history>
    Public Class K2400

        ' based on the SCPI instrument class
        Inherits isr.Visa.Scpi.Instrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

            ' instantiate the reading elements.
            _readingElements = New K2400Readings()

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If _readingElements IsNot Nothing Then
                            _readingElements.Dispose()
                            _readingElements = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " SCPI EVENT FLAGES "

        ''' <summary>Gets or sets the status byte flags of the measurement event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum MeasurementEvents
            None = 0
            Limit1Failed = 1
            LowLimit2Failed = 2
            HighLimit2Failed = 4
            LowLimit3Failed = 8
            HighLimit3Failed = 16
            LimitsPassed = 32
            ReadingAvailable = 64
            ReadingOverflow = 128
            BufferAvailable = 256
            BufferFull = 512
            ContactCheckFailed = 1024
            InterlockAsserted = 2048
            OverTemperature = 4096
            OvervoltageProtection = 8192
            Compliance = 16384
            NotUsed = 32768
            All = 65535
        End Enum

        ''' <summary>Gets or sets the status byte flags of the operation event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum OperationEvents
            None = 0
            Calibrating = 1
            Sweeping = 8
            WaitingForTrigger = 32
            WaitingForArm = 64
            Idle = 1024
            'All = 2047
        End Enum

        ''' <summary>Gets or sets the status byte flags of the questionable event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum QuestionableEvents
            None = 0
            CalibrationEvent = 256
            CommandWarning = 16384
            'All = 32767
        End Enum

#End Region

#Region " PUBLIC TYPES "

        ''' <summary>Gets or sets all the readings the instrument is capable off.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum Elements
            Voltage = 1
            Current = 2
            Resistance = 4
            Time = 8
            Status = 16
        End Enum

        ''' <summary>Gets or sets the units corresponding to the readings.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum ReadingUnits
            Volts = 1
            Amperes = 2
            Ohms = 4
            Seconds = 8
            Unity = 16
        End Enum

        ''' <summary>Board included in the instrument.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum InstrumentBoards
            None = 0
            Analog = 1
            Digital = 2
            ContactCheck = 4
        End Enum

#End Region

#Region " METHODS "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If MyBase.Connect(resourceName) Then

                MyBase.LogMessage("connected", TraceEventType.Verbose)

            Else

                MyBase.LogMessage("Failed connecting.", TraceEventType.Warning)

            End If

            Return MyBase.IsConnected

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property Gui() As K2400Panel
            Get
                Return CType(MyBase.BaseGui, K2400Panel)
            End Get
            Set(ByVal Value As K2400Panel)
                MyBase.BaseGui = Value
            End Set
        End Property

#End Region

#Region " K2400 METHODS "

        ''' <summary>
        ''' Cleares the measurment status and event registers.
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub ClearStatus()
            MyBase.ClearStatus()
            _measurementDone = False
        End Sub

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision with the following items:
        '''   <see cref="FirmwareLevelMajorRevision">firmware level major revision</see> 
        '''   <see cref="FirmwareLevelDate">date</see>
        '''   <see cref="FirmwareLevelMinorRevision">firmware level minor revision</see> 
        '''   <see cref="BoardRevisions">board revisions</see>
        '''   e.g., <c>C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.  
        '''   The last three letters separated by "/" indicate the board revisions 
        '''   (i.e. /Analog/Digital/Contact Check). Contact Check board revisions have the following features:<p>
        '''     Revisions A through C have only one resistance range.</p><p>
        '''     Revisions D and above have selectable resistance ranges.</p><p>
        '''   Using the *opt? command this method also checks if the instrument supports 
        '''   contact check.  The command returns 'CONTACT-CHECK' (sans quotes) if the 
        '''   instrument does.  In addition, this method sets the properties 
        '''   <see cref="Options"/>, <see cref="SupportsContactCheck"/></p></param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Overrides Sub ParseFirmwareRevision(ByVal revision As String)

            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If

            MyBase.ParseFirmwareRevision(revision)

            ' get the revision sections
            Dim revisionSections() As String = revision.Split("/"c)

            ' Rev: C11; revision: Oct 10 1997 09:51:36
            Dim firmwareLevel As String = revisionSections(0)
            _firmwareLevelMajorRevision = isr.Visa.Text.Helper.RetrieveToken(firmwareLevel, " ")

            ' date string: Oct 10 1997 09:51:36
            _firmwareLevelDate = Date.Parse(firmwareLevel.Trim, Globalization.CultureInfo.CurrentCulture)

            ' Minor revision: A02
            _firmwareLevelMinorRevision = revisionSections(1).Trim

            ' set board revisions collection
            _boardRevisions = New System.Collections.Specialized.StringDictionary

            If revisionSections.Length > 2 Then
                _boardRevisions.Add(InstrumentBoards.Analog.ToString, revisionSections(2))
                If revisionSections.Length > 3 Then
                    _boardRevisions.Add(InstrumentBoards.Digital.ToString, revisionSections(3))
                    If revisionSections.Length > 4 Then
                        _boardRevisions.Add(InstrumentBoards.ContactCheck.ToString, revisionSections(4))
                        ' if we have a contact check board then check if we have options
                        _options = MyBase.QueryTrimEnd("*OPT?")
                        ' check if we have contact check selected
                        If _options Is Nothing Then
                            _supportsContactCheck = False
                        Else
                            _supportsContactCheck = _options.ToLower(Globalization.CultureInfo.CurrentCulture).IndexOf("contact-check") >= 0
                        End If
                    Else
                        _options = String.Empty
                        _supportsContactCheck = False
                    End If
                End If
            End If

        End Sub

        ''' <summary>Parses the latest data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        Public Sub ParseReadingElements()

            ' check if we have a fixed or multi mode data
            If (MyBase.Source.FunctionMode = SourceFunctionMode.VOLT AndAlso MyBase.Source.VoltageMode = SourceMode.Fixed) _
                OrElse (MyBase.Source.FunctionMode = SourceFunctionMode.CURR) Then

                ' parse the latest data
                ParseReadingElements(MyBase.Sense.LatestData())

            Else

                ' fetch the data
                Dim lastFetch As String = MyBase.Sense.FetchData()

                ' parse the results of all data sets.
                If MyBase.Source.FunctionMode = SourceFunctionMode.CURR Then
                    Me._readings = isr.Visa.Scpi.K2400Readings.ParseMulti(_readingElements, lastFetch, MyBase.Sense.VoltageLimit)
                ElseIf MyBase.Source.FunctionMode = SourceFunctionMode.VOLT Then
                    Me._readings = isr.Visa.Scpi.K2400Readings.ParseMulti(_readingElements, lastFetch, MyBase.Sense.CurrentLimit)
                End If

                If Me._readings.Length > 0 Then
                    ' select the final set as the valid readings.
                    _readingElements = New isr.Visa.Scpi.K2400Readings(Me._readings(Me._readings.Length - 1))
                Else
                    _readingElements.Reset()
                    _readingElements.MetaOutcome.IsValid = False
                End If

            End If

        End Sub

        ''' <summary>Parses the provided data setting the thresholds for testing for level 
        '''   compliance based on the sense current or voltage limits.</summary>
        ''' <param name="data">Specifies the measurement text to parse into the new reading.</param>
        Public Sub ParseReadingElements(ByVal data As String)

            ' parse the results
            If MyBase.Source.FunctionMode = SourceFunctionMode.CURR Then
                Me.ParseReadingElements(data, MyBase.Sense.VoltageLimit)
            ElseIf MyBase.Source.FunctionMode = SourceFunctionMode.VOLT Then
                Me.ParseReadingElements(data, MyBase.Sense.CurrentLimit)
            End If

        End Sub

        ''' <summary>Parses a new set of reading elements.</summary>
        ''' <param name="data">Specifies the measurement text to parse into the new reading.</param>
        ''' <param name="complianceLimit">Specifies the voltage or current limit for
        '''   compliance.</param>
        Public Sub ParseReadingElements(ByVal data As String, ByVal complianceLimit As Double)

            ' Take a reading and parse the results
            _readingElements.Parse(data, complianceLimit)

            If MyBase.Visible Then
                If Me.ReadingElements.StatusReading.IsHitCompliance Then

                    Me.Gui.PushMessage("Real Compliance.  Instrument sensed an output overflow of the measured value.")
                    Me.Gui.DisplayMessage("Instrument hit compliance")

                ElseIf Me.ReadingElements.StatusReading.IsHitRangeCompliance Then

                    Me.Gui.PushMessage("Range Compliance.  Instrument sensed an output overflow of the measured value.")
                    Me.Gui.DisplayMessage("Instrument hit range compliance")

                ElseIf Me.ReadingElements.IsHitLevelCompliance Then

                    Me.Gui.PushMessage("Level Compliance.  Instrument sensed an output overflow of the measured value.")
                    Me.Gui.DisplayMessage("Instrument hit level compliance")

                Else

                    Me.Gui.PushMessage("Parsed elements.")
                    Me.Gui.DisplayMessage("Instrument parsed data")

                End If

                ' update the display.
                Me.Gui.DisplaySelectedReading(True)

            End If

        End Sub

        ''' <summary>Take a single measurement and parse the result.</summary>
        ''' <remarks>This read uses Output Auto Clear.</remarks>
        Public Sub Read()

            ' set device to auto clear
            MyBase.Source.AutoClear = True

            ' disable service request on measurements
            StatusSubsystem.MeasurementEventEnable((MyBase.GpibSession)) = MeasurementEvents.None

            ' clear the status register and prepare for the service request
            MyBase.ClearStatus()

            ' Take a reading and parse the results
            Me.ParseReadingElements(MyBase.Sense.Read())

        End Sub

        ''' <summary>Sets the measurement service status registers to request a service
        '''   request upon measurement events.</summary>
        Public Sub RequestMeasurementService()
            MyBase.ClearStatus()
            StatusSubsystem.MeasurementEventEnable(MyBase.GpibSession) = MeasurementEvents.All
            MyBase.ServiceRequestEventEnable() = ServiceRequests.All _
              And (Not ServiceRequests.MessageAvailable)
        End Sub

        ''' <summary>Returns the device to its default known state by issuing the *RST 
        '''   IEEE488.2 common command.</summary>
        Public Overloads Overrides Sub ResetToKnownState()

            MyBase.ResetToKnownState()

            MyBase.Sense.ResetToKnownState()
            MyBase.Source.ResetToKnownState()

            ' upgrade: use AccessCache to set default properties in Sense and Source
            ' system that are special to this instrument.
            If Me.Gui.Visible Then

                Me.Gui.concurrentCheckBox.Checked = True
                Me.Gui.senseAutoRangeCheckBox.Checked = True

            End If

        End Sub

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    If MyBase.IsOpc Then

                        ' clear the messages from the error queue
                        Scpi.SystemSubsystem.Clear(MyBase.GpibSession)

                        ' initialized the memory
                        Scpi.SystemSubsystem.InitializeMemory(MyBase.GpibSession)

                        ' set device to auto clear
                        MyBase.Source.AutoClear = True

                        ' allow operations to complete
                        Return MyBase.IsOpc

                    End If

                Catch ex As NationalInstruments.VisaNS.VisaException

                    StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                End Try

            End If

            Return False

        End Function

#End Region

#Region " K2400 PROPERTIES "

        Private _firmwareLevelMajorRevision As String
        ''' <summary>Returns the instrument firmware level major revision name .</summary>
        Public ReadOnly Property FirmwareLevelMajorRevision() As String
            Get
                Return _firmwareLevelMajorRevision
            End Get
        End Property

        Private _firmwareLevelMinorRevision As String
        ''' <summary>Returns the instrument firmware level minor revision name .</summary>
        Public ReadOnly Property FirmwareLevelMinorRevision() As String
            Get
                Return _firmwareLevelMinorRevision
            End Get
        End Property

        Private _firmwareLevelDate As Date
        ''' <summary>Returns the instrument firmware level date.</summary>
        Public ReadOnly Property FirmwareLevelDate() As Date
            Get
                Return _firmwareLevelDate
            End Get
        End Property

        Private _boardRevisions As System.Collections.Specialized.StringDictionary
        ''' <summary>Returns the list of board revisions.</summary>
        Public ReadOnly Property BoardRevisions() As System.Collections.Specialized.StringDictionary
            Get
                Return _boardRevisions
            End Get
        End Property

        Private _measurementDone As Boolean
        ''' <summary>
        ''' Gets the measurement done state.  With a 2-point sweep we get two service requests both
        ''' with complete data in the buffer.  With this flag we process these only once.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MeasurementDone() As Boolean
            Get
                Return _measurementDone
            End Get
        End Property

        ''' <summary>
        ''' Gets or sets an array of readings.
        ''' </summary>
        ''' <remarks></remarks>
        Private _readings As isr.Visa.Scpi.K2400Readings()

        ''' <summary>
        ''' Returns the readings.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Readings() As isr.Visa.Scpi.K2400Readings()
            Return _readings
        End Function

        Private _readingElements As K2400Readings
        ''' <summary>
        ''' Returns the readings from the instrument.</summary>
        Public Function ReadingElements() As K2400Readings
            Return _readingElements
        End Function

        Private _supportsContactCheck As Boolean
        ''' <summary>Returns True if the instrument supprots contat check.</summary>
        Public ReadOnly Property SupportsContactCheck() As Boolean
            Get
                Return _supportsContactCheck
            End Get
        End Property

        Private _options As String
        ''' <summary>Returns the instrument firmware options.</summary>
        Public ReadOnly Property Options() As String
            Get
                Return _options
            End Get
        End Property

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Updates the screen if we have a new measurement and raises the service
        '''   request event. New measurements are parsed including setting the threshold
        '''   levels for testing for level compliance.
        ''' Note that on a two point sweep we get to service request.  The first service request
        ''' Applears befroe the second reading is on screen but already has the values for both sweeps.
        ''' </summary>
        ''' <param name="e">Passes reference to the <see cref="Scpi.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As Scpi.ServiceEventArgs) ' System.EventArgs)

            If Not e.HasError Then

                If (e.MeasurementEventStatus And MeasurementEvents.ReadingAvailable) <> 0 Then
                    If Not _measurementDone Then
                        'Debug.WriteLine("M: " & e.MeasurementEventStatus & "; S: " & e.ServiceRequestStatus _
                        '  & "; O: " & e.OperationEventStatus & "; Q: " & e.QuestionableEventStatus _
                        '  & "; SRQ: " & e.StandardEventStatus & "; Done: " & e.OperationCompleted)
                        ' parse the results
                        Me.ParseReadingElements()
                        _measurementDone = True
                    End If
                End If

            End If

            ' raise the service request event
            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace
