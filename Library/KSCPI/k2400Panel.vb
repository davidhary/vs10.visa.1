Namespace Scpi

    ''' <summary>Provides a user interface for the Keithley 24XX instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("Keithley 2400 Instrument Family - Windows Forms Custom Control")> _
    Public Class K2400Panel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            onInstantiate()

        End Sub

        ''' <summary>Initializes additional components.</summary>
        ''' <remarks>Called from the class constructor to add other controls or 
        '''   change anything set by the InitializeComponent method.</remarks>
        Private Function onInstantiate() As Boolean

            Try

                ' instantiate the base instrument and set reference to the control.
                MyBase.ResourceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib

                ' instantiate the reference to the instrument
                Me._instrument = New K2400(Me.Name)
                MyBase.BaseInstrument = Me._instrument

                ' set the refernece to the GUI
                Me._instrument.Gui = Me

                Me.modalityComboBox.DataSource = Nothing
                Me.modalityComboBox.Items.Clear()
                Me.modalityComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.K2400.Elements))

                '        For Each pair As isr.Support.Collections.EnumPair In elements
                '        Me.modalityComboBox.Items.Add(pair.Name)
                '        Next
                If Me.modalityComboBox.Items.Count > 0 Then
                    Me.modalityComboBox.SelectedIndex = 0
                End If
                sourceFunctionComboBox.SelectedIndex = 0
                Return True

            Catch ex As isr.Visa.BaseException

                ' throw an exception
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed initializing", Me.Name)
                Throw New isr.Visa.BaseException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Updates the user interface upon successful connection.
        ''' </summary>
        Public Overrides Function Connect() As Boolean

            ' enable user interface
            Me.mainTabControl.Enabled = True
            populateModalitiesSelector()
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "SCPI Version: {0:0.0###}", Me._instrument.ScpiSystem.ScpiRevision))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Manufacturer name: {0}", Me._instrument.GpibSession.ResourceManufacturerName))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Manufacturer ID: 0x{0:X}", Me._instrument.GpibSession.ResourceManufacturerID))
            Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "Connectred to {0}", Me._instrument.Id()))

            Return MyBase.Connect()

        End Function

        ''' <summary>
        ''' Updates the user interface upon disconnection.
        ''' </summary>
        Public Overrides Function Disconnect() As Boolean

            Me.mainTabControl.Enabled = False
            Return MyBase.Disconnect()

        End Function

        ''' <summary>Display a status message.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub DisplayMessage(ByVal message As String)

            MyBase.DisplayMessage(message)
            Me.PushMessage(message)

        End Sub

        ''' <summary>Adds a message to the message list.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub PushMessage(ByVal message As String)
            messagesMessageList.PushMessage(message)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.Scpi.K2400
        ''' <summary>Gets a reference to the Keithley 2400 instrument.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        <System.ComponentModel.Browsable(False)> Public ReadOnly Property Instrument() As K2400
            Get
                Return _instrument
            End Get
        End Property

#End Region

#Region " K2400 METHODS AND PROPERTIES "

        ''' <summary>Applies the selected sense settings.</summary>
        Private Sub applySense()

            If Me.Instrument.GpibSession IsNot Nothing AndAlso Me.Instrument.Sense IsNot Nothing Then

                Me.Instrument.ClearStatus()

                ' get the Integration Period
                Dim integrationPeriod As Double = 0.001 * Double.Parse(Me.integrationPeriodTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                Me.Instrument.Sense.CurrentIntegrationPeriod = integrationPeriod
                Me.Instrument.Sense.VoltageIntegrationPeriod = integrationPeriod
                Me.Instrument.Sense.ResistanceIntegrationPeriod = integrationPeriod

                If Me.manualOhmsModeCheckBox.Checked Then
                    Me.Instrument.Write(":SENS:RES:MODE MAN")
                Else
                    Me.Instrument.Write(":SENS:RES:MODE AUTO")
                End If

                If Me.fourWireSenseCheckBox.Checked Then
                    Me.Instrument.ScpiSystem.SenseMode = SenseMode.FourWire
                Else
                    Me.Instrument.ScpiSystem.SenseMode = SenseMode.TwoWire
                End If

                If Me.allSenseFunctionsCheckBox.Checked Then

                    Me.Instrument.Sense.FunctionMode = SenseFunctionModes.CURR_DC Or SenseFunctionModes.RES Or SenseFunctionModes.VOLT_DC

                Else

                    Me.Instrument.Sense.FunctionMode = SenseFunctionModes.CURR_DC

                End If

                Me.Instrument.Sense.IsFunctionConcurrent = Me.concurrentCheckBox.Checked

                If Me.concurrentCheckBox.Checked Then

                    If Me.senseAutoRangeCheckBox.Checked Then

                        Me.Instrument.Sense.AutoVoltageRange = True
                        Me.Instrument.Sense.AutoCurrentRange = True
                        Me.Instrument.Sense.AutoResistanceRange = True

                    Else

                        Me.Instrument.Sense.AutoVoltageRange = False
                        Me.Instrument.Sense.AutoCurrentRange = False
                        Me.Instrument.Sense.AutoResistanceRange = False

                    End If

                End If

            End If
        End Sub

        ''' <summary>Applies the selected source level and limits.</summary>
        Private Sub applySource()

            If Me.Instrument.GpibSession IsNot Nothing AndAlso Me.Instrument.Source IsNot Nothing Then

                Me.Instrument.ClearStatus()

                Select Case sourceFunctionComboBox.Text

                    Case "I"

                        Me.Instrument.Source.FunctionMode = SourceFunctionMode.CURR
                        Me.Instrument.Source.CurrentLevel = Double.Parse(Me.sourceLevelTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                        Me.Instrument.Sense.VoltageLimit = Double.Parse(Me.sourceLimitTextBox.Text, Globalization.CultureInfo.CurrentCulture)

                    Case "V"

                        Me.Instrument.Source.FunctionMode = SourceFunctionMode.VOLT
                        Me.Instrument.Source.VoltageLevel = Double.Parse(Me.sourceLevelTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                        Me.Instrument.Sense.CurrentLimit = Double.Parse(Me.sourceLimitTextBox.Text, Globalization.CultureInfo.CurrentCulture)

                End Select

                If Me.sourceDelayTextBox.Text.Length > 0 AndAlso isr.Visa.Text.Helper.IsNumber(Me.sourceDelayTextBox.Text) Then
                    Me.Instrument.Source.Delay = Double.Parse(Me.sourceDelayTextBox.Text, Globalization.CultureInfo.CurrentCulture)
                ElseIf Me.sourceDelayTextBox.Text.Length = 0 Then
                    Me.Instrument.Source.AutoDelay = True
                End If

            End If

        End Sub

        ''' <summary>List reading elements and enables source functions.
        ''' </summary>
        Private Sub populateModalitiesSelector()

            Me.modalityComboBox.DataSource = Nothing
            Me.modalityComboBox.Items.Clear()
            Me.modalityComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.K2400.Elements))
            If Me.modalityComboBox.Items.Count > 0 Then
                Me.modalityComboBox.SelectedIndex = 0
            End If
            sourceFunctionComboBox.SelectedIndex = 0

        End Sub

        ''' <summary>Update the display of the source settings.</summary>
        Private Sub refreshSourceSettings()

            Select Case sourceFunctionComboBox.Text
                Case "I"
                    Me.sourceLevelTextBoxLabel.Text = "Level [A]"
                    tipsToolTip.SetToolTip(Me.sourceLevelTextBox, "Enter current level in Amperes")
                    Me.sourceLimitTextBoxLabel.Text = "Limit [V]"
                    tipsToolTip.SetToolTip(Me.sourceLimitTextBox, "Enter voltage limit in Volts")
                    If Me.Instrument.GpibSession IsNot Nothing AndAlso Me.Instrument.Source IsNot Nothing _
                        AndAlso Me.Instrument.Sense IsNot Nothing Then
                        Me.sourceLevelTextBox.Text = Me.Instrument.Source.CurrentLevel.ToString(Globalization.CultureInfo.CurrentCulture)
                        Me.sourceLimitTextBox.Text = Me.Instrument.Sense.VoltageLimit.ToString(Globalization.CultureInfo.CurrentCulture)
                    End If
                Case "V"
                    Me.sourceLevelTextBoxLabel.Text = "Level [V]"
                    tipsToolTip.SetToolTip(Me.sourceLevelTextBox, "Enter voltage level in Volts")
                    Me.sourceLimitTextBoxLabel.Text = "Limit [A]"
                    tipsToolTip.SetToolTip(Me.sourceLimitTextBox, "Enter current limit in Amperes")
                    If Me.Instrument.GpibSession IsNot Nothing AndAlso Me.Instrument.Source IsNot Nothing _
                        AndAlso Me.Instrument.Sense IsNot Nothing Then

                        Me.sourceLevelTextBox.Text = Me.Instrument.Source.VoltageLevel.ToString(Globalization.CultureInfo.CurrentCulture)
                        Me.sourceLimitTextBox.Text = Me.Instrument.Sense.CurrentLimit.ToString(Globalization.CultureInfo.CurrentCulture)
                    End If
            End Select

        End Sub

        ''' <summary>Displays a reading based on the selected reading to display.</summary>
        Private Sub _displaySelectedReading()

            If Me.Instrument.GpibSession IsNot Nothing _
              AndAlso Me.Instrument.Sense IsNot Nothing _
              AndAlso Me.Instrument.ReadingElements IsNot Nothing Then

                'Dim selectedItem As isr.Support.Collections.EnumPair
                'Dim selectedModality As ReadingElements
                'selectedItem = CType(modalityComboBox.SelectedItem, isr.Support.Collections.EnumPair)
                'selectedModality = CType(selectedItem.Value, ReadingElements)
                Dim readingElement As Scpi.K2400.Elements = CType([Enum].Parse(GetType(Scpi.K2400.Elements), modalityComboBox.Text, True), Scpi.K2400.Elements)
                Select Case readingElement
                    Case Scpi.K2400.Elements.Current
                        readingLabel.Text = Me.Instrument.ReadingElements.CurrentReading.ToString()
                    Case Scpi.K2400.Elements.Resistance
                        readingLabel.Text = Me.Instrument.ReadingElements.ResistanceReading.ToString()
                    Case Scpi.K2400.Elements.Status
                        readingLabel.Text = Me.Instrument.ReadingElements.StatusReading.ToString()
                    Case Scpi.K2400.Elements.Time
                        readingLabel.Text = Me.Instrument.ReadingElements.TimestampReading.ToString()
                    Case Scpi.K2400.Elements.Voltage
                        readingLabel.Text = Me.Instrument.ReadingElements.VoltageReading.ToString()
                End Select
            End If

        End Sub

        ''' <summary>Displays a reading based on the selected reading to display.</summary>
        ''' <param name="useSafeThread">True to invoke a safe thread display of the new reading.</param>
        ''' <remarks></remarks>
        Public Sub DisplaySelectedReading(ByVal useSafeThread As Boolean)
            If useSafeThread Then
                addDisplayElement(DisplayElement.Reading)
            Else
                _displaySelectedReading()
            End If
        End Sub

#End Region

#Region " THREAD SAFE DISPLAY "

        ''' <summary>
        ''' Enumrates which elements to update using the thread safe update method.
        ''' </summary>
        ''' <remarks></remarks>
        Private Enum DisplayElement
            None
            Reading
        End Enum

        ''' <summary>
        ''' The background worker is used for setting the messages text box
        ''' in a thread safe way. 
        ''' </summary>
        ''' <remarks></remarks>
        Private WithEvents worker As System.ComponentModel.BackgroundWorker

        ''' <summary>
        ''' This event handler sets the Text property of the TextBox control. It is called on the 
        ''' thread that created the TextBox control, so the call is thread-safe.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

            If Not (e.Cancelled OrElse e.Error IsNot Nothing) Then
                invokeDisplayQueuedElements()
            End If

        End Sub

        ''' <summary>
        ''' Executed on the worker thread and makes a thread-safe call on the 
        ''' user interface.
        ''' </summary>
        ''' <remarks>
        ''' If the calling thread is different from the thread that created the 
        ''' TextBox control, this method creates a SetTextCallback and calls 
        ''' itself asynchronously using the Invoke method.
        ''' </remarks>
        Private Sub invokeDisplayQueuedElements()

            ' check if the creating and calling threads are different
            If MyBase.InvokeRequired Then
                ' if so, invoke a new thread.
                Dim d As New displayQueuedElementsCallback(AddressOf displayQueuedElements)
                MyBase.Invoke(d, New Object() {})
            Else
                Me.displayQueuedElements()
            End If

        End Sub

        ''' <summary>
        ''' Enables asynchronous calls for handling updates of the elements.
        ''' </summary>
        Private Delegate Sub displayQueuedElementsCallback()

        ''' <summary>
        ''' Holds the queue of display elements.  The queue gets filled with display requests each time
        ''' the worker is busy.
        ''' </summary>
        ''' <remarks></remarks>
        Private displayElementQueue As New System.Collections.Generic.Queue(Of DisplayElement)

        ''' <summary>
        ''' Displays all the messages that were accumulated in the messages queue.
        ''' </summary>
        Private Sub displayQueuedElements()

            Do While Me.displayElementQueue IsNot Nothing AndAlso Me.displayElementQueue.Count > 0

                Dim element As DisplayElement = Me.displayElementQueue.Dequeue
                Select Case element
                    Case DisplayElement.Reading
                        Me._displaySelectedReading()
                    Case DisplayElement.None
                    Case Else
                End Select
                Windows.Forms.Application.DoEvents()

            Loop

        End Sub

        ''' <summary>
        ''' Adds an element to the display requests and run the worker to display.
        ''' </summary>
        ''' <param name="element">True to add the message or false to ignore.</param>
        ''' <remarks></remarks>
        Private Sub addDisplayElement(ByVal element As DisplayElement)

            If displayElementQueue Is Nothing Then
                displayElementQueue = New System.Collections.Generic.Queue(Of DisplayElement)()
            End If

            If worker Is Nothing Then
                worker = New System.ComponentModel.BackgroundWorker()
            End If

            ' add an item to the queue
            Me.displayElementQueue.Enqueue(element)

            If Not Me.worker.IsBusy Then
                ' Start the form's Background Worker to display the queued messages.
                Me.worker.RunWorkerAsync()
            End If
            Windows.Forms.Application.DoEvents()
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub applySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applySenseSettingsButton.Click
            applySense()
        End Sub

        Private Sub applySourceSettingButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles applySourceSettingButton.Click
            applySource()
        End Sub

        ''' <summary>Initiates a reading for retrieval by way of the service request event.</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub initButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles initButton.Click

            ' set the service request
            Me.Instrument.RequestMeasurementService()

            ' set output to auto clear
            Me.Instrument.Source.AutoClear = True

            ' trigger the initiation of the measurement letting the service request do the rest.
            Me.Instrument.ClearStatus()
            Me.Instrument.Trigger.Initiate()

        End Sub

        Private Sub interfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles interfaceClearButton.Click
            Me.Instrument.GpibInterface.SendInterfaceClear()
        End Sub

        ''' <summary>Selects a new reading to display.</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub modalityComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles modalityComboBox.SelectedIndexChanged

            If modalityComboBox.Enabled _
                AndAlso modalityComboBox.SelectedIndex >= 0 _
                AndAlso Not String.IsNullOrEmpty(modalityComboBox.Text) Then
                Me._displaySelectedReading()
            End If

        End Sub

        ''' <summary>Query the instrument for a reading.</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Me.Instrument.Read()
            Me._displaySelectedReading()
            ' check if we are in sense auto mode
            '      If MyBase.Sense.AutoCurrentRange Then
            '     me.PushMessage("Sense in Auto Range")
            '    Else
            '     me.PushMessage("Sense in Manual Range")
            '  End If
        End Sub

        Private Sub sourceFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sourceFunctionComboBox.SelectedIndexChanged
            refreshSourceSettings()
        End Sub

        ''' <summary>Toggles the terminals</summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub terminalsToggleButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles terminalsToggleButton.CheckedChanged
            If terminalsToggleButton.Checked Then
                RouteSubsystem.TerminalMode(Me.Instrument.GpibSession) = RouteTerminalMode.REAR
                terminalsToggleButton.Text = RouteTerminalMode.FRONT.ToString
            Else
                RouteSubsystem.TerminalMode(Me.Instrument.GpibSession) = RouteTerminalMode.FRONT
                terminalsToggleButton.Text = RouteTerminalMode.REAR.ToString
            End If
        End Sub

#End Region

    End Class

End Namespace
