Namespace Scpi

    ''' <summary>Implements a Visa interface to a Keithley 70XX instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class K7000

        ' based on the instrument class
        Inherits Instrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " SCPI EVENT FLAGS "

        ''' <summary>Gets or sets the status byte flags of the operation event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum OperationEvents
            None = 0
            Settling = 2
            WaitingForTrigger = 32
            WaitingForArm = 64
            Idle = 1024
            'All = 2047
        End Enum

        ''' <summary>Gets or sets the status byte flags of the operation transition event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum OperationTransitionEvents
            None = 0
            Settling = 2
            WaitingForTrigger = 32
            WaitingForArm = 64
            Idle = 1024
            'All = 2047
        End Enum

#End Region

#Region " METHODS "

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If MyBase.Connect(resourceName) Then

                MyBase.LogMessage("connected", TraceEventType.Verbose)

            Else

                MyBase.LogMessage("Failed connecting.", TraceEventType.Warning)

            End If

            Return MyBase.IsConnected

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property Gui() As K7000Panel
            Get
                Return CType(MyBase.BaseGui, K7000Panel)
            End Get
            Set(ByVal Value As K7000Panel)
                MyBase.BaseGui = Value
            End Set
        End Property

#End Region

#Region " K7000 METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' allow operations to complete
                    Return MyBase.IsOpc

                Catch ex As NationalInstruments.VisaNS.VisaException

                    StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                End Try

            End If

            Return False

        End Function

        ''' <summary>Sets the scan list and refreshes the display.</summary>
        ''' <param name="scanList"></param>
        Public Function SetScanList(ByVal scanList As String) As Boolean

            If MyBase.Route IsNot Nothing Then

                MyBase.ClearStatus()

                ' set the scan list
                MyBase.Route.ScanList = scanList
                Me.Gui.refreshDisplay()

            End If

            Return Not MyBase.HadError

        End Function

#End Region

#Region " K7000 PROPERTIES "

        ''' <summary>Returns true if the last operation has settled as signified by the status of the
        '''   operation condition settling flag.</summary>
        Public ReadOnly Property OperationSettled() As Boolean
            Get
                Return (MyBase.LastServiceEventArgs.OperationCondition _
                          And OperationEvents.Settling) = 0
            End Get
        End Property

        ''' <summary>Returns true if the operation is settling assignified by the status of the
        '''   operation condition settling flag.</summary>
        Public ReadOnly Property OperationSettling() As Boolean
            Get
                Return (MyBase.LastServiceEventArgs.OperationCondition _
                            And OperationEvents.Settling) <> 0
            End Get
        End Property

        ''' <summary>Enables or disables the operation service status registers requesting
        '''   a service request upon a negative transition.</summary>
        Public Sub ToggleEndOfScanService(ByVal isOn As Boolean)
            ToggleEndOfScanService(isOn, ServiceRequests.All _
                And (Not ServiceRequests.MessageAvailable))
        End Sub

        ''' <summary>Enables or disables the operation service status registers requesting
        '''   a service request upon a negative transition.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="ServiceRequests">service request flags</see></param>
        Public Sub ToggleEndOfScanService(ByVal turnOn As Boolean, ByVal serviceRequestMask As ServiceRequests)
            MyBase.ClearStatus()
            If turnOn Then
                StatusSubsystem.OperationNegativeTransitionEventEnable(MyBase.GpibSession) = _
                    OperationTransitionEvents.Settling
                StatusSubsystem.OperationPositiveTransitionEventEnable(MyBase.GpibSession) = _
                    OperationTransitionEvents.None
                StatusSubsystem.OperationEventEnable(MyBase.GpibSession) = OperationEvents.Settling
            Else
                StatusSubsystem.OperationNegativeTransitionEventEnable(MyBase.GpibSession) = _
                    OperationTransitionEvents.None
                StatusSubsystem.OperationPositiveTransitionEventEnable(MyBase.GpibSession) = _
                    OperationTransitionEvents.None
                StatusSubsystem.OperationEventEnable(MyBase.GpibSession) = OperationEvents.None
            End If
            ToggleServiceRequest(turnOn, serviceRequestMask)
            If MyBase.Visible Then
                Me.Gui.serviceRequestMaskTextBox.Text = serviceRequestMask.ToString()
                Me.Gui.enableEndOfSettlingRequestCheckBox.Enabled = False
                Me.Gui.enableEndOfSettlingRequestCheckBox.Checked = turnOn
                With Me.Gui.enableEndOfSettlingRequestCheckBox
                    ' needed to work correctly with visual styles provider
                    .Enabled = True
                    .Visible = True
                    .Invalidate()
                End With

            End If
        End Sub

        ''' <summary>Enables or disables service requests.</summary>
        ''' <param name="turnOn">True to turn on or false to turn off the service request.</param>
        ''' <param name="serviceRequestMask">Specifies the 
        '''   <see cref="ServiceRequests">service request flags</see></param>
        Public Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As ServiceRequests)
            MyBase.GpibSession.ServiceRequestEventEnable() = serviceRequestMask
            If MyBase.Visible Then
                Me.Gui.serviceRequestMaskTextBox.Text = serviceRequestMask.ToString()
                Me.Gui.enableServiceRequestCheckBox.Enabled = False
                Me.Gui.enableServiceRequestCheckBox.Checked = turnOn
                With Me.Gui.enableServiceRequestCheckBox
                    ' needed to work correctly with visual styles provider
                    .Enabled = True
                    .Visible = True
                    .Invalidate()
                End With
            End If
        End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="Scpi.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As Scpi.ServiceEventArgs) '  System.EventArgs)

            If e.HasError Then

            Else

                ' check if we have an operation request
                If (e.OperationEventStatus And OperationEvents.Settling) <> 0 Then

                    ' read the operation register condition
                    e.OperationCondition = StatusSubsystem.OperationEventCondition(MyBase.GpibSession)

                    If Me.Gui.Visible Then
                        ' display the operation duration
                        '         MyBase.DisplayInfoProvider("New message available under the Messages tab")
                        If Me.OperationSettled Then
                            Me.Gui.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                              "Operation settled in {0}ms", _
                              e.OperationElapsedTime.TotalMilliseconds))
                            Me.Gui.DisplayMessage("Operation settled.")
                        ElseIf Me.OperationSettling Then
                            Me.Gui.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                              "Operation started settling in {0}ms", _
                              e.OperationElapsedTime.TotalMilliseconds))
                            Me.Gui.DisplayMessage("Operation settling.")
                        End If
                    End If
                End If

            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class

End Namespace
