Namespace Scpi

    ''' <summary>
    ''' Defines a <see cref="System.Int32">Status</see> reading.
    ''' </summary>
    ''' <license>
    ''' (c) 2005 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
    ''' Created
    ''' </history>
    Public Class K2400Status
        Inherits ReadingBase

#Region " TYPES "

        ''' <summary>Gets or sets the status bits of a source measure status word.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum K2400StatusWordBits
            None = 0
            ''' <summary>Measurement was made while in over-range</summary>
            OverRange = CInt(2 ^ 0)
            FilterEnabled = CInt(2 ^ 1)
            FrontTerminals = CInt(2 ^ 2)
            HitCompliance = CInt(2 ^ 3)
            HitVoltageProtection = CInt(2 ^ 4)
            MathExpressionEnabled = CInt(2 ^ 5)
            NullEnabled = CInt(2 ^ 6)
            LimitsEnabled = CInt(2 ^ 7)
            LimitResultBit0 = CInt(2 ^ 8)
            LimitResultBit1 = CInt(2 ^ 9)
            AutoOhmsEnabled = CInt(2 ^ 10)
            VoltageMeasureEnabled = CInt(2 ^ 11)
            CurrentMeasureEnabled = CInt(2 ^ 12)
            ResistanceMeasureEnabled = CInt(2 ^ 13)
            VoltageSourceUsed = CInt(2 ^ 14)
            CurrentSourceUsed = CInt(2 ^ 15)
            HitRangeCompliance = CInt(2 ^ 16)
            OffsetCompensationOhmsEnabled = CInt(2 ^ 17)
            FailedContactCheck = CInt(2 ^ 18)
            LimitResultBit2 = CInt(2 ^ 19)
            LimitResultBit3 = CInt(2 ^ 20)
            LimitResultBit4 = CInt(2 ^ 21) '1048576
            FourWireEnabled = CInt(2 ^ 22)
            InPulseMode = CInt(2 ^ 23)
        End Enum

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs a measured value</summary>
        Public Sub New()
            MyBase.New()
            Me._displayCaption = New ReadingCaption(Of Int64)
            Me._saveCaption = New ReadingCaption(Of Int64)
            Me._displayCaption.Format = "X"
            Me._displayCaption.Units = "Hex"
            Me._saveCaption.Format = "0"
        End Sub

        ''' <summary>Constructs a measured value</summary>
        ''' <param name="newValue">A <see cref="System.Int32">Double</see> value</param>
        Public Sub New(ByVal newValue As Int32)
            Me.new(newValue, True)
        End Sub

        ''' <summary>Constructs a measured value</summary>
        ''' <param name="newValue">A <see cref="System.Int32">Int32</see> value</param>
        ''' <param name="valid">The value auto settings mode</param>
        Public Sub New(ByVal newValue As Int32, ByVal valid As Boolean)
            Me.new()
            SetValue(newValue, valid)
        End Sub

        ''' <summary>Constructs a measured value</summary>
        ''' <param name="model">model</param>
        Public Sub New(ByVal model As K2400Status)
            Me.new()
            Me._displayCaption = New ReadingCaption(Of Int64)(model._displayCaption)
            Me._saveCaption = New ReadingCaption(Of Int64)(model._saveCaption)
            Me._value = model._value
        End Sub

#End Region

#Region " GENERAL METHODS  and  PROPERTIES "

        Private _displayCaption As ReadingCaption(Of Int64)
        ''' <summary>
        ''' Holds the caption for saving values to file.
        ''' </summary>
        Public ReadOnly Property DisplayCaption() As ReadingCaption(Of Int64)
            Get
                Return _displayCaption
            End Get
        End Property

        ''' <summary>
        ''' Parses the reading to create the specific reading type in the inherited class.
        ''' </summary>
        ''' <param name="reading">Specifies the reading text.</param>
        Protected Overloads Overrides Function Parse(ByVal reading As String) As Boolean
            ' convert reading to numeric
            If Int64.TryParse(reading, Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, Me._value) Then
                Me.SetValue(_value, True)
            End If
        End Function

        ''' <summary>Resets measured value to given value.
        ''' </summary>
        ''' <param name="newValue">The new value/</param>
        Public Overloads Sub Reset(ByVal newValue As Int64)
            MyBase.Reset()
            _value = newValue
        End Sub

        Private _saveCaption As ReadingCaption(Of Int64)
        ''' <summary>
        ''' Holds the caption for saving values to file.
        ''' </summary>
        Public ReadOnly Property SaveCaption() As ReadingCaption(Of Int64)
            Get
                Return _saveCaption
            End Get
        End Property

        ''' <summary>Sets the measured value and it validity.</summary>
        ''' <param name="newValue">A <see cref="System.Double">Double</see> value</param>
        ''' <param name="valid">The value auto settings mode</param>
        Public Sub SetValue(ByVal newValue As Int64, ByVal valid As Boolean)
            MyBase.MetaOutcome.IsValid = valid
            _value = newValue
            Me.SaveCaption.ScaledValue = _value
            Me.DisplayCaption.ScaledValue = _value
        End Sub

        ''' <summary>Returns the value to save using the <see cref="SaveCaption"/>.</summary>
        Public Overrides Function ToSave() As String
            Return Me.SaveCaption.ToString()
        End Function

        ''' <summary>Returns the default string representation of the range.</summary>
        Public Overloads Overrides Function ToString() As String
            Return Me.DisplayCaption.ToString()
        End Function

        Private _value As Int64
        ''' <summary>Gets or sets the numeric value for this class.</summary>
        ''' <value>A <see cref="System.int64">double precision</see> value.</value>
        Public ReadOnly Property Value() As Int64
            Get
                Return _value
            End Get
        End Property

#End Region

#Region " SPECIFIC METHODS  and  PROPERTIES "

        ''' <summary>Returns an outcome string depending on the measured outcome or pass code.</summary>
        Public ReadOnly Property LimitResults() As String
            Get

                Select Case Me.K2400Status And _
                    (K2400StatusWordBits.LimitResultBit0 Or K2400StatusWordBits.LimitResultBit1)

                    Case K2400StatusWordBits.LimitResultBit1

                        Return "F1"

                    Case K2400StatusWordBits.LimitResultBit2

                        Return "F2"

                    Case K2400StatusWordBits.LimitResultBit1 _
                            Or K2400StatusWordBits.LimitResultBit2

                        Return "F3"

                    Case Else

                        ' 1.11.14 display P and not...
                        Return "P"

                End Select

            End Get
        End Property

        ''' <summary>
        ''' Holds true if source-measure uses a current source.
        ''' </summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsCurrentSource() As Boolean
            Get
                Return (Me.K2400Status And K2400StatusWordBits.CurrentSourceUsed) <> 0
            End Get
        End Property

        ''' <summary>Gets or sets the condition for hit real compliance.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsHitCompliance() As Boolean
            Get
                Return (Me.K2400Status And K2400StatusWordBits.HitCompliance) <> 0
            End Get
        End Property

        ''' <summary>Gets or sets the condition for hit range compliance.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsHitRangeCompliance() As Boolean
            Get
                Return (Me.K2400Status And K2400StatusWordBits.HitRangeCompliance) <> 0
            End Get
        End Property

        ''' <summary>Gets or sets the condition for source-measure uses a voltage source.</summary>
        ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
        Public ReadOnly Property IsVoltageSource() As Boolean
            Get
                Return (Me.K2400Status And K2400StatusWordBits.VoltageSourceUsed) <> 0
            End Get
        End Property

        ''' <summary>Gets or sets the numeric value of the Keithley 2400 source-measure units.</summary>
        ''' <value>A <see cref="K2400StatusWordBits">Source Measure Status Word Flags</see> value.</value>
        Public ReadOnly Property K2400Status() As K2400StatusWordBits
            Get
                Return CType(_value, K2400StatusWordBits)
            End Get
        End Property

#End Region

    End Class

End Namespace
