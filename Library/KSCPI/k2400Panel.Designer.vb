Namespace Scpi

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class K2400Panel
        Inherits isr.Visa.InstrumentPanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try

        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.mainTabControl = New System.Windows.Forms.TabControl
            Me.readingTabPage = New System.Windows.Forms.TabPage
            Me.terminalsToggleButton = New System.Windows.Forms.CheckBox
            Me.modalityComboBox = New System.Windows.Forms.ComboBox
            Me.readingLabel = New System.Windows.Forms.Label
            Me.readButton = New System.Windows.Forms.Button
            Me.initButton = New System.Windows.Forms.Button
            Me.interfaceTabPage = New System.Windows.Forms.TabPage
            Me.interfaceClearButton = New System.Windows.Forms.Button
            Me.sourceTabPage = New System.Windows.Forms.TabPage
            Me.sourceDelayTextBox = New System.Windows.Forms.TextBox
            Me.sourceDelayTextBoxLabel = New System.Windows.Forms.Label
            Me.applySourceSettingButton = New System.Windows.Forms.Button
            Me.sourceLevelTextBoxLabel = New System.Windows.Forms.Label
            Me.sourceFunctionComboBox = New System.Windows.Forms.ComboBox
            Me.sourceLevelTextBox = New System.Windows.Forms.TextBox
            Me.sourceLimitTextBox = New System.Windows.Forms.TextBox
            Me.sourceLimitTextBoxLabel = New System.Windows.Forms.Label
            Me.sourceFunctionComboBoxLabel = New System.Windows.Forms.Label
            Me.senseTabPage = New System.Windows.Forms.TabPage
            Me.integrationPeriodTextBox = New System.Windows.Forms.TextBox
            Me.IntegrationTimeLabel = New System.Windows.Forms.Label
            Me.manualOhmsModeCheckBox = New System.Windows.Forms.CheckBox
            Me.fourWireSenseCheckBox = New System.Windows.Forms.CheckBox
            Me.allSenseFunctionsCheckBox = New System.Windows.Forms.CheckBox
            Me.senseAutoRangeCheckBox = New System.Windows.Forms.CheckBox
            Me.concurrentCheckBox = New System.Windows.Forms.CheckBox
            Me.applySenseSettingsButton = New System.Windows.Forms.Button
            Me.messagesTabPage = New System.Windows.Forms.TabPage
            Me.messagesMessageList = New isr.Visa.WinForms.MessagesBox
            Me.mainTabControl.SuspendLayout()
            Me.readingTabPage.SuspendLayout()
            Me.interfaceTabPage.SuspendLayout()
            Me.sourceTabPage.SuspendLayout()
            Me.senseTabPage.SuspendLayout()
            Me.messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'mainTabControl
            '
            Me.mainTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom
            Me.mainTabControl.Controls.Add(Me.readingTabPage)
            Me.mainTabControl.Controls.Add(Me.interfaceTabPage)
            Me.mainTabControl.Controls.Add(Me.sourceTabPage)
            Me.mainTabControl.Controls.Add(Me.senseTabPage)
            Me.mainTabControl.Controls.Add(Me.messagesTabPage)
            Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.mainTabControl.Enabled = False
            Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
            Me.mainTabControl.Multiline = True
            Me.mainTabControl.Name = "mainTabControl"
            Me.mainTabControl.SelectedIndex = 0
            Me.mainTabControl.Size = New System.Drawing.Size(312, 238)
            Me.mainTabControl.TabIndex = 15
            '
            'readingTabPage
            '
            Me.readingTabPage.Controls.Add(Me.terminalsToggleButton)
            Me.readingTabPage.Controls.Add(Me.modalityComboBox)
            Me.readingTabPage.Controls.Add(Me.readingLabel)
            Me.readingTabPage.Controls.Add(Me.readButton)
            Me.readingTabPage.Controls.Add(Me.initButton)
            Me.readingTabPage.Location = New System.Drawing.Point(4, 4)
            Me.readingTabPage.Name = "readingTabPage"
            Me.readingTabPage.Size = New System.Drawing.Size(304, 212)
            Me.readingTabPage.TabIndex = 0
            Me.readingTabPage.Text = "Reading"
            Me.readingTabPage.UseVisualStyleBackColor = True
            '
            'terminalsToggleButton
            '
            Me.terminalsToggleButton.Appearance = System.Windows.Forms.Appearance.Button
            Me.terminalsToggleButton.Location = New System.Drawing.Point(232, 152)
            Me.terminalsToggleButton.Name = "terminalsToggleButton"
            Me.terminalsToggleButton.Size = New System.Drawing.Size(57, 24)
            Me.terminalsToggleButton.TabIndex = 3
            Me.terminalsToggleButton.Text = "REAR"
            Me.terminalsToggleButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'modalityComboBox
            '
            Me.modalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.modalityComboBox.Location = New System.Drawing.Point(200, 56)
            Me.modalityComboBox.Name = "modalityComboBox"
            Me.modalityComboBox.Size = New System.Drawing.Size(96, 21)
            Me.modalityComboBox.TabIndex = 1
            '
            'readingLabel
            '
            Me.readingLabel.BackColor = System.Drawing.SystemColors.ControlText
            Me.readingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.readingLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
            Me.readingLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.readingLabel.ForeColor = System.Drawing.Color.Aquamarine
            Me.readingLabel.Location = New System.Drawing.Point(8, 16)
            Me.readingLabel.Name = "readingLabel"
            Me.readingLabel.Size = New System.Drawing.Size(288, 36)
            Me.readingLabel.TabIndex = 2
            Me.readingLabel.Text = "0.0000000 mV"
            Me.readingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'readButton
            '
            Me.readButton.Location = New System.Drawing.Point(8, 56)
            Me.readButton.Name = "readButton"
            Me.readButton.Size = New System.Drawing.Size(50, 23)
            Me.readButton.TabIndex = 0
            Me.readButton.Text = "&Read"
            '
            'initButton
            '
            Me.initButton.Location = New System.Drawing.Point(127, 56)
            Me.initButton.Name = "initButton"
            Me.initButton.Size = New System.Drawing.Size(50, 23)
            Me.initButton.TabIndex = 0
            Me.initButton.Text = "&Initiate"
            '
            'interfaceTabPage
            '
            Me.interfaceTabPage.Controls.Add(Me.interfaceClearButton)
            Me.interfaceTabPage.Location = New System.Drawing.Point(4, 4)
            Me.interfaceTabPage.Name = "interfaceTabPage"
            Me.interfaceTabPage.Size = New System.Drawing.Size(304, 212)
            Me.interfaceTabPage.TabIndex = 2
            Me.interfaceTabPage.Text = "Interface"
            Me.interfaceTabPage.UseVisualStyleBackColor = True
            '
            'interfaceClearButton
            '
            Me.interfaceClearButton.Location = New System.Drawing.Point(127, 100)
            Me.interfaceClearButton.Name = "interfaceClearButton"
            Me.interfaceClearButton.Size = New System.Drawing.Size(50, 23)
            Me.interfaceClearButton.TabIndex = 0
            Me.interfaceClearButton.Text = "&Clear"
            '
            'sourceTabPage
            '
            Me.sourceTabPage.Controls.Add(Me.sourceDelayTextBox)
            Me.sourceTabPage.Controls.Add(Me.sourceDelayTextBoxLabel)
            Me.sourceTabPage.Controls.Add(Me.applySourceSettingButton)
            Me.sourceTabPage.Controls.Add(Me.sourceLevelTextBoxLabel)
            Me.sourceTabPage.Controls.Add(Me.sourceFunctionComboBox)
            Me.sourceTabPage.Controls.Add(Me.sourceLevelTextBox)
            Me.sourceTabPage.Controls.Add(Me.sourceLimitTextBox)
            Me.sourceTabPage.Controls.Add(Me.sourceLimitTextBoxLabel)
            Me.sourceTabPage.Controls.Add(Me.sourceFunctionComboBoxLabel)
            Me.sourceTabPage.Location = New System.Drawing.Point(4, 4)
            Me.sourceTabPage.Name = "sourceTabPage"
            Me.sourceTabPage.Size = New System.Drawing.Size(304, 212)
            Me.sourceTabPage.TabIndex = 1
            Me.sourceTabPage.Text = "Source"
            Me.sourceTabPage.UseVisualStyleBackColor = True
            '
            'sourceDelayTextBox
            '
            Me.sourceDelayTextBox.Location = New System.Drawing.Point(80, 56)
            Me.sourceDelayTextBox.Name = "sourceDelayTextBox"
            Me.sourceDelayTextBox.Size = New System.Drawing.Size(52, 20)
            Me.sourceDelayTextBox.TabIndex = 8
            '
            'sourceDelayTextBoxLabel
            '
            Me.sourceDelayTextBoxLabel.Location = New System.Drawing.Point(16, 56)
            Me.sourceDelayTextBoxLabel.Name = "sourceDelayTextBoxLabel"
            Me.sourceDelayTextBoxLabel.Size = New System.Drawing.Size(64, 16)
            Me.sourceDelayTextBoxLabel.TabIndex = 7
            Me.sourceDelayTextBoxLabel.Text = "Delay [s]: "
            Me.sourceDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'applySourceSettingButton
            '
            Me.applySourceSettingButton.Location = New System.Drawing.Point(224, 107)
            Me.applySourceSettingButton.Name = "applySourceSettingButton"
            Me.applySourceSettingButton.Size = New System.Drawing.Size(50, 23)
            Me.applySourceSettingButton.TabIndex = 6
            Me.applySourceSettingButton.Text = "&Apply"
            '
            'sourceLevelTextBoxLabel
            '
            Me.sourceLevelTextBoxLabel.Location = New System.Drawing.Point(31, 92)
            Me.sourceLevelTextBoxLabel.Name = "sourceLevelTextBoxLabel"
            Me.sourceLevelTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.sourceLevelTextBoxLabel.TabIndex = 0
            Me.sourceLevelTextBoxLabel.Text = "Level: "
            '
            'sourceFunctionComboBox
            '
            Me.sourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.sourceFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
            Me.sourceFunctionComboBox.Location = New System.Drawing.Point(143, 108)
            Me.sourceFunctionComboBox.Name = "sourceFunctionComboBox"
            Me.sourceFunctionComboBox.Size = New System.Drawing.Size(73, 21)
            Me.sourceFunctionComboBox.TabIndex = 5
            '
            'sourceLevelTextBox
            '
            Me.sourceLevelTextBox.Location = New System.Drawing.Point(31, 108)
            Me.sourceLevelTextBox.Name = "sourceLevelTextBox"
            Me.sourceLevelTextBox.Size = New System.Drawing.Size(52, 20)
            Me.sourceLevelTextBox.TabIndex = 1
            Me.sourceLevelTextBox.Text = "0.05"
            '
            'sourceLimitTextBox
            '
            Me.sourceLimitTextBox.Location = New System.Drawing.Point(87, 108)
            Me.sourceLimitTextBox.Name = "sourceLimitTextBox"
            Me.sourceLimitTextBox.Size = New System.Drawing.Size(52, 20)
            Me.sourceLimitTextBox.TabIndex = 3
            Me.sourceLimitTextBox.Text = "5.00"
            '
            'sourceLimitTextBoxLabel
            '
            Me.sourceLimitTextBoxLabel.Location = New System.Drawing.Point(87, 92)
            Me.sourceLimitTextBoxLabel.Name = "sourceLimitTextBoxLabel"
            Me.sourceLimitTextBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.sourceLimitTextBoxLabel.TabIndex = 2
            Me.sourceLimitTextBoxLabel.Text = "Limit: "
            '
            'sourceFunctionComboBoxLabel
            '
            Me.sourceFunctionComboBoxLabel.Location = New System.Drawing.Point(143, 92)
            Me.sourceFunctionComboBoxLabel.Name = "sourceFunctionComboBoxLabel"
            Me.sourceFunctionComboBoxLabel.Size = New System.Drawing.Size(48, 16)
            Me.sourceFunctionComboBoxLabel.TabIndex = 4
            Me.sourceFunctionComboBoxLabel.Text = "Function: "
            '
            'senseTabPage
            '
            Me.senseTabPage.Controls.Add(Me.integrationPeriodTextBox)
            Me.senseTabPage.Controls.Add(Me.IntegrationTimeLabel)
            Me.senseTabPage.Controls.Add(Me.manualOhmsModeCheckBox)
            Me.senseTabPage.Controls.Add(Me.fourWireSenseCheckBox)
            Me.senseTabPage.Controls.Add(Me.allSenseFunctionsCheckBox)
            Me.senseTabPage.Controls.Add(Me.senseAutoRangeCheckBox)
            Me.senseTabPage.Controls.Add(Me.concurrentCheckBox)
            Me.senseTabPage.Controls.Add(Me.applySenseSettingsButton)
            Me.senseTabPage.Location = New System.Drawing.Point(4, 4)
            Me.senseTabPage.Name = "senseTabPage"
            Me.senseTabPage.Size = New System.Drawing.Size(304, 212)
            Me.senseTabPage.TabIndex = 4
            Me.senseTabPage.Text = "Sense"
            Me.senseTabPage.UseVisualStyleBackColor = True
            '
            'integrationPeriodTextBox
            '
            Me.integrationPeriodTextBox.Location = New System.Drawing.Point(48, 24)
            Me.integrationPeriodTextBox.Name = "integrationPeriodTextBox"
            Me.integrationPeriodTextBox.Size = New System.Drawing.Size(52, 20)
            Me.integrationPeriodTextBox.TabIndex = 20
            Me.integrationPeriodTextBox.Text = "16"
            '
            'IntegrationTimeLabel
            '
            Me.IntegrationTimeLabel.Location = New System.Drawing.Point(48, 8)
            Me.IntegrationTimeLabel.Name = "IntegrationTimeLabel"
            Me.IntegrationTimeLabel.Size = New System.Drawing.Size(112, 16)
            Me.IntegrationTimeLabel.TabIndex = 19
            Me.IntegrationTimeLabel.Text = "Integration Period [ms] "
            '
            'manualOhmsModeCheckBox
            '
            Me.manualOhmsModeCheckBox.Location = New System.Drawing.Point(48, 56)
            Me.manualOhmsModeCheckBox.Name = "manualOhmsModeCheckBox"
            Me.manualOhmsModeCheckBox.Size = New System.Drawing.Size(160, 16)
            Me.manualOhmsModeCheckBox.TabIndex = 18
            Me.manualOhmsModeCheckBox.Text = "Manual Ohms Mode"
            '
            'fourWireSenseCheckBox
            '
            Me.fourWireSenseCheckBox.Location = New System.Drawing.Point(48, 80)
            Me.fourWireSenseCheckBox.Name = "fourWireSenseCheckBox"
            Me.fourWireSenseCheckBox.Size = New System.Drawing.Size(160, 16)
            Me.fourWireSenseCheckBox.TabIndex = 17
            Me.fourWireSenseCheckBox.Text = "Four-Wire Sense"
            '
            'allSenseFunctionsCheckBox
            '
            Me.allSenseFunctionsCheckBox.Location = New System.Drawing.Point(48, 104)
            Me.allSenseFunctionsCheckBox.Name = "allSenseFunctionsCheckBox"
            Me.allSenseFunctionsCheckBox.Size = New System.Drawing.Size(160, 16)
            Me.allSenseFunctionsCheckBox.TabIndex = 16
            Me.allSenseFunctionsCheckBox.Text = "All Functions On"
            '
            'senseAutoRangeCheckBox
            '
            Me.senseAutoRangeCheckBox.Location = New System.Drawing.Point(48, 152)
            Me.senseAutoRangeCheckBox.Name = "senseAutoRangeCheckBox"
            Me.senseAutoRangeCheckBox.Size = New System.Drawing.Size(88, 16)
            Me.senseAutoRangeCheckBox.TabIndex = 15
            Me.senseAutoRangeCheckBox.Text = "Auto Range"
            '
            'concurrentCheckBox
            '
            Me.concurrentCheckBox.Location = New System.Drawing.Point(48, 128)
            Me.concurrentCheckBox.Name = "concurrentCheckBox"
            Me.concurrentCheckBox.Size = New System.Drawing.Size(160, 16)
            Me.concurrentCheckBox.TabIndex = 14
            Me.concurrentCheckBox.Text = "Concurrent"
            '
            'applySenseSettingsButton
            '
            Me.applySenseSettingsButton.Location = New System.Drawing.Point(232, 160)
            Me.applySenseSettingsButton.Name = "applySenseSettingsButton"
            Me.applySenseSettingsButton.Size = New System.Drawing.Size(50, 23)
            Me.applySenseSettingsButton.TabIndex = 13
            Me.applySenseSettingsButton.Text = "&Apply"
            '
            'messagesTabPage
            '
            Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
            Me.messagesTabPage.Location = New System.Drawing.Point(4, 4)
            Me.messagesTabPage.Name = "messagesTabPage"
            Me.messagesTabPage.Size = New System.Drawing.Size(304, 212)
            Me.messagesTabPage.TabIndex = 3
            Me.messagesTabPage.Text = "Messages"
            Me.messagesTabPage.UseVisualStyleBackColor = True
            '
            'messagesMessageList
            '
            Me.messagesMessageList.BackColor = System.Drawing.SystemColors.Info
            Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
            Me.messagesMessageList.Multiline = True
            Me.messagesMessageList.Name = "messagesMessageList"
            Me.messagesMessageList.NewMessagesCount = 0
            Me.messagesMessageList.PresetCount = 50
            Me.messagesMessageList.ReadOnly = True
            Me.messagesMessageList.ResetCount = 100
            Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.messagesMessageList.Size = New System.Drawing.Size(304, 212)
            Me.messagesMessageList.TabIndex = 0
            Me.messagesMessageList.TimeFormat = "HH:mm:ss. "
            '
            'K2400Panel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.mainTabControl)
            Me.Name = "K2400Panel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.mainTabControl, 0)
            Me.mainTabControl.ResumeLayout(False)
            Me.readingTabPage.ResumeLayout(False)
            Me.interfaceTabPage.ResumeLayout(False)
            Me.sourceTabPage.ResumeLayout(False)
            Me.sourceTabPage.PerformLayout()
            Me.senseTabPage.ResumeLayout(False)
            Me.senseTabPage.PerformLayout()
            Me.messagesTabPage.ResumeLayout(False)
            Me.messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
        Friend WithEvents readingTabPage As System.Windows.Forms.TabPage
        Friend WithEvents terminalsToggleButton As System.Windows.Forms.CheckBox
        Friend WithEvents modalityComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents readingLabel As System.Windows.Forms.Label
        Friend WithEvents readButton As System.Windows.Forms.Button
        Friend WithEvents initButton As System.Windows.Forms.Button
        Friend WithEvents interfaceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents interfaceClearButton As System.Windows.Forms.Button
        Friend WithEvents sourceTabPage As System.Windows.Forms.TabPage
        Friend WithEvents sourceDelayTextBox As System.Windows.Forms.TextBox
        Friend WithEvents sourceDelayTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents applySourceSettingButton As System.Windows.Forms.Button
        Friend WithEvents sourceLevelTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents sourceFunctionComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents sourceLevelTextBox As System.Windows.Forms.TextBox
        Friend WithEvents sourceLimitTextBox As System.Windows.Forms.TextBox
        Friend WithEvents sourceLimitTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents sourceFunctionComboBoxLabel As System.Windows.Forms.Label
        Friend WithEvents senseTabPage As System.Windows.Forms.TabPage
        Friend WithEvents integrationPeriodTextBox As System.Windows.Forms.TextBox
        Friend WithEvents IntegrationTimeLabel As System.Windows.Forms.Label
        Friend WithEvents manualOhmsModeCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents fourWireSenseCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents allSenseFunctionsCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents senseAutoRangeCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents concurrentCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents applySenseSettingsButton As System.Windows.Forms.Button
        Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents messagesMessageList As isr.Visa.WinForms.MessagesBox

    End Class

End Namespace
