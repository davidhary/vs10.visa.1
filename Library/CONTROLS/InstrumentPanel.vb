Imports NationalInstruments

''' <summary>Provides a user interface for a generic VISA instrument.</summary>
''' <license>
''' (c) 2006 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
''' Created
''' </history>
<System.ComponentModel.Description("Generic Instrument - Windows Forms Custom Control")> _
  Public Class InstrumentPanel

#If False Then
  ' Use inheritable class when editing the derived controls.
  <System.ComponentModel.Description("Generic Instrument - Windows Forms Custom Control")> _
  Public MustInherit Class InstrumentPanel

  <System.ComponentModel.Description("Generic Instrument - Windows Forms Custom Control")> _
  Public Class InstrumentPanel
#End If

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Protected Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Opens a VISA session for the instruments at the given address.
    ''' </summary>
    Public Overridable Function Connect() As Boolean

        If String.IsNullOrEmpty(Me._baseInstrument.InstanceName) Then
            Me.idPanel.Text = String.Empty
        Else
            Me.idPanel.Text = isr.Visa.Text.Helper.SafeSubstring(Me._baseInstrument.InstanceName, 0, 16)
            Me.idPanel.ToolTipText = Me._baseInstrument.InstanceName
        End If
        _statusMessage = "Connected to " & Me._baseInstrument.InstanceName
        Me.DisplayMessage(_statusMessage)
        Me.connector.IsConnected = True
        Return Me.connector.IsConnected

    End Function

    ''' <summary>Disconnect from the instrument.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    Public Overridable Function Disconnect() As Boolean

        Me.idPanel.Text = String.Empty
        _statusMessage = "Disconnected"
        Me.DisplayMessage(_statusMessage)
        Me.connector.IsConnected = False
        Return Not Me.connector.IsConnected

    End Function

    ''' <summary>Display a status message.</summary>
    ''' <param name="message">Specifies the message to add.</param>
    Public Overridable Sub DisplayMessage(ByVal message As String)

        If Not String.IsNullOrEmpty(message) Then
            If message.Length < 50 Then
                Me._statusMessage = message
                Me.AddDisplayElement(DisplayElement.StatusPanel)
            End If
        End If

    End Sub

    ''' <summary>Adds a message to the message list.</summary>
    ''' <param name="message">Specifies the message to add.</param>
    Public Overridable Sub PushMessage(ByVal message As String)
    End Sub

#End Region

#Region " THREAD SAFE DISPLAY "

    ''' <summary>
    ''' Enumrates which elements to update using the thread safe update method.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum DisplayElement
        None
        StatusPanel
    End Enum

    ''' <summary>
    ''' The background worker is used for setting the messages text box
    ''' in a thread safe way. 
    ''' </summary>
    ''' <remarks></remarks>
    Private WithEvents worker As System.ComponentModel.BackgroundWorker

    ''' <summary>
    ''' This event handler sets the Text property of the TextBox control. It is called on the 
    ''' thread that created the TextBox control, so the call is thread-safe.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles worker.RunWorkerCompleted

        If Not (e.Cancelled OrElse e.Error IsNot Nothing) Then
            invokeDisplayQueuedElements()
        End If

    End Sub

    ''' <summary>
    ''' Executed on the worker thread and makes a thread-safe call on the 
    ''' user interface.
    ''' </summary>
    ''' <remarks>
    ''' If the calling thread is different from the thread that created the 
    ''' TextBox control, this method creates a SetTextCallback and calls 
    ''' itself asynchronously using the Invoke method.
    ''' </remarks>
    Private Sub invokeDisplayQueuedElements()

        ' check if the creating and calling threads are different
        If MyBase.InvokeRequired Then
            ' if so, invoke a new thread.
            Dim d As New displayQueuedElementsCallback(AddressOf DisplayQueuedElements)
            MyBase.Invoke(d, New Object() {})
        Else
            Me.DisplayQueuedElements()
        End If

    End Sub

    ''' <summary>
    ''' Enables asynchronous calls for handling updates of the elements.
    ''' </summary>
    Private Delegate Sub displayQueuedElementsCallback()

    ''' <summary>
    ''' Holds the queue of display elements.  The queue gets filled with display requests each time
    ''' the worker is busy.
    ''' </summary>
    ''' <remarks></remarks>
    Private displayElementQueue As System.Collections.Generic.Queue(Of DisplayElement)

    ''' <summary>
    ''' Displays all the messages that were accumulated in the messages queue.
    ''' </summary>
    Private Sub DisplayQueuedElements()

        Do While Me.displayElementQueue IsNot Nothing AndAlso Me.displayElementQueue.Count > 0

            Dim element As DisplayElement = Me.displayElementQueue.Dequeue
            Select Case element
                Case DisplayElement.StatusPanel
                    Me.statusPanel.Text = Me._statusMessage
                Case DisplayElement.None
                Case Else
            End Select
            Windows.Forms.Application.DoEvents()

        Loop

    End Sub

    ''' <summary>
    ''' Adds an element to the display requests and run the worker to display.
    ''' </summary>
    ''' <param name="element">True to add the message or false to ignore.</param>
    ''' <remarks></remarks>
    Private Sub AddDisplayElement(ByVal element As DisplayElement)

        If displayElementQueue Is Nothing Then
            displayElementQueue = New System.Collections.Generic.Queue(Of DisplayElement)()
        End If

        If worker Is Nothing Then
            worker = New System.ComponentModel.BackgroundWorker()
        End If

        ' add an item to the queue
        Me.displayElementQueue.Enqueue(element)
        If Not Me.worker.IsBusy Then
            ' Start the form's Background Worker to display the queued messages.
            Me.worker.RunWorkerAsync()
        End If
        Windows.Forms.Application.DoEvents()
    End Sub

#End Region

#Region " PROPERTIES "

    Private _baseInstrument As IO.ConnectLogBaseClass
    Protected Property BaseInstrument() As IO.ConnectLogBaseClass
        Get
            Return _baseInstrument
        End Get
        Set(ByVal Value As IO.ConnectLogBaseClass)
            _baseInstrument = Value
        End Set
    End Property

    ''' <summary>Returns true if the instrument is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected() As Boolean
        Get
            Return Me._baseInstrument IsNot Nothing AndAlso Me._baseInstrument.IsConnected
        End Get
    End Property

    Private _visible As Boolean = True
    ''' <summary>
    ''' Holds true if the control is visible.  This is used in place
    ''' of the control natural visibilty to control access to the 
    ''' control because it seems in VS 2005 the Visible property
    ''' may be true only if the control is both Visible and Active.
    ''' </summary>
    Public Property IsVisible() As Boolean
        Get
            Return _visible
        End Get
        Set(ByVal value As Boolean)
            _visible = value
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <System.ComponentModel.Browsable(False)> Public Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
        Set(ByVal value As String)
            _statusMessage = value
        End Set
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

        Me.statusPanel.Text = "Find and select a resource."
        Me.idPanel.Text = String.Empty

    End Sub

#End Region

#Region " CONNECTOR EVENT HANDLERS "

    Private _resourceType As NationalInstruments.VisaNS.HardwareInterfaceType
    ''' <summary>Gets or sets the interface type.</summary>
    Public Property ResourceType() As NationalInstruments.VisaNS.HardwareInterfaceType
        Get
            Return _resourceType
        End Get
        Set(ByVal Value As NationalInstruments.VisaNS.HardwareInterfaceType)
            _resourceType = Value
        End Set
    End Property

    ''' <summary>Display the resource names based on the last interface type.
    ''' </summary>
    Public Overloads Sub DisplayNames()
        Me.DisplayNames(Me._resourceType)
    End Sub

    ''' <summary>Displays the resources available to connect to.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    Public Overloads Sub DisplayNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType)

        Try

            ' get the list of available resources
            Dim names() As String = Helper.LocalResourceNames(resourceType)
            Me.connector.DisplayNames(names)

            If names Is Nothing Then

                ' send a verbose message.
                _statusMessage = "Resource names are empty"
                Me.PushMessage(_statusMessage)

            ElseIf names.Length = 0 Then

                ' send a verbose message.
                _statusMessage = "Resources not found"
                Me.PushMessage(_statusMessage)

            Else

                ' send a verbose message.
                _statusMessage = "Resource names listed"
                Me.PushMessage(_statusMessage)

            End If

        Catch ex As NationalInstruments.VisaNS.VisaException

            Me.DisplayMessage("Error occurred")
            _statusMessage = "Failed finding resources.  Connect the resources(s) and click Find."
            Me.DisplayMessage(_statusMessage)

        Catch ex As System.ArgumentException

            Me.DisplayMessage("Error occurred")
            _statusMessage = "Failed finding resources.  Connect the resources(s) and click Find."
            Me.DisplayMessage(_statusMessage)

            If Not TypeOf (ex.InnerException) Is NationalInstruments.VisaNS.VisaException Then
                ' if we have a visa exception, we have no interfaces.
                ' otherwise, throw an exception
                Throw
            End If

        End Try

    End Sub

    ''' <summary>Displays the resources available to connect to.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    ''' <param name="boardNumber">Specifies the board number.</param>
    Public Overloads Sub DisplayNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType, _
        ByVal boardNumber As Integer)

        Try

            ' get the list of available interfaces on the specified board.
            Me.connector.DisplayNames(Helper.LocalResourceNames(resourceType, boardNumber))

        Catch ex As NationalInstruments.VisaNS.VisaException

            Me.DisplayMessage("Error occurred")
            Me.StatusMessage = "Failed finding resources.  Connect the resources(s) and click Find."
            Me.DisplayMessage(Me.StatusMessage)

        Catch ex As System.ArgumentException

            Me.DisplayMessage("Error occurred")
            Me.StatusMessage = "Failed finding resources.  Connect the resources(s) and click Find."
            Me.DisplayMessage(Me.StatusMessage)

            If Not TypeOf (ex.InnerException) Is NationalInstruments.VisaNS.VisaException Then
                ' if we have a visa exception, we have no interfaces.
                ' otherwise, throw an exception
                Throw
            End If

        End Try

    End Sub

    ''' <summary>
    ''' Clears the instrument by calling a propagating clear command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Clear
        Try
            Me._baseInstrument.Clear()
        Catch ex As Exception
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            InstrumentPanel.ProcessException(ex, "Exception occurred clearing", WinForms.ExceptionDisplayButtons.Continue)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Connects the instrument by calling a propagating connect command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Connect

        Dim resourcename As String = Me.connector.SelectedName
        Me.DisplayMessage("Connecting to " & resourcename)
        Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Try
            Me._baseInstrument.Connect(resourcename)
        Catch ex As Exception
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            InstrumentPanel.ProcessException(ex, "Exception occurred connecting", WinForms.ExceptionDisplayButtons.Continue)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Disconnects the instrument by calling a propagating disconnect command.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub connector_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Disconnect
        Try
            If Me._baseInstrument.IsConnected Then
                Me.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Disconnecting from {0}...", Me._baseInstrument.InstanceName))
            End If
            Me._baseInstrument.Disconnect()
        Catch ex As Exception
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            InstrumentPanel.ProcessException(ex, "Exception occurred disconnecting", WinForms.ExceptionDisplayButtons.Continue)
        Finally
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Displays available instrument names.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary>
    ''' Notifies that an instrument name was selected.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Selected(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Selected
        Me._statusMessage = "Selected interface " & connector.SelectedName
        Me.DisplayMessage(Me._statusMessage)
        Me.PushMessage(Me._statusMessage)
    End Sub

#End Region

#Region " EXCEPTION MANAGEMENT "

    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from UI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    ''' <param name="additionalInfo">Specifies additional log information.</param>
    ''' <param name="buttons">Specifies the buttons to show on the exception display.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, _
        ByVal buttons As WinForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

        Dim result As Windows.Forms.DialogResult
        Try

            ' log the exception
            Dim frm As New WinForms.ExceptionDisplay
            result = frm.ShowDialog(ex, buttons)
            My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "{0} requested by user.", result), TraceEventType.Verbose)

        Catch displayException As System.Exception

            ' Log but also display the error in a message box
            Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
            errorMessage.Append("The following error occured while displaying the application exception:")
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
            If Not String.IsNullOrEmpty(additionalInfo) Then
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Additional Info:{0}{1}", Environment.NewLine, additionalInfo)
            End If
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
            errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}Click Abort to exit application.  Otherwise, the aplication will continue.", Environment.NewLine)
            result = Windows.Forms.MessageBox.Show( _
                                                  errorMessage.ToString(), "Application Error", _
                                                  Windows.Forms.MessageBoxButtons.AbortRetryIgnore, Windows.Forms.MessageBoxIcon.Stop, _
                                                  Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

        End Try

        Return result

    End Function

#End Region

End Class
