Imports NationalInstruments

''' <summary>Implements a GPIB VISA Interface.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' Created
''' </history>
<System.ComponentModel.Description("GPIB Interface - Windows Forms Custom Control")> _
Public Class GpibInterfacePanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()

    End Sub

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
    Private Sub onDisposeManagedResources()

        If _gpibInterface IsNot Nothing Then
            _gpibInterface.Dispose()
            _gpibInterface = Nothing
        End If

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Opens a GPIB VISA interface for the specified resource.</summary>
    ''' <param name="boardNumber">Specifies the GPIB board name.</param>
    ''' <returns>Returns True if success or false if it failed opening the session.</returns>
    ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
    Public Function Connect(ByVal boardNumber As Int32) As Boolean

        Return Me.Connect(isr.Visa.Helper.BuildGpibInterfaceName(boardNumber))

    End Function

    ''' <summary>Opens a GPIB VISA interface for the specified resource.</summary>
    ''' <param name="resourceName">Specifies the interface resource name.</param>
    ''' <returns>Returns True if success or false if it failed opening the session.</returns>
    ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
    Public Function Connect(ByVal resourceName As String) As Boolean

        Try

            Me.DisplayMessage("Connecting to " & resourceName)
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor

            _gpibInterface = New isr.Visa.GpibInterface(resourceName)

            If _gpibInterface Is Nothing Then

                ' if failed, alert and release the connect button
                Me.connector.IsConnected = False

                Me.resourceChooser.Enabled = False
                With Me.resourceChooser
                    ' needed to work correctly with visual styles provider
                    .Enabled = False
                    .Visible = True
                    .Invalidate()
                End With
                Me.clearSelectedResourceButton.Enabled = False
                With Me.clearSelectedResourceButton
                    ' needed to work correctly with visual styles provider
                    .Enabled = False
                    .Visible = True
                    .Invalidate()
                End With
                Me.clearAllResourcesButton.Enabled = False
                With Me.clearAllResourcesButton
                    ' needed to work correctly with visual styles provider
                    .Enabled = False
                    .Visible = True
                    .Invalidate()
                End With

            Else

                Me.GpibInterface.Gui = Me

                ' connect the connector.
                Me.connector.IsConnected = True

                Dim message As String = "Interface IsConnected"
                Me.statusPanel.Text = message
                message = "Connected to " & resourceName
                Me.DisplayMessage(message)

                ' display the selected GPIB resources.
                Me.resourceChooser.Enabled = True
                With Me.resourceChooser
                    .Enabled = True
                    .Visible = True
                    .Invalidate()
                End With
                Me.resourceChooser.DisplayNames(isr.Visa.Helper.LocalResourceNames(VisaNS.HardwareInterfaceType.Gpib))

                message = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer name: {0}", _gpibInterface.ResourceManufacturerName)
                messagesMessageList.PushMessage(message)

                message = String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer ID: 0x{0:X}", _gpibInterface.ResourceManufacturerID)
                messagesMessageList.PushMessage(message)

            End If

        Catch ex As isr.Visa.BaseException

            ' close to meet strong guarantees
            Try
                Me.Disconnect()
            Finally
            End Try

            ' throw an exception
            Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed opening", resourceName)
            Me.statusPanel.Text = message
            messagesMessageList.PushMessage(message)

            Throw New isr.Visa.OperationOpenException(message, ex)

        Finally

            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

        End Try

        Return _gpibInterface IsNot Nothing

    End Function

    ''' <summary>Closes the instance.</summary>
    ''' <returns>A Boolean data type</returns>
    ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
    ''' <remarks>Use this method to close the instance.  The method returns true if success or 
    '''   false if it failed closing the instance.</remarks>
    Public Function Disconnect() As Boolean

        If _gpibInterface IsNot Nothing Then

            Me.DisplayMessage("Disconnecting from " & _gpibInterface.ResourceName)

            _gpibInterface.Dispose()
            _gpibInterface = Nothing
            Me.connector.IsConnected = False

        End If

        Me.DisplayMessage("Disconnected.")

        Me.resourceChooser.Enabled = False
        With Me.resourceChooser
            .Enabled = False
            .Visible = True
            .Invalidate()
        End With
        Me.clearSelectedResourceButton.Enabled = False
        With Me.clearSelectedResourceButton
            .Enabled = False
            .Visible = True
            .Invalidate()
        End With
        Me.clearAllResourcesButton.Enabled = False
        With Me.clearAllResourcesButton
            .Enabled = False
            .Visible = True
            .Invalidate()
        End With

    End Function

    ''' <summary>Adds a message to the message list.</summary>
    ''' <param name="message">Specifies the message to add.</param>
    Public Sub DisplayMessage(ByVal message As String)

        If Not String.IsNullOrEmpty(message) Then
            If message.Length < 50 Then
                Me.statusPanel.Text = message
            End If
            messagesMessageList.PushMessage(message)
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    Private _gpibInterface As isr.Visa.GpibInterface
    ''' <summary>Gets or sets reference to the Gpib interface for this panel.</summary>
    <System.ComponentModel.Browsable(False)> Public Property GpibInterface() As isr.Visa.GpibInterface
        Get
            Return _gpibInterface
        End Get
        Set(ByVal Value As isr.Visa.GpibInterface)
            _gpibInterface = Value
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <System.ComponentModel.Browsable(False)> Public Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
        Set(ByVal value As String)
            _statusMessage = value
        End Set
    End Property

    ''' <summary>Returns true if the interface is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected() As Boolean
        Get
            Return Me.connector.IsConnected AndAlso Me._gpibInterface IsNot Nothing
        End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

        Me.statusPanel.Text = "Find and select an interface."

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    Private Sub clearSelectedResourceButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles clearSelectedResourceButton.Click
        Me.GpibInterface.SelectiveDeviceClear(Me.resourceChooser.SelectedName)
    End Sub

    Private Sub deviceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearAllResourcesButton.Click
        ' Transmit the SDC command to the interface.
        Me.GpibInterface.DevicesClear()
    End Sub

    Private Sub resourceChooser_NameSelected(ByVal sender As Object, ByVal e As System.EventArgs) Handles resourceChooser.Selected
        Me.clearSelectedResourceButton.Enabled = True
        With Me.clearSelectedResourceButton
            .Enabled = True
            .Visible = True
            .Invalidate()
        End With
        Me.clearAllResourcesButton.Enabled = True
        With Me.clearAllResourcesButton
            .Enabled = True
            .Visible = True
            .Invalidate()
        End With
    End Sub

#End Region

#Region " CONNECTOR EVENT HANDLERS "

    ''' <summary>Gets or sets the interface type.</summary>
    Private Const interfaceType As NationalInstruments.VisaNS.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib

    ''' <summary>Display the interface names based on the last interface type.
    ''' </summary>
    Public Sub DisplayNames()
        Me.DisplayNames(interfaceType)
    End Sub

    ''' <summary>Displays the interfaces available to connect to.</summary>
    Public Sub DisplayNames(ByVal interfaceType As NationalInstruments.VisaNS.HardwareInterfaceType)
        Try

            ' get the list of available GPIB interfaces 
            Dim names() As String = Helper.LocalInterfaceNames(interfaceType.ToString)
            Me.connector.DisplayNames(names)

            If names Is Nothing Then

                ' send a verbose message.
                _statusMessage = "Resource names are empty"
                messagesMessageList.PushMessage(_statusMessage)

            ElseIf names.Length = 0 Then

                ' send a verbose message.
                _statusMessage = "Resources not found"
                messagesMessageList.PushMessage(_statusMessage)

            Else

                ' send a verbose message.
                _statusMessage = "Resource names listed"
                messagesMessageList.PushMessage(_statusMessage)

            End If

        Catch ex As NationalInstruments.VisaNS.VisaException

            Me.DisplayMessage("Error occurred")
            _statusMessage = "Failed finding interfaces.  Connect the interface(s) and click Find."
            Me.DisplayMessage(_statusMessage)

        Catch ex As System.ArgumentException

            Me.DisplayMessage("Error occurred")
            _statusMessage = "Failed finding interfaces.  Connect the interface(s) and click Find."
            Me.DisplayMessage(_statusMessage)

            If Not TypeOf (ex.InnerException) Is NationalInstruments.VisaNS.VisaException Then
                ' if we have a visa exception, we have no interfaces.
                ' otherwise, throw an exception
                Throw
            End If

        End Try

    End Sub

    ''' <summary>
    ''' Clears the interface by issueing an interface clear.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Clear
        If _gpibInterface Is Nothing Then
            Exit Sub
        End If
        Try
            Me._gpibInterface.InterfaceClear()
        Catch ex As NationalInstruments.VisaNS.VisaException
            ' trap timeout error.  Apparently, VISA is trying to send commands to the devices
            ' even if none are present.
            '      Run-time exception thrown : NationalInstruments.VisaNS.VisaException - Timeout expired before operation completed.  VISA error code -1073807339 (0xBFFF0015), ErrorTimeout

        End Try
    End Sub

    ''' <summary>
    ''' Connects the interface.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Connect
        Dim resourcename As String = Me.connector.SelectedName
        Me.Connect(resourcename)
    End Sub

    ''' <summary>
    ''' Disconnects the interface.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Disconnect
        Me.Disconnect()
    End Sub

    ''' <summary>
    ''' Displays available interface names.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary>
    ''' Notifies that an interface name was selected.
    ''' </summary>
    ''' <param name="sender">Specifies the object where the call originated.</param>
    ''' <param name="e">Specifies the event arguments provided with the call.</param>
    ''' <remarks></remarks>
    Private Sub connector_Selected(ByVal sender As Object, ByVal e As System.EventArgs) Handles connector.Selected
        Me.DisplayMessage("Selected interface " & connector.SelectedName)
    End Sub

#End Region

End Class
