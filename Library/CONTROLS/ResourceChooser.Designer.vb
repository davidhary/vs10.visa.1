<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResourceChooser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NameSelector1 = New WinForms.SelectorConnector
        Me.cancelButton1 = New System.Windows.Forms.Button
        Me.okButton = New System.Windows.Forms.Button
        Me.availableNamesComboBoxLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'NameSelector1
        '
        Me.NameSelector1.IsConnected = False
        Me.NameSelector1.Location = New System.Drawing.Point(14, 26)
        Me.NameSelector1.Name = "NameSelector1"
        Me.NameSelector1.SelectedName = ""
        Me.NameSelector1.Size = New System.Drawing.Size(165, 21)
        Me.NameSelector1.TabIndex = 28
        '
        'cancelButton1
        '
        Me.cancelButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancelButton1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cancelButton1.Location = New System.Drawing.Point(14, 50)
        Me.cancelButton1.Name = "cancelButton1"
        Me.cancelButton1.Size = New System.Drawing.Size(75, 23)
        Me.cancelButton1.TabIndex = 27
        Me.cancelButton1.Text = "&Cancel"
        '
        'okButton
        '
        Me.okButton.Enabled = False
        Me.okButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.okButton.Location = New System.Drawing.Point(102, 50)
        Me.okButton.Name = "okButton"
        Me.okButton.Size = New System.Drawing.Size(75, 23)
        Me.okButton.TabIndex = 26
        Me.okButton.Text = "&OK"
        '
        'availableNamesComboBoxLabel
        '
        Me.availableNamesComboBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me.availableNamesComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.availableNamesComboBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.availableNamesComboBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.availableNamesComboBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.availableNamesComboBoxLabel.Location = New System.Drawing.Point(16, 10)
        Me.availableNamesComboBoxLabel.Name = "availableNamesComboBoxLabel"
        Me.availableNamesComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.availableNamesComboBoxLabel.Size = New System.Drawing.Size(134, 16)
        Me.availableNamesComboBoxLabel.TabIndex = 25
        Me.availableNamesComboBoxLabel.Text = "Select a Resource by Name"
        Me.availableNamesComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'ResourceChooser
        '
        Me.AcceptButton = Me.okButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cancelButton1
        Me.ClientSize = New System.Drawing.Size(193, 82)
        Me.Controls.Add(Me.NameSelector1)
        Me.Controls.Add(Me.cancelButton1)
        Me.Controls.Add(Me.okButton)
        Me.Controls.Add(Me.availableNamesComboBoxLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "ResourceChooser"
        Me.Text = "Select a board"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NameSelector1 As WinForms.SelectorConnector
    Friend WithEvents cancelButton1 As System.Windows.Forms.Button
    Friend WithEvents okButton As System.Windows.Forms.Button
    Friend WithEvents availableNamesComboBoxLabel As System.Windows.Forms.Label
End Class
