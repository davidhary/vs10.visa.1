''' <summary>Selects a resource.</summary>
''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
'''   instance.</remarks>
''' <license>
''' (c) 2006 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/08/06" by="David Hary" revision="1.0.2229.x">
''' created.
''' </history>
Public Class ResourceChooser

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' instantiate a single instance of this form
        If _instance Is Nothing Then
            If _initializingDefInstance Then
                _instance = Me
            Else
                ' For the start-up form, the first instance created is the default instance.
                If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
                    _instance = Me
                End If
            End If
        End If

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()

    End Sub

#If False Then
  ''' <summary>Initializes the class objects.</summary>
  ''' <exception cref="ApplicationException" guarantee="strong">
  '''   failed instantiating objects.</exception>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()
  End Sub

#End If

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()
        ' tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        ' tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
        NameSelector1.DisplayNames(isr.Visa.Helper.LocalResourceNames())
    End Sub

#End Region

#Region " PROPERTIES "

    Private Shared _instance As ResourceChooser
    Private Shared _initializingDefInstance As Boolean
    ''' <summary>Returns a new or existing instance of this class.</summary>
    ''' <value>The default instance of this form for using this form as a 
    '''   singleton form.</value>
    Public Shared ReadOnly Property [Get]() As ResourceChooser
        Get
            If _instance Is Nothing OrElse _instance.IsDisposed Then
                _initializingDefInstance = True
                _instance = New ResourceChooser
                _initializingDefInstance = False
            End If
            Return _instance
        End Get
    End Property

    Private _resourceType As NationalInstruments.VisaNS.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib
    ''' <summary>Gets or sets the interface type.</summary>
    Public Property ResourceType() As NationalInstruments.VisaNS.HardwareInterfaceType
        Get
            Return _resourceType
        End Get
        Set(ByVal Value As NationalInstruments.VisaNS.HardwareInterfaceType)
            _resourceType = Value
        End Set
    End Property

    ''' <summary>Returns the selected Resource name or gets the
    '''   last resource from the caller.</summary>
    Public Property SelectedResourceName() As String
        Get
            Return Me.NameSelector1.SelectedName
        End Get
        Set(ByVal Value As String)
            Me.NameSelector1.SelectedName = Value
        End Set
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    Private Sub okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles okButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub cancelButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton1.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Closed
    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
      Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Initialize and set the user interface
            initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub NameSelector1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NameSelector1.DoubleClick
        If String.IsNullOrEmpty(Me.SelectedResourceName) Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub NameSelector1_NameSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NameSelector1.Selected
        Me.okButton.Enabled = NameSelector1.SelectedName.Length > 0
    End Sub

#End Region

End Class