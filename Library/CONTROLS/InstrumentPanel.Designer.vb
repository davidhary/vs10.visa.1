<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InstrumentPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.connector = New WinForms.SelectorConnector
        Me.mainStatusBar = New System.Windows.Forms.StatusBar
        Me.statusPanel = New System.Windows.Forms.StatusBarPanel
        Me.idPanel = New System.Windows.Forms.StatusBarPanel
        Me.tipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.idPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'connector
        '
        Me.connector.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.connector.IsConnected = False
        Me.connector.Location = New System.Drawing.Point(0, 1)
        Me.connector.Name = "connector"
        Me.connector.SelectedName = ""
        Me.connector.Size = New System.Drawing.Size(312, 26)
        Me.connector.TabIndex = 14
        '
        'mainStatusBar
        '
        Me.mainStatusBar.Location = New System.Drawing.Point(0, 27)
        Me.mainStatusBar.Name = "mainStatusBar"
        Me.mainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusPanel, Me.idPanel})
        Me.mainStatusBar.ShowPanels = True
        Me.mainStatusBar.Size = New System.Drawing.Size(312, 24)
        Me.mainStatusBar.SizingGrip = False
        Me.mainStatusBar.TabIndex = 13
        Me.mainStatusBar.Text = "StatusBar1"
        '
        'statusPanel
        '
        Me.statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanel.Name = "statusPanel"
        Me.statusPanel.Text = "<status>"
        Me.statusPanel.Width = 285
        '
        'idPanel
        '
        Me.idPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.idPanel.Name = "idPanel"
        Me.idPanel.Text = "<>"
        Me.idPanel.Width = 27
        '
        'InstrumentPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.connector)
        Me.Controls.Add(Me.mainStatusBar)
        Me.Name = "InstrumentPanel"
        Me.Size = New System.Drawing.Size(312, 51)
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.idPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents connector As WinForms.SelectorConnector
    Friend WithEvents mainStatusBar As System.Windows.Forms.StatusBar
    Friend WithEvents statusPanel As System.Windows.Forms.StatusBarPanel
    Friend WithEvents idPanel As System.Windows.Forms.StatusBarPanel
    Friend WithEvents tipsToolTip As System.Windows.Forms.ToolTip

End Class
