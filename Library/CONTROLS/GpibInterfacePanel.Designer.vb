<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GpibInterfacePanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then
                onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.mainTabControl = New System.Windows.Forms.TabControl
        Me.resourcesTabPage = New System.Windows.Forms.TabPage
        Me.resourceChooser = New isr.Visa.WinForms.SelectorConnector
        Me.interfaceLabel = New System.Windows.Forms.Label
        Me.connector = New isr.Visa.WinForms.SelectorConnector
        Me.gpibResourcesComboBoxLabel = New System.Windows.Forms.Label
        Me.clearSelectedResourceButton = New System.Windows.Forms.Button
        Me.clearAllResourcesButton = New System.Windows.Forms.Button
        Me.messagesTabPage = New System.Windows.Forms.TabPage
        Me.messagesMessageList = New isr.Visa.WinForms.MessagesBox
        Me.mainStatusBar = New System.Windows.Forms.StatusBar
        Me.statusPanel = New System.Windows.Forms.StatusBarPanel
        Me.tipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.mainTabControl.SuspendLayout()
        Me.resourcesTabPage.SuspendLayout()
        Me.messagesTabPage.SuspendLayout()
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mainTabControl
        '
        Me.mainTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.mainTabControl.Controls.Add(Me.resourcesTabPage)
        Me.mainTabControl.Controls.Add(Me.messagesTabPage)
        Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
        Me.mainTabControl.Name = "mainTabControl"
        Me.mainTabControl.SelectedIndex = 0
        Me.mainTabControl.Size = New System.Drawing.Size(312, 242)
        Me.mainTabControl.TabIndex = 12
        '
        'resourcesTabPage
        '
        Me.resourcesTabPage.Controls.Add(Me.resourceChooser)
        Me.resourcesTabPage.Controls.Add(Me.interfaceLabel)
        Me.resourcesTabPage.Controls.Add(Me.connector)
        Me.resourcesTabPage.Controls.Add(Me.gpibResourcesComboBoxLabel)
        Me.resourcesTabPage.Controls.Add(Me.clearSelectedResourceButton)
        Me.resourcesTabPage.Controls.Add(Me.clearAllResourcesButton)
        Me.resourcesTabPage.Location = New System.Drawing.Point(4, 4)
        Me.resourcesTabPage.Name = "resourcesTabPage"
        Me.resourcesTabPage.Size = New System.Drawing.Size(304, 216)
        Me.resourcesTabPage.TabIndex = 0
        Me.resourcesTabPage.Text = "Resources"
        Me.resourcesTabPage.UseVisualStyleBackColor = True
        '
        'resourceChooser
        '
        Me.resourceChooser.BackColor = System.Drawing.Color.Transparent
        Me.resourceChooser.Clearable = False
        Me.resourceChooser.Connectible = False
        Me.resourceChooser.Enabled = False
        Me.resourceChooser.IsConnected = False
        Me.resourceChooser.Location = New System.Drawing.Point(46, 88)
        Me.resourceChooser.Name = "resourceChooser"
        Me.resourceChooser.Searchable = False
        Me.resourceChooser.SelectedName = ""
        Me.resourceChooser.Size = New System.Drawing.Size(216, 25)
        Me.resourceChooser.TabIndex = 12
        '
        'interfaceLabel
        '
        Me.interfaceLabel.Location = New System.Drawing.Point(16, 16)
        Me.interfaceLabel.Name = "interfaceLabel"
        Me.interfaceLabel.Size = New System.Drawing.Size(136, 16)
        Me.interfaceLabel.TabIndex = 11
        Me.interfaceLabel.Text = "GPIB Interfaces: "
        Me.interfaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'connector
        '
        Me.connector.BackColor = System.Drawing.Color.Transparent
        Me.connector.Clearable = False
        Me.connector.Connectible = False
        Me.connector.IsConnected = False
        Me.connector.Location = New System.Drawing.Point(16, 32)
        Me.connector.Name = "connector"
        Me.connector.Searchable = False
        Me.connector.SelectedName = ""
        Me.connector.Size = New System.Drawing.Size(268, 26)
        Me.connector.TabIndex = 10
        '
        'gpibResourcesComboBoxLabel
        '
        Me.gpibResourcesComboBoxLabel.Location = New System.Drawing.Point(42, 72)
        Me.gpibResourcesComboBoxLabel.Name = "gpibResourcesComboBoxLabel"
        Me.gpibResourcesComboBoxLabel.Size = New System.Drawing.Size(136, 16)
        Me.gpibResourcesComboBoxLabel.TabIndex = 6
        Me.gpibResourcesComboBoxLabel.Text = "GPIB Resources: "
        Me.gpibResourcesComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'clearSelectedResourceButton
        '
        Me.clearSelectedResourceButton.Enabled = False
        Me.clearSelectedResourceButton.Location = New System.Drawing.Point(42, 128)
        Me.clearSelectedResourceButton.Name = "clearSelectedResourceButton"
        Me.clearSelectedResourceButton.Size = New System.Drawing.Size(216, 24)
        Me.clearSelectedResourceButton.TabIndex = 8
        Me.clearSelectedResourceButton.Text = "Clear Selected Resource"
        '
        'clearAllResourcesButton
        '
        Me.clearAllResourcesButton.Enabled = False
        Me.clearAllResourcesButton.Location = New System.Drawing.Point(42, 168)
        Me.clearAllResourcesButton.Name = "clearAllResourcesButton"
        Me.clearAllResourcesButton.Size = New System.Drawing.Size(216, 24)
        Me.clearAllResourcesButton.TabIndex = 9
        Me.clearAllResourcesButton.Text = "Clear All Resources"
        '
        'messagesTabPage
        '
        Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
        Me.messagesTabPage.Location = New System.Drawing.Point(4, 4)
        Me.messagesTabPage.Name = "messagesTabPage"
        Me.messagesTabPage.Size = New System.Drawing.Size(304, 216)
        Me.messagesTabPage.TabIndex = 1
        Me.messagesTabPage.Text = "Messages"
        Me.messagesTabPage.UseVisualStyleBackColor = True
        '
        'messagesMessageList
        '
        Me.messagesMessageList.BackColor = System.Drawing.SystemColors.Info
        Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
        Me.messagesMessageList.Multiline = True
        Me.messagesMessageList.Name = "messagesMessageList"
        Me.messagesMessageList.NewMessagesCount = 0
        Me.messagesMessageList.PresetCount = 50
        Me.messagesMessageList.ReadOnly = True
        Me.messagesMessageList.ResetCount = 100
        Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.messagesMessageList.Size = New System.Drawing.Size(304, 216)
        Me.messagesMessageList.TabIndex = 0
        Me.messagesMessageList.TimeFormat = "HH:mm:ss. "
        '
        'mainStatusBar
        '
        Me.mainStatusBar.Location = New System.Drawing.Point(0, 242)
        Me.mainStatusBar.Name = "mainStatusBar"
        Me.mainStatusBar.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusPanel})
        Me.mainStatusBar.ShowPanels = True
        Me.mainStatusBar.Size = New System.Drawing.Size(312, 22)
        Me.mainStatusBar.SizingGrip = False
        Me.mainStatusBar.TabIndex = 13
        Me.mainStatusBar.Text = "StatusBar1"
        '
        'statusPanel
        '
        Me.statusPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanel.Name = "statusPanel"
        Me.statusPanel.Width = 312
        '
        'GpibInterfacePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.mainTabControl)
        Me.Controls.Add(Me.mainStatusBar)
        Me.Name = "GpibInterfacePanel"
        Me.Size = New System.Drawing.Size(312, 264)
        Me.mainTabControl.ResumeLayout(False)
        Me.resourcesTabPage.ResumeLayout(False)
        Me.messagesTabPage.ResumeLayout(False)
        Me.messagesTabPage.PerformLayout()
        CType(Me.statusPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
    Friend WithEvents resourcesTabPage As System.Windows.Forms.TabPage
    Friend WithEvents resourceChooser As WinForms.SelectorConnector
    Friend WithEvents interfaceLabel As System.Windows.Forms.Label
    Friend WithEvents connector As WinForms.SelectorConnector
    Friend WithEvents gpibResourcesComboBoxLabel As System.Windows.Forms.Label
    Friend WithEvents clearSelectedResourceButton As System.Windows.Forms.Button
    Friend WithEvents clearAllResourcesButton As System.Windows.Forms.Button
    Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
    Friend WithEvents messagesMessageList As isr.Visa.WinForms.MessagesBox
    Friend WithEvents mainStatusBar As System.Windows.Forms.StatusBar
    Friend WithEvents statusPanel As System.Windows.Forms.StatusBarPanel
    Friend WithEvents tipsToolTip As System.Windows.Forms.ToolTip

End Class
