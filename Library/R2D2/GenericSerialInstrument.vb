Namespace R2D2

    ''' <summary>Implements a Visa interface for a generic serial instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class GenericSerialInstrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.SerialInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " SCPI EVENT FLAGS "

        ''' <summary>Gets or sets the status byte flags of the operation event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum OperationEvents
            None = 0
            Settling = 2
            WaitingForTrigger = 32
            WaitingForArm = 64
            Idle = 1024
            'All = 2047
        End Enum

        ''' <summary>Gets or sets the status byte flags of the operation transition event register.</summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")> _
        <System.Flags()> Public Enum OperationTransitionEvents
            None = 0
            Settling = 2
            WaitingForTrigger = 32
            WaitingForArm = 64
            Idle = 1024
            'All = 2047
        End Enum

#End Region

#Region " METHODS "

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property Gui() As GenericSerialInstrumentPanel
            Get
                Return CType(MyBase.BaseGui, GenericSerialInstrumentPanel)
            End Get
            Set(ByVal Value As GenericSerialInstrumentPanel)
                MyBase.BaseGui = Value
            End Set
        End Property

#End Region

#Region " GENERIC SERIAL INSTRUMENT METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' allow operations to complete
                    Return True

                Catch ex As NationalInstruments.VisaNS.VisaException

                    StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                End Try

            End If

            Return False

        End Function

#End Region

#Region " GENERIC SERIAL INSTRUMENT PROPERTIES "

#End Region

#Region " PRIVATE  and  PROTECTED "

#End Region

    End Class
End Namespace
