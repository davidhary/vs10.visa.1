Namespace R2D2

    ''' <summary>Provides event arguments for instrument events.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class ServiceEventArgs
        Inherits System.EventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

            ' Date.Now set the member properties.
            _operationTime = Date.Now

        End Sub

        ''' <summary>Constructs this class with a status message.</summary>
        ''' <param name="eventMessage">Specifies the event message.</param>
        ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
        Public Sub New(ByVal eventMessage As String)

            ' instantiate the base class
            Me.New()

            ' Date.Now set the member properties.
            _eventMessage = eventMessage

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Processes a service request.</summary>
        ''' <param name="session">A reference to an open message-based
        '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see> 
        '''   requesting the service.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        Public Sub ProcessRequest(ByVal session As NationalInstruments.VisaNS.MessageBasedSession)
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            If session.HardwareInterfaceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib Then
                _serviceRequestStatus = session.ReadStatusByte()
            Else
                _serviceRequestStatus = NationalInstruments.VisaNS.StatusByteFlags.RequestingService
            End If
            _eventTime = Date.Now
            _requestCount += 1
            If Me.HasError Then
                '   _lastError = SystemSubsystem.ReadLastError(gpib)
            Else
            End If

        End Sub

#End Region

#Region " PROPERTIES "

        Private _lastError As String
        ''' <summary>Returns the last error read from the instrument.</summary>
        Public ReadOnly Property LastError() As String
            Get
                Return _lastError
            End Get
        End Property

        Private _eventMessage As String = ""
        ''' <summary>Returns the event message.</summary>
        Public ReadOnly Property EventMessage() As String
            Get
                Return _eventMessage
            End Get
        End Property

        Private _eventTime As DateTime = Date.Now
        ''' <summary>Gets or sets the event time.</summary>
        Public ReadOnly Property EventTime() As DateTime
            Get
                Return _eventTime
            End Get
        End Property

        ''' <summary>Gets or sets the condition for the service request status error flag is on.</summary>
        Public ReadOnly Property HasError() As Boolean
            Get
                Dim errorAvailable As Int32 = 4
                Return (_serviceRequestStatus And errorAvailable) <> 0
            End Get
        End Property

        Private _servicingRequest As Boolean
        Public Property ServicingRequest() As Boolean
            Get
                Return _servicingRequest
            End Get
            Set(ByVal value As Boolean)
                _servicingRequest = value
            End Set
        End Property

        Private _measurementEventStatus As Int32
        ''' <summary>Returns the measurement Event register status.  To decipher, case
        '''   to the <see cref="isr.Visa.SCPI.K2400.MeasurementEvents">event flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property MeasurementEventStatus() As Int32
            Get
                Return _measurementEventStatus
            End Get
        End Property

        Private _operationCompleted As Boolean
        Public Property OperationCompleted() As Boolean
            Get
                Return _operationCompleted
            End Get
            Set(ByVal value As Boolean)
                _operationCompleted = value
            End Set
        End Property

        Private _operationCondition As Int32
        ''' <summary>Gets or sets the operation condition status. To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.OperationEvents">Operation Events flags</see>
        '''   of the specific instrument type.</summary>
        Public Property OperationCondition() As Int32
            Get
                Return _operationCondition
            End Get
            Set(ByVal value As Int32)
                _operationCondition = value
            End Set
        End Property

        ''' <summary>Returns the operation elapsed time.</summary>
        Public ReadOnly Property OperationElapsedTime() As TimeSpan
            Get
                Return _eventTime.Subtract(_operationTime)
            End Get
        End Property

        Private _operationTime As DateTime = Date.Now
        ''' <summary>Returns the operation time.  This is typically set to when the 
        '''   operation started so the operation can be timed to its event time.</summary>
        Public ReadOnly Property OperationTime() As DateTime
            Get
                Return _operationTime
            End Get
        End Property

        Private _operationEventStatus As Int32
        ''' <summary>Returns the operation event register status.  To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.OperationEvents">Operation Events flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property OperationEventStatus() As Int32
            Get
                Return _operationEventStatus
            End Get
        End Property

        Private _questionableEventStatus As Int32
        ''' <summary>Returns the Questionable Event register status. To decipher, cast 
        '''   cast to the <see cref="isr.Visa.SCPI.K2400.QuestionableEvents">Questionable Events flags</see>
        '''   of the specific instrument type.</summary>
        Public ReadOnly Property QuestionableEventStatus() As Int32
            Get
                Return _questionableEventStatus
            End Get
        End Property

        Private _receivedMessage As String
        ''' <summary>Returns the message that was received from the instrument.</summary>
        Public ReadOnly Property ReceivedMessage() As String
            Get
                Return _receivedMessage
            End Get
        End Property

        Private _requestCount As Int32
        ''' <summary>Returns the number of service request that were recorded since the
        '''   last Clear Status command.</summary>
        Public ReadOnly Property RequestCount() As Int32
            Get
                Return _requestCount
            End Get
        End Property

        Private _serviceRequestStatus As NationalInstruments.VisaNS.StatusByteFlags
        ''' <summary>Returns the service request 
        '''   <see cref="NationalInstruments.VisaNS.StatusByteFlags">status byte</see></summary>
        Public ReadOnly Property ServiceRequestStatus() As NationalInstruments.VisaNS.StatusByteFlags
            Get
                Return _serviceRequestStatus
            End Get
        End Property

        Private _standardEventStatus As isr.Visa.Scpi.StandardEvents
        ''' <summary>Returns the standard event register
        '''   <see cref="isr.Visa.SCPI.StandardEvents">status</see></summary>
        Public ReadOnly Property StandardEventStatus() As isr.Visa.Scpi.StandardEvents
            Get
                Return _standardEventStatus
            End Get
        End Property

#End Region

    End Class

End Namespace
