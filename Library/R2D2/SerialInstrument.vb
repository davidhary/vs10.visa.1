Imports NationalInstruments

Namespace R2D2

    ''' <summary>Provides a Core. for non-SCPI Serial instruments.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class SerialInstrument

        ' based on the OpenCloseBaseClass inheritable class
        Inherits IO.ConnectLogBaseClass

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Protected Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        ' dispose of 'status' properties
                        _Id = String.Empty

                        If _serialSession IsNot Nothing Then
                            If MyBase.IsConnected Then
                                ' remove the service request handler ignoring exceptions.
                            End If
                            _serialSession.Dispose()
                            _serialSession = Nothing
                        End If

                        ' remove the reference to the gui
                        _gui = Nothing

                    End If

                    ' Free shared unmanaged resources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " INSTRUMENT METHODS "

        ''' <summary>Clears the resource.</summary>
        Public Overrides Sub Clear()
            Me.ResetAndClear()
        End Sub

        ''' <summary>Opens a VISA session for the instruments at the given address.</summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            Dim lastAction As String = String.Empty

            Try

                If MyBase.UsingDevices Then

                    ' open a session to this instrument
                    Me._serialSession = New NationalInstruments.VisaNS.SerialSession(resourceName)

                    If _serialSession Is Nothing Then

                        Me.Disconnect()

                    Else

                        ' clear status and disable service request on all operations.
                        Me.ClearStatus()

                        ' issue a selective device clear and total clear of the instrument.
                        Me.ResetAndClear()

                        MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} opened.", Me.InstanceName)

                        lastAction = "connecting"
                        MyBase.Connect(resourceName)

                    End If

                Else

                    lastAction = "connecting"
                    MyBase.Connect(resourceName)

                End If

                If Me.IsConnected Then
                    If Me.Visible Then
                        _gui.Connect()
                    End If
                Else
                    If Me.Visible Then
                        _gui.Disconnect()
                    End If
                End If

                Return MyBase.IsConnected

            Catch ex As isr.Visa.BaseException

                ' close to meet strong guarantees
                Try
                    Me.Disconnect()
                Finally
                End Try

                ' throw an exception
                MyBase.StatusMessage = String.Format(System.Windows.Forms.Application.CurrentCulture, _
                    "{0} failed {1}", Me.InstanceName, lastAction)
                Throw New isr.Visa.OperationOpenException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>Disconnect the instrument.</summary>
        ''' <returns>A Boolean data type</returns>
        ''' <exception cref="OperationCloseException" guarantee="strong"></exception>
        ''' <remarks>Use this method to close the instance.  The method returns true if success or 
        '''   false if it failed closing the instance.</remarks>
        Public Overrides Function Disconnect() As Boolean

            Dim lastAction As String = String.Empty

            Try

                lastAction = "disconnecting"
                MyBase.Disconnect()

                If Not Me.IsConnected Then
                    If Me.Visible Then
                        _gui.Disconnect()
                    End If
                End If

                Return Not MyBase.IsConnected

            Catch ex As isr.Visa.BaseException
                ' throw an exception
                MyBase.StatusMessage = String.Format(System.Windows.Forms.Application.CurrentCulture, _
                    "{0} failed {1}", Me.InstanceName, lastAction)
                Throw New isr.Visa.OperationCloseException(MyBase.StatusMessage, ex)
            End Try

        End Function

        ''' <summary>This overrides the ToString method returning the instance name if not empty.</summary>
        ''' <remarks>Use this method to return the instance name.</remarks>
        Public Overrides Function ToString() As String
            If MyBase.InstanceName.Length > 0 Then
                Return MyBase.InstanceName
            Else
                Return MyBase.ToString
            End If
        End Function

#End Region

#Region " INSTRUMENT PROPERTIES "

        Private _gui As InstrumentPanel
        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Protected Property BaseGui() As InstrumentPanel
            Get
                Return _gui
            End Get
            Set(ByVal Value As InstrumentPanel)
                _gui = Value
            End Set
        End Property

        ''' <summary>Returns True if the last operation reported an error by
        '''   way of a service request.</summary>
        Public Shared ReadOnly Property HadError() As Boolean
            Get
                Return False
            End Get
        End Property

        ''' <summary>Gets or sets the condition for this control interface is visible.</summary>
        Public ReadOnly Property Visible() As Boolean
            Get
                Return Me._gui IsNot Nothing AndAlso Me._gui.IsVisible
            End Get
        End Property

#End Region

#Region " SCPI METHODS "

        ''' <summary>Parses the instrument firmware revision.</summary>
        ''' <param name="revision">Specifies the instrument firmware revision..</param>
        Public Overridable Sub ParseFirmwareRevision(ByVal revision As String)
            If revision Is Nothing Then
                Throw New ArgumentNullException("revision")
            End If
        End Sub

        ''' <summary>Parses the instrument ID.</summary>
        ''' <param name="id">Specifies the instrument ID, which includes at a minimum the following information:
        '''   <see cref="ManufacturerName">manufacturer</see>, <see cref="Model">model</see>, 
        '''   <see cref="SerialNumber">serial number</see>, e.g., 
        '''   <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.</param>
        ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
        ''' <remarks>The firmware revision can be further interpreted by the child instruments.</remarks>
        Public Overridable Sub ParseInstrumentId(ByVal id As String)

            If id Is Nothing Then
                Throw New ArgumentNullException("id")
            End If

            ' Parse the id to get the revision number
            Dim idItems() As String = id.Split(","c)

            ' company, e.g., KEITHLEY INSTRUMENTS INC.,
            _manufacturerName = idItems(0)

            ' model: MODEL 2420
            _model = idItems(1)

            ' Serial Number: 0669977
            _serialNumber = idItems(2)

            ' firmware: C11 Oct 10 1997 09:51:36/A02 /D/B/E
            _firmwareRevision = idItems(3)

            ' parse thee firmware revision
            ParseFirmwareRevision(_firmwareRevision)

        End Sub

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overridable Overloads Function ResetAndClear() As Boolean

            Try

                ' Clear the device Status and set more defaults
                Me.ClearStatus()

                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} reset and cleared.", Me.InstanceName)

                Return True

            Catch ex As NationalInstruments.VisaNS.VisaException

                MyBase.StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                Return False

            End Try

        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue using a longer timeout
        '''   than the minimal timeout set for the session.  Typically, the source meter may
        '''   required a 5000 milliseconds timeout.</summary>
        ''' <param name="timeout"></param>
        ''' <remarks>This also sets output to Auto Clear.</remarks>
        Public Overridable Overloads Function ResetAndClear(ByVal timeout As Int32) As Boolean

            If _serialSession Is Nothing Then
                Return True
            End If

            Dim newTimeout As Int32 = _serialSession.Timeout
            _serialSession.Timeout = Math.Max(_serialSession.Timeout, timeout)
            Dim outcome As Boolean = ResetAndClear()
            _serialSession.Timeout = newTimeout
            Return outcome

        End Function

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
        Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
            Me.Write(output & Scpi.Instrument.IIf(Of String)(isOn, "ON", "OFF"))
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        ''' <param name="one">True to add "1" or false to add "0".</param>
        Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
            Me.Write(output & Scpi.Instrument.IIf(Of String)(one, "1", "0"))
        End Sub

#End Region

#Region " SCPI PROPERTIES "

        Private _firmwareRevision As String

        Private _lastServiceEventArgs As NationalInstruments.VisaNS.SerialSessionEventArgs
        ''' <summary>Gets or sets the last service event arguments
        '''   <see cref="NationalInstruments.VisaNS.SerialSessionEventArgs">status</see></summary>
        ''' <remarks>Also used to hold values read from instrument by way of static methods
        '''   such as *OPC?</remarks>
        Public Property LastServiceEventArgs() As NationalInstruments.VisaNS.SerialSessionEventArgs
            Get
                Return _lastServiceEventArgs
            End Get
            Set(ByVal value As NationalInstruments.VisaNS.SerialSessionEventArgs)
                _lastServiceEventArgs = value
            End Set
        End Property

        Private _manufacturerName As String
        ''' <summary>Returns the instrument manufacturer name .</summary>
        Public ReadOnly Property ManufacturerName() As String
            Get
                Return _manufacturerName
            End Get
        End Property

        Private _model As String
        ''' <summary>Returns the instrument model number.</summary>
        ''' <value>A string property that may include additional precursors such as
        '''   'Model' to the relevant information.</value>
        Public ReadOnly Property Model() As String
            Get
                Return _model
            End Get
        End Property

        Private _serialNumber As String
        ''' <summary>returns the instrument serial number.</summary>
        Public ReadOnly Property SerialNumber() As String
            Get
                Return _serialNumber
            End Get
        End Property

#End Region

#Region " SERIAL VISA INTERFACE METHODS "

        ''' <summary>Issues an interface clear.</summary>
        Public Shared Sub InterfaceClear()

        End Sub

#End Region

#Region " COMMON COMMANDS "

        ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
        '''   IEEE488.2 common command.</summary>
        ''' <remarks>Also clears all service registers as well as the 
        '''   <see cref="LastServiceEventArgs">last service event arguments</see>.</remarks>
        Public Sub ClearStatus()

            If _serialSession IsNot Nothing Then

                ' Clear the device status.  This will set all the
                ' standard subsystem properties.
                Me._serialSession.Clear()

            End If

            ' Clear the event arguments
            Me._lastServiceEventArgs = Nothing

            ' clear the error and information enunciators
            If Me.Visible Then
                Me.BaseGui.DisplayMessage("Status Cleared")
            End If

        End Sub

        Private _Id As String
        ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
        Public ReadOnly Property Id() As String
            Get
                If String.IsNullOrEmpty(_Id) Then
                    If _serialSession Is Nothing Then
                        _Id = "Serial Corp., Model 1, 1901, A"
                    Else
                        _Id = _serialSession.Query("*idn?")
                    End If
                    Me.ParseInstrumentId(_Id)
                End If
                Return _Id
            End Get
        End Property

#End Region

#Region " SERIAL SESSION METHODS AND PROPERTIES "

        Private _lastCommandTime As DateTime = DateTime.Now
        ''' <summary>Gets or sets the time of the last command for delay purposes.
        ''' </summary>
        Public ReadOnly Property LastCommandTime() As DateTime
            Get
                Return _lastCommandTime
            End Get
        End Property

        Private _minimumCommandInterval As Int32 = 200
        ''' <summary>Gets or sets the time between commands in milliseconds.
        ''' </summary>
        Public Property MinimumCommandInterval() As Int32
            Get
                Return _minimumCommandInterval
            End Get
            Set(ByVal Value As Int32)
                _minimumCommandInterval = Value
            End Set
        End Property

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval()
            AwaitCommandInterval(_minimumCommandInterval)
        End Sub

        ''' <summary>Waits till the previous command is completed.
        ''' </summary>
        Protected Sub AwaitCommandInterval(ByVal milliseconds As Int32)

            Do While DateTime.Compare(DateTime.Now, _lastCommandTime.AddMilliseconds(milliseconds)) < 0
                Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(2)
            Loop
            _lastCommandTime = DateTime.Now
        End Sub

        Private _serialSession As NationalInstruments.VisaNS.SerialSession
        ''' <summary>Gets or sets reference to the Serial session.</summary>
        Public ReadOnly Property SerialSession() As NationalInstruments.VisaNS.SerialSession
            Get
                Return _serialSession
            End Get
        End Property

        ''' <summary>Sends a query and gets back the result.</summary>
        ''' <param name="output">The main command.</param>
        Public Function QueryTerminate(ByVal output As String) As String
            If _serialSession IsNot Nothing Then
                output &= Convert.ToChar(Me._serialSession.TerminationCharacter)
            End If
            Return Me.QueryTrimNewLine(output)
        End Function

        ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
        ''' <param name="question">The query to use.</param>
        Public Function QueryTrimNewLine(ByVal question As String) As String
            If _serialSession Is Nothing Then
                Return String.Empty
            Else
                Return MessageBasedSession.QueryTrimNewLine(Me._serialSession, question)
            End If
        End Function

        ''' <summary>Writes output and terminate.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub WriteTerminate(ByVal output As String)
            If _serialSession IsNot Nothing Then
                output &= Convert.ToChar(Me._serialSession.TerminationCharacter)
                Me._serialSession.Write(output)
            End If
        End Sub

        ''' <summary>Writes On or Off command to the instrument.</summary>
        ''' <param name="output">The main command.</param>
        Public Sub Write(ByVal output As String)
            If _serialSession IsNot Nothing Then
                Me._serialSession.Write(output)
            End If
        End Sub

        Public Function WaitOnEvent(ByVal timeout As Int32) As Boolean
            If _serialSession IsNot Nothing Then
                Me._serialSession.WaitOnEvent(VisaNS.MessageBasedSessionEventType.AllEnabledEvents, timeout)
            End If
        End Function

#End Region

    End Class

End Namespace