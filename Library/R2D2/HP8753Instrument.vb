Namespace R2D2

    ''' <summary>Implements a Visa interface to a generic GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class HP8753Instrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.GpibInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Connects the instrument to a GPIB session and sends the instrument to local allowing
        ''' the operator access.
        ''' </summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        Public Overrides Function Connect(ByVal resourceName As String) As Boolean
            If MyBase.Connect(resourceName) Then
                ' goto local to allow the operator to load the instrument state.
                ' set a long time out after connecting.
                Me.GotoLocal()
                Return True
            Else
                Return False
            End If
        End Function

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' preset the aalyzer and wait
                    Dim opc As Int32 = MyBase.QueryInt32("OPC?; PRES;")

                    ' allow operations to complete
                    Return opc = 1

                Catch ex As NationalInstruments.VisaNS.VisaException

                    StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                End Try

            End If

            Return False

        End Function

        ''' <summary>Allows th eoperator to go to local.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub GotoLocal()
            If MyBase.GpibSession IsNot Nothing Then
                Dim defaultTimeout As Int32 = MyBase.GpibSession.Timeout
                MyBase.GpibSession.Timeout = 10000
                MyBase.GpibSession.ControlRen(NationalInstruments.VisaNS.RenMode.AddressAndGtl)
                MyBase.GpibSession.Query("IDN?;")
                MyBase.GpibSession.Timeout = defaultTimeout
            End If
        End Sub

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property Gui() As HP8753InstrumentPanel
            Get
                Return CType(MyBase.BaseGui, HP8753InstrumentPanel)
            End Get
            Set(ByVal Value As HP8753InstrumentPanel)
                MyBase.BaseGui = Value
            End Set
        End Property

#End Region

#Region " HP8753 GPIB INSTRUMENT METHODS "

        ''' <summary>Set auto scale.
        ''' </summary>
        Public Sub AutoScale()
            MyBase.Write("AUTO")
        End Sub

        ''' <summary>Reads data at the specified marker.
        ''' </summary>
        ''' <param name="markerNumber">Specifies the marker number between 1 and 4.</param>
        ''' <remarks></remarks>
        Public Sub ReadMarker(ByVal markerNumber As Int32)

            If Me.Visible Then
                Me.Gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Reading marker {0}...", markerNumber))
            End If

            ' select the active marker
            Dim marker As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK{0};", markerNumber)
            MyBase.Write(marker)

            ' read the data
            MyBase.GpibSession.Write("OUTPMARK;")
            Threading.Thread.Sleep(300)
            Me.Reading = MyBase.GpibSession.ReadString()
            'Me.Reading = MyBase.GpibSession.QueryTrimEnd("OUTPMARK;")

            ' log the message
            MyBase.LogMessage(marker & "," & Me.Reading, TraceEventType.Information)
            If MyBase.UsingLogWriter Then
                MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                      "Marker {0},{1}", markerNumber, Me.Reading), TraceEventType.Information)
            End If

            If Me.Visible Then
                Me.Gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Done reading marker {0}.", markerNumber))
            End If

        End Sub

        ''' <summary>Starts the requested sequence.
        ''' </summary>
        ''' <param name="sequenceNumber">Specifies the sequence number between 1 and 6.</param>
        ''' <remarks></remarks>
        Public Sub StartSequence(ByVal sequenceNumber As Int32)

            If Me.Visible Then
                Me.Gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Starting Sequence {0}...", sequenceNumber))
            End If

            ' starts the sequence
            MyBase.Write(String.Format(Globalization.CultureInfo.CurrentCulture, "DOSEQ{0};", sequenceNumber))

            If Me.Visible Then
                Me.Gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Sequence {0} started.", sequenceNumber))
            End If

        End Sub

        Public Sub ContinueSequence()

            If Me.Visible Then
                Me.Gui.DisplayMessage("Continuing Sequence...")
            End If

            ' starts the sequence
            MyBase.Write("CONS;")

            If Me.Visible Then
                Me.Gui.DisplayMessage("Sequence continued.")
            End If

        End Sub

#End Region

#Region " HP8753C GPIB INSTRUMENT PROPERTIES "

        ''' <summary>Sets channel 1 as the active channel.
        ''' </summary>
        Property ChannelOne() As Boolean
            Get
                Dim message As String = "CHAN1?;"
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) = 1
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    Dim message As String = "OPC?; CHAN1;"
                    MyBase.QueryTrimEnd(message)
                    If Me.Visible Then
                        Me.Gui.channelNumeric.Value = 1
                    End If
                    If MyBase.UsingLogWriter Then
                        MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                              "Channel 1"), TraceEventType.Information)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Sets channel 2 as the active channel.
        ''' </summary>
        Property ChannelTwo() As Boolean
            Get
                Dim message As String = "CHAN2?;"
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) = 1
            End Get
            Set(ByVal Value As Boolean)
                If Value Then
                    Dim message As String = "OPC?; CHAN2;"
                    MyBase.QueryTrimEnd(message)
                    If Me.Visible Then
                        Me.Gui.channelNumeric.Value = 1
                    End If
                    If MyBase.UsingLogWriter Then
                        MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                              "Channel 2"), TraceEventType.Information)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the position of the marker one in MHz.
        ''' </summary>
        Public Property MarkerOnePosition() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK1?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK1{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.markerOneNumeric.Value = Value
                End If
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Marker 1, {0}MHz", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the position of the marker two in MHz.
        ''' </summary>
        Public Property MarkerTwoPosition() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK2?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "MARK2{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.markerTwoNumeric.Value = Value
                End If
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Marker 2, {0}MHz", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        Private _measure As String
        ''' <summary>Gets or sets the measurement.  Valid measurements are
        '''   S11, S21, ... 
        ''' </summary>
        Public Property Measure() As String
            Get
                ' TODO: GET THE MEASUREMENT STATUS FROM THE INSTRUMENT
                Return _measure
            End Get
            Set(ByVal Value As String)
                _measure = Value
                MyBase.Write(Value)
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Measurement, {0}", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the power level.
        ''' </summary>
        Public Property PowerLevel() As Double
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "POWE?;")
                Return Int32.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture)
            End Get
            Set(ByVal Value As Double)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "POWE{0}DB;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.powerNumeric.Value = Convert.ToDecimal(Value)
                End If
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Power Level, {0}DB", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        Private _scaleFormat As String
        ''' <summary>Gets or sets the scale format.  Valid formats are
        '''   LOGM, PHAS, ...
        ''' </summary>
        Public Property ScaleFormat() As String
            Get
                Return _scaleFormat
            End Get
            Set(ByVal Value As String)
                If Not String.IsNullOrEmpty(Value) Then
                    MyBase.Write(Value.Substring(0, 4))
                    If MyBase.UsingLogWriter Then
                        MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                              "Scale, {0}", Value), TraceEventType.Information)
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the start position of the trace in MHz.
        ''' </summary>
        Public Property TraceStart() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STAR?;")
                Return CInt(Double.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) / 1000000.0)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STAR{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.traceStartNumeric.Value = Value
                End If
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Trace Start, {0} MHz", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the start position of the trace in MHz.
        ''' </summary>
        Public Property TraceStop() As Int32
            Get
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STOP?;")
                Return CInt(Double.Parse(MyBase.QueryTrimEnd(message), Globalization.CultureInfo.CurrentCulture) / 1000000.0)
            End Get
            Set(ByVal Value As Int32)
                Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, "STOP{0}MHZ;", Value)
                MyBase.Write(message)
                If Me.Visible Then
                    Me.Gui.traceStopNumeric.Value = Value
                End If
                If MyBase.UsingLogWriter Then
                    MyBase.LogMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                          "Trace Stop, {0} MHz", Value), TraceEventType.Information)
                End If
            End Set
        End Property

        Private _firstValue As Double
        ''' <summary>Returns the measured first Value.</summary>
        Public ReadOnly Property FirstValue() As Double
            Get
                Return _firstValue
            End Get
        End Property

        Private _reading As String
        ''' <summary>Gets or sets the last reading.
        ''' </summary>
        Public Property Reading() As String
            Get
                Return _reading
            End Get
            Set(ByVal Value As String)
                If Not String.IsNullOrEmpty(Value) Then
                    _reading = Value
                    If Value.Length > 0 Then
                        Dim values As String() = Value.Split(","c)
                        If values.Length >= 1 Then
                            If Me.Visible Then
                                Me.Gui.FirstValueLabel.Text = values(0)
                            End If
                            If Double.TryParse(values(0), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, _firstValue) Then
                            Else
                                _firstValue = 0
                            End If
                        End If
                        If values.Length >= 2 Then
                            If Me.Visible Then
                                Me.Gui.SecondValueLabel.Text = values(1)
                            End If
                            If Not Double.TryParse(values(1), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, _secondValue) Then
                                _secondValue = 0
                            End If
                        End If
                        If values.Length >= 3 Then
                            If Me.Visible Then
                                Me.Gui.ThirdValueLabel.Text = values(2)
                            End If
                            If Not Double.TryParse(values(2), Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, _thirdValue) Then
                                _thirdValue = 0
                            End If
                        End If
                    End If
                    If Me.Visible Then
                        Me.Gui.PushMessage(Value)
                    End If
                End If
            End Set
        End Property

        Private _secondValue As Double
        ''' <summary>Returns the measured second Value.</summary>
        Public ReadOnly Property SecondValue() As Double
            Get
                Return _secondValue
            End Get
        End Property

        Private _thirdValue As Double
        ''' <summary>Returns the measured third Value.</summary>
        Public ReadOnly Property ThirdValue() As Double
            Get
                Return _thirdValue
            End Get
        End Property

#End Region

#Region " ON EVENT HANDLERS "

        ''' <summary>Interprets the scan transitions and raises the service request event.</summary>
        ''' <param name="e">Passes reference to the <see cref="Scpi.ServiceEventArgs">SCPI service request event arguments</see>.</param>
        Protected Overrides Sub OnServiceRequest(ByVal e As Scpi.ServiceEventArgs) ' System.EventArgs)

            If e.HasError Then

            Else

            End If

            MyBase.OnServiceRequest(e)

        End Sub

#End Region

    End Class
End Namespace
