Namespace R2D2

    ''' <summary>Provides a user interface for a generic R2D2 GPIB instrument.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("Generic Gpib Instrument - Windows Forms Custom Control")> _
    Public Class GenericGpibInstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            onInstantiate()

        End Sub

        ''' <summary>Initializes additional components.</summary>
        ''' <remarks>Called from the class constructor to add other controls or 
        '''   change anything set by the InitializeComponent method.</remarks>
        Private Function onInstantiate() As Boolean

            Try

                ' instantiate the base instrument and set reference to the control.
                MyBase.ResourceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib

                ' instantiate the reference to the instrument
                Me._instrument = New GenericGpibInstrument(Me.Name)
                MyBase.BaseInstrument = Me._instrument

                ' set the refernece to the GUI
                Me._instrument.Gui = Me

                ' set combo
                serviceRequestFlagsComboBox.DataSource = Nothing
                serviceRequestFlagsComboBox.Items.Clear()
                serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.ServiceRequests))

                Return True

            Catch ex As isr.Visa.BaseException

                ' throw an exception
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed initializing", Me.Name)
                Throw New isr.Visa.BaseException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Updates the user interface upon successful connection.
        ''' </summary>
        Public Overrides Function Connect() As Boolean

            ' enable user interface
            Me.mainTabControl.Enabled = True

            ' update the current scan list
            Me.refreshDisplay()

            ' set combo
            serviceRequestFlagsComboBox.DataSource = Nothing
            serviceRequestFlagsComboBox.Items.Clear()
            serviceRequestFlagsComboBox.DataSource = [Enum].GetNames(GetType(isr.Visa.Scpi.ServiceRequests))

            If True Then
            Else
                ' check if a SCPI instrument and if so display this 
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer name: {0}", Me._instrument.GpibSession.ResourceManufacturerName))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer ID: 0x{0:X}", Me._instrument.GpibSession.ResourceManufacturerID))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Connectred to {0}", Me._instrument.Id()))
            End If

            Return MyBase.Connect()

        End Function

        ''' <summary>
        ''' Updates the user interface upon disconnection.
        ''' </summary>
        Public Overrides Function Disconnect() As Boolean

            Me.mainTabControl.Enabled = False
            Return MyBase.Disconnect()

        End Function

        ''' <summary>Display a status message.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub DisplayMessage(ByVal message As String)

            MyBase.DisplayMessage(message)
            Me.PushMessage(message)

        End Sub

        ''' <summary>Adds a message to the message list.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub PushMessage(ByVal message As String)
            messagesMessageList.PushMessage(message)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.R2D2.GenericGpibInstrument
        ''' <summary>Gets a reference to the Keithley 7000 instrument.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        <System.ComponentModel.Browsable(False)> Public ReadOnly Property Instrument() As isr.Visa.R2D2.GenericGpibInstrument
            Get
                Return _instrument
            End Get
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

#Region " READ / WRITE "

        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Me.readTextBox.Text = Me._instrument.GpibSession.QueryTrimEnd(Me.writeTextBox.Text)
            Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                Me._instrument.GpibSession.ReadStatusByte())
        End Sub

        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.ReadString()
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Me._instrument.GpibSession.Write(Me.writeTextBox.Text)
            Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                Me._instrument.GpibSession.ReadStatusByte())
        End Sub

#End Region

#Region " SRQ "

        Private Sub enableServiceRequestCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enableServiceRequestCheckBox.CheckedChanged
            If MyBase.Visible AndAlso enableServiceRequestCheckBox.Enabled Then
                Me.Instrument.ClearStatus()
                Me.Instrument.ToggleServiceRequest(enableServiceRequestCheckBox.Checked, CType(serviceRequestMaskTextBox.Text, isr.Visa.Scpi.ServiceRequests))
            End If
        End Sub

        Private Sub serviceRequestMaskAddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskAddButton.Click

            Dim selectedFlag As isr.Visa.Scpi.ServiceRequests = _
                CType(serviceRequestFlagsComboBox.SelectedItem, isr.Visa.Scpi.ServiceRequests)

            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte Or selectedFlag)
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskRemoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles serviceRequestMaskRemoveButton.Click

            Dim selectedFlag As isr.Visa.Scpi.ServiceRequests = _
                CType(serviceRequestFlagsComboBox.SelectedItem, isr.Visa.Scpi.ServiceRequests)

            Dim serviceByte As Byte = Byte.Parse(serviceRequestMaskTextBox.Text, Globalization.CultureInfo.CurrentCulture)
            serviceByte = Convert.ToByte(serviceByte And (Not selectedFlag))
            serviceRequestMaskTextBox.Text = serviceByte.ToString(Globalization.CultureInfo.CurrentCulture)

        End Sub

        Private Sub serviceRequestMaskTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles serviceRequestMaskTextBox.Validating
            Dim data As String = serviceRequestMaskTextBox.Text.Trim
            If (data.Length = 0) OrElse (Not isr.Visa.Text.Helper.IsNumber(data)) Then
                serviceRequestMaskTextBox.Text = "0"
                e.Cancel = True
                Return
            Else
                Dim value As Int32 = Convert.ToInt32(data, Globalization.CultureInfo.CurrentCulture)
                If value < 0 Or value > 255 Then
                    serviceRequestMaskTextBox.Text = "0"
                    e.Cancel = True
                    Return
                End If
            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace
