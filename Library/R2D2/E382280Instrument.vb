Namespace R2D2

    ''' <summary>Implements a Visa interface for the EXTECH 382280 Power Supply.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    Public Class E382280Instrument

        ' based on the instrument class
        Inherits isr.Visa.R2D2.SerialInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the instrument name.</param>
        Public Sub New(ByVal instanceName As String)

            ' instantiate the base class
            MyBase.New(instanceName)
            Me._currentReading = New Visa.ReadingR
            Me._currentReading.DisplayCaption.Units = "A"
            Me._voltageReading = New Visa.ReadingR
            Me._voltageReading.DisplayCaption.Units = "V"

        End Sub

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If Not MyBase.IsDisposed Then

                    If disposing Then

                        ' Free managed resources when explicitly called
                        If Me._currentReading IsNot Nothing Then
                            Me._currentReading.Dispose()
                            Me._currentReading = Nothing
                        End If

                        If Me._voltageReading IsNot Nothing Then
                            Me._voltageReading.Dispose()
                            Me._voltageReading = Nothing
                        End If

                    End If

                    ' Free shared unmanaged resources
                    ' onDisposeUnmanagedResources

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Opens a VISA session for the instruments at the given resource name.
        ''' </summary>
        ''' <param name="resourceName">Specifies the instrument resource name.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Overrides Function Connect(ByVal resourceName As String) As Boolean

            If MyBase.Connect(resourceName) Then

                ' set termination character to carriage return
                If MyBase.SerialSession IsNot Nothing Then
                    MyBase.SerialSession.TerminationCharacter = 13
                End If

                Return True
            Else
                Return False
            End If

        End Function

        ''' <summary>
        ''' Opens a VISA session for the instruments at the given port.
        ''' </summary>
        ''' <param name="address">Specifies the instrument port number.</param>
        ''' <returns>Returns True if success or false if it failed opening the session.</returns>
        Public Overloads Function Connect(ByVal address As Int32) As Boolean

            Return Connect(Helper.BuildSerialResourceName(address))

        End Function

#End Region

#Region " PROPERTIES "

        ''' <summary>Gets or sets reference to the instrument interface.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        Public Property Gui() As E382280InstrumentPanel
            Get
                Return CType(MyBase.BaseGui, E382280InstrumentPanel)
            End Get
            Set(ByVal Value As E382280InstrumentPanel)
                MyBase.BaseGui = Value
            End Set
        End Property

#End Region

#Region " EXTECH 382280 METHODS "

        ''' <summary>Resets and clears the device. Issues selective-device-clear, reset 
        '''   (RST), Clear Status (CLS, and clear error queue.</summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Overloads Overrides Function ResetAndClear() As Boolean

            ' Issue the Scpi specific set
            If MyBase.ResetAndClear() Then

                Try

                    ' allow operations to complete
                    Return True

                Catch ex As NationalInstruments.VisaNS.VisaException

                    StatusMessage = "Failed reset and clear due to Visa Exception. " & ex.Message

                End Try

            End If

            Return False

        End Function

#End Region

#Region " EXTECH 382280 PROPERTIES "

        Private _currentReading As Visa.ReadingR
        Public ReadOnly Property CurrentReading() As Visa.ReadingR
            Get
                Return _currentReading
            End Get
        End Property

        Const usingExternalCurrentMeter As Boolean = True
        Private _lastCurrentValue As String = "0"
        ''' <summary>Gets or sets the supply current
        ''' </summary>
        Public Property Current() As Double
            Get
                Dim outcome As Double = 0
                If usingExternalCurrentMeter Then
                    Dim inputBox As WinForms.InputBox = New WinForms.InputBox
                    inputBox.EnteredValue = _lastCurrentValue
                    inputBox.NumberStyle = Globalization.NumberStyles.Number
                    inputBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Enter {0} Supply Current", Me.InstanceName)
                    If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim newValue As String = inputBox.EnteredValue
                        If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                            Me._lastCurrentValue = newValue
                        End If
                    End If
                Else
                    MyBase.AwaitCommandInterval()
                    Dim question As String = "A?"
                    Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                    outcome = reply.MeasuredValue
                End If
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "A {0}", Value))
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Public Property NewCurrent() As Double
            Get
                If usingExternalCurrentMeter Then
                    Dim inputBox As WinForms.InputBox = New WinForms.InputBox
                    inputBox.EnteredValue = _lastCurrentValue
                    inputBox.NumberStyle = Globalization.NumberStyles.Number
                    inputBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Enter {0} Supply Current in {1}", Me.InstanceName, _currentReading.DisplayCaption.UnitsCaption)
                    If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim newValue As String = inputBox.EnteredValue
                        If Me._currentReading.ParseScaledReading(newValue) Then
                            Me._lastCurrentValue = newValue
                        End If
                    End If
                Else
                    MyBase.AwaitCommandInterval()
                    Dim question As String = "A?"
                    Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                    Me._currentReading.Reading = reply.MeasuredValueReply
                End If
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(_currentReading.Value, Globalization.CultureInfo.CurrentCulture)
                End If
                Return _currentReading.Value
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "A {0}", Value))
                If Me.Visible Then
                    Me.Gui.currentNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Private _lastFixedSupplyCurrent As String = "0"
        ''' <summary>Gets the value of the fixed supply current from the user.
        ''' </summary>
        Public ReadOnly Property FixedSupplyCurrent() As Double
            Get
                Dim inputBox As WinForms.InputBox = New WinForms.InputBox
                inputBox.EnteredValue = _lastFixedSupplyCurrent
                inputBox.NumberStyle = Globalization.NumberStyles.Number
                Dim outcome As Double = 0
                If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim newValue As String = inputBox.EnteredValue
                    If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                        Me._lastFixedSupplyCurrent = newValue
                    End If
                End If
                If Me.Visible Then
                    Me.Gui.fixedSupplyCurrentNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
        End Property

        Private _lastFixedSupplyVoltage As String = "0"
        ''' <summary>Gets the value of the fixed supply voltage from the user.
        ''' </summary>
        Public ReadOnly Property FixedSupplyVoltage() As Double
            Get
                Dim inputBox As WinForms.InputBox = New WinForms.InputBox
                inputBox.EnteredValue = _lastFixedSupplyVoltage
                inputBox.NumberStyle = Globalization.NumberStyles.Number
                Dim outcome As Double = 0
                If inputBox.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim newValue As String = inputBox.EnteredValue
                    If Double.TryParse(newValue, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, outcome) Then
                        Me._lastFixedSupplyVoltage = newValue
                    End If
                End If
                If Me.Visible Then
                    Me.Gui.fixedSupplyVoltageNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
        End Property

        'Private Const currentStatus As String = "CC"
        'Private Const voltageStatus As String = "CV"
        Private Const standbyStatus As String = "0V"
        ''' <summary>Gets or sets the instrument operation status.
        ''' </summary>
        Public Property OperationOn() As Boolean
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Return Not reply.Status.Equals(standbyStatus)
            End Get
            Set(ByVal Value As Boolean)
                MyBase.AwaitCommandInterval()
                If Value Then
                    MyBase.WriteTerminate("OPER")
                Else
                    MyBase.WriteTerminate("STBY")
                    MyBase.AwaitCommandInterval(2000)
                End If
                If Me.Visible Then
                    If Me.Gui.OperationToggleButton.Enabled Then
                        Me.Gui.OperationToggleButton.Enabled = False
                        Me.Gui.OperationToggleButton.Checked = Me.OperationOn
                        Me.Gui.OperationToggleButton.Invalidate()
                        Me.Gui.OperationToggleButton.Enabled = True
                    Else
                        Me.Gui.OperationToggleButton.Checked = Me.OperationOn
                    End If
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the supply voltage
        ''' </summary>
        Public Property Voltage() As Double
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Dim outcome As Double = reply.MeasuredValue
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(outcome, Globalization.CultureInfo.CurrentCulture)
                End If
                Return outcome
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "V {0}", Value))
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        ''' <summary>Gets or sets the supply voltage
        ''' </summary>
        Public Property NewVoltage() As Double
            Get
                MyBase.AwaitCommandInterval()
                Dim question As String = "V?"
                Dim reply As New OutputReply(MyBase.QueryTerminate(question))
                Me._voltageReading.Reading = reply.MeasuredValueReply
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Me._voltageReading.Value, Globalization.CultureInfo.CurrentCulture)
                End If
                Return Me._voltageReading.Value
            End Get
            Set(ByVal Value As Double)
                MyBase.AwaitCommandInterval()
                MyBase.WriteTerminate(String.Format(Globalization.CultureInfo.CurrentCulture, "V {0}", Value))
                If Me.Visible Then
                    Me.Gui.voltageNumericUpDown.Value = Convert.ToDecimal(Value, Globalization.CultureInfo.CurrentCulture)
                End If
            End Set
        End Property

        Private _voltageReading As Visa.ReadingR
        Public ReadOnly Property VoltageReading() As Visa.ReadingR
            Get
                Return _voltageReading
            End Get
        End Property

#End Region

    End Class
End Namespace

Public Class OutputReply

    Public Sub New(ByVal reply As String)
        MyBase.new()
        Me.ParseReply(reply)
    End Sub

    Private _queryType As String
    Public ReadOnly Property QueryType() As String
        Get
            Return _outputElements(0)
        End Get
    End Property

    Private _setValue As Double
    Public ReadOnly Property SetValue() As Double
        Get
            Return _setValue
        End Get
    End Property

    Private _measuredValue As Double
    Public ReadOnly Property MeasuredValue() As Double
        Get
            Return _measuredValue
        End Get
    End Property

    Private _status As String
    Public ReadOnly Property Status() As String
        Get
            Return _outputElements(3)
        End Get
    End Property

    Private _outputElements As String()
    Public ReadOnly Property SetValueReply() As String
        Get
            Return _outputElements(1)
        End Get
    End Property

    Public ReadOnly Property MeasuredValueReply() As String
        Get
            Return _outputElements(2)
        End Get
    End Property

    Private Function DefaultElements() As String()
        Dim elements As String() = {_queryType, _setValue.ToString(Globalization.CultureInfo.CurrentCulture) _
            , _measuredValue.ToString(Globalization.CultureInfo.CurrentCulture), _status}
        Return elements
    End Function

    ''' <summary>Query voltage or current and return a response array with elements.    ''' </summary>
    ''' <param name="reply"></param>
    Public Sub ParseReply(ByVal reply As String)
        _queryType = String.Empty
        _setValue = 0
        _measuredValue = 0
        _status = String.Empty
        _outputElements = DefaultElements()
        If Not String.IsNullOrEmpty(reply) Then
            _outputElements = reply.Split(" "c)
            If _outputElements.Length >= 4 Then
                _queryType = Me.QueryType
                _status = Me.Status
                If Double.TryParse(Me.SetValueReply, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, _setValue) Then
                End If
                If Double.TryParse(Me.MeasuredValueReply, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, _measuredValue) Then
                End If
            Else
                _outputElements = DefaultElements()
            End If
        End If
    End Sub

End Class