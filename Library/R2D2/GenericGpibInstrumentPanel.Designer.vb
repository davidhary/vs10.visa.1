Namespace R2D2

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class GenericGpibInstrumentPanel
        Inherits isr.Visa.InstrumentPanel

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)

            Try
                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.onDisposeManagedResources()

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

                ' Free shared unmanaged resources
                'onDisposeUnManagedResources()

            Finally

                ' Invoke the base class dispose method

                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.mainTabControl = New System.Windows.Forms.TabControl
            Me.readWriteTabPage = New System.Windows.Forms.TabPage
            Me.queryButton = New System.Windows.Forms.Button
            Me.readButton = New System.Windows.Forms.Button
            Me.readTextBox = New System.Windows.Forms.TextBox
            Me.readTextBoxLabel = New System.Windows.Forms.Label
            Me.readWriteStatusTextBox = New System.Windows.Forms.TextBox
            Me.readWriteStatusTextBoxLabel = New System.Windows.Forms.Label
            Me.writeButton = New System.Windows.Forms.Button
            Me.writeTextBox = New System.Windows.Forms.TextBox
            Me.writeTextBoxLabel = New System.Windows.Forms.Label
            Me.serviceRequestTabPage = New System.Windows.Forms.TabPage
            Me.enableEndOfSettlingRequestCheckBox = New System.Windows.Forms.CheckBox
            Me.serviceRequestRegisterGroupBox = New System.Windows.Forms.GroupBox
            Me.serviceRequestMaskRemoveButton = New System.Windows.Forms.Button
            Me.serviceRequestMaskAddButton = New System.Windows.Forms.Button
            Me.serviceRequestFlagsComboBox = New System.Windows.Forms.ComboBox
            Me.enableServiceRequestCheckBox = New System.Windows.Forms.CheckBox
            Me.serviceRequestByteTextBoxLabel = New System.Windows.Forms.Label
            Me.serviceRequestMaskTextBox = New System.Windows.Forms.TextBox
            Me.messagesTabPage = New System.Windows.Forms.TabPage
            Me.messagesMessageList = New isr.Visa.WinForms.MessagesBox
            Me.mainTabControl.SuspendLayout()
            Me.readWriteTabPage.SuspendLayout()
            Me.serviceRequestTabPage.SuspendLayout()
            Me.serviceRequestRegisterGroupBox.SuspendLayout()
            Me.messagesTabPage.SuspendLayout()
            Me.SuspendLayout()
            '
            'mainTabControl
            '
            Me.mainTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom
            Me.mainTabControl.Controls.Add(Me.readWriteTabPage)
            Me.mainTabControl.Controls.Add(Me.serviceRequestTabPage)
            Me.mainTabControl.Controls.Add(Me.messagesTabPage)
            Me.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.mainTabControl.Enabled = False
            Me.mainTabControl.Location = New System.Drawing.Point(0, 0)
            Me.mainTabControl.Multiline = True
            Me.mainTabControl.Name = "mainTabControl"
            Me.mainTabControl.SelectedIndex = 0
            Me.mainTabControl.Size = New System.Drawing.Size(312, 238)
            Me.mainTabControl.TabIndex = 15
            '
            'readWriteTabPage
            '
            Me.readWriteTabPage.Controls.Add(Me.queryButton)
            Me.readWriteTabPage.Controls.Add(Me.readButton)
            Me.readWriteTabPage.Controls.Add(Me.readTextBox)
            Me.readWriteTabPage.Controls.Add(Me.readTextBoxLabel)
            Me.readWriteTabPage.Controls.Add(Me.readWriteStatusTextBox)
            Me.readWriteTabPage.Controls.Add(Me.readWriteStatusTextBoxLabel)
            Me.readWriteTabPage.Controls.Add(Me.writeButton)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBox)
            Me.readWriteTabPage.Controls.Add(Me.writeTextBoxLabel)
            Me.readWriteTabPage.Location = New System.Drawing.Point(4, 4)
            Me.readWriteTabPage.Name = "readWriteTabPage"
            Me.readWriteTabPage.Size = New System.Drawing.Size(304, 212)
            Me.readWriteTabPage.TabIndex = 1
            Me.readWriteTabPage.Text = "Read/Write"
            Me.readWriteTabPage.UseVisualStyleBackColor = True
            '
            'queryButton
            '
            Me.queryButton.Location = New System.Drawing.Point(35, 184)
            Me.queryButton.Name = "queryButton"
            Me.queryButton.Size = New System.Drawing.Size(75, 23)
            Me.queryButton.TabIndex = 10
            Me.queryButton.Text = "&Query"
            '
            'readButton
            '
            Me.readButton.Location = New System.Drawing.Point(195, 184)
            Me.readButton.Name = "readButton"
            Me.readButton.Size = New System.Drawing.Size(75, 23)
            Me.readButton.TabIndex = 9
            Me.readButton.Text = "&Read"
            '
            'readTextBox
            '
            Me.readTextBox.Location = New System.Drawing.Point(12, 84)
            Me.readTextBox.Multiline = True
            Me.readTextBox.Name = "readTextBox"
            Me.readTextBox.Size = New System.Drawing.Size(280, 92)
            Me.readTextBox.TabIndex = 8
            '
            'readTextBoxLabel
            '
            Me.readTextBoxLabel.Location = New System.Drawing.Point(12, 68)
            Me.readTextBoxLabel.Name = "readTextBoxLabel"
            Me.readTextBoxLabel.Size = New System.Drawing.Size(148, 16)
            Me.readTextBoxLabel.TabIndex = 7
            Me.readTextBoxLabel.Text = "Message Received:"
            Me.readTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'readWriteStatusTextBox
            '
            Me.readWriteStatusTextBox.Location = New System.Drawing.Point(220, 8)
            Me.readWriteStatusTextBox.Name = "readWriteStatusTextBox"
            Me.readWriteStatusTextBox.Size = New System.Drawing.Size(72, 20)
            Me.readWriteStatusTextBox.TabIndex = 4
            '
            'readWriteStatusTextBoxLabel
            '
            Me.readWriteStatusTextBoxLabel.Location = New System.Drawing.Point(159, 10)
            Me.readWriteStatusTextBoxLabel.Name = "readWriteStatusTextBoxLabel"
            Me.readWriteStatusTextBoxLabel.Size = New System.Drawing.Size(56, 16)
            Me.readWriteStatusTextBoxLabel.TabIndex = 3
            Me.readWriteStatusTextBoxLabel.Text = "Status: "
            Me.readWriteStatusTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'writeButton
            '
            Me.writeButton.Location = New System.Drawing.Point(115, 184)
            Me.writeButton.Name = "writeButton"
            Me.writeButton.Size = New System.Drawing.Size(75, 23)
            Me.writeButton.TabIndex = 2
            Me.writeButton.Text = "&Write"
            '
            'writeTextBox
            '
            Me.writeTextBox.Location = New System.Drawing.Point(12, 32)
            Me.writeTextBox.Multiline = True
            Me.writeTextBox.Name = "writeTextBox"
            Me.writeTextBox.Size = New System.Drawing.Size(280, 32)
            Me.writeTextBox.TabIndex = 1
            Me.writeTextBox.Text = "*IDN?"
            '
            'writeTextBoxLabel
            '
            Me.writeTextBoxLabel.Location = New System.Drawing.Point(12, 16)
            Me.writeTextBoxLabel.Name = "writeTextBoxLabel"
            Me.writeTextBoxLabel.Size = New System.Drawing.Size(100, 16)
            Me.writeTextBoxLabel.TabIndex = 0
            Me.writeTextBoxLabel.Text = "Message To Send: "
            Me.writeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'serviceRequestTabPage
            '
            Me.serviceRequestTabPage.Controls.Add(Me.enableEndOfSettlingRequestCheckBox)
            Me.serviceRequestTabPage.Controls.Add(Me.serviceRequestRegisterGroupBox)
            Me.serviceRequestTabPage.Location = New System.Drawing.Point(4, 4)
            Me.serviceRequestTabPage.Name = "serviceRequestTabPage"
            Me.serviceRequestTabPage.Size = New System.Drawing.Size(304, 212)
            Me.serviceRequestTabPage.TabIndex = 4
            Me.serviceRequestTabPage.Text = "SRQ"
            Me.serviceRequestTabPage.UseVisualStyleBackColor = True
            '
            'enableEndOfSettlingRequestCheckBox
            '
            Me.enableEndOfSettlingRequestCheckBox.Location = New System.Drawing.Point(11, 102)
            Me.enableEndOfSettlingRequestCheckBox.Name = "enableEndOfSettlingRequestCheckBox"
            Me.enableEndOfSettlingRequestCheckBox.Size = New System.Drawing.Size(232, 32)
            Me.enableEndOfSettlingRequestCheckBox.TabIndex = 4
            Me.enableEndOfSettlingRequestCheckBox.Text = "Enable End of Settling Request"
            '
            'serviceRequestRegisterGroupBox
            '
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskRemoveButton)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskAddButton)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestFlagsComboBox)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.enableServiceRequestCheckBox)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestByteTextBoxLabel)
            Me.serviceRequestRegisterGroupBox.Controls.Add(Me.serviceRequestMaskTextBox)
            Me.serviceRequestRegisterGroupBox.Location = New System.Drawing.Point(8, 8)
            Me.serviceRequestRegisterGroupBox.Name = "serviceRequestRegisterGroupBox"
            Me.serviceRequestRegisterGroupBox.Size = New System.Drawing.Size(288, 88)
            Me.serviceRequestRegisterGroupBox.TabIndex = 3
            Me.serviceRequestRegisterGroupBox.TabStop = False
            Me.serviceRequestRegisterGroupBox.Text = "Service Request Register"
            '
            'serviceRequestMaskRemoveButton
            '
            Me.serviceRequestMaskRemoveButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.serviceRequestMaskRemoveButton.Location = New System.Drawing.Point(222, 56)
            Me.serviceRequestMaskRemoveButton.Name = "serviceRequestMaskRemoveButton"
            Me.serviceRequestMaskRemoveButton.Size = New System.Drawing.Size(24, 23)
            Me.serviceRequestMaskRemoveButton.TabIndex = 5
            Me.serviceRequestMaskRemoveButton.Text = "-  "
            '
            'serviceRequestMaskAddButton
            '
            Me.serviceRequestMaskAddButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.serviceRequestMaskAddButton.Location = New System.Drawing.Point(197, 56)
            Me.serviceRequestMaskAddButton.Name = "serviceRequestMaskAddButton"
            Me.serviceRequestMaskAddButton.Size = New System.Drawing.Size(24, 23)
            Me.serviceRequestMaskAddButton.TabIndex = 4
            Me.serviceRequestMaskAddButton.Text = "+  "
            '
            'serviceRequestFlagsComboBox
            '
            Me.serviceRequestFlagsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.serviceRequestFlagsComboBox.Location = New System.Drawing.Point(9, 56)
            Me.serviceRequestFlagsComboBox.Name = "serviceRequestFlagsComboBox"
            Me.serviceRequestFlagsComboBox.Size = New System.Drawing.Size(187, 21)
            Me.serviceRequestFlagsComboBox.TabIndex = 3
            '
            'enableServiceRequestCheckBox
            '
            Me.enableServiceRequestCheckBox.Location = New System.Drawing.Point(8, 16)
            Me.enableServiceRequestCheckBox.Name = "enableServiceRequestCheckBox"
            Me.enableServiceRequestCheckBox.Size = New System.Drawing.Size(152, 24)
            Me.enableServiceRequestCheckBox.TabIndex = 0
            Me.enableServiceRequestCheckBox.Text = "Enable Service Request"
            '
            'serviceRequestByteTextBoxLabel
            '
            Me.serviceRequestByteTextBoxLabel.Location = New System.Drawing.Point(8, 40)
            Me.serviceRequestByteTextBoxLabel.Name = "serviceRequestByteTextBoxLabel"
            Me.serviceRequestByteTextBoxLabel.Size = New System.Drawing.Size(120, 16)
            Me.serviceRequestByteTextBoxLabel.TabIndex = 1
            Me.serviceRequestByteTextBoxLabel.Text = "Service Request Mask: "
            '
            'serviceRequestMaskTextBox
            '
            Me.serviceRequestMaskTextBox.Location = New System.Drawing.Point(247, 56)
            Me.serviceRequestMaskTextBox.MaxLength = 3
            Me.serviceRequestMaskTextBox.Name = "serviceRequestMaskTextBox"
            Me.serviceRequestMaskTextBox.Size = New System.Drawing.Size(32, 20)
            Me.serviceRequestMaskTextBox.TabIndex = 2
            Me.serviceRequestMaskTextBox.Text = "239"
            '
            'messagesTabPage
            '
            Me.messagesTabPage.Controls.Add(Me.messagesMessageList)
            Me.messagesTabPage.Location = New System.Drawing.Point(4, 4)
            Me.messagesTabPage.Name = "messagesTabPage"
            Me.messagesTabPage.Size = New System.Drawing.Size(304, 212)
            Me.messagesTabPage.TabIndex = 3
            Me.messagesTabPage.Text = "Messages"
            Me.messagesTabPage.UseVisualStyleBackColor = True
            '
            'messagesMessageList
            '
            Me.messagesMessageList.BackColor = System.Drawing.SystemColors.Info
            Me.messagesMessageList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.messagesMessageList.Location = New System.Drawing.Point(0, 0)
            Me.messagesMessageList.Multiline = True
            Me.messagesMessageList.Name = "messagesMessageList"
            Me.messagesMessageList.NewMessagesCount = 0
            Me.messagesMessageList.PresetCount = 50
            Me.messagesMessageList.ReadOnly = True
            Me.messagesMessageList.ResetCount = 100
            Me.messagesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.messagesMessageList.Size = New System.Drawing.Size(304, 212)
            Me.messagesMessageList.TabIndex = 0
            Me.messagesMessageList.TimeFormat = "HH:mm:ss. "
            '
            'GenericGpibInstrumentPanel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.mainTabControl)
            Me.Name = "GenericGpibInstrumentPanel"
            Me.Size = New System.Drawing.Size(312, 288)
            Me.Controls.SetChildIndex(Me.mainTabControl, 0)
            Me.mainTabControl.ResumeLayout(False)
            Me.readWriteTabPage.ResumeLayout(False)
            Me.readWriteTabPage.PerformLayout()
            Me.serviceRequestTabPage.ResumeLayout(False)
            Me.serviceRequestRegisterGroupBox.ResumeLayout(False)
            Me.serviceRequestRegisterGroupBox.PerformLayout()
            Me.messagesTabPage.ResumeLayout(False)
            Me.messagesTabPage.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents mainTabControl As System.Windows.Forms.TabControl
        Friend WithEvents readWriteTabPage As System.Windows.Forms.TabPage
        Friend WithEvents queryButton As System.Windows.Forms.Button
        Friend WithEvents readButton As System.Windows.Forms.Button
        Friend WithEvents readTextBox As System.Windows.Forms.TextBox
        Friend WithEvents readTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents readWriteStatusTextBox As System.Windows.Forms.TextBox
        Friend WithEvents readWriteStatusTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents writeButton As System.Windows.Forms.Button
        Friend WithEvents writeTextBox As System.Windows.Forms.TextBox
        Friend WithEvents writeTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents serviceRequestTabPage As System.Windows.Forms.TabPage
        Friend WithEvents enableEndOfSettlingRequestCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents serviceRequestRegisterGroupBox As System.Windows.Forms.GroupBox
        Friend WithEvents serviceRequestMaskRemoveButton As System.Windows.Forms.Button
        Friend WithEvents serviceRequestMaskAddButton As System.Windows.Forms.Button
        Friend WithEvents serviceRequestFlagsComboBox As System.Windows.Forms.ComboBox
        Friend WithEvents enableServiceRequestCheckBox As System.Windows.Forms.CheckBox
        Friend WithEvents serviceRequestByteTextBoxLabel As System.Windows.Forms.Label
        Friend WithEvents serviceRequestMaskTextBox As System.Windows.Forms.TextBox
        Friend WithEvents messagesTabPage As System.Windows.Forms.TabPage
        Friend WithEvents messagesMessageList As isr.Visa.WinForms.MessagesBox

    End Class

End Namespace

