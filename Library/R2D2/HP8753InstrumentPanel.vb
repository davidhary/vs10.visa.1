Namespace R2D2

    ''' <summary>
    ''' A user interface user control for the HP8753 Network Analyzer GPIB instrument
    ''' class. Inherits <see cref="isr.Visa.InstrumentPanel">Instrument Panel</see>.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("HP8753 Network Analyzer Instrument - Windows Forms Custom Control")> _
    Public Class HP8753InstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            onInstantiate()

        End Sub

        ''' <summary>Initializes additional components.</summary>
        ''' <remarks>Called from the class constructor to add other controls or 
        '''   change anything set by the InitializeComponent method.</remarks>
        Private Function onInstantiate() As Boolean

            Try

                ' instantiate the base instrument and set reference to the control.
                MyBase.ResourceType = NationalInstruments.VisaNS.HardwareInterfaceType.Gpib

                ' instantiate the reference to the instrument
                Me._instrument = New HP8753Instrument(Me.Name)
                MyBase.BaseInstrument = Me._instrument

                ' set the refernece to the GUI
                Me._instrument.Gui = Me

                ' set default values.
                Me.defaultsComboBox.SelectedIndex = 0
                Me.SetDefaults()

                ' set default value for the device and enable connection:
                Me.connector.SelectedName = "GPIB0::16::INSTR"

                Return True

            Catch ex As isr.Visa.BaseException

                ' throw an exception
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed initializing", Me.Name)
                Throw New isr.Visa.BaseException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Updates the user interface upon successful connection.
        ''' </summary>
        Public Overrides Function Connect() As Boolean

            ' enable user interface
            Me.mainTabControl.Enabled = True

            ' refresh the display.
            Me.refreshDisplay()

            If True Then
            Else
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer name: {0}", Me._instrument.GpibSession.ResourceManufacturerName))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer ID: 0x{0:X}", Me._instrument.GpibSession.ResourceManufacturerID))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Connectred to {0}", Me._instrument.Id()))
            End If

            Return MyBase.Connect()

        End Function

        ''' <summary>
        ''' Updates the user interface upon disconnection.
        ''' </summary>
        Public Overrides Function Disconnect() As Boolean

            Me.mainTabControl.Enabled = False
            Return MyBase.Disconnect()

        End Function

        ''' <summary>Display a status message.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub DisplayMessage(ByVal message As String)

            MyBase.DisplayMessage(message)
            Me.PushMessage(message)

        End Sub

        ''' <summary>Adds a message to the message list.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub PushMessage(ByVal message As String)
            messagesMessageList.PushMessage(message)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.R2D2.HP8753Instrument
        ''' <summary>Gets a reference to the HP 8753 instrument.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        <System.ComponentModel.Browsable(False)> Public ReadOnly Property Instrument() As isr.Visa.R2D2.HP8753Instrument
            Get
                Return _instrument
            End Get
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub readMarkerButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readMarkerButton.Click
            Try
                Me._instrument.ReadMarker(Convert.ToInt32(Me.markerNumberUpDown.Value, Globalization.CultureInfo.CurrentCulture))
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading marker"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub localButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles localButton.Click
            Try
                Me.Instrument.GotoLocal()
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout going to local"
                Else
                    Throw
                End If
            End Try
        End Sub

        Friend Enum TestParameterIndex
            None
            S11
            S22
            S21Magnitude
            S21Phase
        End Enum

        Friend Enum MeasurementParameterIndex
            None
            S11
            S12
            S21
            S22
        End Enum

        Friend Enum ScaleIndex
            None
            LogMagnitude
            Phase
        End Enum

        Private Sub SetDefaults()

            MyBase.statusPanel.Text = "Setting defaults..."

            Dim testIndex As TestParameterIndex = CType(Me.defaultsComboBox.SelectedIndex + 1, TestParameterIndex)
            Select Case testIndex
                Case TestParameterIndex.S11
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S11 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S22
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S22 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S21Magnitude
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S21 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.LogMagnitude - 1
                Case TestParameterIndex.S21Phase
                    Me.parameterComboBox.SelectedIndex = MeasurementParameterIndex.S21 - 1
                    Me.scaleComboBox.SelectedIndex = ScaleIndex.Phase - 1
            End Select

            Me.markerOneNumeric.Value = 1227
            Me.markerTwoNumeric.Value = 1575
            Me.traceStartNumeric.Value = 1200
            Me.traceStopNumeric.Value = 1600
            Me.powerNumeric.Value = -20
            Me.channelNumeric.Value = 1

            MyBase.statusPanel.Text = "Done setting defaults."

        End Sub

        Private Sub defaultsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles defaultsButton.Click
            SetDefaults()
        End Sub

        Private Sub applyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyButton.Click

            Dim message As String = String.Empty
            Try

                MyBase.statusPanel.Text = "Applying..."

                message = "Trace Start"
                Me._instrument.TraceStart = Convert.ToInt32(Me.traceStartNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Trace Stop"
                Me._instrument.TraceStop = Convert.ToInt32(Me.traceStopNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Marker 1 Position"
                Me._instrument.MarkerOnePosition = Convert.ToInt32(Me.markerOneNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Marker 2 Position"
                Me._instrument.MarkerTwoPosition = Convert.ToInt32(Me.markerTwoNumeric.Value, Globalization.CultureInfo.CurrentCulture)
                message = "Power Level"
                Me._instrument.PowerLevel = Convert.ToDouble(Me.powerNumeric.Value, Globalization.CultureInfo.CurrentCulture)

                If Me.channelNumeric.Value = 1 Then
                    message = "channel one"
                    Me._instrument.ChannelOne = True
                ElseIf Me.channelNumeric.Value = 2 Then
                    message = "channel two"
                    Me._instrument.ChannelTwo = True
                End If

                message = "measurement"
                Me._instrument.Measure = Me.parameterComboBox.Text

                message = "scale format"
                Me._instrument.ScaleFormat = Me.scaleComboBox.Text

                MyBase.statusPanel.Text = "Apply completed."

            Catch ex As NationalInstruments.VisaNS.VisaException

                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout applying " & message
                Else
                    Throw New isr.Visa.BaseException("Failed setting " & message, ex)
                End If

            Catch ex As Exception

                Throw New isr.Visa.BaseException("Failed setting " & message, ex)

            End Try

        End Sub

#Region " READ / WRITE "

        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.QueryTrimEnd(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout querying"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.GpibSession.ReadString()
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Try
                Me._instrument.GpibSession.Write(Me.writeTextBox.Text)
                Me.readWriteStatusTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "0x{0:X}", _
                    Me._instrument.GpibSession.ReadStatusByte())
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading status"
                Else
                    Throw
                End If
            End Try
        End Sub

#End Region

#End Region

    End Class

End Namespace
