Namespace R2D2

    ''' <summary>Provides a user interface for the EXTECH 382280 Power Supply.</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
    ''' Created
    ''' </history>
    <System.ComponentModel.Description("EXTECH 382280 Power Supply - Windows Forms Custom Control")> _
    Public Class E382280InstrumentPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()

            MyBase.New()

            ' Initialize user components that might be affected by resize or paint actions
            'onInitialize()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call
            onInstantiate()

        End Sub

        ''' <summary>Initializes additional components.</summary>
        ''' <remarks>Called from the class constructor to add other controls or 
        '''   change anything set by the InitializeComponent method.</remarks>
        Private Function onInstantiate() As Boolean

            Try

                ' instantiate the base instrument and set reference to the control.
                MyBase.ResourceType = NationalInstruments.VisaNS.HardwareInterfaceType.Serial

                ' instantiate the reference to the instrument
                Me._instrument = New E382280Instrument(Me.Name)
                MyBase.BaseInstrument = Me._instrument

                ' set the refernece to the GUI
                Me._instrument.Gui = Me

                ' set default value for the device and enable connection:
                Me.connector.SelectedName = "ASRL1::INSTR"

                Return True

            Catch ex As isr.Visa.BaseException

                ' throw an exception
                MyBase.StatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} failed initializing", Me.Name)
                Throw New isr.Visa.BaseException(MyBase.StatusMessage, ex)

            End Try

        End Function

        ''' <summary>
        ''' Disposed of managed resources.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub onDisposeManagedResources()
            If Me._instrument IsNot Nothing Then
                Me._instrument.Dispose()
                Me._instrument = Nothing
            End If
        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Updates the user interface upon successful connection.
        ''' </summary>
        Public Overrides Function Connect() As Boolean

            ' enable user interface
            Me.mainTabControl.Enabled = True

            ' enable the operation control
            Me.OperationToggleButton.Enabled = False
            Me.OperationToggleButton.Checked = True
            Me.OperationToggleButton.Checked = False
            Me.OperationToggleButton.Invalidate()
            Me.OperationToggleButton.Enabled = True

            ' update the current scan list
            Me.refreshDisplay()

            If True Then
            Else
                ' check if a SCPI instrument and if so display this 
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer name: {0}", Me._instrument.SerialSession.ResourceManufacturerName))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Manufacturer ID: 0x{0:X}", Me._instrument.SerialSession.ResourceManufacturerID))
                Me.PushMessage(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "Connectred to {0}", Me._instrument.Id()))
            End If

            Return MyBase.Connect()

        End Function

        ''' <summary>
        ''' Updates the user interface upon disconnection.
        ''' </summary>
        Public Overrides Function Disconnect() As Boolean

            ' disable the control panel
            Me.controlPanel.Enabled = False

            ' disable the operation control
            Me.OperationToggleButton.Enabled = False

            ' disable the tabs
            Me.mainTabControl.Enabled = False

            Return MyBase.Disconnect()

        End Function

        ''' <summary>Display a status message.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub DisplayMessage(ByVal message As String)

            MyBase.DisplayMessage(message)
            Me.PushMessage(message)

        End Sub

        ''' <summary>Adds a message to the message list.</summary>
        ''' <param name="message">Specifies the message to add.</param>
        Public Overrides Sub PushMessage(ByVal message As String)
            messagesMessageList.PushMessage(message)
        End Sub

#End Region

#Region " PROPERTIES "

        Private _instrument As isr.Visa.R2D2.E382280Instrument
        ''' <summary>Gets a reference to the EXTECH 382280 instrument.</summary>
        ''' <value></value>
        ''' <remarks></remarks>
        <System.ComponentModel.Browsable(False)> Public ReadOnly Property Instrument() As isr.Visa.R2D2.E382280Instrument
            Get
                Return _instrument
            End Get
        End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Updates the display.</summary>
        Friend Sub refreshDisplay()

            If MyBase.Visible Then

            End If

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

#Region " READ / WRITE "

        Private Sub queryButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles queryButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.SerialSession.Query( _
                    Me.writeTextBox.Text & Convert.ToChar(Me._instrument.SerialSession.TerminationCharacter))
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub readButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readButton.Click
            Try
                Me.readTextBox.Text = Me._instrument.SerialSession.ReadString()
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout reading"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub writeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeButton.Click
            Me._instrument.SerialSession.Write(Me.writeTextBox.Text)
            Me._instrument.SerialSession.Write(Convert.ToChar(Me._instrument.SerialSession.TerminationCharacter))
        End Sub

#End Region

        Private Sub OperationToggleButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperationToggleButton.CheckedChanged

            If Me.OperationToggleButton.Enabled Then
                Try
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    Me._instrument.OperationOn = Me.OperationToggleButton.Checked
                    Me.controlPanel.Enabled = Me.OperationToggleButton.Checked
                Catch
                    Throw
                Finally
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                End Try
            End If

            If Me.OperationToggleButton.Checked Then
                Me.OperationToggleButton.Text = "OPERATION: ON"
            Else
                Me.OperationToggleButton.Text = "OPERATION: STANDBY"
            End If

        End Sub

        Private Sub getVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getVoltageButton.Click
            Try
                MyBase.statusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
                  "Reading = {0}", Me._instrument.Voltage)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub setVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles setVoltageButton.Click
            Try
                Me._instrument.Voltage = Me.voltageNumericUpDown.Value
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub setCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles setCurrentButton.Click
            Try
                Me._instrument.Current = Me.currentNumericUpDown.Value
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getCurrentButton.Click
            Try
                MyBase.statusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
                  "Reading = {0}", Me._instrument.Current)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getFixedSupplyCurrentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getFixedSupplyCurrentButton.Click
            Try
                MyBase.statusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
                  "Reading = {0}", Me._instrument.FixedSupplyCurrent)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub getFixedSupplyVoltageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getFixedSupplyVoltageButton.Click
            Try
                MyBase.statusPanel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
                  "Reading = {0}", Me._instrument.FixedSupplyVoltage)
            Catch ex As NationalInstruments.VisaNS.VisaException
                If ex.ErrorCode = NationalInstruments.VisaNS.VisaStatusCode.ErrorTimeout Then
                    MyBase.statusPanel.Text = "Timeout"
                Else
                    Throw
                End If
            End Try
        End Sub

#End Region

    End Class

End Namespace
