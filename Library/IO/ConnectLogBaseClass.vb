Namespace IO
    ''' <summary>An abstract base class for use with class libraries that require
    '''   Connect and Disconnect constracts. 
    ''' </summary>
    ''' <remarks>
    ''' Provides standard properties such as an instance name and 
    ''' status message and requires a dispose method.  This class also provides 
    ''' Connect and Disconnect templates.
    ''' Implements IDisposable.
    ''' </remarks>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/07/06" by="David Hary" revision="1.0.2228.x">
    ''' Created
    ''' </history>
    Public MustInherit Class ConnectLogBaseClass

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Protected Sub New()

            Me.New(String.Empty)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        Protected Sub New(ByVal instanceName As String)

            MyBase.New()

            Me._instanceName = instanceName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' GC.SuppressFinalize is issued to take this object off the 
            ' finalization(Queue) and prevent finalization code for this 
            ' prevent from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                Try

                    If disposing Then

                        ' Free managed resources when explicitly called
                        _statusMessage = String.Empty
                        _instanceName = String.Empty

                    End If

                    ' Free shared unmanaged resources

                Finally

                    Me.IsDisposed = True

                End Try

            End If

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

        ''' <summary>
        ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
        ''' </summary>
        ''' <remarks>
        ''' Use this method to return the instance name. If instance name is not set, 
        ''' returns the base class ToString value.
        ''' </remarks>
        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return MyBase.ToString & "." & Me._instanceName
            End If
        End Function

        Private _instanceName As String = String.Empty
        ''' <summary>Gets or sets the name given to an instance of this class.</summary>
        ''' <value><c>InstanceName</c> is a String property.</value>
        Public Property InstanceName() As String
            Get
                If Not String.IsNullOrEmpty(_instanceName) Then
                    Return _instanceName
                Else
                    Return MyBase.ToString
                End If
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _statusMessage As String = String.Empty
        ''' <summary>Gets or sets the status message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        Public Property StatusMessage() As String
            Get
                Return _statusMessage
            End Get
            Set(ByVal value As String)
                _statusMessage = value
            End Set
        End Property

#End Region

#Region " CONNECT and DISCONNECT "

        ''' <summary>Clears the resource.</summary>
        Public MustOverride Sub Clear()

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <remarks></remarks>
        Public Overridable Overloads Function Connect(ByVal resourceName As String) As Boolean
            Me.ResourceName = resourceName
            Me.IsConnected = True
            Return Me.IsConnected
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        Public Overridable Overloads Function Connect() As Boolean
            Me.IsConnected = True
            Return Me.IsConnected
        End Function

        ''' <summary>Disconnects this instance.</summary>
        ''' <returns><c>True</c> if the instance disconnected.</returns>
        Public Overridable Function Disconnect() As Boolean
            Me.IsConnected = False
            Return Not Me.IsConnected
        End Function

        Private _isConnected As Boolean
        ''' <summary>Gets or sets the connect status flag.</summary>
        ''' <value><c>True</c> if connected.</value>
        Public Property IsConnected() As Boolean
            Get
                Return _isConnected
            End Get
            Set(ByVal value As Boolean)
                _isConnected = value
            End Set
        End Property

        Private _resourceName As String = String.Empty
        ''' <summary>Gets or sets the resource name.</summary>
        ''' <value><c>ResourceName</c> is a String property.</value>
        Public Property ResourceName() As String
            Get
                Return _resourceName
            End Get
            Set(ByVal value As String)
                _resourceName = value
                If String.IsNullOrEmpty(Me._instanceName) Then
                    Me._instanceName = value
                End If
            End Set
        End Property

        Private _usingDevices As Boolean = True
        ''' <summary>
        ''' True when using actual devices or False for runnning the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean
            Get
                Return _usingDevices
            End Get
            Set(ByVal Value As Boolean)
                _usingDevices = Value
            End Set
        End Property

#End Region

#Region " LOG WRITER "

        ''' <summary>
        ''' Logs the specified message.
        ''' </summary>
        ''' <param name="message"></param>
        ''' <param name="severity"></param>
        ''' <remarks></remarks>
        Protected Sub LogMessage(ByVal message As String, ByVal severity As TraceEventType)
            If Me.UsingLogWriter Then
                Me.LogWriter.WriteLine(message, severity)
            End If
        End Sub

        Private _logWriter As IO.LogWriter
        ''' <summary>Gets or sets reference to the application log writer.</summary>
        Protected ReadOnly Property LogWriter() As IO.LogWriter
            Get
                If _logWriter Is Nothing Then
                    ' instantiate a log writer
                    _logWriter = New IO.LogWriter(Me.TraceLevelSwitch)
                    _logWriter.FileNameBuilder.ApplicationSiblingFolderName = "Logs"
                    _logWriter.FileNameBuilder.SubfolderName = Me.ToString
                    _logWriter.FileNameBuilder.FilePathName = _logWriter.FileNameBuilder.BuildFilePathName()
                End If
                Return _logWriter
            End Get
        End Property

        Private _logWriterSwitch As BooleanSwitch
        ''' <summary>Gets or sets the Log Writer switch value.</summary>
        ''' <value><c>UsingLogWriter</c> is a Boolean property that can be read from (read only).</value>
        ''' <remarks>Use this property to check if the application is to use a log writer tracer.</remarks>
        Public Property LogWriterSwitch() As BooleanSwitch
            Get
                If _logWriterSwitch Is Nothing Then
                    Dim elementName As String = String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                        "Datalog{0}", Me)
                    _logWriterSwitch = New BooleanSwitch(elementName, "True(1) if using a Log Writer")
                End If
                Return _logWriterSwitch
            End Get
            Set(ByVal Value As BooleanSwitch)
                _logWriterSwitch = Value
            End Set
        End Property

        ''' <summary>Gets or sets the Log Writer switch value.</summary>
        ''' <value><c>UsingLogWriter</c> is a Boolean property that can be read from (read only).</value>
        ''' <remarks>Use this property to check if the application is to use a log writer tracer.</remarks>
        Public Property UsingLogWriter() As Boolean
            Get
                Return Me.LogWriterSwitch.Enabled
            End Get
            Set(ByVal Value As Boolean)
                Me.LogWriterSwitch.Enabled = Value
            End Set
        End Property

        Private _traceLevelSwitch As TraceSwitch
        ''' <summary>Gets or sets the Trace Level switch value.</summary>
        ''' <value><c>TraceLevelSwitch</c> is a Boolean property that can be read from (read only).</value>
        ''' <remarks>Use this property to get the trace level.</remarks>
        Protected Property TraceLevelSwitch() As TraceSwitch
            Get
                If _traceLevelSwitch Is Nothing Then
                    _traceLevelSwitch = New TraceSwitch("TraceLevel", "Trace Level Switch")
                End If
                Return _traceLevelSwitch
            End Get
            Set(ByVal Value As TraceSwitch)
                _traceLevelSwitch = Value
            End Set
        End Property

#End Region

    End Class

End Namespace
