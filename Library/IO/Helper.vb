Namespace IO
    ''' <summary>Includes file support operations</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="08/03/04" by="David Hary" revision="1.0.1676.x">
    ''' Created
    ''' </history>
    ''' <history date="01/14/06" by="David Hary" revision="1.0.2205.x">
    '''   Use path name for the full folder and file names.  Rename from Filer.
    ''' </history>
    Public NotInheritable Class Helper

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Private 'constructor' to prevent instantiation of this class.</summary>
        Private Sub New()
        End Sub

#End Region

#Region " FOLDER "

        ''' <summary>Creates a sibling folder for the application.</summary>
        ''' <param name="folderName">Specifies the folder name.</param>
        Public Shared Function CreateApplicationSiblingFolder(ByVal folderName As String) As String

            Dim parentFolderPathName As String = New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath).Directory.Parent.FullName
            Dim di As New System.IO.DirectoryInfo(parentFolderPathName)
            If di.Exists Then
                di = di.CreateSubdirectory(folderName)
                If di.Exists Then
                    Return di.FullName
                Else
                    Return String.Empty
                End If
            Else
                Return String.Empty
            End If
            If Not di.Exists Then
                di.Create()
                If di.Exists Then
                    Return di.FullName
                Else
                    Return String.Empty
                End If
            End If

        End Function

        ''' <summary>Creates a folder if not there</summary>
        ''' <param name="folderPathName"></param>
        Public Shared Function CreateFolder(ByVal folderPathName As String) As String

            ' check if the foler exists
            If String.IsNullOrEmpty(folderPathName) Then
                Return String.Empty
            Else
                If folderPathName.LastIndexOf("\") < folderPathName.Length - 1 Then
                    ' make sure the path name ends with the \ otherwise, Exists may return
                    ' true refering to the parent rather than the sought-for folder.
                    folderPathName &= "\"
                End If
                Dim fi As New System.IO.FileInfo(folderPathName)
                If Not fi.Directory.Exists Then
                    fi.Directory.Create()
                End If
                Return fi.Directory.ToString
            End If

        End Function

        ''' <summary>Gets a sibling folder to the application folder</summary>
        ''' <param name="folderName"></param>
        ''' <returns>App.Path\..\Data</returns>
        Public Shared Function GetApplicationSiblingFolderPathName(ByVal folderName As String) As String

            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            Return fi.Directory.Parent.FullName & "\" & folderName

        End Function

        ''' <summary>Returns true if folder exists.</summary>
        ''' <param name="folderPathName"></param>
        Public Shared Function FolderExists(ByVal folderPathName As String) As Boolean

            If String.IsNullOrEmpty(folderPathName) Then
                Return False
            Else
                Return System.IO.Directory.Exists(folderPathName)
            End If

        End Function

#End Region

#Region " FILE "

        ''' <summary>Returns a file information for the given or default file names.  If both are
        '''   empty, this method returns the file info based on the execution path.</summary>
        ''' <param name="pathName">Specifies the file path name.</param>
        ''' <param name="defaultPathName"></param>
        Public Shared Function GetFileInfo(ByVal pathName As String, ByVal defaultPathName As String) As System.IO.FileInfo

            If Not String.IsNullOrEmpty(pathName) Then
                Return New System.IO.FileInfo(pathName)
            ElseIf Not String.IsNullOrEmpty(defaultPathName) Then
                Return New System.IO.FileInfo(defaultPathName)
            Else
                Return New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            End If

        End Function

        ''' <summary>Returns the file name title</summary>
        ''' <param name="pathName">Specifies the file path name.</param>
        Public Shared Function GetFileTitle(ByVal pathName As String) As String

            If String.IsNullOrEmpty(pathName) Then
                Return String.Empty
            Else
                Return System.IO.Path.GetFileNameWithoutExtension(pathName)
            End If

        End Function

        ''' <summary>Returns the file name appended with a serial number if the file
        '''   already exists using the default number format {0:D}.</summary>
        ''' <param name="pathName">Specifies the file path name.</param>
        Public Shared Function GetNextFilePathName(ByVal pathName As String) As String

            Return GetNextFilePathName(pathName, "D")

        End Function

        ''' <summary>Returns the file name appended with a serial number if the file
        '''   already exists.</summary>
        ''' <param name="pathName">Specifies the file path name.</param>
        ''' <param name="fileCountFormat">Specifies the format for the file number suffix.</param>
        Public Shared Function GetNextFilePathName(ByVal pathName As String, ByVal fileCountFormat As String) As String

            If String.IsNullOrEmpty(pathName) Then

                Return String.Empty

            End If

            ' the number given to the file equals the number of files located.
            Dim fileCount As Int64

            ' >> Initialize The Objects
            Dim fileNameFormat As String = System.IO.Directory.GetParent(pathName).ToString _
                & "\" & System.IO.Path.GetFileNameWithoutExtension(pathName) & "{0:" & _
                fileCountFormat & "}" & System.IO.Path.GetExtension(pathName)

            ' loop until the file is not found
            While System.IO.File.Exists(pathName)

                ' if file found, create a new name
                fileCount += 1
                pathName = String.Format(System.Globalization.CultureInfo.CurrentCulture, fileNameFormat, fileCount)

            End While

            ' return the new file name
            Return pathName

        End Function

        ''' <summary>Returns true if file exists</summary>
        ''' <param name="pathName"></param>
        Public Shared Function FileExists(ByVal pathName As String) As Boolean

            If String.IsNullOrEmpty(pathName) Then
                Return False
            Else
                Return System.IO.File.Exists(pathName)
            End If

        End Function

        ''' <summary>Returns the file size or negative if error of file not found.</summary>
        ''' <param name="pathName"></param>
        Public Shared Function FileSize(ByVal pathName As String) As Long

            If String.IsNullOrEmpty(pathName) Then
                Return -2
            ElseIf Not System.IO.File.Exists(pathName) Then
                Return -1
            Else
                Dim info As System.IO.FileInfo = New System.IO.FileInfo(pathName)
                Return info.Length
            End If
        End Function

#End Region

#Region " TRACE LOG "

        ''' <summary>
        ''' Derive a <see cref="SourceLevels">source level</see> for the trace switch given the trace level.
        ''' </summary>
        ''' <param name="eventType"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ParseSourceLevel(ByVal eventType As TraceEventType) As SourceLevels

            Select Case eventType
                Case TraceEventType.Error
                    Return SourceLevels.Error
                Case TraceEventType.Information
                    Return SourceLevels.Information
                Case TraceEventType.Suspend
                    Return SourceLevels.Off
                Case TraceEventType.Verbose
                    Return SourceLevels.Verbose
                Case TraceEventType.Warning
                    Return SourceLevels.Warning
                Case TraceEventType.Critical
                    Return SourceLevels.Critical
                Case Else
                    Return SourceLevels.Information
            End Select

        End Function

#End Region
    End Class

End Namespace
