Namespace Algebra

    ''' <summary>Provides scalar calculations</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="03/19/04" by="David Hary" revision="1.0.1539.x">
    ''' Created
    ''' </history>
    Public NotInheritable Class Scalar

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Private 'constructor' to prevent instantiation of this class.</summary>
        Private Sub New()
        End Sub

#End Region

#Region " NUMERIC CONVERSIONS "

        ''' <summary>
        ''' Truncates a value to the desired precision.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Truncate(ByVal value As Int32) As Int16

            value = value And UInt16.MaxValue ' &HFFFFI ' 65535
            If value > Int16.MaxValue Then
                value -= (UInt16.MaxValue + 1) ' -65536
            End If
            Return Convert.ToInt16(value)

        End Function

        ''' <summary>
        ''' Truncates a value to the desired precision.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Truncate(ByVal value As Int64) As Integer

            value = value And UInt32.MaxValue ' &HFFFFFFFFI
            If value > Int32.MaxValue Then
                value -= (UInt32.MaxValue + 1)
            End If
            Return Convert.ToInt32(value)

        End Function

#End Region

#Region " SAFE NATH "

        ''' <summary>
        ''' Returns true if the two values are within the specified absolute precision.
        ''' </summary>
        ''' <param name="left">left hand value</param>
        ''' <param name="right">right hand value</param>
        ''' <param name="precision">Absolute minimal diffecrence for relative equality.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Approximates(ByVal left As Double, ByVal right As Double, ByVal precision As Double) As Boolean

            Return (right = left) OrElse Math.Abs(left - right) <= precision

        End Function

        ''' <summary>
        ''' Returns true if the two values are within the specified relative precision.
        ''' </summary>
        ''' <param name="left">left hand value</param>
        ''' <param name="right">right hand value</param>
        ''' <param name="significantDigits">Defines relative precising based on the significant digits of the 
        ''' <see cref="Hypotenuse"></see></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function Approximates(ByVal left As Double, ByVal right As Double, ByVal significantDigits As Integer) As Boolean
            Return (right = left) OrElse Math.Abs(left - right) < Hypotenuse(left, right) / If(significantDigits >= 15, 1.0E+16, 10 ^ (significantDigits - 1))
        End Function

        ''' <summary>Calculates sqrt(a^2 + b^2) without under/overflow.</summary>
        Public Shared Function Hypotenuse(ByVal left As Double, ByVal right As Double) As Double
            Dim r As Double
            If Math.Abs(left) > Math.Abs(right) Then
                r = right / left
                r = Math.Abs(left) * Math.Sqrt((1 + r * r))
            ElseIf right <> 0 Then
                r = left / right
                r = Math.Abs(right) * Math.Sqrt((1 + r * r))
            Else
                r = 0.0
            End If
            Return r
        End Function

        ''' <summary>Calculate a base 10 logarithm in a safe manner to avoid math exceptions</summary>
        ''' <param name="value">The value for which the logarithm is to be calculated</param>
        ''' <returns>The value of the logarithm, or 0 if the <paramref name="x"/>
        ''' argument was negative or zero</returns>
        Public Shared Function Log10(ByVal value As Double) As Double
            Return Math.Log10(Math.Max(value, Double.Epsilon))
        End Function

        ''' <summary>Calculate the modulus (remainder) in a safe manner so that divide
        '''   by zero errors are avoided</summary>
        ''' <param name="value">The divisor</param>
        ''' <param name="dividend">The dividend</param>
        ''' <returns>the value of the modulus, or zero for the divide-by-zero case</returns>
        Public Shared Function [Mod](ByVal value As Double, ByVal dividend As Double) As Double

            Dim temp As Double
            If dividend = 0 Then
                Return 0
            End If
            temp = value / dividend
            Return dividend * (temp - Math.Floor(temp))
        End Function

#End Region

#Region " SCALE METHODS "

        ''' <summary>Returns the number of decimal places for displaying the 
        '''   auto value.</summary>
        Public Shared Function DecimalPlaces(ByVal value As Double) As Integer
            Dim candidate As Int32 = 0
            value = Math.IEEERemainder(Math.Abs(value), 1)
            Dim minimum As Double = Math.Max(value / 1000.0, 0.0000001)
            Do While value > minimum
                candidate += 1
                value = Math.IEEERemainder(10 * value, 1)
            Loop
            Return candidate
        End Function

#End Region

#Region " INTERPOLATE "

        ''' <summary>
        ''' Interpolate over the unity range.
        ''' </summary>
        ''' <param name="minValue">The minimum range point.</param>
        ''' <param name="maxValue">The maximum range point.</param>
        ''' <returns>Returns the interpolated output over the specified range relative to the 
        ''' value over the unity [0,1) range.</returns>
        ''' <remarks></remarks>
        Public Shared Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double) As Double
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary>
        ''' Interpolate over the unity range.
        ''' </summary>
        ''' <param name="minValue">The minimum range point.</param>
        ''' <param name="maxValue">The maximum range point.</param>
        ''' <returns>Returns the interpolated output over the specified range relative to the 
        ''' value over the unity [0,1) range.</returns>
        ''' <remarks></remarks>
        Public Shared Function Interpolate(ByVal value As Int64, ByVal minValue As Int64, ByVal maxValue As Int64) As Int64
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary>
        ''' Interpolate over the unity range.
        ''' </summary>
        ''' <param name="minValue">The minimum range point.</param>
        ''' <param name="maxValue">The maximum range point.</param>
        ''' <returns>Returns the interpolated output over the specified range relative to the 
        ''' value over the unity [0,1) range.</returns>
        ''' <remarks></remarks>
        Public Shared Function Interpolate(ByVal value As Int32, ByVal minValue As Int32, ByVal maxValue As Int32) As Int32
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary>
        ''' Interpolate over the unity range.
        ''' </summary>
        ''' <param name="minValue">The minimum range point.</param>
        ''' <param name="maxValue">The maximum range point.</param>
        ''' <returns>Returns the interpolated output over the specified range relative to the 
        ''' value over the unity [0,1) range.</returns>
        ''' <remarks></remarks>
        Public Shared Function Interpolate(ByVal value As Single, ByVal minValue As Single, ByVal maxValue As Single) As Single
            Return value * (maxValue - minValue) + minValue
        End Function

        ''' <summary>Linearly interpolated value.</summary>
        ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
        ''' <param name="minValue">The first abscissa value.</param>
        ''' <param name="maxValue">The second abscissa value.</param>
        ''' <param name="minOutput">The first ordinate value.</param>
        ''' <param name="maxOutput">The second ordinate value.</param>
        ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
        '''   coordinates.</returns>
        ''' <remarks></remarks>
        Public Shared Function Interpolate(ByVal value As Double, ByVal minValue As Double, ByVal maxValue As Double, ByVal minOutput As Double, ByVal maxOutput As Double) As Double

            ' check if we have two distinct pairs
            If maxValue = minValue Then
                ' if we have identical value values, return the
                ' average of the ordinate value
                Return 0.5 * (minOutput + maxOutput)
            Else
                Return minOutput + (value - minValue) * (maxOutput - minOutput) / (maxValue - minValue)
            End If

        End Function

        ''' <summary>Linearly interpolated value.</summary>
        ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
        ''' <param name="point1">The first point.</param>
        ''' <param name="point2">The second point.</param>
        ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
        '''   coordinates.</returns>
        ''' <remarks></remarks>
        Shared Function Interpolate(ByVal value As Double, ByVal point1 As System.Drawing.PointF, ByVal point2 As System.Drawing.PointF) As Double

            ' check if we have two distinct pairs
            If point2.X = point2.X Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return 0.5 * (point1.Y + point2.Y)
            Else
                Return point1.Y + (value - point1.X) * (point2.Y - point1.Y) / (point2.X - point1.X)
            End If

        End Function

        ''' <summary>Linearly interpolated value.</summary>
        ''' <param name="value">The abscissa value to interpolate to an ordinate value</param>
        ''' <param name="rectangle">The rectangle of coordinates.</param>
        ''' <returns>The abscissa value derived using linear interpolation based on a pair of 
        '''   coordinates.</returns>
        ''' <remarks></remarks>
        Shared Function Interpolate(ByVal value As Double, ByVal rectangle As System.Drawing.RectangleF) As Double

            ' check if we have two distinct pairs
            If rectangle.Width = 0 Then
                ' if we have identical X values, return the
                ' average of the ordinate value
                Return rectangle.Y + 0.5 * rectangle.Height
            Else
                Return rectangle.Y + (value - rectangle.X) * rectangle.Height / rectangle.Width
            End If

        End Function

#End Region

    End Class

End Namespace
