Namespace IO

    ''' <summary>
    ''' An abstract base class for use with class libraries that require
    ''' Connect and Disconnect constracts.</summary>
    ''' <remarks>Inherit this class in non-visible component class libraries.
    ''' Provides standard properties such as an instance name and 
    ''' status message and requires a dispose method.  
    ''' Implements IDisposable.
    ''' </remarks>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="02/07/06" by="David Hary" revision="1.0.2228.x">
    ''' Created
    ''' </history>
    Public MustInherit Class ConnectBaseClass

        Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Protected Sub New()

            Me.New(String.Empty)

        End Sub

        ''' <summary>Constructs this class.</summary>
        ''' <param name="instanceName">Specifies the name of the instance.</param>
        Protected Sub New(ByVal instanceName As String)

            MyBase.New()

            Me._instanceName = instanceName

        End Sub

        ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
        ''' <remarks>Do not make this method overridable (virtual) because a derived 
        '''   class should not be able to override this method.</remarks>
        Public Sub Dispose() Implements IDisposable.Dispose

            ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            ' this disposes all child classes.
            Dispose(True)

            ' Take this object off the finalization(Queue) and prevent finalization code 
            ' from executing a second time.
            GC.SuppressFinalize(Me)

        End Sub

        Private _isDisposed As Boolean
        ''' <summary>
        ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
        ''' provided proper implementation.
        ''' </summary>
        Protected Property IsDisposed() As Boolean
            Get
                Return _isDisposed
            End Get
            Private Set(ByVal value As Boolean)
                _isDisposed = value
            End Set
        End Property

        ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
        ''' <param name="disposing">True if this method releases both managed and unmanaged 
        '''   resources; False if this method releases only unmanaged resources.</param>
        ''' <remarks>Executes in two distinct scenarios as determined by
        '''   its disposing parameter.  If True, the method has been called directly or 
        '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
        '''   If disposing equals False, the method has been called by the 
        '''   runtime from inside the finalizer and you should not reference 
        '''   other objects--only unmanaged resources can be disposed.</remarks>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    _statusMessage = String.Empty
                    _instanceName = String.Empty

                End If

                ' Free shared unmanaged resources

            End If

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Sub

        ''' <summary>This destructor will run only if the Dispose method 
        '''   does not get called. It gives the base class the opportunity to 
        '''   finalize. Do not provide destructors in types derived from this class.</summary>
        Protected Overrides Sub Finalize()
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal in terms of
            ' readability and maintainability.
            Dispose(False)
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

        ''' <summary>
        ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
        ''' </summary>
        ''' <remarks>
        ''' Use this method to return the instance name. If instance name is not set, 
        ''' returns the base class ToString value.
        ''' </remarks>
        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return MyBase.ToString & "." & Me._instanceName
            End If
        End Function

        Private _instanceName As String = String.Empty
        ''' <summary>Gets or sets the name given to an instance of this class.</summary>
        ''' <value><c>InstanceName</c> is a String property.</value>
        Public Property InstanceName() As String
            Get
                If Not String.IsNullOrEmpty(_instanceName) Then
                    Return _instanceName
                Else
                    Return MyBase.ToString
                End If
            End Get
            Set(ByVal value As String)
                _instanceName = value
            End Set
        End Property

        Private _statusMessage As String = String.Empty
        ''' <summary>Gets or sets the status message.</summary>
        ''' <value>A <see cref="System.String">String</see>.</value>
        Public Property StatusMessage() As String
            Get
                Return _statusMessage
            End Get
            Set(ByVal value As String)
                _statusMessage = value
            End Set
        End Property

#End Region

#Region " CONNECT and DISCONNECT "

        ''' <summary>Clears the resource.</summary>
        Public MustOverride Sub Clear()

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        ''' <param name="resourceName">Specifies the name of the resource to which
        ''' to connect.</param>
        ''' <remarks></remarks>
        Public Overridable Overloads Function Connect(ByVal resourceName As String) As Boolean
            Me.ResourceName = resourceName
            Me.IsConnected = True
            Return Me.IsConnected
        End Function

        ''' <summary>Connects this instance.</summary>
        ''' <returns><c>True</c> if the instance connected.</returns>
        Public Overridable Overloads Function Connect() As Boolean
            Me.IsConnected = True
            Return Me.IsConnected
        End Function

        ''' <summary>Disconnects this instance.</summary>
        ''' <returns><c>True</c> if the instance disconnected.</returns>
        Public Overridable Function Disconnect() As Boolean
            Me.IsConnected = False
            Return Not Me.IsConnected
        End Function

        Private _isConnected As Boolean
        ''' <summary>Gets or sets the connect status flag.</summary>
        ''' <value><c>True</c> if connected.</value>
        Public Property IsConnected() As Boolean
            Get
                Return _isConnected
            End Get
            Set(ByVal value As Boolean)
                _isConnected = value
            End Set
        End Property

        Private _resourceName As String = String.Empty
        ''' <summary>Gets or sets the resource name.</summary>
        ''' <value><c>ResourceName</c> is a String property.</value>
        Public Property ResourceName() As String
            Get
                Return _resourceName
            End Get
            Set(ByVal value As String)
                _resourceName = value
            End Set
        End Property

        Private _usingDevices As Boolean = True
        ''' <summary>
        ''' True when using actual devices or False for runnning the
        ''' program without connecting to devices.
        ''' </summary>
        Public Property UsingDevices() As Boolean
            Get
                Return _usingDevices
            End Get
            Set(ByVal Value As Boolean)
                _usingDevices = Value

            End Set
        End Property

#End Region

    End Class

End Namespace

