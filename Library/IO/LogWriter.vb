Namespace IO
    ''' <summary>Data and event log to file with trace level.</summary>
    ''' <license>
    ''' (c) 2004 Integrated Scientific Resources, Inc.
    ''' Licensed under the Apache License Version 2.0. 
    ''' Unless required by applicable law or agreed to in writing, this software is provided
    ''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    ''' </license>
    ''' <history date="08/09/04" by="David Hary" revision="1.0.1682.x">
    ''' Created
    ''' </history>
    ''' <history date="01/14/06" by="David Hary" revision="1.0.2205.x">
    '''   Use path name for the full folder and file names.
    ''' </history>
    Public Class LogWriter

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        ''' <param name="traceSwitch">Provides reference to the application trace 
        '''   switch.</param>
        Public Sub New(ByVal traceSwitch As System.Diagnostics.TraceSwitch)

            ' instantiate the base class
            MyBase.New()

            ' set reference to the application trace switch.
            _traceSwitch = traceSwitch

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>
        ''' Returns true if the write condition is met.
        ''' </summary>
        ''' <param name="severity"></param>
        ''' <param name="traceSwitch"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function writeConditionMet(ByVal severity As TraceEventType, _
            ByVal traceSwitch As System.Diagnostics.TraceSwitch) As Boolean

            Select Case severity And _
                (TraceEventType.Critical Or TraceEventType.Error Or TraceEventType.Information _
                 Or TraceEventType.Verbose Or TraceEventType.Warning)
                Case TraceEventType.Critical
                    Return traceSwitch.TraceError
                Case TraceEventType.Error
                    Return traceSwitch.TraceError
                Case TraceEventType.Information
                    Return traceSwitch.TraceInfo
                Case TraceEventType.Verbose
                    Return traceSwitch.TraceVerbose
                Case TraceEventType.Warning
                    Return traceSwitch.TraceWarning
                Case Else
                    Return traceSwitch.TraceVerbose
            End Select

        End Function

        ''' <summary>
        ''' Returns a default applicaiton product name and version header
        ''' </summary>
        Private Shared Function defaultApplicationHeader() As String
            Return String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0} Version {1}", Windows.Forms.Application.ProductName, Windows.Forms.Application.ProductVersion)
        End Function

        ''' <summary>Writes a message to a file.</summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, Unrestricted:=True)> _
      Public Sub WriteLine(ByVal data As String)

            If String.IsNullOrEmpty(data) Then
                Exit Sub
            End If

            ' Set the default file name
            If Me.FileNameBuilder.UseNewDataFile Then
                Me.FileNameBuilder.FilePathName = Me.FileNameBuilder.BuildFilePathName()
            End If

            ' use a stream writer to write to the file
            Dim sr As System.IO.StreamWriter

            ' check if file exists
            If System.IO.File.Exists(Me.FileNameBuilder.FilePathName) Then

                ' open the file to append to
                sr = New System.IO.StreamWriter(Me.FileNameBuilder.FilePathName, True)

            Else

                ' create the file.
                sr = System.IO.File.CreateText(Me.FileNameBuilder.FilePathName)

                ' log system information if file is new
                sr.WriteLine(defaultApplicationHeader)

                ' output the header
                sr.WriteLine(Header)

            End If

            ' write the data to the file
            sr.WriteLine(LogEntry(data))

            ' close the file
            sr.Close()

        End Sub

        ''' <summary>
        ''' Reutnrs the default header.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function DefaultHeader() As String
            Return String.Format(System.Globalization.CultureInfo.CurrentCulture, "Time{0}Severity{0}{1}", _
                _delimiter, _extendedHeader)
        End Function

        Private _header As String
        ''' <summary>
        ''' Holds the datalog header.  Defaults to the default header.
        ''' </summary>
        Public Property Header() As String
            Get
                If String.IsNullOrEmpty(_header) Then
                    _header = Me.DefaultHeader
                End If
                Return _header
            End Get
            Set(ByVal value As String)
                _header = value
            End Set
        End Property

        ''' <summary>
        ''' Returns a data log record. 
        ''' </summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LogEntry(ByVal data As String) As String

            If String.IsNullOrEmpty(data) Then
                Return String.Empty
            End If
            Return String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                  "{0:HH:mm:ss.fff}{1}{2}{1}{3}", Date.Now, _delimiter, _
                  String.Empty, data)

        End Function

        ''' <summary>
        ''' Returns a data log record. 
        ''' </summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <param name="severity">Specifies the type of message.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LogEntry(ByVal data As String, ByVal severity As TraceEventType) As String

            If String.IsNullOrEmpty(data) Then
                Return String.Empty
            End If

            Return String.Format(System.Globalization.CultureInfo.CurrentCulture, _
                  "{0:HH:mm:ss.fff}{1}{2}{1}{3}", Date.Now, _delimiter, _
                  severity.ToString(), data)
        End Function

        ''' <summary>
        ''' Returns a data log record. 
        ''' </summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <param name="severity">Specifies the type of message.</param>
        ''' <param name="additionalInfo">A parameter array specifying additional information 
        '''   to log to the file.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function LogEntry(ByVal data As String, ByVal severity As TraceEventType, _
            ByVal ParamArray additionalInfo() As String) As String

            If String.IsNullOrEmpty(data) Then
                data = String.Empty
            End If

            If additionalInfo Is Nothing OrElse additionalInfo.Length = 0 Then
                Return data
            Else
                Dim dataBuilder As New System.Text.StringBuilder
                dataBuilder.Append(data)
                For Each dataItem As String In additionalInfo
                    If dataBuilder.Length > 0 Then
                        dataBuilder.Append(_delimiter)
                    End If
                    dataBuilder.Append(dataItem)
                Next dataItem
                Return LogEntry(dataBuilder.ToString, severity)
            End If

        End Function

        ''' <summary>Writes a message to a file.</summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <param name="severity">Specifies the type of message.</param>
        ''' <history date="05/02/06" by="David Hary" revision="1.0.2313.x">
        '''   Change argument order
        ''' </history>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, Unrestricted:=True)> _
        Public Sub WriteLine(ByVal data As String, ByVal severity As TraceEventType)

            If Not writeConditionMet(severity, Me._traceSwitch) Then
                Exit Sub
            End If

            If String.IsNullOrEmpty(data) Then
                Exit Sub
            End If

            ' Set the default file name
            If Me.FileNameBuilder.UseNewDataFile Then
                Me.FileNameBuilder.FilePathName = Me.FileNameBuilder.BuildFilePathName()
            End If

            ' use a stream writer to write to the file
            Dim sr As System.IO.StreamWriter

            ' check if file exists
            If System.IO.File.Exists(Me.FileNameBuilder.FilePathName) Then

                ' open the file to append to
                sr = New System.IO.StreamWriter(Me.FileNameBuilder.FilePathName, True)

            Else

                ' create the file.
                sr = System.IO.File.CreateText(Me.FileNameBuilder.FilePathName)

                ' log system information if file is new
                sr.WriteLine(defaultApplicationHeader)

                ' output the header
                sr.WriteLine(Header)

            End If

            ' write the data to the file
            sr.WriteLine(LogEntry(data, severity))

            ' close the file
            sr.Close()

        End Sub

        ''' <summary>Writes a message to a file.</summary>
        ''' <param name="data">Specifies the data to log to the file.</param>
        ''' <param name="severity">Specifies the type of message.</param>
        ''' <param name="additionalInfo">A parameter array specifying additional information 
        '''   to log to the file.</param>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, Unrestricted:=True)> _
        Public Sub WriteLine(ByVal data As String, ByVal severity As TraceEventType, _
          ByVal ParamArray additionalInfo() As String)

            If String.IsNullOrEmpty(data) Then
                Exit Sub
            End If

            If Not writeConditionMet(severity, Me._traceSwitch) Then
                Exit Sub
            End If

            If additionalInfo Is Nothing OrElse additionalInfo.Length = 0 Then
                Exit Sub
            End If

            Dim dataBuilder As New System.Text.StringBuilder
            dataBuilder.Append(data)
            For Each dataItem As String In additionalInfo
                If dataBuilder.Length > 0 Then
                    dataBuilder.Append(Delimiter)
                End If
                dataBuilder.Append(dataItem)
            Next dataItem

            Me.WriteLine(data, severity)

        End Sub

        ''' <summary>Writes a message to a file.</summary>
        ''' <param name="data">A parameter array that specifies the information 
        '''   to log to the file.</param>
        ''' <remarks></remarks>
        <System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.LinkDemand, Unrestricted:=True)> _
      Public Sub WriteLine(ByVal ParamArray data() As String)

            If data Is Nothing OrElse data.Length = 0 Then
                Exit Sub
            End If

            Dim dataBuilder As New System.Text.StringBuilder
            For Each dataItem As String In data
                If dataBuilder.Length > 0 Then
                    dataBuilder.Append(Delimiter)
                End If
                dataBuilder.Append(dataItem)
            Next dataItem

            Me.WriteLine(dataBuilder.ToString)

        End Sub

#End Region

#Region " PROPERTIES "

        Private _fileNameBuilder As IO.FileNameBuilder
        ''' <summary>
        ''' Holds the file name builder for setting this writer file name.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property FileNameBuilder() As IO.FileNameBuilder
            Get
                If _fileNameBuilder Is Nothing Then
                    _fileNameBuilder = New IO.FileNameBuilder
                End If
                Return _fileNameBuilder
            End Get
            Set(ByVal value As IO.FileNameBuilder)
                _fileNameBuilder = value
            End Set
        End Property

        Private _delimiter As String = ", "
        ''' <summary>Gets or sets the delimiter for separater the date, category, and data.</summary>
        Public Property Delimiter() As String
            Get
                Return _delimiter
            End Get
            Set(ByVal value As String)
                If String.IsNullOrEmpty(value) Then
                    Throw New ArgumentNullException("value")
                End If
                _delimiter = value
            End Set
        End Property

        Private _extendedHeader As String = "Message(s)"
        ''' <summary>Gets or sets the extedned header.  Defaults to Message(s).</summary>
        Public Property ExtendedHeader() As String
            Get
                Return _extendedHeader
            End Get
            Set(ByVal value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _extendedHeader = value
            End Set
        End Property

        Private _traceSwitch As System.Diagnostics.TraceSwitch
        ''' <summary>Gets or sets the reference to the trace switch.</summary>
        Public ReadOnly Property TraceSwitch() As System.Diagnostics.TraceSwitch
            Get
                Return _traceSwitch
            End Get
        End Property

#End Region

    End Class

End Namespace
