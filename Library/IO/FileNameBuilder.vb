Namespace IO

    Public Class FileNameBuilder

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Constructs this class.</summary>
        Public Sub New()

            ' instantiate the base class
            MyBase.New()

        End Sub

#End Region

#Region " SHARED "

        ''' <summary>
        ''' Builds a file title.
        ''' </summary>
        ''' <param name="prefix">The file prefix</param>
        ''' <param name="dateFormat">Specifies the date format to append to the file title.</param>
        ''' <param name="useApplicationFileTitle">Specifies the option for using the file title as part of the
        '''   file name.</param>
        Public Shared Function BuildFileTitle(ByVal prefix As String, _
        ByVal dateFormat As String, ByVal useApplicationFileTitle As Boolean) As String

            ' set basic format using the application path
            Dim titleBuilder As New System.Text.StringBuilder
            If Not String.IsNullOrEmpty(prefix) Then
                titleBuilder.Append(prefix)
            End If
            If useApplicationFileTitle Then
                Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
                titleBuilder.Append(fi.Name.Split("."c)(0))
            End If
            If Not String.IsNullOrEmpty(dateFormat) Then
                titleBuilder.Append(Date.Now.ToString(dateFormat, System.Globalization.CultureInfo.CurrentCulture))
            End If

            ' return the build file path name
            Return titleBuilder.ToString

        End Function

        ''' <summary>
        ''' Returns the file path name for a standard application related file.
        ''' </summary>
        ''' <param name="prefix">The prefix for the log file name</param>
        ''' <param name="fileExtension">The file extension including the preceeding period</param>
        ''' <param name="dateFormat">Specifies the date format to append to the file title.</param>
        ''' <param name="useApplicationFileTitle">Specifies true to use the application title.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' <history>
        ''' </history>
        Public Shared Function BuildFilePathName(ByVal prefix As String, _
        ByVal fileExtension As String, _
        ByVal dateFormat As String, _
        ByVal useApplicationFileTitle As Boolean) As String

            If prefix Is Nothing Then
                Throw New ArgumentNullException("prefix")
            End If
            If fileExtension Is Nothing Then
                Throw New ArgumentNullException("fileExtension")
            End If
            If dateFormat Is Nothing Then
                Throw New ArgumentNullException("dateFormat")
            End If

            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            Dim pathNameBuilder As New System.Text.StringBuilder
            pathNameBuilder.Append(fi.Directory.FullName)
            pathNameBuilder.Append("\")
            pathNameBuilder.Append(IO.FileNameBuilder.BuildFileTitle(prefix, dateFormat, useApplicationFileTitle))
            pathNameBuilder.Append(fileExtension)

            ' return the build file path name
            Return pathNameBuilder.ToString

        End Function

        ''' <summary>
        ''' Build a file path name using a sub folder under a sinbling folder
        ''' </summary>
        ''' <param name="applicationSiblingFolderName">Specifies the name of the sibling folder.</param>
        ''' <param name="logFolderName">Specifies the name of the sub folder</param>
        ''' <param name="prefix">The prefix for the log file name</param>
        ''' <param name="fileExtension">The file extension including the preceeding period</param>
        ''' <param name="dateFormat">Specifies the date format to append to the file title.</param>
        ''' <param name="useApplicationFileTitle">Specifies true to use the application title.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function BuildFilePathName(ByVal applicationSiblingFolderName As String, _
          ByVal logFolderName As String, ByVal prefix As String, _
          ByVal fileExtension As String, ByVal dateFormat As String, _
          ByVal useApplicationFileTitle As Boolean) As String

            Dim pathNameBuilder As New System.Text.StringBuilder
            If String.IsNullOrEmpty(applicationSiblingFolderName) Then
                pathNameBuilder.Append(Windows.Forms.Application.StartupPath)
            Else
                pathNameBuilder.Append(IO.Helper.CreateApplicationSiblingFolder(applicationSiblingFolderName))
            End If
            If pathNameBuilder.Length > 0 Then
                pathNameBuilder.Append("\")
                If String.IsNullOrEmpty(logFolderName) Then
                    pathNameBuilder.Append(logFolderName)
                    pathNameBuilder.Append("\")
                End If
                Return String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0}\{1}{2}", _
                  IO.Helper.CreateFolder(pathNameBuilder.ToString), _
                  IO.FileNameBuilder.BuildFileTitle(prefix, dateFormat, useApplicationFileTitle), _
                  fileExtension)
            Else
                Throw New BaseException("Failed creating folder")
            End If
        End Function

#End Region

#Region " METHODS AND PROPERTIES "

        Private _applicationSiblingFolderName As String
        ''' <summary>
        ''' Holds the sibling folder for creating a path name relative to the application
        ''' folder.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ApplicationSiblingFolderName() As String
            Get
                Return _applicationSiblingFolderName
            End Get
            Set(ByVal value As String)
                _applicationSiblingFolderName = value
            End Set
        End Property

        Private _dateFormat As String = "yyyyMMdd"
        ''' <summary>Specifies the data format for setting the file name.  
        '''   Defaults to year month and day digits with no delimiters.
        '''   This format must yield valid file name characters only</summary>
        ''' <value></value>
        Public Property DateFormat() As String
            Get
                Return _dateFormat
            End Get
            Set(ByVal value As String)
                If String.IsNullOrEmpty(value) Then
                    Throw New ArgumentNullException("value")
                End If
                _dateFormat = value
            End Set
        End Property

        ''' <summary>
        ''' Returns a file title based on properties.
        ''' </summary>
        Public Function BuildFileTitle() As String
            Return IO.FileNameBuilder.BuildFileTitle(Me._fileTitlePrefix, Me._dateFormat, Me._usingApplicationFileTitle)
        End Function

        ''' <summary>
        ''' Builds a path name based on properties.
        ''' </summary>
        Public Function BuildFilePathName() As String
            Return IO.FileNameBuilder.BuildFilePathName(Me._applicationSiblingFolderName, Me._subfolderName, _
                Me._fileTitlePrefix, Me._fileExtension, Me._dateFormat, Me._usingApplicationFileTitle)
        End Function

        Private _fileTitlePrefix As String = String.Empty
        ''' <summary>Specifies the file prefix </summary>
        ''' <value></value>
        Public Property FileTitlePrefix() As String
            Get
                Return _fileTitlePrefix
            End Get
            Set(ByVal value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _fileTitlePrefix = value
            End Set
        End Property

        Private _fileExtension As String = ".log"
        ''' <summary>Gets or sets the file name extension for building the 
        '''   log file name.</summary>
        ''' <value></value>
        Public Property FileExtension() As String
            Get
                Return _fileExtension
            End Get
            Set(ByVal value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _fileExtension = value
            End Set
        End Property

        Private _filePathName As String = String.Empty
        ''' <summary>Gets or sets the file fath name.</summary>
        ''' <value><c>FilePathName</c> is a String property.</value>
        ''' <remarks>Use this property to get or set the file name.</remarks>
        Public Property FilePathName() As String
            Get
                Return _filePathName
            End Get
            Set(ByVal value As String)
                If value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _filePathName = value
            End Set
        End Property

        Private _subfolderName As String
        ''' <summary>
        ''' Holds the sub-folder for creating a path name relative to the application
        ''' or the application sibling folder.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property SubfolderName() As String
            Get
                Return _subfolderName
            End Get
            Set(ByVal value As String)
                _subfolderName = value
            End Set
        End Property

        Private _usingApplicationFileTitle As Boolean
        ''' <summary>True if using the application file title in the 
        '''   datalog file name.</summary>
        Public Property UsingApplicationPathName() As Boolean
            Get
                Return _usingApplicationFileTitle
            End Get
            Set(ByVal Value As Boolean)
                _usingApplicationFileTitle = Value
            End Set
        End Property

        ''' <summary>Returns true if a new log write data-based file needs to be created, i.e., if the
        '''   file name was not specified or if date of the file name is not current.</summary>
        Public ReadOnly Property UseNewDataFile() As Boolean
            Get
                Return String.IsNullOrEmpty(_filePathName) OrElse _
                   Not Text.Helper.Contained(_filePathName, Date.Now.ToString(_dateFormat, System.Globalization.CultureInfo.CurrentCulture))
            End Get
        End Property

#End Region

    End Class

End Namespace
