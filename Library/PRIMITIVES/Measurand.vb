''' <summary>
''' Implements a generic measurand.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <remarks></remarks>
Public Class Measurand(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Public Sub New()
        MyBase.New()
        Me._outcome = New MeasurandOutcome()
        Me._generator = New RandomNumberGenerator()
        Me._displayCaption = New CaptionValue(Of T)
        Me._saveCaption = New CaptionValue(Of T)
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Public Sub New(ByVal newValue As T)
        Me.New()
        Me.SetValue(newValue, True)
    End Sub

    ''' <summary>Constructs a measured value</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    ''' <param name="valid">The value auto settings mode</param>
    Public Sub New(ByVal newValue As T, ByVal valid As Boolean)
        Me.New()
        SetValue(newValue, valid)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Returns True if the value of the <param>obj</param> equals to the
    '''   instance value.</summary>
    ''' <param name="obj">The object to compare for equality with this instance.
    '''   This object should be type <see cref="Measurand"/></param>
    ''' <returns>Returns <c>True</c> if <param>obj</param> is the same value as this
    '''   instance; otherwise, <c>False</c></returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Measurand(Of T).Equals(Me, TryCast(obj, Measurand(Of T)))
#If False Then
    If obj IsNot Nothing AndAlso TypeOf obj Is Measurand(Of T) Then
      Return Equals(CType(obj, Measurand(Of T)))
    Else
      Return False
    End If
#End If

    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return _value.GetHashCode
    End Function

    ''' <summary>
    ''' Resets measured value to given value.  This also sets <see cref="Outcome"/>
    ''' to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Sub Reset(ByVal resetValue As T)
        _value = resetValue
        Me._outcome.Reset()
    End Sub

    ''' <summary>
    ''' Sets the measured value and it validity.  
    ''' An invalid value indicates, for instance, 
    ''' a failed measurement.
    ''' </summary>
    ''' <param name="newValue">Specifies the new value.</param>
    ''' <param name="valid">True to tag the value as valid or false to tag the value as
    ''' invalid.  A value is invalid in cases where the measurement could be met such as
    ''' contact check failure or complaince.  In thises cases, the relevant 
    ''' <see cref="MeasurandOutcome">outcomes</see> must also be set.</param>
    Public Sub SetValue(ByVal newValue As T, ByVal valid As Boolean)
        _value = newValue
        Me.SaveCaption.ScaledValue = _value
        Me.DisplayCaption.ScaledValue = _value
        Me._outcome.IsValid = valid
        If valid Then
            Me._outcome.IsHigh = _value.CompareTo(Me._highLimit) > 0
            Me._outcome.IsLow = _value.CompareTo(Me._lowLimit) < 0
        End If
    End Sub

    ''' <summary>
    ''' Returns the default string representation of the value.
    ''' This uses the display format with units.
    ''' </summary>
    Public Overloads Function ToString() As String
        Return Me.DisplayCaption.Caption(Me._outcome)
    End Function

    ''' <summary>Returns the default string representation of the value.</summary>
    ''' <param name="format">The format string</param>
    Public Overloads Function ToString(ByVal format As String) As String
        If Me._outcome.IsValid Then
            Return _value.ToString(format, Globalization.CultureInfo.CurrentCulture)
        Else
            Return Me._outcome.ShortOutcome
        End If
    End Function

#End Region

#Region " PROPERTIES "

    Private _displayCaption As CaptionValue(Of T)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DisplayCaption() As CaptionValue(Of T)
        Get
            Return _displayCaption
        End Get
    End Property

    Private _heading As String
    Public Property Heading() As String
        Get
            Return _heading
        End Get
        Set(ByVal value As String)
            _heading = value
        End Set
    End Property

    Private _highLimit As T
    ''' <summary>Gets or sets the high limit</summary>
    Public Property HighLimit() As T
        Get
            Return _highLimit
        End Get
        Set(ByVal value As T)
            _highLimit = value
        End Set
    End Property

    Private _lowLimit As T
    ''' <summary>Gets or sets the low limit</summary>
    Public Property LowLimit() As T
        Get
            Return _lowLimit
        End Get
        Set(ByVal value As T)
            _lowLimit = value
        End Set
    End Property

    Private _outcome As MeasurandOutcome
    ''' <summary>
    ''' Holds the measureand outcome value.
    ''' </summary>
    Public Property Outcome() As MeasurandOutcome
        Get
            Return _outcome
        End Get
        Set(ByVal value As MeasurandOutcome)
            _outcome = value
        End Set
    End Property

    Private _value As T
    ''' <summary>
    ''' Holds the measured value.
    ''' </summary>
    Public ReadOnly Property [Value]() As T
        Get
            Return _value
        End Get
    End Property

    Private _generator As RandomNumberGenerator
    Public ReadOnly Property Generator() As RandomNumberGenerator
        Get
            Return _generator
        End Get
    End Property

    Private _saveCaption As CaptionValue(Of T)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SaveCaption() As CaptionValue(Of T)
        Get
            Return _saveCaption
        End Get
    End Property

    ''' <summary>
    ''' Holds the simulated value.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SimulatedValue() As Double
        Get
            Return _generator.Value
        End Get
    End Property

#End Region

End Class

#Region " MEASURAND-RELATED TYPES "

''' <summary>
''' Holds the measurand outcome flags.
''' </summary>
''' <remarks></remarks>
<System.Flags()> Public Enum MetaOutcomes
    None = 0
    FailedContactCheck = 1
    HasValue = 2
    High = 4
    HitCompliance = 8
    HitRangeCompliance = 16
    Low = 32
    Pass = 64
    Valid = 128
End Enum

#End Region

''' <summary>
''' Holds the measurand outcome meta data and derived properties.
''' </summary>
''' <remarks></remarks>
Public Class MeasurandOutcome

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        Me.Reset()
    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Returns an outcome string depending on the outcome code.
    ''' </summary>
    ''' <param name="outcomeCode">Specifies an outcome code such as that returned by the 2400 Keithley
    ''' source measure unit.
    ''' </param>
    Public Shared Function GetOutcome(ByVal outcomeCode As Int64) As String

        Select Case (Convert.ToInt32(outcomeCode) And &H300S)

            Case &H100S

                Return "F1"

            Case &H200S

                Return "F2"

            Case &H300S

                Return "F3"

            Case Else

                ' 1.11.14 display P and not...
                Return "P"

        End Select

    End Function

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Resets the outcome to None.
    ''' </summary>
    Public Sub Reset()
        Me._value = MetaOutcomes.None
    End Sub

#End Region

#Region " PROPERTIES "

    Private _invalidCaption As String = "n/a"

    ''' <summary>
    ''' Returns a short outcome string representing the measurand meta outcome.
    ''' </summary>
    Public ReadOnly Property ShortOutcome() As String
        Get
            If Me.IsOutcome(MetaOutcomes.None) Then
                ' if not set then return NO
                Return "no"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "nv"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "p"
                ElseIf Me.IsHigh Then
                    Return "hi"
                ElseIf Me.IsLow Then
                    Return "lo"
                Else
                    Return _invalidCaption
                End If
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "cc"
                ElseIf Me.HitCompliance Then
                    Return "c"
                ElseIf Me.HitRangeCompliance Then
                    Return "rc"
                Else
                    Return _invalidCaption
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns a long outcome string representing the measurand meta outcome.
    ''' </summary>
    Public ReadOnly Property LongOutcome() As String
        Get
            If Me.IsOutcome(MetaOutcomes.None) Then
                ' if not set then return NO
                Return "Not Set"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "No Value"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "Pass"
                ElseIf Me.IsHigh Then
                    Return "High"
                ElseIf Me.IsLow Then
                    Return "Low"
                Else
                    Return _invalidCaption
                End If
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "Contact Check"
                ElseIf Me.HitCompliance Then
                    Return "Compliance"
                ElseIf Me.HitRangeCompliance Then
                    Return "Range Compliance"
                Else
                    Return _invalidCaption
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the outcome failed contact check.
    ''' </summary>
    Public Property FailedContactCheck() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.FailedContactCheck)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.FailedContactCheck) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome Has Value, namely if a value was set.
    ''' </summary>
    Public Property HasValue() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HasValue)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HasValue) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is high.
    ''' </summary>
    Public Property IsHigh() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.High)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.High) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Hit Compliance.
    ''' </summary>
    Public Property HitCompliance() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HitCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HitCompliance) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome Hit Range Compliance.
    ''' </summary>
    Public Property HitRangeCompliance() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HitRangeCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HitRangeCompliance) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if theoutcome is low.
    ''' </summary>
    Public Property IsLow() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Low)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Low) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the outcome.  This is the only valid path for setting the 
    ''' outcome.
    ''' </summary>
    ''' <param name="outcomeBit">Speciifies the outcome bit which to get or set.</param>
    Public Property IsOutcome(ByVal outcomeBit As MetaOutcomes) As Boolean
        Get
            Return (Me._value And outcomeBit) = outcomeBit
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me._value = Me._value Or outcomeBit
            Else
                Me._value = Me._value And (Not outcomeBit)
            End If
            ' set the pass flag
            If Me.Passed Then
                Me._value = Me._value Or MetaOutcomes.Pass
            Else
                Me._value = Me._value And (Not MetaOutcomes.Pass)
            End If
            ' set the Has Value flag
            Me._value = Me._value Or MetaOutcomes.HasValue
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Pass.
    ''' </summary>
    Public ReadOnly Property IsPass() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Pass)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Valid. The measured value is
    ''' valid when its value or pass/fail outcomes or compliance or
    ''' contact check conditions are set.
    ''' </summary>
    Public Property IsValid() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Valid)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Valid) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns True if passed.
    ''' </summary>
    Public Function Passed() As Boolean

        Return Me.IsValid AndAlso _
            (Not (Me.FailedContactCheck OrElse Me.HitCompliance OrElse Me.HitRangeCompliance)) AndAlso _
            (Not (Me.IsHigh OrElse Me.IsLow))

    End Function

    Private _value As MetaOutcomes
    ''' <summary>
    ''' Holds the outcome
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property [Value]() As MetaOutcomes
        Get
            Return _value
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Provides a simulated value for testing measurements without having the benefit of 
''' instruments.
''' </summary>
''' <remarks></remarks>
Public Class RandomNumberGenerator

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        If generator Is Nothing Then
            generator = New Random() ' CInt(Date.Now.Ticks Mod Integer.MaxValue))
        End If
        Me.Min = 0
        Me.Max = 1
    End Sub

    ''' <summary>
    ''' Holds a shared refernece to the number generator.
    ''' </summary>
    ''' <remarks>A shared generator was selected because multiple generators tended 
    ''' to create the same random number.</remarks>
    Private Shared generator As Random
    Private range As Double

    Private _min As Double
    Public Property Min() As Double
        Get
            Return _min
        End Get
        Set(ByVal value As Double)
            _min = value
            range = _max - _min
        End Set
    End Property

    Private _max As Double
    Public Property Max() As Double
        Get
            Return _max
        End Get
        Set(ByVal value As Double)
            _max = value
            range = _max - _min
        End Set
    End Property

    ''' <summary>
    ''' Returns a simulated value
    ''' </summary>
    ''' <value></value>
    Public ReadOnly Property [Value]() As Double
        Get
            Dim newValue As Double = generator.NextDouble * Me.range + Me.Min
            '      Debug.WriteLine(newValue)
            Return newValue
        End Get
    End Property

End Class

''' <summary>
''' Encapsulation conversion of measured values for display or storage.
''' </summary>
''' <typeparam name="T"></typeparam>
Public Class CaptionValue(Of T As IFormattable)

    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>
    ''' Returns the value to display using the <see cref="CaptionFormat"/> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="outcome">Specifies the measured outcome for handling invalid values.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Caption(ByVal outcome As MeasurandOutcome) As String

        Return Me.Caption(outcome, Me.CaptionFormat)

    End Function

    ''' <summary>
    ''' Returns the value to display using the <see cref="CaptionFormat"/> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="outcome">Specifies the measured outcome for handling invalid values.</param>
    ''' <param name="format">Specifies the format for the caption.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Caption(ByVal outcome As MeasurandOutcome, ByVal format As String) As String

        If outcome.IsValid Then
            If Me.ShowUnits Then
                Return Me.ScaledValue.ToString(format, Globalization.CultureInfo.CurrentCulture) & _
                  String.Empty.PadRight(1) & _units
            Else
                Return Me.ScaledValue.ToString(Me.CaptionFormat, Globalization.CultureInfo.CurrentCulture)
            End If
        Else
            Return outcome.LongOutcome
        End If

    End Function
    Private _captionFormat As String = "0.000E+0"

    ''' <summary>Gets or sets the display format for the value</summary>
    Public Property CaptionFormat() As String
        Get
            Return _captionFormat
        End Get
        Set(ByVal value As String)
            _captionFormat = value
        End Set
    End Property

    Private _scaleFactor As Double = 1
    ''' <summary>Gets or sets the display scale factor</summary>
    Public Property ScaleFactor() As Double
        Get
            Return _scaleFactor
        End Get
        Set(ByVal value As Double)
            _scaleFactor = value
        End Set
    End Property

    Private _scaledValue As T
    ''' <summary>
    ''' Holds the scaled value to use for display.  This equals the value times
    ''' the display scale factor.  In this generic class, we have to
    ''' expose this for the user to set otherwaise we need to add algebra to 
    ''' this generic class which hopfully will be done with the next Visual Studio.
    ''' </summary>
    Public Property ScaledValue() As T
        Get
            Return _scaledValue
        End Get
        Set(ByVal value As T)
            _scaledValue = value
        End Set
    End Property

    Private _showUnits As Boolean
    ''' <summary>
    ''' Holds true for adding units to the display.
    ''' </summary>
    Public Property ShowUnits() As Boolean
        Get
            Return _showUnits
        End Get
        Set(ByVal value As Boolean)
            _showUnits = value
        End Set
    End Property

    Private _units As String
    ''' <summary>Gets or sets the units format for the value</summary>
    Public Property Units() As String
        Get
            Return _units
        End Get
        Set(ByVal value As String)
            _units = value
        End Set
    End Property

End Class