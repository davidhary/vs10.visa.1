Imports NationalInstruments

''' <summary>Extends the VISA GPIB message based session.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
Public Class GpibSession

    Inherits NationalInstruments.VisaNS.GpibSession

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName ">Specifies the name of the visa resource.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        ' Rev 4.1 and 5.0 of visa did not support this call and could not verify the 
        ' resource.  
        MyBase.New(resourceName, VisaNS.AccessModes.NoLock, 2000, False)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        Finally

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SCPI COMMANDS "

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    Public Sub ClearStatus()

        isr.Visa.MessageBasedSession.ClearStatus(Me)

    End Sub

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    Public Function IsOpc() As Boolean
        Return MessageBasedSession.IsOpc(Me)
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    Public Function ReadInstrumentId() As String
        Return MessageBasedSession.ReadInstrumentId(Me)
    End Function

    ''' <summary>Returns the device to the it default know state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    Public Sub ResetToKnownState()

        MessageBasedSession.ResetToKnownState(Me)

        ' Set default values if any

    End Sub

    ''' <summary>Selectively clears the instrument.</summary>
    Public Sub SelectiveDeviceClear()

        ' clear the device
        MyBase.Clear()

    End Sub

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public ReadOnly Property ServiceRequestEventStatus() As Scpi.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventStatus(Me)
        End Get
    End Property

    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <returns>The <see cref="Scpi.ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <remarks>When set clears all service registers.</remarks>
    Public Property ServiceRequestEventEnable() As Scpi.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventEnable(Me)
        End Get
        Set(ByVal value As Scpi.ServiceRequests)
            MessageBasedSession.ServiceRequestEventEnable(Me) = value
        End Set
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    Public ReadOnly Property StandardEventStatus() As Scpi.StandardEvents
        Get
            Return MessageBasedSession.StandardEventStatus(Me)
        End Get
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Scpi.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Property StandardEventEnable() As Scpi.StandardEvents
        Get
            Return MessageBasedSession.StandardEventEnable(Me)
        End Get
        Set(ByVal value As Scpi.StandardEvents)
            MessageBasedSession.StandardEventEnable(Me) = value
        End Set
    End Property

#End Region

#Region " VISA Session Methods and Properties "

    ''' <summary>Returns a True is the service request register 
    '''   status byte reports error available.</summary>
    Public Function HasError() As Boolean
        Return MessageBasedSession.HasError(Me)
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimEnd(ByVal question As String) As String
        Return MessageBasedSession.QueryTrimEnd(Me, question)
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadStringTrimEnd() As String
        Return MessageBasedSession.ReadStringTrimEnd(Me)
    End Function

#End Region

#Region " VISA I/O METHODS "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryBoolean(ByVal question As String) As Boolean
        Return MessageBasedSession.QueryBoolean(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryDouble(ByVal question As String) As Double
        Return MessageBasedSession.QueryDouble(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInt32(ByVal question As String) As Int32
        Return MessageBasedSession.QueryInt32(Me, question)
    End Function

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
        MessageBasedSession.WriteOnOff(Me, output, isOn)
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
        MessageBasedSession.WriteOneZero(Me, output, one)
    End Sub

#End Region

End Class

