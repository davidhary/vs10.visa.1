Imports NationalInstruments

''' <summary>Provides shared methods for managing VISA resources.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
Public NotInheritable Class Helper

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Prevents construction of this class.</summary>
    Private Sub New()
    End Sub

#End Region

#Region " INTERFACES "

    Private Shared gpibInterfaceName As String = "GPIB"
    Private Shared gpibInterfaceFormat As String = "GPIB{0}::INTFC"
    ''' <summary>Returns a Gpib Interface name.</summary>
    ''' <param name="boardNumber">A gpib instrument address.</param>
    ''' <returns>A Visa resource interface name in the form "GPIBboardNumber::INTFC".</returns>
    Public Shared Function BuildGpibInterfaceName(ByVal boardNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, gpibInterfaceFormat, boardNumber)
    End Function

    Private Shared interfaceSearchString As String = "{0}?*INTFC"
    ''' <summary>Returns a string array of all local Gpib interfaces.</summary>
    Public Shared Function LocalGpibInterfaceNames() As String()
        Return LocalInterfaceNames(gpibInterfaceName)
        ' Return VisaNS.ResourceManager.GetLocalManager().FindResources("GPIB?*INTFC")
    End Function

    ''' <summary>Returns a string array of all local interfaces.</summary>
    Public Shared Function LocalInterfaceNames() As String()
        Return LocalInterfaceNames(String.Empty)
        ' Return VisaNS.ResourceManager.GetLocalManager().FindResources("?*INTFC")
    End Function

    ''' <summary>Returns a string array of all local interfaces matching
    '''   the interface base name such as GPIB.</summary>
    Public Shared Function LocalInterfaceNames(ByVal interfaceBaseName As String) As String()
        Return VisaNS.ResourceManager.GetLocalManager().FindResources( _
             String.Format(Globalization.CultureInfo.CurrentCulture, interfaceSearchString, interfaceBaseName))
    End Function

    ''' <summary>Returns an open GPIB interface.</summary>
    ''' <returns>An open GPIB interface.</returns>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function OpenGpibInterface(ByVal resourceName As String) As VisaNS.GpibInterface
        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Try
            Return CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), VisaNS.GpibInterface)
        Catch ex As InvalidCastException
            Throw New isr.Visa.CastException("Resource selected must be a GPIB Interface", ex)
        End Try
    End Function

#End Region

#Region " RESOURCES "

    ''' <summary>Returns a Gpib resource name string.</summary>
    ''' <param name="gpibAddress">A gpib instrument address</param>
    ''' <returns>A VISA resource Gpib instrument name in the form "GPIB0::gpibAddress::INSTR".</returns>
    Public Shared Function BuildGpibResourceName(ByVal gpibAddress As Int32) As String
        Return BuildGpibResourceName(0, gpibAddress)
    End Function

    Private Shared gpibResourceFormat As String = "GPIB{0}::{1}::INSTR"

    ''' <summary>Returns a Gpib resource name string.</summary>
    ''' <param name="gpibAddress">A gpib instrument address</param>
    ''' <returns>A VISA resource Gpib instrument name in the form 
    '''   "GPIBboardNumber::gpibAddress::INSTR".</returns>
    Public Shared Function BuildGpibResourceName(ByVal boardNumber As Int32, ByVal gpibAddress As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, gpibResourceFormat, boardNumber, gpibAddress)
    End Function

    Private Shared serialResourceFormat As String = "ASRL{0}::INSTR"
    ''' <summary>Returns a Serial Port resource name string.</summary>
    ''' <param name="portNumber">A gpib instrument address</param>
    ''' <returns>A VISA resource Gpib instrument name in the form "GPIB0::gpibAddress::INSTR".</returns>
    Public Shared Function BuildSerialResourceName(ByVal portNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, serialResourceFormat, portNumber)
    End Function

    Private Shared allSearchString As String = "?*"
    Private Shared resoruceSearchString As String = "{0}?*INSTR"
    Private Shared resoruceBoardSearchString As String = "{0}{1}?*INSTR"

    ''' <summary>Returns a resource base name.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    Public Shared Function ResourceBaseName(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String
        Select Case resourceType
            Case VisaNS.HardwareInterfaceType.Serial
                Return "ASRL"
            Case Else
                Return resourceType.ToString
        End Select
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    Public Shared Function LocalResourceNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType) As String()
        Return Helper.LocalResourceNames(ResourceBaseName(resourceType))
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceBaseName">Specifies the resourcebase name.</param>
    Public Shared Function LocalResourceNames(ByVal resourceBaseName As String) As String()
        Try
            Return VisaNS.ResourceManager.GetLocalManager().FindResources( _
                 String.Format(Globalization.CultureInfo.CurrentCulture, resoruceSearchString, resourceBaseName))
        Catch ex As System.ArgumentException
            ' trap the Run-time exception thrown : System.ArgumentException - Insufficient location 
            ' information or the requested device or resource is not present in the system.  
            ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
            Dim returnValue As String() = {}
            Return returnValue
            '      Return New String(0) = "Resources not present"
        End Try
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceType">Specifies the 
    '''   <see cref="NationalInstruments.VisaNS.HardwareInterfaceType">hardware resource type</see>.</param>
    ''' <param name="boardNumber">Specifies the resource board number.</param>
    Public Shared Function LocalResourceNames(ByVal resourceType As NationalInstruments.VisaNS.HardwareInterfaceType, _
        ByVal boardNumber As Int32) As String()
        Return Helper.LocalResourceNames(ResourceBaseName(resourceType), boardNumber)
    End Function

    ''' <summary>Returns a string array of all local resources matching
    '''   the resource base name such as GPIB.</summary>
    ''' <param name="resourceBaseName">Specifies the resource base name.</param>
    ''' <param name="boardNumber">Specifies the resource board number.</param>
    Public Shared Function LocalResourceNames(ByVal resourceBaseName As String, _
        ByVal boardNumber As Int32) As String()
        Try
            Return VisaNS.ResourceManager.GetLocalManager().FindResources( _
                 String.Format(Globalization.CultureInfo.CurrentCulture, resoruceBoardSearchString, resourceBaseName, boardNumber))
        Catch ex As System.ArgumentException
            ' trap the Run-time exception thrown : System.ArgumentException - Insufficient location 
            ' information or the requested device or resource is not present in the system.  
            ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
            Dim returnValue As String() = {}
            Return returnValue
            '      Return New String(0) = "Resources not present"
        End Try
    End Function

    ''' <summary>Returns a string array of all local resources.</summary>
    Public Shared Function LocalResourceNames() As String()
        Return VisaNS.ResourceManager.GetLocalManager().FindResources(allSearchString)
        'Return VisaNS.ResourceManager.GetLocalManager().FindResources("?*")
    End Function

    ''' <summary>Parses the GPIB instrument address from the GPIB resource name.</summary>
    ''' <param name="resourceName"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ParseGpibResourceAddress(ByVal resourceName As String) As Int32

        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If

        Dim addressLocation As Int32 = resourceName.IndexOf("::") + 2
        Dim addressWidth As Int32 = resourceName.LastIndexOf("::") - addressLocation
        Return Convert.ToInt32(resourceName.Substring(addressLocation, addressWidth), Globalization.CultureInfo.CurrentCulture)

    End Function

#End Region

#Region " SESSIONS "

    ''' <summary>Gets or sets the last resource name that was opened.</summary>
    Private Shared lastResourceName As String

    ''' <summary>Returns an open message based session.</summary>
    ''' <returns>An open Gpib VISA session.</returns>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function OpenGpibSession(ByVal resourceName As String) As isr.Visa.GpibSession
        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Try
            '      Dim session As VisaNS.GpibSession = CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), VisaNS.GpibSession)
            Return New isr.Visa.GpibSession(resourceName)
        Catch ex As InvalidCastException
            Throw New isr.Visa.CastException("Resource selected must be a Gpib session", ex)
        End Try
    End Function

    ''' <summary>Returns an open Gpib session.</summary>
    ''' <param name="owner">A reference to a <see cref="Windows.forms.IWin32Window">windows form</see></param>
    ''' <returns>An open Gpib VISA session.</returns>
    Public Shared Function OpenGpibSession(ByVal owner As Windows.Forms.IWin32Window) As GpibSession
        Dim rc As ResourceChooser = New ResourceChooser
        If lastResourceName IsNot Nothing Then
            rc.SelectedResourceName = lastResourceName
        End If
        Dim result As Windows.Forms.DialogResult = rc.ShowDialog(owner)
        If result = Windows.Forms.DialogResult.OK Then
            lastResourceName = rc.SelectedResourceName
            OpenGpibSession = OpenGpibSession(rc.SelectedResourceName)
        Else
            OpenGpibSession = Nothing
        End If
        rc.Dispose()
    End Function

    ''' <summary>Returns an open message based session.  Message-based sessions access VISA 
    '''   resources in an interface-independent manner. Such sessions can communicate with an 
    '''   instrument over a GPIB interface or serial interface.</summary>
    ''' <param name="owner">A reference to a <see cref="Windows.forms.IWin32Window">windows form</see></param>
    ''' <returns>An open message-based VISA session.</returns>
    Public Shared Function OpenMessageBasedSession(ByVal owner As Windows.Forms.IWin32Window) As MessageBasedSession
        Dim rc As ResourceChooser = ResourceChooser.Get
        If lastResourceName IsNot Nothing Then
            rc.SelectedResourceName = lastResourceName
        End If
        Dim result As Windows.Forms.DialogResult = rc.ShowDialog(owner)
        If result = Windows.Forms.DialogResult.OK Then
            lastResourceName = rc.SelectedResourceName
            OpenMessageBasedSession = New isr.Visa.MessageBasedSession(lastResourceName)
            '      Return Manager.OpenMessageBasedSession(rc.SelectedResourceName)
        Else
            OpenMessageBasedSession = Nothing
        End If
        rc.Dispose()
    End Function

    ''' <summary>Returns an open message based session.  Message-based sessions access VISA 
    '''   resources in an interface-independent manner. Such sessions can communicate with an 
    '''   instrument over a GPIB interface or serial interface.</summary>
    ''' <returns>An open message-based VISA session.</returns>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function OpenMessageBasedSession(ByVal resourceName As String) As MessageBasedSession
        If resourceName Is Nothing Then
            Throw New ArgumentNullException("resourceName")
        End If
        Try
            Return New isr.Visa.MessageBasedSession(lastResourceName)
            '      Return CType(VisaNS.ResourceManager.GetLocalManager().Open(resourceName), MessageBasedSession)
        Catch ex As InvalidCastException
            Throw New isr.Visa.CastException("Resource selected must be a message-based session", ex)
        End Try
    End Function

#End Region

#Region " ESCAPE SEQUENCES "

    ''' <summary>Replace common escape sequences such as '\n' or '\r\ with the 
    '''   corresponding environment characters.</summary>
    ''' <param name="old">The string which characters are replaced.</param>
    Public Shared Function ReplaceCommonEscapeSequences(ByVal old As String) As String
        If old Is Nothing Then
            Return String.Empty
        Else
            Return old.Replace("\n", Convert.ToChar(10)).Replace("\r", Convert.ToChar(13))
        End If
    End Function

    ''' <summary>Replaces environment characters with common escape sequences such as 
    '''   '\n', for new line, or '\r\ for carriage return.</summary>
    ''' <param name="old">The string which characters are replaced.</param>
    Public Shared Function InsertCommonEscapeSequences(ByVal old As String) As String
        If old Is Nothing Then
            Return String.Empty
        Else
            Return old.Replace(Convert.ToChar(10), "\n").Replace(Convert.ToChar(13), "\r")
        End If
    End Function

#End Region

End Class

