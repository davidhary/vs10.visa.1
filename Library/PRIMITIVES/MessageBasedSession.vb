Imports NationalInstruments

''' <summary>Extends the VISA message based session.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
Public Class MessageBasedSession

    Inherits VisaNS.MessageBasedSession

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName ">Specifies the name of the visa resource.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set the instance name, which is useful in tracing</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        MyBase.New(resourceName)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up managed and unmanaged resources.</summary>
    ''' <param name="disposing">If true, the method has been called directly or 
    '''   indirectly by a user's code. Managed and unmanaged resources can be disposed.
    '''   If disposing equals false, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects. Only unmanaged resources can be disposed.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        Finally

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SHARED COMMON SCPI COMMANDS "

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub ClearStatus(ByVal session As VisaNS.MessageBasedSession)

        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        ' Clear the device status.  This will set all the
        ' standard subsystem properties.
        MessageBasedSession.Write(session, "*CLS")

    End Sub

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function IsOpc(ByVal session As VisaNS.MessageBasedSession) As Boolean
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return (MessageBasedSession.QueryInt32(session, "*OPC?") = 1)
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadInstrumentId(ByVal session As VisaNS.MessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return MessageBasedSession.QueryTrimEnd(session, "*IDN?")
    End Function

    ''' <summary>Returns the device to the it default know state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub ResetToKnownState(ByVal session As VisaNS.MessageBasedSession)

        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, "*RST")

    End Sub

    ''' <summary>Reads the service request event status from the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property ServiceRequestEventStatus(ByVal session As VisaNS.MessageBasedSession) As Scpi.ServiceRequests
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, "*STB?"), Scpi.ServiceRequests)
        End Get
    End Property

    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <returns>The <see cref="isr.Visa.Scpi.ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    ''' <remarks>When set clears all service registers .</remarks>
    Public Shared Property ServiceRequestEventEnable(ByVal session As VisaNS.MessageBasedSession) As Scpi.ServiceRequests
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, "*SRE?"), Scpi.ServiceRequests)
        End Get
        Set(ByVal value As Scpi.ServiceRequests)
            MessageBasedSession.Write(session, String.Format(Globalization.CultureInfo.CurrentCulture, "*SRE {0:D}", value))
        End Set
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property StandardEventStatus(ByVal session As VisaNS.MessageBasedSession) As Scpi.StandardEvents
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, "*ESR?"), Scpi.StandardEvents)
        End Get
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    ''' <returns>The <see cref="isr.Visa.Scpi.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Shared Property StandardEventEnable(ByVal session As VisaNS.MessageBasedSession) As Scpi.StandardEvents
        Get
            If session Is Nothing Then
                Throw New ArgumentNullException("session")
            End If
            Return CType(MessageBasedSession.QueryInt32(session, "*ESE?"), Scpi.StandardEvents)
        End Get
        Set(ByVal value As Scpi.StandardEvents)
            MessageBasedSession.Write(session, String.Format(Globalization.CultureInfo.CurrentCulture, "*ESE {0:D}", value))
        End Set
    End Property

#End Region

#Region " SHARED VISA SESSION METHODS AND PROPERTIES "

    ''' <summary>Returns the service request register status byte.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStatusByte(ByVal session As VisaNS.MessageBasedSession) As isr.Visa.Scpi.ServiceRequests
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim status As Int32 = CType(session.ReadStatusByte(), Int32)
        If status >= 0 Then
            Return CType(status, isr.Visa.Scpi.ServiceRequests)
        Else
            Return CType(256 + status, isr.Visa.Scpi.ServiceRequests)
        End If
    End Function

    ''' <summary>Returns True if the service request register 
    '''   status byte reports error available.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function HasError(ByVal session As VisaNS.MessageBasedSession) As Boolean
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return (MessageBasedSession.ReadStatusByte(session) And Scpi.ServiceRequests.ErrorAvailable) <> 0
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function QueryTrimEnd(ByVal session As VisaNS.MessageBasedSession, ByVal question As String) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.Query(question).TrimEnd(Convert.ToChar(session.TerminationCharacter))
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function QueryTrimNewLine(ByVal session As VisaNS.MessageBasedSession, ByVal question As String) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.Query(question).TrimEnd(Convert.ToChar(13)).TrimEnd(Convert.ToChar(10))
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStringTrimEnd(ByVal session As VisaNS.MessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.ReadString().TrimEnd(Convert.ToChar(session.TerminationCharacter))
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination new line
    '''   characters.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Function ReadStringNewLine(ByVal session As VisaNS.MessageBasedSession) As String
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return session.ReadString().TrimEnd(Convert.ToChar(13)).TrimEnd(Convert.ToChar(10))
    End Function

    ''' <summary>Writes to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overloads Shared Sub Write(ByVal session As VisaNS.MessageBasedSession, ByVal output As String)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        session.Write(output)
    End Sub

#End Region

#Region " SHARED VISA I/O METHODS "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryBoolean(ByVal session As VisaNS.MessageBasedSession, ByVal question As String) As Boolean
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Dim queryResult As String = session.Query(question)
        Return queryResult.StartsWith("1") OrElse queryResult.StartsWith("ON")
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryDouble(ByVal session As VisaNS.MessageBasedSession, ByVal question As String) As Double
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return CType(MessageBasedSession.QueryTrimEnd(session, question), Double)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="question">The query to use.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function QueryInt32(ByVal session As VisaNS.MessageBasedSession, ByVal question As String) As Int32
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        Return CType(MessageBasedSession.QueryTrimEnd(session, question), Int32)
    End Function

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub WriteOnOff(ByVal session As VisaNS.MessageBasedSession, ByVal output As String, ByVal isOn As Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, output & Scpi.Instrument.IIf(Of String)(isOn, " ON", " OFF"))
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="session">A reference to an open
    '''   <see cref="NationalInstruments.VisaNS.MessageBasedSession">session</see>.</param>
    ''' <param name="output">The main command.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub WriteOneZero(ByVal session As VisaNS.MessageBasedSession, _
        ByVal output As String, ByVal one As Boolean)
        If session Is Nothing Then
            Throw New ArgumentNullException("session")
        End If
        MessageBasedSession.Write(session, output & Scpi.Instrument.IIf(Of String)(one, " 1", " 0"))
    End Sub

#End Region

#Region " COMMON SCPI COMMANDS "

    ''' <summary>Clears the error Queue and resets all event registers to zero by issuing the
    '''   IEEE488.2 common command.</summary>
    Public Sub ClearStatus()

        MessageBasedSession.ClearStatus(Me)

    End Sub

    ''' <summary>Returns true if the instrument returned 1 to the *OPC? query.</summary>
    Public Function IsOpc() As Boolean
        Return MessageBasedSession.IsOpc(Me)
    End Function

    ''' <summary>Queries the instrument and returns the string save the termination character.</summary>
    Public Function ReadInstrumentId() As String
        Return MessageBasedSession.ReadInstrumentId(Me)
    End Function

    ''' <summary>Returns the device to the it default know state by issuing the *RST 
    '''   IEEE488.2 common command.</summary>
    Public Sub ResetToKnownState()

        MessageBasedSession.ResetToKnownState(Me)

        ' Set default values if any

    End Sub

    ''' <summary>Reads the service request event status from the instrument.</summary>
    Public ReadOnly Property ServiceRequestEventStatus() As Scpi.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventStatus(Me)
        End Get
    End Property

    ''' <summary>Programs or reads back the service request event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Scpi.ServiceRequests">mask</see>
    '''    to use for enabling the events.</returns>
    ''' <remarks>When set clears all service registers.</remarks>
    Public Property ServiceRequestEventEnable() As Scpi.ServiceRequests
        Get
            Return MessageBasedSession.ServiceRequestEventEnable(Me)
        End Get
        Set(ByVal value As Scpi.ServiceRequests)
            MessageBasedSession.ServiceRequestEventEnable(Me) = value
        End Set
    End Property

    ''' <summary>Reads the standard event status from the instrument.</summary>
    Public ReadOnly Property StandardEventStatus() As Scpi.StandardEvents
        Get
            Return MessageBasedSession.StandardEventStatus(Me)
        End Get
    End Property

    ''' <summary>Programs or reads back the standard event request.</summary>
    ''' <returns>The <see cref="isr.Visa.Scpi.StandardEvents">mask</see>
    '''    to use for enabling the events.</returns>
    Public Property StandardEventEnable() As Scpi.StandardEvents
        Get
            Return MessageBasedSession.StandardEventEnable(Me)
        End Get
        Set(ByVal value As Scpi.StandardEvents)
            MessageBasedSession.StandardEventEnable(Me) = value
        End Set
    End Property

#End Region

#Region " VISA I/O METHODS "

    ''' <summary>Returns True if the service request register 
    '''   status byte reports error available.</summary>
    Public Function HasError() As Boolean
        Return MessageBasedSession.HasError(Me)
    End Function

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryBoolean(ByVal question As String) As Boolean
        Return MessageBasedSession.QueryBoolean(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryDouble(ByVal question As String) As Double
        Return MessageBasedSession.QueryDouble(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns an Int32 value.</summary>
    ''' <param name="question">The query to use.</param>
    Public Function QueryInt32(ByVal question As String) As Int32
        Return MessageBasedSession.QueryInt32(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimEnd(ByVal question As String) As String
        Return MessageBasedSession.QueryTrimEnd(Me, question)
    End Function

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Public Overloads Function QueryTrimNewLine(ByVal question As String) As String
        Return MessageBasedSession.QueryTrimNewline(Me, question)
    End Function

    ''' <summary>Reads a string from the instrument and returns the string save the termination character.</summary>
    Public Overloads Function ReadString() As String
        Return MessageBasedSession.ReadStringTrimEnd(Me)
    End Function

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="isOn">True to add "ON" or false to add "OFF".</param>
    Public Sub WriteOnOff(ByVal output As String, ByVal isOn As Boolean)
        MessageBasedSession.WriteOnOff(Me, output, isOn)
    End Sub

    ''' <summary>Writes On or Off command to the instrument.</summary>
    ''' <param name="output">The main command.</param>
    ''' <param name="one">True to add "1" or false to add "0".</param>
    Public Sub WriteOneZero(ByVal output As String, ByVal one As Boolean)
        MessageBasedSession.WriteOneZero(Me, output, one)
    End Sub

#End Region

End Class

