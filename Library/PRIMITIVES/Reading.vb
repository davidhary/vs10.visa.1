#Region " TYPES "

''' <summary>
''' Enumerates a tristate boolean value for defining outcome values
''' as set or not set.
''' </summary>
Public Enum TristateBoolean
    None
    Affirmative
    NotAffirmative
End Enum

''' <summary>
''' Enumerates the reading outcome flags.
''' </summary>
<System.Flags()> Public Enum ReadingOutcomes
    None = 0
    FailedContactCheck = 1
    HasValue = 2
    High = 4
    HitCompliance = 8
    HitRangeCompliance = 16
    HitLevelCompliance = 32
    Infinity = 64
    Low = 128
    Nan = 256
    NegativeInfinity = 512
    Pass = 1024
    Valid = 2048
End Enum

''' <summary>
''' Enumerates the international units scale factors.
''' </summary>
Public Enum UnitsPrefix
    None
    Atto
    Femto
    Pico
    Nano
    Micro
    Milli
    Centi
    Deci
    Deka
    Hecto
    Kilo
    Mega
    Giga
    Tera
    Peta
    Exa
    Kibi
    Mebi
    Gibi
    Tebi
    Pebi
    Exbi
End Enum

''' <summary>
''' Enumerats the formatting length.
''' </summary>
''' <remarks></remarks>
Public Enum FormattingLength
    None
    [Short]
    Medium
    [Long]
End Enum

#End Region

Public MustInherit Class ReadingBase
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New()

        ' instantiate the base class
        MyBase.New()
        Me._metaOutcome = New ReadingOutcome()

    End Sub

    ''' <summary>
    ''' Constructs a copy of an exisitng reading.
    ''' </summary>
    ''' <param name="model"></param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal model As ReadingBase)

        ' instantiate the base class
        MyBase.New()

        Me._heading = model._heading
        Me._lastValidReading = model._lastValidReading
        Me._metaOutcome = New ReadingOutcome(model._metaOutcome)
        Me._reading = model._reading

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            If disposing Then

                ' Free managed resources when explicitly called

            End If

            ' Free shared unmanaged resources

        End If

        ' set the sentinel indicating that the class was disposed.
        Me.IsDisposed = True

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()
    End Sub

#End Region

#Region " SHARED "

    ''' <summary>Gets or sets the SCPI value for infinity</summary>
    Public Const Infinity As Double = 9.9E+37
    Public Const InfinityCaption As String = "9.90000E+37"

    ''' <summary>Gets or sets the SCPI value for negative infinity</summary>
    Public Const NegativeInfinity As Double = -9.9E+37
    Public Const NegativeInfinityCaption As String = "-9.90000E+37"

    ''' <summary>Gets or sets the SCPI value for 'not-a-number</summary>
    Public Const Nan As Double = 9.91E+37
    Public Const NanCaption As String = "9.91000E+37"

    ''' <summary>
    ''' Returns true if the <paramref name="value">value</paramref> is 
    ''' <see cref="TristateBoolean.Affirmative">affirmative</see>.
    ''' </summary>
    ''' <param name="value"></param>
    Public Shared Function IsAffirmative(ByVal value As TristateBoolean) As Boolean
        Return value = TristateBoolean.Affirmative
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="value">value</paramref> is 
    ''' true.
    ''' </summary>
    ''' <param name="value"></param>
    Public Shared Function IsAffirmative(ByVal value As Boolean) As TristateBoolean
        If value Then
            Return TristateBoolean.Affirmative
        Else
            Return TristateBoolean.NotAffirmative
        End If
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="value">value</paramref> is 
    ''' <see cref="TristateBoolean.NotAffirmative">not affirmative</see>.
    ''' </summary>
    ''' <param name="value"></param>
    Public Shared Function IsNotAffirmative(ByVal value As TristateBoolean) As Boolean
        Return value = TristateBoolean.NotAffirmative
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="value">value</paramref> is 
    ''' not true.
    ''' </summary>
    ''' <param name="value"></param>
    Public Shared Function IsNotAffirmative(ByVal value As Boolean) As TristateBoolean
        If value Then
            Return TristateBoolean.NotAffirmative
        Else
            Return TristateBoolean.Affirmative
        End If
    End Function


#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary>Returns True if the value of the <param>obj</param> equals to the
    '''   instance value.</summary>
    ''' <param name="obj">The object to compare for equality with this instance.
    '''   This object should be type <see cref="ReadingR"/></param>
    ''' <returns>Returns <c>True</c> if <param>obj</param> is the same value as this
    '''   instance; otherwise, <c>False</c></returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return ReadingBase.Equals(Me, TryCast(obj, ReadingBase))
#If False Then
    If obj IsNot Nothing AndAlso TypeOf obj Is ReadingBase Then
      Return Equals(CType(obj, ReadingBase))
    Else
      Return False
    End If
#End If

        ' Return compared.Reading.Equals(Reading) AndAlso compared.MetaOutcome.Equals(_metaOutcome)

    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return _reading.GetHashCode
    End Function

    Private _heading As String
    ''' <summary>Gets or sets a heading to display when saving this reading.</summary>
    Public Property Heading() As String
        Get
            Return _heading
        End Get
        Set(ByVal value As String)
            _heading = value
        End Set
    End Property

    Private _lastValidReading As String
    ''' <summary>
    ''' Holds the last valid reading.
    ''' </summary>
    Public ReadOnly Property LastValidReading() As String
        Get
            Return _lastValidReading
        End Get
    End Property

    Private _metaOutcome As ReadingOutcome
    ''' <summary>
    ''' Holds the reading meta outcome.
    ''' </summary>
    Public Property MetaOutcome() As ReadingOutcome
        Get
            Return _metaOutcome
        End Get
        Set(ByVal value As ReadingOutcome)
            _metaOutcome = value
        End Set
    End Property

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="reading">Specifies the reading text.</param>
    ''' <returns>True if the reading was parse successfuly.</returns>
    ''' <remarks></remarks>
    Protected MustOverride Overloads Function Parse(ByVal reading As String) As Boolean

    Private _reading As String
    ''' <summary>Gets or sets the reading text.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overridable Property Reading() As String
        Get
            Return _reading
        End Get
        Set(ByVal value As String)
            _reading = value
            Me.MetaOutcome.HasValue = True
            If String.IsNullOrEmpty(value) Then
                Me.MetaOutcome.IsValid = False
                Me.MetaOutcome.HasValue = False
            ElseIf Parse(value) Then
                Me._lastValidReading = value
                Me.MetaOutcome.IsValid = True
            Else
                Me.MetaOutcome.IsValid = False
            End If
        End Set
    End Property

    ''' <summary>Resets measured value and flags.</summary>
    Public Overridable Overloads Sub Reset()
        Me._metaOutcome.Reset()
        Me.Reading = String.Empty
    End Sub

    ''' <summary>
    ''' Returns a value to save to file.
    ''' </summary>
    Public MustOverride Function ToSave() As String

    ''' <summary>
    ''' Returns the value to display.
    ''' </summary>
    Public MustOverride Overrides Function ToString() As String

#End Region

End Class

''' <summary>Defines a base for a measurement reading.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
Public MustInherit Class ReadingMeasurementBase
    Inherits ReadingBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New()
        MyBase.New()
        Me._generator = New RandomNumberGenerator()
    End Sub

    ''' <summary>Constructs a measured value</summary>
    ''' <param name="lowLimit">The low limit</param>
    ''' <param name="highLimit">The high limit</param>
    Protected Sub New(ByVal lowLimit As Double, ByVal highLimit As Double)
        Me.New()
        _lowLimit = lowLimit
        _highLimit = highLimit
    End Sub

    ''' <summary>
    ''' Construct a copy of the model.
    ''' </summary>
    ''' <param name="model"></param>
    ''' <remarks></remarks>
    Protected Sub New(ByVal model As ReadingMeasurementBase)
        MyBase.New(model)
        Me._generator = model._generator
        Me._highLimit = model._highLimit
        Me._lowLimit = model._lowLimit
    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    Private _generator As RandomNumberGenerator
    Public ReadOnly Property Generator() As RandomNumberGenerator
        Get
            Return _generator
        End Get
    End Property

    Private _highLimit As Double = Double.MaxValue
    ''' <summary>Gets or sets the high limit</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    Public Property HighLimit() As Double
        Get
            Return _highLimit
        End Get
        Set(ByVal value As Double)
            _highLimit = value
        End Set
    End Property

    Private _lowLimit As Double = -Double.MaxValue
    ''' <summary>Gets or sets the low limit</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    Public Property LowLimit() As Double
        Get
            Return _lowLimit
        End Get
        Set(ByVal value As Double)
            _lowLimit = value
        End Set
    End Property

    ''' <summary>Gets or sets the reading text.</summary>
    Public Overrides Property Reading() As String
        Get
            Return MyBase.Reading
        End Get
        Set(ByVal value As String)
            MyBase.Reading = value
            If Not String.IsNullOrEmpty(value) Then
                Dim numericValue As Double
                If Double.TryParse(value, Globalization.NumberStyles.Any, _
                    Globalization.CultureInfo.CurrentCulture, numericValue) Then
                    MyBase.MetaOutcome.IsOutcome(ReadingOutcomes.Infinity) = _
                        Math.Abs(numericValue - Visa.ReadingBase.Infinity) < 1
                    MyBase.MetaOutcome.IsOutcome(ReadingOutcomes.NegativeInfinity) = _
                        Math.Abs(numericValue - Visa.ReadingBase.NegativeInfinity) < 1
                    MyBase.MetaOutcome.IsOutcome(ReadingOutcomes.Nan) = _
                        Math.Abs(numericValue - Visa.ReadingBase.Nan) < 1
                End If
            End If
        End Set
    End Property

    ''' <summary>Sets the measured value and it validity.</summary>
    ''' <param name="newValue">A <see cref="System.Double">Double</see> value</param>
    ''' <param name="valid">The value auto settings mode</param>
    Public MustOverride Sub SetValue(ByVal newValue As Double, ByVal valid As Boolean)

#End Region

End Class

''' <summary>Defines a <see cref="System.Double">Double</see> reading.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' created.
''' </history>
Public Class ReadingR
    Inherits ReadingMeasurementBase

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value</summary>
    Public Sub New()
        MyBase.New()
        Me._displayCaption = New ReadingCaption(Of Double)
        Me._saveCaption = New ReadingCaption(Of Double)
    End Sub

    ''' <summary>
    ''' Construct a copy of the model.
    ''' </summary>
    ''' <param name="model"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal model As ReadingR)
        MyBase.New(model)
        Me._displayCaption = New ReadingCaption(Of Double)(model._displayCaption)
        Me._saveCaption = New ReadingCaption(Of Double)(model._saveCaption)
        Me._value = model._value
    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    Private _displayCaption As ReadingCaption(Of Double)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DisplayCaption() As ReadingCaption(Of Double)
        Get
            Return _displayCaption
        End Get
    End Property

    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' <param name="reading">Specifies the reading text.</param>
    Protected Overloads Overrides Function Parse(ByVal reading As String) As Boolean
        ' convert reading to numeric
        If Double.TryParse(reading, Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, Me._value) Then
            Me.SetValue(_value, True)
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Parses a scaled reading.
    ''' </summary>
    ''' <param name="reading"></param>
    Public Overridable Function ParseScaledReading(ByVal reading As String) As Boolean
        Dim scaledValue As Double
        If Double.TryParse(reading, Globalization.NumberStyles.Float, Globalization.CultureInfo.CurrentCulture, scaledValue) Then
            ' if the value parses correctly, set the reading
            Me.Reading = (scaledValue * Me.DisplayCaption.ScaleFactor).ToString(Globalization.CultureInfo.CurrentCulture)
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>Resets measured value to given value.
    ''' </summary>
    ''' <param name="newValue">The new value/</param>
    Public Overloads Sub Reset(ByVal newValue As Double)
        MyBase.Reset()
        _value = newValue
    End Sub

    ''' <summary>Sets the measured value and it validity.</summary>
    ''' <param name="newValue">A <see cref="System.Double">Double</see> value</param>
    ''' <param name="valid">The value auto settings mode</param>
    Public Overrides Sub SetValue(ByVal newValue As Double, ByVal valid As Boolean)
        MyBase.MetaOutcome.IsValid = valid
        _value = newValue
        Me.SaveCaption.ScaledValue = _value * Me.SaveCaption.ScaleFactor
        Me.DisplayCaption.ScaledValue = _value * Me.DisplayCaption.ScaleFactor
        If valid Then
            MyBase.MetaOutcome.IsHigh = _value > MyBase.HighLimit
            MyBase.MetaOutcome.IsLow = _value < MyBase.LowLimit
        End If
    End Sub

    Private _saveCaption As ReadingCaption(Of Double)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SaveCaption() As ReadingCaption(Of Double)
        Get
            Return _saveCaption
        End Get
    End Property

    ''' <summary>Returns the value to save using the <see cref="SaveCaption"/>.</summary>
    Public Overrides Function ToSave() As String
        Return Me.SaveCaption.ToString()
    End Function

    ''' <summary>Returns the default string representation of the range.</summary>
    Public Overloads Overrides Function ToString() As String
        Return Me.DisplayCaption.ToString()
    End Function

    Private _value As Double
    ''' <summary>Gets or sets the numeric value for this class.</summary>
    ''' <value>A <see cref="System.Double">double precision</see> value.</value>
    Public ReadOnly Property Value() As Double
        Get
            Return _value
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Converts readings for display or storage.
''' </summary>
''' <typeparam name="T">Specifies the generic type.</typeparam>
Public Class ReadingCaption(Of T As IFormattable)

    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>
    ''' Construct a copy of the model.
    ''' </summary>
    ''' <param name="model"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal model As ReadingCaption(Of T))
        MyBase.New()
        Me._format = model._format
        Me._invalid = model._invalid
        Me._maxLength = model._maxLength
        Me._scaledValue = model._scaledValue
        Me._scaleFactor = model._scaleFactor
        Me._scaleValue = model._scaleValue
        Me._shortFormat = model._shortFormat
        Me._showUnits = model._showUnits
        Me._units = model._units
        Me._unitsPrefix = model._unitsPrefix
        Me._unitsShortPrefix = model._unitsShortPrefix

    End Sub

#Region " PROPERTIES "

    Private _format As String = "0.000E+00"
    ''' <summary>
    ''' Holds the format string.
    ''' </summary>
    Public Property Format() As String
        Get
            Return _format
        End Get
        Set(ByVal value As String)
            _format = value
        End Set
    End Property

    Private _invalid As String = "NA"
    ''' <summary>
    ''' Holds the string for indicating an invalid value.
    ''' </summary>
    Public Property Invalid() As String
        Get
            Return _invalid
        End Get
        Set(ByVal value As String)
            _invalid = value
        End Set
    End Property

    Private _maxLength As Int32 = 9
    ''' <summary>
    ''' Holds the maximum number of characters to use for formatted value (not including the units).  
    ''' If, the length of the formatted value exceeds this limit, the program will use the 
    ''' alternate short format.
    ''' </summary>
    Public Property MaxLength() As Int32
        Get
            Return _maxLength
        End Get
        Set(ByVal value As Int32)
            _maxLength = value
        End Set
    End Property

    Private _scaleFactor As Double = 1
    ''' <summary>
    ''' Holds the actual scale factor.  This is the magnitude by which to multiply a raw 
    ''' value to express it in the scaled units.  For instance, to format a value as Kilo, 
    ''' multiply the value by 0.001. 
    ''' </summary>
    Public Property ScaleFactor() As Double
        Get
            Return _scaleFactor
        End Get
        Set(ByVal value As Double)
            _scaleFactor = value
            If Math.Abs(value) < Single.Epsilon Then
                _scaleValue = Double.NaN
            Else
                _scaleValue = 1 / value
            End If
        End Set
    End Property

    Private _scaleValue As Double = 1
    ''' <summary>Gets or sets the actual scale value.  This is one over the 
    '''   <see cref="ScaleFactor">scale factor</see>.  For instance the Kilo scale value
    '''   is 1000 whereas the scale factor is 0.001.</summary>
    Public ReadOnly Property ScaleValue() As Double
        Get
            Return _scaleValue
        End Get
    End Property

    Private _scaledValue As T
    ''' <summary>
    ''' Holds the scaled value to use for display.  This equals the value times
    ''' the display scale factor.  In this generic class, we have to
    ''' expose this for the user to set otherwaise we need to add algebra to 
    ''' this generic class which hopfully will be done with the next Visual Studio.
    ''' </summary>
    Public Property ScaledValue() As T
        Get
            Return _scaledValue
        End Get
        Set(ByVal value As T)
            _scaledValue = value
        End Set
    End Property

    Private _shortFormat As String = "0.000E+00"
    ''' <summary>
    ''' Holds an alternate format to use in case the formatted value exceeds 
    ''' the length limit.
    ''' </summary>
    Public Property ShortFormat() As String
        Get
            Return _shortFormat
        End Get
        Set(ByVal value As String)
            _shortFormat = value
        End Set
    End Property

    Private _showUnits As Boolean
    ''' <summary>
    ''' Holds true for adding units to the display.
    ''' </summary>
    Public Property ShowUnits() As Boolean
        Get
            Return _showUnits
        End Get
        Set(ByVal value As Boolean)
            _showUnits = value
        End Set
    End Property

    Private _units As String
    ''' <summary>Gets or sets the units format for the value</summary>
    Public Property Units() As String
        Get
            Return _units
        End Get
        Set(ByVal value As String)
            _units = value
        End Set
    End Property

    Private _unitsPrefix As UnitsPrefix = UnitsPrefix.None
    ''' <summary>
    ''' Holds the units prefix.  Setting this value sets the
    ''' <see cref="ScaleFactor">scale factor</see>.
    ''' </summary>
    Public Property UnitsPrefix() As UnitsPrefix
        Get
            Return _unitsPrefix
        End Get
        Set(ByVal value As UnitsPrefix)
            _unitsPrefix = value
            Select Case value
                Case UnitsPrefix.Atto
                    Me._scaleFactor = 1.0E+18
                    _scaleValue = 1.0E-18
                    _unitsShortPrefix = "a"
                Case UnitsPrefix.Centi
                    _scaleFactor = 100.0
                    _scaleValue = 0.01
                    _unitsShortPrefix = "c"
                Case UnitsPrefix.Deci
                    _scaleFactor = 10.0
                    _scaleValue = 0.1
                    _unitsShortPrefix = "d"
                Case UnitsPrefix.Deka
                    _scaleFactor = 0.1
                    _scaleValue = 10.0
                    _unitsShortPrefix = "da"
                Case UnitsPrefix.Femto
                    _scaleFactor = 1.0E+15
                    _scaleValue = 0.000000000000001
                    _unitsShortPrefix = "f"
                Case UnitsPrefix.Giga
                    _scaleFactor = 0.000000001
                    _scaleValue = 1000000000.0
                    _unitsShortPrefix = "G"
                Case UnitsPrefix.Hecto
                    _scaleFactor = 0.01
                    _scaleValue = 100.0
                    _unitsShortPrefix = "h"
                Case UnitsPrefix.Kilo
                    _scaleFactor = 0.001
                    _scaleValue = 1000.0
                    _unitsShortPrefix = "k"
                Case UnitsPrefix.Mega
                    _scaleFactor = 0.000001
                    _scaleValue = 1000000.0
                    _unitsShortPrefix = "M"
                Case UnitsPrefix.Micro
                    _scaleFactor = 1000000.0
                    _scaleValue = 0.000001
                    _unitsShortPrefix = Convert.ToChar(&H3BC)  '  ChrW(&HB5)   ' "�"
                Case UnitsPrefix.Milli
                    _scaleFactor = 1000.0
                    _scaleValue = 0.001
                    _unitsShortPrefix = "m"
                Case UnitsPrefix.Nano
                    _scaleFactor = 1000000000.0
                    _scaleValue = 0.000000001
                    _unitsShortPrefix = "n" ' ChrW(&H3B7)  ' "n"
                Case UnitsPrefix.None
                    _scaleFactor = 1.0
                    _scaleValue = 1.0
                    _unitsShortPrefix = ""
                Case UnitsPrefix.Pico
                    _scaleFactor = 1000000000000.0
                    _scaleValue = 0.000000000001
                    _unitsShortPrefix = "p"
                Case UnitsPrefix.Tera
                    _scaleFactor = 0.000000000001
                    _scaleValue = 1000000000000.0
                    _unitsShortPrefix = "T"
                Case UnitsPrefix.Peta
                    _scaleFactor = 0.000000000000001
                    _scaleValue = 1.0E+15
                    _unitsShortPrefix = "P"
                Case UnitsPrefix.Exa
                    _scaleFactor = 1.0E-18
                    _scaleValue = 1.0E+18
                    _unitsShortPrefix = "E"
                Case UnitsPrefix.Kibi
                    _scaleValue = Math.Pow(2, 10)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Ki"
                Case UnitsPrefix.Mebi
                    _scaleValue = Math.Pow(2, 20)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Mi"
                Case UnitsPrefix.Gibi
                    _scaleValue = Math.Pow(2, 30)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Gi"
                Case UnitsPrefix.Tebi
                    _scaleValue = Math.Pow(2, 40)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Ti"
                Case UnitsPrefix.Pebi
                    _scaleValue = Math.Pow(2, 50)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Pi"
                Case UnitsPrefix.Exbi
                    _scaleValue = Math.Pow(2, 60)
                    _scaleFactor = 1 / _scaleValue
                    _unitsShortPrefix = "Ei"
            End Select
        End Set
    End Property

    Private _unitsShortPrefix As String = String.Empty
    ''' <summary>
    ''' Holds the prefix to use when formatting the value with units.
    ''' </summary>
    Public Property UnitsShortPrefix() As String
        Get
            Return _unitsShortPrefix
        End Get
        Set(ByVal value As String)
            _unitsShortPrefix = value
        End Set
    End Property

#End Region

#Region " FORMATTING "

    ''' <summary>
    ''' Returns the value to display using the <see cref="ScaledValue">scaled value</see>,
    ''' <see cref="Units">units</see>, <see cref="UnitsShortPrefix">units prefix</see>, and
    ''' <see cref="Format"/>.
    ''' </summary>
    Public Overloads Overrides Function ToString() As String
        Dim displayText As String = Me.ScaledValue.ToString(Me.Format, Globalization.CultureInfo.CurrentCulture)
        If displayText.Length > Me.MaxLength Then
            displayText = Me.ScaledValue.ToString(Me.ShortFormat, Globalization.CultureInfo.CurrentCulture)
        End If
        If ShowUnits Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, _
                "{0} {1}{2}", displayText, Me.UnitsShortPrefix, Me.Units)
        Else
            Return displayText
        End If
    End Function

    ''' <summary>
    ''' Returns the value to display using the <see cref="Format"/> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="outcome">Specifies the outcome for handling invalid values.</param>
    Public Overloads Function ToString(ByVal outcome As ReadingOutcome) As String

        If outcome.IsValid Then
            Return Me.ToString
        Else
            Return outcome.LongOutcome
        End If

    End Function

    ''' <summary>
    ''' Returns the value to display using the <see cref="Format"/> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="isValid">True if outcome is valid.</param>
    Public Overloads Function ToString(ByVal isValid As Boolean) As String

        If isValid Then
            Return Me.ToString()
        Else
            Return Me.Invalid
        End If

    End Function

    ''' <summary>
    ''' Returns the units string with its prefix.
    ''' </summary>
    Public ReadOnly Property UnitsCaption() As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}", Me.UnitsShortPrefix, Me.UnitsShortPrefix)
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Holds the measurand outcome meta data and derived properties.
''' </summary>
''' <remarks></remarks>
Public Class ReadingOutcome

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="ReadingOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        Me.Reset()
    End Sub

    ''' <summary>
    ''' Constructs an outcome as a copy of an exiting outcome.
    ''' </summary>
    ''' <param name="model"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal model As ReadingOutcome)
        MyBase.New()
        Me._formattingLength = model._formattingLength
        Me._value = model._value
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Resets the outcome to None.
    ''' </summary>
    Public Sub Reset()
        Me._value = ReadingOutcomes.None
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Returns a short outcome string representing the meta outcome.
    ''' </summary>
    Public ReadOnly Property ShortOutcome() As String
        Get
            If Me.IsOutcome(ReadingOutcomes.None) Then
                ' if not set then return NO
                Return "no"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "nv"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "p"
                ElseIf Me.IsHigh Then
                    Return "hi"
                ElseIf Me.IsLow Then
                    Return "lo"
                Else
                    Return "n/a"
                End If
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "cc"
                ElseIf Me.HitCompliance Then
                    Return "c"
                ElseIf Me.HitLevelCompliance Then
                    Return "lc"
                ElseIf Me.HitRangeCompliance Then
                    Return "rc"
                ElseIf Me.Infinity Then
                    Return "in"
                ElseIf Me.Infinity Then
                    Return "inf"
                ElseIf Me.NegativeInfinity Then
                    Return "ninf"
                ElseIf Me.IsNaN Then
                    Return "NAN"
                Else
                    Return "n/a"
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns a long outcome string representing the meta outcome.
    ''' </summary>
    Public ReadOnly Property LongOutcome() As String
        Get
            If Me.IsOutcome(ReadingOutcomes.None) Then
                ' if not set then return NO
                Return "Not Set"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "No Value"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "Pass"
                ElseIf Me.IsHigh Then
                    Return "High"
                ElseIf Me.IsLow Then
                    Return "Low"
                Else
                    Return "n/a"
                End If
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "Contact Check"
                ElseIf Me.HitCompliance Then
                    Return "Compliance"
                ElseIf Me.HitLevelCompliance Then
                    Return "Level Compliance"
                ElseIf Me.HitRangeCompliance Then
                    Return "Range Compliance"
                ElseIf Me.Infinity Then
                    Return "Infinity"
                ElseIf Me.NegativeInfinity Then
                    Return "Negative Infinity"
                ElseIf Me.IsNaN Then
                    Return "Not a Number"
                Else
                    Return "n/a"
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns a medium length outcome string representing the meta outcome.
    ''' </summary>
    Public ReadOnly Property MediumOutcome() As String
        Get
            If Me.IsOutcome(ReadingOutcomes.None) Then
                ' if not set then return NO
                Return "Not Set"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "No Val."
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "Pass"
                ElseIf Me.IsHigh Then
                    Return "High"
                ElseIf Me.IsLow Then
                    Return "Low"
                Else
                    Return "n/a"
                End If
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "CCheck"
                ElseIf Me.HitCompliance Then
                    Return "Comp."
                ElseIf Me.HitLevelCompliance Then
                    Return "L.Comp."
                ElseIf Me.HitRangeCompliance Then
                    Return "R.Comp."
                ElseIf Me.Infinity Then
                    Return "Inf"
                ElseIf Me.NegativeInfinity Then
                    Return "N.Inf"
                ElseIf Me.IsNaN Then
                    Return "NAN"
                Else
                    Return "n/a"
                End If
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the outcome failed contact check.
    ''' </summary>
    Public Property FailedContactCheck() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.FailedContactCheck)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.FailedContactCheck) = value
            Me.IsOutcome(ReadingOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome Has Value, namely if a value was set.
    ''' </summary>
    Public Property HasValue() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.HasValue)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.HasValue) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is high.
    ''' </summary>
    Public Property IsHigh() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.High)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.High) = value
            ' set the pass flag
            If Me.Passed Then
                Me._value = Me._value Or ReadingOutcomes.Pass
            Else
                Me._value = Me._value And (Not ReadingOutcomes.Pass)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Hit Compliance.
    ''' </summary>
    Public Property HitCompliance() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.HitCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.HitCompliance) = value
            Me.IsOutcome(ReadingOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome Hit Level Compliance.
    ''' </summary>
    Public Property HitLevelCompliance() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.HitLevelCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.HitLevelCompliance) = value
            Me.IsOutcome(ReadingOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome Hit Range Compliance.
    ''' </summary>
    Public Property HitRangeCompliance() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.HitRangeCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.HitRangeCompliance) = value
            Me.IsOutcome(ReadingOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if theoutcome is low.
    ''' </summary>
    Public Property IsLow() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.Low)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.Low) = value
            ' set the pass flag
            If Me.Passed Then
                Me._value = Me._value Or ReadingOutcomes.Pass
            Else
                Me._value = Me._value And (Not ReadingOutcomes.Pass)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the reading is infinity.
    ''' </summary>
    Public ReadOnly Property Infinity() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.Infinity)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the reading is not a number.
    ''' </summary>
    Public ReadOnly Property IsNaN() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.Nan)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the reading is negative infinity.
    ''' </summary>
    Public ReadOnly Property NegativeInfinity() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.NegativeInfinity)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the value is either negative or positive infinity.
    ''' </summary>
    Public ReadOnly Property Overflowed() As Boolean
        Get
            Return Me.Infinity Or Me.NegativeInfinity
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the outcome.  This is the only valid path for setting the 
    ''' outcome.
    ''' </summary>
    ''' <param name="outcomeBit">Speciifies the outcome bit which to get or set.</param>
    Public Property IsOutcome(ByVal outcomeBit As ReadingOutcomes) As Boolean
        Get
            Return (Me._value And outcomeBit) = outcomeBit
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me._value = Me._value Or outcomeBit
            Else
                Me._value = Me._value And (Not outcomeBit)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Pass.
    ''' </summary>
    Public ReadOnly Property IsPass() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.Pass)
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the outcome is Valid. The measured value is
    ''' valid when its value or pass/fail outcomes or compliance or
    ''' contact check conditions are set.
    ''' </summary>
    Public Property IsValid() As Boolean
        Get
            Return Me.IsOutcome(ReadingOutcomes.Valid) AndAlso Not ( _
                       Me.IsOutcome(ReadingOutcomes.Nan) _
                OrElse Me.IsOutcome(ReadingOutcomes.Infinity) _
                OrElse Me.IsOutcome(ReadingOutcomes.NegativeInfinity) _
                OrElse Me.IsOutcome(ReadingOutcomes.FailedContactCheck) _
                OrElse Me.IsOutcome(ReadingOutcomes.HitLevelCompliance) _
                OrElse Me.IsOutcome(ReadingOutcomes.HitRangeCompliance))
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(ReadingOutcomes.Valid) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns True if passed.
    ''' </summary>
    Public Function Passed() As Boolean

        Return Me.IsValid AndAlso (Not (Me.IsHigh OrElse Me.IsLow))

    End Function

    Private _value As ReadingOutcomes
    ''' <summary>
    ''' Holds the reading outcome
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property [Value]() As ReadingOutcomes
        Get
            Return _value
        End Get
    End Property

    Private _formattingLength As FormattingLength
    ''' <summary>
    ''' Holds the formatting length forgetting the outcome as the default <see cref="ToString"/>
    ''' value.
    ''' </summary>
    Public Property FormattingLength() As FormattingLength
        Get
            Return Me._formattingLength
        End Get
        Set(ByVal value As FormattingLength)
            Me._formattingLength = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the default outcome.
    ''' </summary>
    Public Overrides Function ToString() As String
        If Me.FormattingLength = FormattingLength.Long Then
            Return Me.LongOutcome
        ElseIf Me.FormattingLength = FormattingLength.Medium Then
            Return Me.MediumOutcome
        ElseIf Me.FormattingLength = FormattingLength.Short Then
            Return Me.ShortOutcome
        Else
            Return "n/a"
        End If
    End Function

#End Region

End Class

''' <summary>
''' Provides a simulated value for testing measurements without having the benefit of 
''' instruments.
''' </summary>
''' <remarks></remarks>
Public Class ReadingGenerator

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="ReadingOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        If generator Is Nothing Then
            generator = New Random() ' CInt(Date.Now.Ticks Mod Integer.MaxValue))
        End If
        Me.Min = 0
        Me.Max = 1
    End Sub

    ''' <summary>
    ''' Holds a shared refernece to the number generator.
    ''' </summary>
    ''' <remarks>A shared generator was selected because multiple generators tended 
    ''' to create the same random number.</remarks>
    Private Shared generator As Random
    Private range As Double

    Private _min As Double
    Public Property Min() As Double
        Get
            Return _min
        End Get
        Set(ByVal value As Double)
            _min = value
            range = _max - _min
        End Set
    End Property

    Private _max As Double
    Public Property Max() As Double
        Get
            Return _max
        End Get
        Set(ByVal value As Double)
            _max = value
            range = _max - _min
        End Set
    End Property

    ''' <summary>
    ''' Returns a simulated value
    ''' </summary>
    ''' <value></value>
    Public ReadOnly Property [Value]() As Double
        Get
            Dim newValue As Double = generator.NextDouble * Me.range + Me.Min
            ' Debug.WriteLine(newValue)
            Return newValue
        End Get
    End Property

    ''' <summary>
    ''' Returns a formatted random value
    ''' </summary>
    Public Overrides Function ToString() As String
        Return Me.Value.ToString(Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a formatted random value
    ''' </summary>
    ''' <param name="format">Specifies the format string.</param>
    Public Overloads Function ToString(ByVal format As String) As String
        Return Me.Value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    End Function

End Class

