Imports NationalInstruments

''' <summary>Implements a Basic interface for VISA sessions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/08/06" by="David Hary" revision="1.0.2229.x">
''' created.
''' </history>
Public Class GpibInterface

    ' based on the connect base inheritable class
    Inherits NationalInstruments.VisaNS.GpibInterface

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="resourceName">Specifies the resource name of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the resource name.</remarks>
    Public Sub New(ByVal resourceName As String)

        ' instantiate the base class
        MyBase.New(resourceName, VisaNS.AccessModes.NoLock, 2000, False)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="gpibBoardNumber">Specifies the board number of the 
    '''   interface.</param>
    ''' <remarks>Use this constructor to instantiate this class
    '''   based on the board number.</remarks>
    Public Sub New(ByVal gpibBoardNumber As Int32)

        ' instantiate the base class
        Me.New(Helper.BuildGpibInterfaceName(gpibBoardNumber))

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Returns all instruments to some default state.</summary>
    Public Sub DevicesClear()

        ' Transmit the DCL command to the interface.
        Dim commands(4) As Byte
        Dim gpibCommand As GpibCommandCode
        gpibCommand = GpibCommandCode.Untalk
        commands(0) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(1) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.DeviceClear
        commands(2) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Untalk
        commands(3) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(4) = Convert.ToByte(gpibCommand)
        MyBase.SendCommand(commands)

        If _gui IsNot Nothing Then
            _gui.DisplayMessage("All Devices Cleared")
        End If

    End Sub

    Private _gui As GpibInterfacePanel
    ''' <summary>Gets or sets reference to the instrument interface.</summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public Property Gui() As GpibInterfacePanel
        Get
            Return _gui
        End Get
        Set(ByVal Value As GpibInterfacePanel)
            _gui = Value
        End Set
    End Property

    ''' <summary>Issues an interface clear.</summary>
    Public Sub InterfaceClear()
        Try
            MyBase.SendInterfaceClear()
            If _gui IsNot Nothing Then
                _gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "{0} Interface cleared", MyBase.ResourceName))
            End If
        Catch ex As NationalInstruments.VisaNS.VisaException
            ' trap timeout error.  Apparently, VISA is trying to send commands to the devices
            ' even if none are present.
            '      Run-time exception thrown : NationalInstruments.VisaNS.VisaException - Timeout expired before operation completed.  VISA error code -1073807339 (0xBFFF0015), ErrorTimeout
            If _gui IsNot Nothing Then
                _gui.DisplayMessage("Timed out on interface clear")
            End If

        End Try
    End Sub

    ''' <summary>Clear the specified device.</summary>
    ''' <param name="gpibAddress"></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Sub SelectiveDeviceClear(ByVal gpibAddress As Int32)

        ' Transmit the SDC command to the interface.
        '    Visa.GpibInterface.Write(gpib, String.Format(Globalization.CultureInfo.CurrentCulture, "UNT UNL LISTEN {0} SDC UNT UNL", gpibAddress))
        Dim commands(5) As Byte
        Dim gpibCommand As GpibCommandCode
        gpibCommand = GpibCommandCode.Untalk
        commands(0) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(1) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.ListenAddressGroup
        commands(2) = Convert.ToByte(gpibCommand) Or Convert.ToByte(gpibAddress)
        gpibCommand = GpibCommandCode.SelectiveDeviceClear
        commands(3) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Untalk
        commands(4) = Convert.ToByte(gpibCommand)
        gpibCommand = GpibCommandCode.Unlisten
        commands(5) = Convert.ToByte(gpibCommand)
        MyBase.SendCommand(commands)
        '    gpib.Write(String.Format(Globalization.CultureInfo.CurrentCulture, "UNT UNL LISTEN {0} SDC UNT UNL", gpibAddress))

        If _gui IsNot Nothing Then
            _gui.DisplayMessage(String.Format(Globalization.CultureInfo.CurrentCulture, "Cleared device at {0}", gpibAddress))
        End If

    End Sub

    ''' <summary>Clear the specified device.</summary>
    ''' <param name="resourceName "></param>
    Public Sub SelectiveDeviceClear(ByVal resourceName As String)

        Dim gpibAddress As Int32 = Helper.ParseGpibResourceAddress(resourceName)
        Me.SelectiveDeviceClear(gpibAddress)

    End Sub

#End Region

End Class

