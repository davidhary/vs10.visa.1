''' <summary>Provides the base exception for this library.</summary>
''' <remarks>Inherit this class to define derivative exceptions.</remarks>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="11/22/04" by="David Hary" revision="1.0.1787.x">
''' created.
''' </history>
<Serializable()> Public Class BaseException
    Inherits System.Exception

    Private Const _defMessage As String = "failed VISA operation."

#Region " CUSTOM CONSTRUCTORS "

    ''' <summary>Constructor with custom data that uses a custom format string.</summary>
    ''' <param name="messageFormat">String expression that specifies the message format.</param>
    ''' <param name="data1">String expression that ,,,.</param>
    Protected Sub New(ByVal messageFormat As String, ByVal data1 As String)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, messageFormat, data1))
    End Sub

    ''' <summary>Constructor with custom data that uses a custom format string.</summary>
    ''' <param name="messageFormat">String expression that specifies the message format.</param>
    ''' <param name="data1">String expression that ,,,.</param>
    ''' <param name="data2">String expression that ,,,.</param>
    Protected Sub New(ByVal messageFormat As String, ByVal data1 As String, ByVal data2 As String)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, messageFormat, data1, data2))
    End Sub

#End Region

#Region " BASE CONSTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor specifying the Message property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, _
        ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        _machineName = info.GetString("machineName")
        _createdDateTime = info.GetDateTime("createdDateTime")
        _appDomainName = info.GetString("appDomainName")
        _threadIdentity = info.GetString("threadIdentity")
        _windowsIdentity = info.GetString("windowsIdentity")
        _OSVersion = info.GetString("OSversion")
        _additionalInformation = CType(info.GetValue("additionalInformation", GetType(System.Collections.Specialized.NameValueCollection)), System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
      Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("machineName", _machineName, GetType(String))
        info.AddValue("createdDateTime", _createdDateTime)
        info.AddValue("appDomainName", _appDomainName, GetType(String))
        info.AddValue("threadIdentity", _threadIdentity, GetType(String))
        info.AddValue("windowsIdentity", _windowsIdentity, GetType(String))
        info.AddValue("OSversion", _OSVersion, GetType(String))
        info.AddValue("additionalInformation", _additionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

#Region " Public Properties "

    Private _additionalInformation As New System.Collections.Specialized.NameValueCollection
    ''' <summary>Collection allowing additional information to be added to the exception.</summary>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return _additionalInformation
        End Get
    End Property

    Private _appDomainName As String = String.Empty
    ''' <summary>AppDomain name where the exception occurred.</summary>
    Public ReadOnly Property AppDomainName() As String
        Get
            Return _appDomainName
        End Get
    End Property

    Private _createdDateTime As DateTime = DateTime.Now
    ''' <summary>Date and Time the exception was created.</summary>
    Public ReadOnly Property CreatedDateTime() As DateTime
        Get
            Return _createdDateTime
        End Get
    End Property

    Private _machineName As String = String.Empty
    ''' <summary>Machine name where the exception occurred.</summary>
    Public ReadOnly Property MachineName() As String
        Get
            Return _machineName
        End Get
    End Property

    ''' <summary>OS Version where the exception occurred.</summary>
    Private _OSVersion As String = Environment.OSVersion.ToString()
    Public ReadOnly Property OSVersion() As String
        Get
            Return _OSVersion
        End Get
    End Property

    Private _threadIdentity As String = String.Empty
    ''' <summary>Identity of the executing thread on which the exception was created.</summary>
    Public ReadOnly Property ThreadIdentityName() As String
        Get
            Return _threadIdentity
        End Get
    End Property

    Private _windowsIdentity As String = String.Empty
    ''' <summary>Windows identity under which the code was running.</summary>
    Public ReadOnly Property WindowsIdentityName() As String
        Get
            Return _windowsIdentity
        End Get
    End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

    Private Const unknown As String = "N/A"
    ''' <summary>Function that gathers environment information safely.</summary>
    ''' <history date="01/10/03" by="David Hary" revision="1.0.1105.x">
    ''' 	Create
    ''' </history>
    Private Sub obtainEnvironmentInformation()

        _machineName = Environment.MachineName
        If _machineName.Length = 0 Then
            _machineName = "N/A"
        End If

        _threadIdentity = System.Threading.Thread.CurrentPrincipal.Identity.Name
        If _threadIdentity.Length = 0 Then
            _threadIdentity = "N/A"
        End If

        _windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent().Name
        If _windowsIdentity.Length = 0 Then
            _windowsIdentity = "N/A"
        End If

        _appDomainName = AppDomain.CurrentDomain.FriendlyName
        If _appDomainName.Length = 0 Then
            _appDomainName = "N/A"
        End If
        _OSVersion = Environment.OSVersion.ToString
        If _OSVersion.Length = 0 Then
            _OSVersion = "N/A"
        End If

    End Sub

#End Region

End Class

