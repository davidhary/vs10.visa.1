''' <summary>Handles operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   exercising open, close, hardware access, and other similar operations.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
''' created.
''' </history>
<Serializable()> Public Class OperationException
    Inherits isr.Visa.BaseException

    Private Const _defMessage As String = "Operation failed."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a new exception using the internal default message.</summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ' Protected constructor to de-serialize data
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Overrides the GetObjectData method to serialize custom values.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    ''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
    '''   Inherit operation exception
    ''' </history>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)> _
    Public Overrides Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles open operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attemting to open an operation such as a state machine.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
''' Make serializable
''' </history>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
''' Inherit operation exception
''' </history>
<Serializable()> Public Class OperationOpenException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Operation failed opening."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles close operation exceptions.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attemting to close an operation such as a state machine.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
'''   Make serializable
''' </history>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
'''   Inherit operation exception
''' </history>
<Serializable()> Public Class OperationCloseException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Operation failed closing."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles initialize operation exception.</summary>
''' <remarks>Use this class to handle exceptions that might be thrown
'''   when attemting to initialize an operation.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
'''   Make serializable
''' </history>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
'''   Inherit operation exception
''' </history>
<Serializable()> Public Class OperationInitializeException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Operation failed initializing."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles logon operation exception.</summary>
''' <remarks>Use this class to handle logon that might occur when attemting to login.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
'''   Make serializable
''' </history>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
'''   Inherit operation exception
''' </history>
<Serializable()> Public Class LogOnException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Failed to logOn."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles hardware not found operation exception.</summary>
''' <remarks>Use this class to handle an exception raised if the hardware was not found.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="07/29/03" by="David Hary" revision="1.0.1305.x">
'''   Make serializable
''' </history>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
'''   Inherit operation exception
''' </history>
<Serializable()> Public Class HardwareNotFoundException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Hardware not found."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

''' <summary>Handles close operation exceptions.</summary>
''' <remarks>Use this class to throw exceptions for operations that were not
'''   implemented yet.</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="08/23/03" by="David Hary" revision="1.0.1333.x">
'''   Inherit operation exception
''' </history>
<Serializable()> Public Class NotImplementedException
    Inherits isr.Visa.OperationException

    Private Const _defMessage As String = "Operation not implemented."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no params.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructs a new exception using the given message.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructs a new exception using the given message and inner exception.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Specifies the exception that was trapped for 
    '''   throwing this exception.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class

