''' <summary>Handles Input Output exceptions.</summary>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' (c) 2005 Integrated Scientific Resources, Inc.
''' </history>
<Serializable()> Public Class IOException
    Inherits isr.Visa.BaseException
	
    Private Const _defMessage As String = "failed writing or reading INI Settings file."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub


    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub
	
	#End Region
	
End Class

''' <summary>Handles data conversion exceptions.</summary>
''' <history date="01/15/05" by="David Hary" revision="1.0.1841.x">
''' (c) 2005 Integrated Scientific Resources, Inc.
''' </history>
<Serializable()> Public Class CastException
    Inherits isr.Visa.BaseException
	
    Private Const _defMessage As String = "failed converting setting value to the expected type."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New(_defMessage)
    End Sub

    ''' <summary>Constructor specifying the Message to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>Constructor used for deserialization of the exception class.</summary>
    ''' <param name="info">Represents the SerializationInfo of the exception.</param>
    ''' <param name="context">Represents the context information of the exception.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, _
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class
