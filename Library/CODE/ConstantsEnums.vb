#Region " TYPES "

Public Enum GpibCommandCode
    None = 0 ' added to conform to FxCop 
    GoToLocal = &H1 ' GTL
    SelectiveDeviceClear = &H4 ' SDC
    GroupExecuteTrigger = &H8  ' GET
    LocalLockout = &H11 ' LLO
    DeviceClear = &H14 ' DCL 
    SerialPollEnable = &H18  ' SPE
    SerialPollDisable = &H19 ' SPD
    ListenAddressGroup = &H20 ' LAG 
    TalkAddressGroup = &H40 ' TAG 
    SecondaryCommandGroup = &H60 ' SCG 
    Unlisten = &H3F ' UNL 
    Untalk = &H5F ' UNT 
End Enum

#End Region
