Imports System.Windows.Forms

Namespace My

  Public Module MyLibraryProperty
    ''' <summary>
    ''' Returns a singleton instance of the <see cref="Library">library manager</see>.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property [Library]() As My.MyLibrary
      Get
        Return Global.isr.Visa.My.MyLibrary.Default
      End Get
    End Property
  End Module

  ''' <summary>Defines a singleton class to provide project management
  ''' for this project. This class has the Public Shared Not Creatable
  ''' instancing property.
  ''' </summary>
  ''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
  ''' <history date="11/07/06" by="David Hary" revision="1.0.2502.x">
''' Created
''' </history>
  Public Class MyLibrary

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

      MyBase.New()

    End Sub

    Private Shared _default As MyLibrary
    ''' <summary>Returns a new or existing instance of this class.</summary>
    ''' <value>The default instance of this class for using this class as a singleton.</value>
    Public Shared ReadOnly Property [Default]() As MyLibrary
      Get
        If _default Is Nothing OrElse _default.IsDisposed Then
          _default = New MyLibrary
        End If
        Return _default
      End Get
    End Property

    ''' <summary>
    ''' Gets True if the singleton instance was instantiated.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property Instantiated() As Boolean
      Get
        Return Not (_default Is Nothing OrElse _default.IsDisposed)
      End Get
    End Property

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

      ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

      ' this disposes all child classes.
      Dispose(True)

      ' Take this object off the finalization(Queue) and prevent finalization code 
      ' from executing a second time.
      GC.SuppressFinalize(Me)

    End Sub

  Private _isDisposed As Boolean
  ''' <summary>
  ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
  ''' provided proper implementation.
  ''' </summary>
  Protected Property IsDisposed() As Boolean
    Get
      Return _isDisposed
    End Get
    Private Set(ByVal value As Boolean)
      _isDisposed = value
    End Set
  End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

      If Not Me.IsDisposed Then

        Try

          If disposing Then

            ' Free managed resources when explicitly called

          End If

          ' Free shared unmanaged resources

        Finally

          ' set the sentinel indicating that the class was disposed.
          Me.IsDisposed = True

        End Try

      End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
      ' Do not re-create Dispose clean-up code here.
      ' Calling Dispose(false) is optimal in terms of
      ' readability and maintainability.
      Dispose(False)
      ' The compiler automatically adds a call to the base class finalizer 
      ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
      MyBase.Finalize()
    End Sub

#End Region

#Region " BROADCAST AND TRACING "

    ''' <summary>
    ''' Gets or sets the Broadcast level.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property BroadcastLevel() As TraceEventType
      Get
        Return My.Settings.BroadcastLevel
      End Get
      Set(ByVal value As TraceEventType)
        My.Settings.BroadcastLevel = value
      End Set
    End Property

    ''' <summary>
    ''' Returns true if the specified broadcast or trace levels are lower than the library levels.
    ''' </summary>
    ''' <param name="broadcastLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MustBroadcast(ByVal broadcastLevel As TraceEventType) As Boolean
      Return (broadcastLevel <= My.MyLibrary.BroadcastLevel)
    End Function

    ''' <summary>
    ''' Returns true if the specified broadcast or trace levels are lower than the library levels.
    ''' </summary>
    ''' <param name="broadcastLevel"></param>
    ''' <param name="traceLevel"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function MustBroadcast(ByVal broadcastLevel As TraceEventType, ByVal traceLevel As TraceEventType) As Boolean
      Return (broadcastLevel <= My.MyLibrary.BroadcastLevel) OrElse _
          (traceLevel <= My.MyLibrary.TraceLevel)
    End Function

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one.
    ''' </summary>
    ''' <param name="logWriter"></param>
    ''' <remarks></remarks>
    Public Shared Sub ReplaceDefaultTraceListener(ByVal logWriter As Microsoft.VisualBasic.Logging.FileLogTraceListener)
      Application.Log.TraceSource.Listeners.Remove("FileLog")
      Application.Log.TraceSource.Listeners.Add(logWriter)
    End Sub

    ''' <summary>
    ''' Gets or sets the trace level.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TraceLevel() As TraceEventType
      Get
        Return My.Settings.TraceLevel
      End Get
      Set(ByVal value As TraceEventType)
        ' required for design time.
        ' isr.Provers.Library.Selector.TraceLevel = value
        My.Settings.TraceLevel = value
        My.Application.Log.TraceSource.Switch.Level = CType([Enum].Parse(GetType(SourceLevels), My.Settings.TraceLevel.ToString), SourceLevels)
      End Set
    End Property

#End Region

#Region " SETTINGS "

    ''' <summary>
    ''' Saves the settings.
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub SaveSettings()
      My.Settings.Save()
    End Sub

#End Region

#Region " APPLICATION LOG "

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="details">Specifies the message details</param>
    ''' <returns>Message or empty string.</returns>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
      If details IsNot Nothing Then
        My.Application.Log.WriteEntry(details, severity)
        Return details
      End If
      Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="format">Specifies the message format</param>
    ''' <param name="args">Specified the message arguments</param>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
      If format IsNot Nothing Then
        Return WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
      End If
      Return ""
    End Function

    ''' <summary>
    ''' Adds a log message and severity to the log.
    ''' </summary>
    ''' <param name="severity">Specifies the message severity.</param>
    ''' <param name="messages">Message information to log.</param>
    ''' <remarks></remarks>
    Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
      If messages IsNot Nothing Then
        Return WriteLogEntry(severity, String.Join(",", messages))
      End If
      Return ""
    End Function

#End Region

#Region " EXCEPTION MANAGEMENT "

    ''' <summary>
    ''' Process any unhandled exceptions that occur in the application. 
    ''' Call this method from UI entry points in the application, such as button 
    ''' click events, when an unhandled exception occurs.  
    ''' This could also handle the Application.ThreadException event, however 
    ''' the VS2005 debugger breaks before the event Application.ThreadException 
    ''' is called.
    ''' </summary>
    ''' <param name="ex">Specifies the unhandled exception.</param>
    ''' <param name="additionalInfo">Specifies additional log information.</param>
    ''' <param name="buttons">Specifies the buttons to show on the exception display.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, _
        ByVal buttons As WinForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

      Dim result As Windows.Forms.DialogResult
      Try

        ' log the exception
        WriteExceptionDetails(ex, TraceEventType.Critical, additionalInfo)

        Dim frm As New WinForms.ExceptionDisplay
        result = frm.ShowDialog(ex, buttons)
        My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture, _
            "{0} requested by user.", result), TraceEventType.Verbose)

      Catch displayException As System.Exception

        ' Log but also display the error in a message box
        WriteExceptionDetails(displayException, TraceEventType.Critical, "Exception occurred displaying application exception.")

        Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
        errorMessage.Append("The following error occured while displaying the application exception:")
        errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
        errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}Click Abort to exit application.  Otherwise, the aplication will continue.", Environment.NewLine)
        result = MessageBox.Show( _
          errorMessage.ToString(), "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop)

      End Try

      Return result

    End Function

    Private Shared _traceLevelSwitch As TraceSwitch
    ''' <summary>
    ''' Holds the Trace Level switch value.
    ''' </summary>
    ''' <value><c>TraceLevelSwitch</c> is a Boolean property that can be read from (read only).</value>
    ''' <remarks>Use this property to get the trace level.</remarks>
    Public Shared ReadOnly Property TraceLevelSwitch() As TraceSwitch
      Get
        If _traceLevelSwitch Is Nothing Then
          _traceLevelSwitch = New TraceSwitch("TraceLevelSwitch", "Trace Level Switch", "Info")
        End If
        Return _traceLevelSwitch
      End Get
    End Property

    ''' <summary>
    ''' Adds exception details to the error log.  Includes stack and data.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <param name="severity">Specifies the exception severity.</param>
    ''' <param name="additionalInfo">Specifies additional information.</param>
    ''' <remarks></remarks>
    Private Shared Sub _writeExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, _
        ByVal additionalInfo As String)

      My.Application.Log.WriteException(ex, severity, additionalInfo)
      If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
        Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
        WriteLogEntry(severity, stackTrace)
      End If
      If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
        For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
          My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
        Next
      End If
      If ex.InnerException IsNot Nothing Then
        _writeExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
      End If

    End Sub

    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, _
        ByVal additionalInfo As String)

      ' write exception details.
      _writeExceptionDetails(ex, severity, additionalInfo)

    End Sub

    ''' <summary>
    ''' Adds exception details to the error log.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <remarks></remarks>
    Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
      WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
    End Sub


#End Region

#Region " PROPERTIES "

    ''' <summary>Returns a string selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

    ''' <summary>Returns an Integer selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Integer, ByVal falsePart As Integer) As Integer
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function

#If False Then
    ''' <summary>Returns a double selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Double, ByVal falsePart As Double) As Double
      If condition Then
        Return truePart
      Else
        Return falsePart
      End If
    End Function
#End If

    Private Shared _NA As String = "NA"
    Public Shared ReadOnly Property NA() As String
      Get
        Return _NA
      End Get
    End Property

#End Region

  End Class
End Namespace

